package com.jxzg.share.v1.controller.youpeng;

import com.jxzg.share.domain.other.Loginlog;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.service.v1.*;
import com.jxzg.share.toolUtil.GetIp;
import com.jxzg.share.toolUtil.JudgeRequestDeviceUtil;
import com.jxzg.share.toolUtil.SameUrlData;
import com.jxzg.share.vo.VerifyCodeVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.jxzg.share.toolUtil.JudgeRequestDeviceUtil.check;

/**
 * 登录,注册前台控制器
 */
@Controller
@CrossOrigin
@RequestMapping(value = "v1")
public class LoginController {
    @Autowired
    private IVerifylogService verifylogService;

    @Autowired
    private ILoginlogService loginlogService;

    //注册
    @RequestMapping(value = "register", method = RequestMethod.POST)
    @ResponseBody
    public ReturnMap register(@RequestBody VerifyCodeVO register, HttpServletRequest request, HttpServletResponse response) {
        ReturnMap jsonResult = null;
        String ip = GetIp.getIpAddr(request);
        try {
            if (JudgeRequestDeviceUtil.check(request, response)) {
                jsonResult = verifylogService.register(register.getLoginname(), register.getLoginpassword(), register.getVerifyCode(), ip, register.getLat(), register.getLng());
            } else {
                jsonResult = verifylogService.register_pc(register.getLoginname(), register.getLoginpassword(), register.getVerifyCode(), ip, register.getLat(), register.getLng());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonResult;
    }

    //登录
    @ResponseBody
    @RequestMapping(value = "login", method = RequestMethod.POST)
    public ReturnMap login(@RequestBody Loginlog userinfo, HttpServletRequest request, HttpServletResponse response) {
        ReturnMap returnMap = null;
        try {
            if (JudgeRequestDeviceUtil.check(request, response)) {
                returnMap = loginlogService.login(userinfo, GetIp.getIpAddr(request));
            } else {
                returnMap = loginlogService.pclogin(userinfo, GetIp.getIpAddr(request));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnMap;
    }
}