package com.jxzg.share.v1.controller.weixinpay;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSON;
import com.jxzg.share.domain.other.Userconnect;
import com.jxzg.share.domain.other.Usertoken;
import com.jxzg.share.pay.weixinpc.HttpUtil;
import com.jxzg.share.pay.weixinpc.PayCommonUtil;
import com.jxzg.share.pay.weixinutils.RequestHandler;
import com.jxzg.share.pay.weixinutils.TenpayUtil;
import com.jxzg.share.pay.weixinutils.WeixinPayConstants;
import com.jxzg.share.pay.weixinutils.sendpost;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.service.v1.IWeixinService;
import com.jxzg.share.toolUtil.SHA1;
import com.jxzg.share.vo.WeixinLoginVO;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.DigestException;
import java.util.*;

import static com.jxzg.share.pay.alipay.config.AlipayConfig.key;
import static org.aspectj.bridge.Version.getTime;


/**
 * 微信相关
 */
@Controller
@CrossOrigin
@RequestMapping(value = "v1")
public class WeixinLoginController {
    public static final String WX_AUTH_LOGIN_URL = "https://api.weixin.qq.com/sns/oauth2/access_token";
    public static final String WX_USERINFO_URL = "https://api.weixin.qq.com/sns/userinfo";

    public static final String APPID_PC_LOGIN = "wxd2343f283c59889c";//微信登录pcAppid

    public static final String APPID_PC_LOGIN_SECRET = "2d3bae9a142d390dbbc3bca688d7983a";



    public static final String WX_MP_APP_ID = "wx9cd56cb5e2c83a14";
    //AppSecret
    public static final String WX_APP_KEY = "931b91d0d757ee0578957c6195103f6f";//公众品台
    @Autowired
    private IWeixinService weixinService;

    @RequestMapping(value = "weixin_login", method = RequestMethod.POST)
    @ResponseBody
    public ReturnMap weixin_login(@RequestBody WeixinLoginVO userconnect) {
        ReturnMap result = null;
        try {
            userconnect.setType(Usertoken.LOGINTYPE_MOVE);
            result = weixinService.weixinmp_login(userconnect);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(value = "weixinmp_jssdk_ticket", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> weixinmp_jssdk_ticket(@RequestBody String url){
        Date date = new Date();
        String access_token = HttpUtil.sendGet("https://api.weixin.qq.com/cgi-bin/token", "grant_type=client_credential&appid=" + WX_MP_APP_ID + "&secret=" + WX_APP_KEY + "");
        JSONObject js = null;
        try {
            js = new JSONObject(access_token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        access_token = js.optString("access_token");
        String jsapi_ticket = HttpUtil.sendGet("https://api.weixin.qq.com/cgi-bin/ticket/getticket", "access_token=" + access_token + "&type=jsapi");
        try {
            js = new JSONObject(jsapi_ticket);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        jsapi_ticket = js.optString("ticket");
        String currTime = TenpayUtil.getCurrTime();
        String strTime = currTime.substring(8, currTime.length());
        String strRandom = TenpayUtil.buildRandom(4) + "";
        //10位序列号,可以自行调整。
        String strReq = strTime + strRandom;
        Map<String, Object> maps = new HashMap<>();
        try {
            long tine = date.getTime();
            String time = String.valueOf(tine);
            js = new JSONObject(url);
            String uuu = js.optString("url");
            String s = "jsapi_ticket=" + jsapi_ticket + "&noncestr=" + strReq + "&timestamp=" + time + "&url=" + uuu + "";
            URLEncoder.encode(s, "utf-8");
            String sign = SHA1.SHA1(s);
            maps.put("nonceStr", strReq);
            maps.put("appId", WX_MP_APP_ID);
            maps.put("signature", sign);
            maps.put("timestamp", tine);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return maps;
    }

    @RequestMapping(value = "weixinpc_login", method = RequestMethod.POST)
    @ResponseBody
    public ReturnMap weixinpc_login(@RequestBody String code) throws JSONException {
        ReturnMap result = null;
        JSONObject js = new JSONObject(code);
        code = js.optString("code");
        //获取授权 access_token
        StringBuffer loginUrl = new StringBuffer();
        loginUrl.append(WX_AUTH_LOGIN_URL).append("?appid=")
                .append(APPID_PC_LOGIN).append("&secret=")
                .append(APPID_PC_LOGIN_SECRET).append("&code=").append(code)
                .append("&grant_type=authorization_code");
        String loginRet = get(loginUrl.toString());
        JSONObject grantObj = new JSONObject(loginRet);
        String errcode = grantObj.optString("errcode");
        System.out.println("errcode = " + errcode);
        String errmsg = grantObj.optString("errmsg");
        System.out.println("errmsg = " + errmsg);
        if (!StringUtils.isEmpty(errcode)) {
            return null;
        }
        String openId = grantObj.optString("openid");
        if (StringUtils.isEmpty(openId)) {
            return null;
        }
        String accessToken = grantObj.optString("access_token");
        String expiresIn = grantObj.optString("expires_in");
        String refreshToken = grantObj.optString("refresh_token");
        String scope = grantObj.optString("scope");

        //获取用户信息
        StringBuffer userUrl = new StringBuffer();
        userUrl.append(WX_USERINFO_URL).append("?access_token=").append(accessToken).append("&openid=").append(openId);
        String userRet = get(userUrl.toString());
        JSONObject userObj = new JSONObject(userRet);

        WeixinLoginVO userInfo = new WeixinLoginVO();
        userInfo.setOpenid(openId);
        userInfo.setAccessToken(accessToken);
        userInfo.setRefreshToken(refreshToken);
        userInfo.setScope(scope);
        userInfo.setExpiration(Integer.valueOf(expiresIn));
        String nickname = userObj.optString("nickname");
        int sex = userObj.optInt("sex");
        String userImg = userObj.optString("headimgurl");
        String unionid = userObj.optString("unionid");
        userInfo.setNickname(nickname);
        userInfo.setHeadimgurl(userImg);
        userInfo.setSex(sex);
        userInfo.setUnionid(unionid);
        userInfo.setType(Usertoken.LOGINTYPE_PC);
        try {
            result = weixinService.weixinmp_login(userInfo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(value = "weixinmp_login", method = RequestMethod.POST)
    @ResponseBody
    public ReturnMap weixinmp_login(@RequestBody String code) throws JSONException {
        ReturnMap result = null;
        JSONObject js = new JSONObject(code);
        code = js.optString("code");
        //获取授权 access_token
        StringBuffer loginUrl = new StringBuffer();
        loginUrl.append(WX_AUTH_LOGIN_URL).append("?appid=")
                .append(WX_MP_APP_ID).append("&secret=")
                .append(WX_APP_KEY).append("&code=").append(code)
                .append("&grant_type=authorization_code");
        String loginRet = get(loginUrl.toString());
        JSONObject grantObj = new JSONObject(loginRet);
        String errcode = grantObj.optString("errcode");
        String errmsg = grantObj.optString("errmsg");
        if (!StringUtils.isEmpty(errcode)) {
            return null;
        }
        String openId = grantObj.optString("openid");
        if (StringUtils.isEmpty(openId)) {
            return null;
        }
        String accessToken = grantObj.optString("access_token");
        String expiresIn = grantObj.optString("expires_in");
        String refreshToken = grantObj.optString("refresh_token");
        String scope = grantObj.optString("scope");

        //获取用户信息
        StringBuffer userUrl = new StringBuffer();
        userUrl.append(WX_USERINFO_URL).append("?access_token=").append(accessToken).append("&openid=").append(openId);
        String userRet = get(userUrl.toString());
        JSONObject userObj = new JSONObject(userRet);

        WeixinLoginVO userInfo = new WeixinLoginVO();
        userInfo.setOpenid(openId);
        userInfo.setAccessToken(accessToken);
        userInfo.setRefreshToken(refreshToken);
        userInfo.setScope(scope);
        userInfo.setExpiration(Integer.valueOf(expiresIn));
        String nickname = userObj.optString("nickname");
        int sex = userObj.optInt("sex");
        String userImg = userObj.optString("headimgurl");
        String unionid = userObj.optString("unionid");
        userInfo.setNickname(nickname);
        userInfo.setHeadimgurl(userImg);
        userInfo.setSex(sex);
        userInfo.setUnionid(unionid);
        userInfo.setType(Usertoken.LOGINTYPE_WEIXIN_MP);
        try {
            result = weixinService.weixinmp_login(userInfo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String get(String url) {
        String body = null;
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpGet get = new HttpGet(url);
            get.addHeader("Accept-Charset", "utf-8");
            HttpResponse response = sendRequest(httpClient, get);
            body = parseResponse(response);
        } catch (IOException e) {
        }
        return body;
    }

    private static String paramsToString(Map<String, String> params) {
        StringBuilder sb = new StringBuilder();
        try {
            for (String key : params.keySet()) {
                sb.append(String.format("&%s=%s", key, URLEncoder.encode(params.get(key), StandardCharsets.UTF_8.toString())));
            }
        } catch (UnsupportedEncodingException e) {
        }
        return sb.length() > 0 ? "?".concat(sb.substring(1)) : "";
    }

    private static HttpResponse sendRequest(CloseableHttpClient httpclient, HttpUriRequest httpost)
            throws ClientProtocolException, IOException {
        HttpResponse response = null;
        response = httpclient.execute(httpost);
        return response;
    }

    private static String parseResponse(HttpResponse response) {
        HttpEntity entity = response.getEntity();

        Charset charset = ContentType.getOrDefault(entity).getCharset();
        if (charset != null) {
        }
        String body = null;
        try {
            body = EntityUtils.toString(entity, "utf-8");
        } catch (IOException e) {

        }
        return body;
    }
}
