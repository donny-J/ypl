package com.jxzg.share.v1.controller.youpeng;

import com.jxzg.share.querypage.MeetingQueryObject;
import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.returndomain.ReturnList;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.service.v1.*;
import com.jxzg.share.toolUtil.FileUploadUtil;
import com.jxzg.share.toolUtil.JudgeRequestDeviceUtil;
import com.jxzg.share.vo.ActivityMeeting.MeetingBody;
import com.jxzg.share.vo.IdAndToken;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.*;

/**
 * 会议控制器
 */
@Controller
@CrossOrigin
@RequestMapping(value = "v1")
public class MeetingController {

    @Autowired
    private IMeetingService meetingService;
    @Autowired
    private IMeetingpicService meetingpicService;
    @Autowired
    private IUsertokenService usertokenService;
    @Autowired
    private IMeetingticketService meetingticketService;

    @Value("${pic.meetingpic}")
    private String meetingpic;
    @Value("${pic.meetingpicurl}")
    private String meetingpicurl;

    /**
     * 创建会议(发活动)
     */
    @RequestMapping(value = "meeting_save", method = RequestMethod.POST)
    @ResponseBody
    public ReturnMap addmetting(@RequestBody MeetingBody body) {
        ReturnMap result = null;
        try {
            result = meetingService.addmetting(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 编辑会议回显
     */
    @RequestMapping(value = "update_meeting_see", method = RequestMethod.POST)
    @ResponseBody
    public ReturnMap update_meeting(@RequestBody IdAndToken idAndToken) {
        ReturnMap result = null;
        try {
            result = meetingService.update_meeting_see(idAndToken);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 编辑会议
     */
    @RequestMapping(value = "update_meeting", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult update_meeting(@RequestBody MeetingBody meetingBody) {
        JSONResult result = null;
        try {
            result = meetingService.update_meeting(meetingBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 删除会议
     */
    @RequestMapping(value = "del_meeting", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult del_meeting(@RequestBody IdAndToken idAndToken) {
        JSONResult result = null;
        try {
            result = meetingService.del_meeting(idAndToken);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 上传会议图片
     */
    @RequestMapping(value = "upload_meetingpic", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> upload_meetingpic(MultipartFile file, HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> map = new HashMap<>();
        try {
            String uuid = UUID.randomUUID().toString();
            String orgFileName = file.getOriginalFilename();
            String fileName = uuid + "."
                    + FilenameUtils.getExtension(orgFileName);
            String fileNamepc = uuid + "_pc" + "."
                    + FilenameUtils.getExtension(orgFileName);
            String fileNamemove = uuid + "_move" + "."
                    + FilenameUtils.getExtension(orgFileName);

            String name = FileUploadUtil.saveFile(file, meetingpic, fileName);
            CommonsMultipartFile cf = (CommonsMultipartFile) file;
            DiskFileItem fi = (DiskFileItem) cf.getFileItem();
            File f = fi.getStoreLocation();
            File toPicPc = new File(meetingpic + "/" + fileNamepc);
            Thumbnails.of(f).size(1920,1080).toFile(toPicPc);
            File toPicmove = new File(meetingpic + "/" + fileNamemove);
            Thumbnails.of(f).size(1920,1080).toFile(toPicmove);
            if (JudgeRequestDeviceUtil.check(request, response)){
                map.put("url", meetingpicurl + fileNamemove);
            } else {
                map.put("url", meetingpicurl + fileNamepc);
            }
            if (name != null) {
                map.put("success", true);
                map.put("msg", "上传成功!");
            } else if (name == null) {
                map.put("success", false);
                map.put("msg", "上传失败!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    /**
     * 会议列表(推荐会议)
     *
     * @param
     * @return
     */
    @RequestMapping(value = "meeting_list", method = RequestMethod.POST)
    @ResponseBody
    public ReturnList meeting(@RequestBody MeetingQueryObject qo) {
        ReturnList result = new ReturnList();
        try {
            result = meetingService.selectMeetingList(qo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 会议列表(关注的会议)
     *
     * @param
     * @return
     */
    @RequestMapping(value = "meeting_attention", method = RequestMethod.POST)
    @ResponseBody
    public ReturnList meeting_attention(@RequestBody MeetingQueryObject qo) {
        ReturnList result = new ReturnList();
        try {
            result = meetingService.meeting_attention(qo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 会议列表(附近的会议)
     *
     * @param
     * @return
     */
    @RequestMapping(value = "meeting_nearby", method = RequestMethod.POST)
    @ResponseBody
    public ReturnList meeting_nearby(@RequestBody MeetingQueryObject qo) {
        ReturnList result = new ReturnList();
        try {
            result = meetingService.meeting_nearby(qo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}