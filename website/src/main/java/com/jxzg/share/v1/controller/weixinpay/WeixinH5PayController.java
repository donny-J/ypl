package com.jxzg.share.v1.controller.weixinpay;

import com.alibaba.fastjson.JSONObject;
import com.jxzg.share.domain.other.Userconnect;
import com.jxzg.share.domain.pay.Orders;
import com.jxzg.share.mapper.OrdersMapper;
import com.jxzg.share.mapper.UserconnectMapper;
import com.jxzg.share.pay.weixinpc.PayCommonUtil;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.pay.weixinutils.*;
import com.jxzg.share.toolUtil.SHA1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import static com.jxzg.share.pay.alipay.config.AlipayConfig.key;
import static com.jxzg.share.pay.weixinpc.XMLUtil.doXMLParse;
import static com.jxzg.share.toolUtil.DateUtil.time;
import static java.lang.System.currentTimeMillis;

/**
 * Created by 00818Wolf on 2017/5/20.
 */
@Controller
@CrossOrigin
@RequestMapping(value = "v1")
public class WeixinH5PayController {
    @Autowired
    private OrdersMapper ordersMapper;
    @Autowired
    private UserconnectMapper userconnectMapper;
    @Value("${wechat.mpAppid}")
    private String mpAppid;
    @Value("${wechat.openAppid}")
    private String openAppid;
    @Value("${wechat.appsecret}")
    private String appsecret;
    @Value("${wechat.partner}")
    private String partner;
    @Value("${wechat.mpPartner}")
    private String mpPartner;
    @Value("${wechat.partnerkey}")
    private String partnerKey;
    @Value("${wechat.createOrderURL}")
    private String createOrderURL;
    @Value("${wechat.notifyUrl}")
    private String notifyUrl;
    @Value("${ypl.share.domain}")
    private String shareDomain;


    @RequestMapping(value = "weixinopen_pay", method = RequestMethod.POST)
    @ResponseBody
    public ReturnMap topay(@RequestBody Orders orders, HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        Orders orders1 = ordersMapper.selectByOrderno(orders.getOrderno());
        if (orders1.getStatus() != Orders.ORDER_STATUS_NO_PAY) {
            return new ReturnMap(false, "订单超时");
        }
        if (orders.getOrderno() == null || orders1 == null) {
            return new ReturnMap(false, "订单状态异常");
        }
        JSONObject retMsgJson = new JSONObject();
        //金额转化为分为单位
        String currTime = TenpayUtil.getCurrTime();
        //8位日期
        String strTime = currTime.substring(8, currTime.length());
        //四位随机数
        String strRandom = TenpayUtil.buildRandom(4) + "";
        //10位序列号,可以自行调整。
        String strReq = strTime + strRandom;
        //商户号
        String mch_id = partner;
        //子商户号  非必输
        //String sub_mch_id="";
        //设备号   非必输
        String device_info = "";
        //随机数
        String nonce_str = strReq;
        String body = null;
        if (orders1.getOrdertype() == Orders.ORDERTYPE_GENERAL || orders1.getOrdertype() == Orders.ORDERTYPE_TEAM) {
            body = "门票订单";
        } else if (orders1.getOrdertype() == Orders.ORDERTYPE_NOTE) {
            body = "短信订单";
        } else {
            body = "打赏订单";
        }
        body = new String(body.getBytes("UTF-8"), "ISO-8859-1");
        //附加数据
        String attach = "";
        //商户订单号
        String out_trade_no = orders1.getOrderno();//时间戳
        //总金额以分为单位，不带小数点
        String money = orders1.getMoney().toString();
        float sessionmoney = Float.parseFloat(money);
        BigDecimal total = orders1.getMoney().multiply(new BigDecimal("100"));
        total.toString().replace(".", "");
        String finalmoney = String.format("%.2f", sessionmoney);
        finalmoney = finalmoney.replace(".", "");
        int total_fee = Integer.parseInt(finalmoney);
        //订单生成的机器 IP
        String spbill_create_ip = request.getRemoteAddr();
        String notify_url = notifyUrl;//微信异步通知地址
        String trade_type = "JSAPI";//app支付必须填写为APP
        //对以下字段进行签名
        SortedMap<String, String> packageParams = new TreeMap<String, String>();
        packageParams.put("appid", openAppid);
        packageParams.put("attach", attach);
        packageParams.put("body", body);
        packageParams.put("mch_id", mch_id);
        packageParams.put("nonce_str", nonce_str);
        packageParams.put("notify_url", notify_url);
        packageParams.put("out_trade_no", out_trade_no);
        packageParams.put("spbill_create_ip", spbill_create_ip);
        packageParams.put("total_fee", String.valueOf(total_fee));
        packageParams.put("trade_type", trade_type);
        RequestHandler reqHandler = new RequestHandler(request, response);
        reqHandler.init(openAppid, appsecret, partnerKey);
        String sign = reqHandler.createSign(packageParams);
        String xml = "<xml>" +
                "<appid>" + openAppid + "</appid>" +
                "<attach>" + attach + "</attach>" +
                "<body>" + body + "</body>" +
                "<mch_id>" + mch_id + "</mch_id>" +
                "<nonce_str>" + nonce_str + "</nonce_str>" +
                "<notify_url>" + notify_url + "</notify_url>" +
                "<out_trade_no>" + out_trade_no + "</out_trade_no>" +
                "<spbill_create_ip>" + spbill_create_ip + "</spbill_create_ip>" +
                "<total_fee>" + total_fee + "</total_fee>" +
                "<trade_type>" + trade_type + "</trade_type>" +
                "<sign>" + sign + "</sign>" +
                "</xml>";
        String allParameters = "";
        try {
            allParameters = reqHandler.genPackage(packageParams);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String prepay_id = "";
        try {
            prepay_id = new GetWxOrderno().getPayNo(createOrderURL, xml);
            if (prepay_id.equals("")) {
                retMsgJson.put("msg", "error");
                return new ReturnMap(false, "支付错误");
            }
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        //获取到prepayid后对以下字段进行签名最终发送给app
        SortedMap<String, String> finalpackage = new TreeMap<String, String>();
        String timestamp = Sha1Util.getTimeStamp();
        finalpackage.put("appid", openAppid);
        finalpackage.put("timestamp", timestamp);
        finalpackage.put("noncestr", nonce_str);
        finalpackage.put("partnerid", partner);
        finalpackage.put("package", "Sign=WXPay");
        finalpackage.put("prepayid", prepay_id);
        String finalsign = reqHandler.createSign(finalpackage);
        retMsgJson.put("appid", openAppid);
        retMsgJson.put("timestamp", timestamp);
        retMsgJson.put("noncestr", nonce_str);
        retMsgJson.put("partnerid", partnerKey);
        retMsgJson.put("prepayid", prepay_id);
        retMsgJson.put("package", "Sign=WXPay");
        retMsgJson.put("sign", finalsign);
        return new ReturnMap("请求成功", retMsgJson);
    }

    @RequestMapping(value = "weixinmp_pay", method = RequestMethod.POST)
    @ResponseBody
    public ReturnMap weixinmp_pay(@RequestBody Orders orders, HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        Orders orders1 = ordersMapper.selectByOrderno(orders.getOrderno());
        JSONObject retMsgJson = new JSONObject();
        //金额转化为分为单位
        String currTime = TenpayUtil.getCurrTime();
        //8位日期
        String strTime = currTime.substring(8, currTime.length());

        //Orders orders1 = ordersMapper.selectByOrderno(orders.getOrderno());
        //四位随机数
        String strRandom = TenpayUtil.buildRandom(4) + "";
        //10位序列号,可以自行调整。
        String strReq = strTime + strRandom;
        //子商户号  非必输
        //String sub_mch_id="";
        //设备号   非必输
        String device_info = "";
        //随机数
        String nonce_str = strReq;
        String body = null;
        if (orders1.getOrdertype() == Orders.ORDERTYPE_GENERAL || orders1.getOrdertype() == Orders.ORDERTYPE_TEAM) {
            body = "门票订单";
        } else if (orders1.getOrdertype() == Orders.ORDERTYPE_NOTE) {
            body = "短信订单";
        } else {
            body = "打赏订单";
        }
        //附加数据
        String attach = "";
        //商户订单号
        String out_trade_no = orders1.getOrderno();//时间戳
        //总金额以分为单位，不带小数点
        String money = orders1.getMoney().toString();
        float sessionmoney = Float.parseFloat(money);
        BigDecimal total = new BigDecimal("100");
        total.toString().replace(".", "");
        String finalmoney = String.format("%.2f", sessionmoney);
        finalmoney = finalmoney.replace(".", "");
        int total_fee = Integer.parseInt(finalmoney);
        //订单生成的机器 IP
        String spbill_create_ip = request.getRemoteAddr();
        String notify_url = notifyUrl;//微信异步通知地址
        String trade_type = "JSAPI";//app支付必须填写为APP
        //对以下字段进行签名
        SortedMap<Object, Object> packageParams = new TreeMap<Object, Object>();
        packageParams.put("appid", mpAppid);
        packageParams.put("attach", attach);
        packageParams.put("body", body);
        packageParams.put("mch_id", mpPartner);
        packageParams.put("nonce_str", nonce_str);
        packageParams.put("notify_url", notify_url);
        packageParams.put("out_trade_no", out_trade_no);
        packageParams.put("spbill_create_ip", spbill_create_ip);
        packageParams.put("total_fee", String.valueOf(total_fee));
        packageParams.put("trade_type", trade_type);
        Userconnect userconnect = userconnectMapper.selectByUidAndType(orders1.getUid(), Userconnect.LOGINTYPE_WEIXIN_MP);
        String openid = userconnect.getThirdid();
        packageParams.put("openid", openid);
        String sign = PayCommonUtil.createSign("UTF-8", packageParams, partnerKey);
        String xml = "<xml>" +
                "<appid>" + mpAppid + "</appid>" +
                "<attach>" + attach + "</attach>" +
                "<body>" + body + "</body>" +
                "<mch_id>" + mpPartner + "</mch_id>" +
                "<nonce_str>" + nonce_str + "</nonce_str>" +
                "<notify_url>" + notify_url + "</notify_url>" +
                "<openid>" + openid + "</openid>" +
                "<out_trade_no>" + out_trade_no + "</out_trade_no>" +
                "<spbill_create_ip>" + spbill_create_ip + "</spbill_create_ip>" +
                "<total_fee>" + total_fee + "</total_fee>" +
                "<trade_type>" + trade_type + "</trade_type>" +
                "<sign>" + sign + "</sign>" +
                "</xml>";
        String prepay_id = "";
        Map map = null;
        try {
            String result = sendpost.sendPost(createOrderURL, xml);
            xmltomap xmlParam1 = new xmltomap();
            map = xmlParam1.doXMLParse(result);
            prepay_id = (String) map.get("prepay_id");
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        String stime = String.valueOf(System.currentTimeMillis());
        SortedMap<Object, Object> signMap = new TreeMap<Object, Object>();
        signMap.put("appId", mpAppid);
        signMap.put("timeStamp", stime);
        signMap.put("nonceStr", nonce_str);
        signMap.put("package", "prepay_id=" + prepay_id);
        signMap.put("signType", "MD5");
        String paySign = PayCommonUtil.createSign("UTF-8", signMap, partnerKey);
        Map<String, Object> mpmaps = new HashMap<>();
        mpmaps.put("timestamp", stime);
        mpmaps.put("nonceStr", nonce_str);
        mpmaps.put("package", "prepay_id=" + prepay_id);
        mpmaps.put("signType", "MD5");
        mpmaps.put("paySign", paySign);
        return new ReturnMap("请求成功", mpmaps);
    }

    @RequestMapping(value = "weixinh5_pay", method = RequestMethod.POST)
    @ResponseBody
    public ReturnMap weixinh5_pay(@RequestBody Orders orders, HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        Orders orders1 = ordersMapper.selectByOrderno(orders.getOrderno());
        JSONObject retMsgJson = new JSONObject();
        //金额转化为分为单位
        String currTime = TenpayUtil.getCurrTime();
        //8位日期
        String strTime = currTime.substring(8, currTime.length());
        //四位随机数
        String strRandom = TenpayUtil.buildRandom(4) + "";
        //10位序列号,可以自行调整。
        String strReq = strTime + strRandom;
        //商户号
        //子商户号  非必输
        //String sub_mch_id="";
        //设备号   非必输
        String device_info = "";
        //随机数
        String nonce_str = strReq;
        String body = null;
        if (orders1.getOrdertype() == Orders.ORDERTYPE_GENERAL || orders1.getOrdertype() == Orders.ORDERTYPE_TEAM) {
            body = "门票订单";
        } else if (orders1.getOrdertype() == Orders.ORDERTYPE_NOTE) {
            body = "短信订单";
        } else {
            body = "打赏订单";
        }
        body = new String(body.getBytes("UTF-8"), "ISO-8859-1");
        //附加数据
        String attach = "";
        //商户订单号
        String out_trade_no = orders1.getOrderno();//时间戳
        //总金额以分为单位，不带小数点
        String money = orders1.getMoney().toString();
        float sessionmoney = Float.parseFloat(money);
        BigDecimal total = new BigDecimal("100");
        total.toString().replace(".", "");
        String finalmoney = String.format("%.2f", sessionmoney);
        finalmoney = finalmoney.replace(".", "");
        int total_fee = Integer.parseInt(finalmoney);
        //订单生成的机器 IP
        String spbill_create_ip = request.getRemoteAddr();
        String notify_url = notifyUrl;//微信异步通知地址
        String trade_type = "MWEB";
        //对以下字段进行签名
        SortedMap<String, String> packageParams = new TreeMap<String, String>();
        packageParams.put("appid", openAppid);
        packageParams.put("attach", attach);
        packageParams.put("body", body);
        packageParams.put("mch_id", mpPartner);
        packageParams.put("nonce_str", nonce_str);
        packageParams.put("notify_url", notify_url);
        packageParams.put("out_trade_no", out_trade_no);
        packageParams.put("spbill_create_ip", spbill_create_ip);
        packageParams.put("total_fee", String.valueOf(total_fee));
        packageParams.put("trade_type", trade_type);
        String scene_info = "{\"h5_info\": {\"type\":\"Wap\",\"wap_url\": \"" + shareDomain + "\",\"wap_name\": \"有朋来官网\"}}";
        packageParams.put("scene_info", scene_info);
        RequestHandler reqHandler = new RequestHandler(request, response);
        reqHandler.init(openAppid, appsecret, partnerKey);
        String sign = reqHandler.createSign(packageParams);
        String xml = "<xml>" +
                "<appid>" + openAppid + "</appid>" +
                "<attach>" + attach + "</attach>" +
                "<body>" + body + "</body>" +
                "<mch_id>" + mpPartner + "</mch_id>" +
                "<nonce_str>" + nonce_str + "</nonce_str>" +
                "<notify_url>" + notify_url + "</notify_url>" +
                "<out_trade_no>" + out_trade_no + "</out_trade_no>" +
                "<scene_info>" + scene_info + "</scene_info>" +
                "<spbill_create_ip>" + spbill_create_ip + "</spbill_create_ip>" +
                "<total_fee>" + total_fee + "</total_fee>" +
                "<trade_type>" + trade_type + "</trade_type>" +
                "<sign>" + sign + "</sign>" +
                "</xml>";
        String allParameters = "";
        try {
            allParameters = reqHandler.genPackage(packageParams);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String prepay_id = "";
        Map map = null;
        try {
            String result = sendpost.sendPost(createOrderURL, xml);
            xmltomap xmlParam1 = new xmltomap();
            map = xmlParam1.doXMLParse(result);
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        return new ReturnMap("请求成功", map);
    }
}
