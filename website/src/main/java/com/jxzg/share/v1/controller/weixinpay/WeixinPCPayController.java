package com.jxzg.share.v1.controller.weixinpay;

import com.jxzg.share.domain.pay.Orders;
import com.jxzg.share.mapper.OrdersMapper;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.pay.weixinutils.WeixinPayConstants;
import com.jxzg.share.pay.weixinpc.PayCommonUtil;
import com.jxzg.share.pay.weixinpc.XMLUtil;
import com.jxzg.share.pay.weixinutils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;


/**
 * Created by 00818Wolf on 2017/4/25.
 */
@Controller
@CrossOrigin
@RequestMapping(value = "v1")
public class WeixinPCPayController {
    @Autowired
    private OrdersMapper ordersMapper;

    @Value("${wechat.mpAppid}")
    private String mpAppid;

    @Value("${wechat.openAppid}")
    private String openAppid;

    @Value("${wechat.partner}")
    private String partner;
    @Value("${wechat.partnerkey}")
    private String partnerKey;

    @Value("${wechat.mpPartner}")
    private String mpPartner;

    @Value("${wechat.createOrderURL}")
    private String createOrderURL;
    @Value("${wechat.notifyUrl}")
    private String notifyUrl;

    @RequestMapping(value = "weixinpc_pay", method = RequestMethod.POST)
    @ResponseBody
    public ReturnMap weixin_pay(@RequestBody Orders orders, HttpServletRequest request) throws Exception {
        // 账号信息

        Orders orders1 = ordersMapper.selectByOrderno(orders.getOrderno());
        if (orders1.getStatus() != Orders.ORDER_STATUS_NO_PAY) {
            return new ReturnMap(false, "订单超时");
        }
        if (orders.getOrderno() == null || orders1 == null) {
            return new ReturnMap(false, "订单状态异常");
        }
        String currTime = PayCommonUtil.getCurrTime();
        String strTime = currTime.substring(8, currTime.length());
        String strRandom = PayCommonUtil.buildRandom(4) + "";
        String nonce_str = strTime + strRandom;
        String money = orders1.getMoney().toString();
        float sessionmoney = Float.parseFloat(money);
        BigDecimal total = orders1.getMoney().multiply(new BigDecimal("100"));
        total.toString().replace(".", "");
        String finalmoney = String.format("%.2f", sessionmoney);
        finalmoney = finalmoney.replace(".", "");
        int total_fee = Integer.parseInt(finalmoney);
        String body = null;
        if (orders1.getOrdertype() == Orders.ORDERTYPE_GENERAL || orders1.getOrdertype() == Orders.ORDERTYPE_TEAM) {
            body = "门票订单";
        } else if (orders1.getOrdertype() == Orders.ORDERTYPE_NOTE) {
            body = "短信订单";
        } else {
            body = "打赏订单";
        }
        String out_trade_no = orders1.getOrderno(); // 订单号

        // 获取发起电脑 ip
        String spbill_create_ip = request.getRemoteAddr();
        // 回调接口
        String trade_type = "NATIVE";

        SortedMap<Object, Object> packageParams = new TreeMap<Object, Object>();
        packageParams.put("appid", openAppid);
        packageParams.put("mch_id", partner);
        packageParams.put("nonce_str", nonce_str);
        packageParams.put("body", body);
        packageParams.put("out_trade_no", out_trade_no);
        packageParams.put("total_fee", String.valueOf(total_fee));
        packageParams.put("spbill_create_ip", spbill_create_ip);
        packageParams.put("notify_url", notifyUrl);
        packageParams.put("trade_type", trade_type);
        String sign = PayCommonUtil.createSign("UTF-8", packageParams, partnerKey);
        packageParams.put("sign", sign);
        String requestXML = PayCommonUtil.getRequestXml(packageParams);
        requestXML.toString().getBytes("UTF-8");
        String resXml = sendpost.sendPost(createOrderURL, requestXML);

        Map map = XMLUtil.doXMLParse(resXml);
        String return_code = (String) map.get("return_code");
        String prepay_id = (String) map.get("prepay_id");
        String urlCode = (String) map.get("code_url");
        Map<String, Object> hashmap = new HashMap<>();
        hashmap.put("urlCode", urlCode);
        hashmap.put("totalFee", orders1.getMoney());
        return new ReturnMap("查询成功", hashmap);
    }

    public static String QRfromGoogle(String chl) throws Exception {
        int widhtHeight = 300;
        String EC_level = "L";
        int margin = 0;
        chl = UrlEncode(chl);
        String QRfromGoogle = "http://chart.apis.google.com/chart?chs=" + widhtHeight + "x" + widhtHeight
                + "&cht=qr&chld=" + EC_level + "|" + margin + "&chl=" + chl;

        return QRfromGoogle;
    }

    public static String UrlEncode(String src) throws UnsupportedEncodingException {
        return URLEncoder.encode(src, "UTF-8").replace("+", "%20");
    }
}