package com.jxzg.share.v1.Interceptor;

import com.jxzg.share.domain.meeting.Sponsor;
import com.jxzg.share.service.v1.IEhcacheService;
import com.jxzg.share.toolUtil.*;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * Created by 00818wolf on 2017/5/26.
 */
public class SameUrlDataInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            Method method = handlerMethod.getMethod();
            SameUrlData annotation = method.getAnnotation(SameUrlData.class);
            if (annotation != null) {
                if (repeatDataValidator(request))//如果重复相同数据
                    return false;
                else
                    return true;
            }
            return true;
        } else {
            return super.preHandle(request, response, handler);
        }
    }

    /**
     * 验证同一个url数据是否相同提交  ,相同返回true
     *
     * @param httpServletRequest
     * @return
     */
    public boolean repeatDataValidator(HttpServletRequest httpServletRequest) {

//       String url=processRequest(httpServletRequest);
//        if(url!=null){
//            Map<String, Object> map = JsonUtil.jsonStringToMap(url);
//            //map.put("timestamp",System.currentTimeMillis()/1000);
//            String keyUrl= JsonUtil.convertObjectToJson(map);
//            IEhcacheService ehcacheService=(IEhcacheService) ApplicationContextUtil.getBean("ehcacheServiceImpl");
//            String returnValue = ehcacheService.getUrlToCache(keyUrl);
            // 缓存中不存在
//            if(returnValue==null){
//                System.out.println(keyUrl+" no cache");
//                ehcacheService.putUrlToCache(keyUrl);
//            }else{
//                System.out.println(keyUrl+" in cache");
//                return true;
//            }
//        }
        return false;
    }
    /**
     * @Description: requestBody的处理
     * @userName: donny
     * @date:  2017-06-03
     * @param request
     */
    private String processRequest(HttpServletRequest request)
    {
        try
        {
            request.setCharacterEncoding("UTF-8");
            int size = request.getContentLength();

            InputStream is = request.getInputStream();

            byte[] reqBodyBytes = readBytes(is, size);
            //获取请求body中的内容
            String res = new String(reqBodyBytes);

            return res;

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    private static final byte[] readBytes(InputStream is, int contentLen)
    {
        if (contentLen > 0)
        {
            int readLen = 0;
            int readLengthThisTime = 0;
            byte[] message = new byte[contentLen];
            try
            {
                while (readLen != contentLen)
                {
                    readLengthThisTime = is.read(message, readLen, contentLen - readLen);
                    if (readLengthThisTime == -1)
                    {// Should not happen.
                        break;
                    }
                    readLen += readLengthThisTime;
                }
                return message;
            }
            catch (IOException e)
            {
                // Ignore
                e.printStackTrace();
            }
        }

        return new byte[] {};
    }
}