package com.jxzg.share.v1.controller.youpeng;

import com.jxzg.share.domain.other.Collectattention;
import com.jxzg.share.service.v1.ICollectattentionService;
import com.jxzg.share.returndomain.JSONResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 收藏和关注
 */
@Controller
@CrossOrigin
@RequestMapping(value = "v1")
public class CollectattentionController {
    @Autowired
    private ICollectattentionService collectattentionService;

    //收藏
    @RequestMapping(value = "collect", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult collectsave(@RequestBody Collectattention collectattention) {
        JSONResult json = null;
        try {
            json = collectattentionService.collect_save(collectattention);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    //关注
    @RequestMapping(value = "attention", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult attentionsave(@RequestBody Collectattention collectattention) {
        JSONResult json = null;
        try {
            json = collectattentionService.attention_save(collectattention);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }
}
