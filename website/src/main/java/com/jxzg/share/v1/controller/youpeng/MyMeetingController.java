package com.jxzg.share.v1.controller.youpeng;

import com.jxzg.share.domain.other.Token;
import com.jxzg.share.querypage.CommetQueryObject;
import com.jxzg.share.querypage.MeetingQueryObject;
import com.jxzg.share.querypage.QueryObject;
import com.jxzg.share.returndomain.ReturnList;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.service.v1.*;
import com.jxzg.share.vo.IdAndToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 我的管理
 */
@Controller
@CrossOrigin
@RequestMapping(value = "v1")
public class MyMeetingController {
    @Autowired
    private ISponsorService sponsorService;
    @Autowired
    private IMeetingService meetingService;
    @Autowired
    private IUsertokenService usertokenService;
    @Autowired
    private ICollectattentionService collectattentionService;
    @Autowired
    private IMyMeetingService myMeetingService;
    @Autowired
    private IUserService userService;
    @Autowired
    private IMyticketService myticketService;

    /**
     * 我的收藏
     */
    @RequestMapping(value = "mycollection", method = RequestMethod.POST)
    @ResponseBody
    public ReturnList mycollection(@RequestBody QueryObject qo) {
        ReturnList result = null;
        try {
            result = collectattentionService.mycollection(qo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 我的关注
     */
    @RequestMapping(value = "myattention", method = RequestMethod.POST)
    @ResponseBody
    public ReturnList myattention(@RequestBody QueryObject qo) {
        ReturnList result = null;
        try {
            result = collectattentionService.myattention(qo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 活动数据
     */
    @RequestMapping(value = "activity_data", method = RequestMethod.POST)
    @ResponseBody
    public ReturnList activity_data(@RequestBody CommetQueryObject qo) {
        ReturnList result = null;
        try {
            result = myticketService.activity_data(qo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 我的钱包
     */
    @RequestMapping(value = "mywallet", method = RequestMethod.POST)
    @ResponseBody
    public ReturnMap mywallet(@RequestBody Token token) {
        ReturnMap result = null;
        try {
            result = userService.mywallet(token.getToken());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 我的发布
     */
    @RequestMapping(value = "mymeeting", method = RequestMethod.POST)
    @ResponseBody
    public ReturnList mymeeting(@RequestBody MeetingQueryObject qo) {
        ReturnList result = null;
        try {
            result = meetingService.mymeeting(qo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}
