package com.jxzg.share.v1.controller.youpeng;

import com.jxzg.share.querypage.MeetingQueryObject;
import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.returndomain.ReturnList;
import com.jxzg.share.service.v1.ISignInService;
import com.jxzg.share.vo.SignInVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 签到控制器
 */
@Controller
@CrossOrigin
@RequestMapping(value = "v1")
public class SignInController {
    @Autowired
    private ISignInService signInService;

    @RequestMapping(value = "sign_in", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult searchSponsor(@RequestBody SignInVO signInVO) {
        JSONResult result = null;
        try {
            result = signInService.signin(signInVO);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
