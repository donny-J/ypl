package com.jxzg.share.v1.Interceptor;

import com.jxzg.share.toolUtil.JsonUtil;
import org.apache.log4j.MDC;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

@Aspect
@Component
public class LogAspectJ {

    private static final Logger log = LoggerFactory.getLogger(LogAspectJ.class);

    private static final Integer REST_TIMEOUT_INTERVAL = 5000;

    private static final Integer DAO_TIMEOUT_INTERVAL = 500;


    @Around("execution(* com.jxzg.share.mapper.*.*(..))")
    private Object daoMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        return execute(joinPoint, DAO_TIMEOUT_INTERVAL);
    }

    private Object execute(ProceedingJoinPoint joinPoint, Integer interval) throws Throwable {
        Object retVal;
        StringBuffer logMessage = new StringBuffer();
        logMessage.append(joinPoint.getTarget().getClass().getName());
        logMessage.append(".");
        logMessage.append(joinPoint.getSignature().getName());

        StringBuffer paramMessage = new StringBuffer();
        paramMessage.append("    Params: (");

        Object[] args = joinPoint.getArgs();
        for (int i = 0; i < args.length; i++) {
            String argsJsonstr = JsonUtil.convertObjectToJson(args[i]);
            paramMessage.append(argsJsonstr).append(",");
        }
        if (args.length > 0) {
            paramMessage.deleteCharAt(paramMessage.length() - 1);
        }
        paramMessage.append(")");
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            retVal = joinPoint.proceed();
            stopWatch.stop();

            logMessage.append("    executionTime: ");
            logMessage.append(stopWatch.getTotalTimeMillis());
            logMessage.append("ms");
            logMessage.append(paramMessage);
            String JSStr = JSONObject.toJSONString(retVal);
            logMessage.append("     return: " + JSStr);

            boolean isError = stopWatch.getTotalTimeMillis() > interval ? true : false;
            //请求来自API
            if (requestIsFromExternalApi()) {
                if (isError)
                    log.error(logMessage.toString());
                else
                    log.debug(logMessage.toString());
            } else {    //来自内部定时器
                if (isError)
                    log.error(logMessage.toString());
            }
            return retVal;
        } catch (Throwable e) {
            logMessage.append(paramMessage);
            log.error(logMessage.toString());
            throw e;
        }
    }

    private boolean requestIsFromExternalApi() {
        String sid = (String) MDC.get(SystemProcessTimerFilter.SID);
        if (StringUtils.isEmpty(sid))
            return false;
        else
            return true;
    }

}