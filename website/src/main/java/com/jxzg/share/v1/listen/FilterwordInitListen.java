package com.jxzg.share.v1.listen;

import com.jxzg.share.service.v1.IFilterwordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * Created by 00818Wolf on 2017/2/20.
 */
@Component
public class FilterwordInitListen implements ApplicationListener<ContextRefreshedEvent> {
    @Autowired
    private IFilterwordService filterwordService;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        //filterwordService.initfilterword();
    }
}
