package com.jxzg.share.v1.controller.youpeng;

import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.service.v1.IUserService;
import com.jxzg.share.service.v1.IUsertokenService;
import com.jxzg.share.service.v1.IVerifylogService;
import com.jxzg.share.vo.SendNoteVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 短信相关服务
 */
@Controller
@CrossOrigin
@RequestMapping(value = "v1")
public class NoteController {
    @Autowired
    private IVerifylogService verifylogService;

    @RequestMapping(value = "send_note", method = RequestMethod.POST)
    @ResponseBody
    private JSONResult send_note(@RequestBody SendNoteVO sendNote) {
        JSONResult result = null;
        try {
            result = verifylogService.send_note(sendNote);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
