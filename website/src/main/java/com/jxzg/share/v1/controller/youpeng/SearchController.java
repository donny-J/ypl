package com.jxzg.share.v1.controller.youpeng;

import com.jxzg.share.querypage.MeetingQueryObject;
import com.jxzg.share.querypage.SearchQueryObject;
import com.jxzg.share.returndomain.ReturnList;
import com.jxzg.share.service.v1.IFilterwordService;
import com.jxzg.share.service.v1.ISearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 搜索相关
 */
@Controller
@CrossOrigin
@RequestMapping(value = "v1")
public class SearchController {
    @Autowired
    private ISearchService searchService;
    @Autowired
    private IFilterwordService filterwordService;

    /**
     * 发现页面搜索框(主办方)
     *
     * @return
     */
    @RequestMapping(value = "searchSponsor", method = RequestMethod.POST)
    @ResponseBody
    public ReturnList searchSponsor(@RequestBody MeetingQueryObject qo) {
        ReturnList result = null;
        try {
            result = searchService.searchSponsor(qo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 发现页面搜索框(会议)
     *
     * @return
     */
    @RequestMapping(value = "searchMeeting", method = RequestMethod.POST)
    @ResponseBody
    public ReturnList searchMeeting(@RequestBody SearchQueryObject qo) {
        ReturnList result = null;
        try {
            result = searchService.searchMeeting(qo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 热门关键词
     */
    @RequestMapping(value = "filterword", method = RequestMethod.POST)
    @ResponseBody
    public ReturnList filterword() {
        ReturnList result = null;
        try {
            result = filterwordService.filterword();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
