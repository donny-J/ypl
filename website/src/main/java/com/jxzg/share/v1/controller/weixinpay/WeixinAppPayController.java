package com.jxzg.share.v1.controller.weixinpay;

import com.alibaba.fastjson.JSONObject;
import com.jxzg.share.domain.pay.Orders;
import com.jxzg.share.mapper.OrdersMapper;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.service.v1.IOrdersService;
import com.jxzg.share.pay.weixinutils.WeixinPayConstants;
import com.jxzg.share.pay.weixinpc.PayCommonUtil;
import com.jxzg.share.pay.weixinpc.XMLUtil;
import com.jxzg.share.pay.weixinutils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigDecimal;
import java.util.*;


/**
 * Created by 00818Wolf on 2017/4/25.
 */
@Controller
@CrossOrigin
@RequestMapping(value = "v1")
public class WeixinAppPayController {
    @Autowired
    private OrdersMapper ordersMapper;
    @Autowired
    private IOrdersService ordersService;
    //商户相关资料

    @Value("${wechat.openAppid}")
    private String appId;
    @Value("${wechat.appsecret}")
    private String appsecret;
    @Value("${wechat.partner}")
    private String partner;
    @Value("${wechat.partnerkey}")
    private String partnerKey;
    @Value("${wechat.createOrderURL}")
    private String createOrderURL;
    @Value("${wechat.notifyUrl}")
    private String notifyUrl;
    @Value("${wechat.orderQueryUrl}")
    private String orderQueryUrl;

    @RequestMapping(value = "weixin_pay", method = RequestMethod.POST)
    @ResponseBody
    public ReturnMap topay(@RequestBody Orders orders, HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        Orders orders1 = ordersMapper.selectByOrderno(orders.getOrderno());
        if (orders1.getStatus() != Orders.ORDER_STATUS_NO_PAY) {
            return new ReturnMap(false, "订单超时");
        }
        if (orders.getOrderno() == null || orders1 == null) {
            return new ReturnMap(false, "订单状态异常");
        }
        JSONObject retMsgJson = new JSONObject();
        //金额转化为分为单位
        String currTime = TenpayUtil.getCurrTime();
        //8位日期
        String strTime = currTime.substring(8, currTime.length());
        //四位随机数
        String strRandom = TenpayUtil.buildRandom(4) + "";
        //10位序列号,可以自行调整。
        String strReq = strTime + strRandom;
        //商户号
        String mch_id = partner;
        //子商户号  非必输
        //String sub_mch_id="";
        //设备号   非必输
        String device_info = "";
        //随机数
        String nonce_str = strReq;
        String body = null;
        if (orders1.getOrdertype() == Orders.ORDERTYPE_GENERAL || orders1.getOrdertype() == Orders.ORDERTYPE_TEAM) {
            body = "门票订单";
        } else if (orders1.getOrdertype() == Orders.ORDERTYPE_NOTE) {
            body = "短信订单";
        } else {
            body = "打赏订单";
        }
        //附加数据
        String attach = "";
        //商户订单号
        String out_trade_no = orders1.getOrderno();
        //总金额以分为单位，不带小数点
        String money = orders1.getMoney().toString();
        float sessionmoney = Float.parseFloat(money);
        BigDecimal total = orders1.getMoney().multiply(new BigDecimal("100"));
        total.toString().replace(".", "");
        String finalmoney = String.format("%.2f", sessionmoney);
        finalmoney = finalmoney.replace(".", "");
        int total_fee = Integer.parseInt(finalmoney);
        //订单生成的机器 IP
        String spbill_create_ip = request.getRemoteAddr();
        String notify_url = notifyUrl;//微信异步通知地址
        String trade_type = "APP";//app支付必须填写为APP
        //对以下字段进行签名
        SortedMap<String, String> packageParams = new TreeMap<String, String>();
        packageParams.put("appid", appId);
        packageParams.put("attach", attach);
        packageParams.put("body", body);
        packageParams.put("mch_id", mch_id);
        packageParams.put("nonce_str", nonce_str);
        packageParams.put("notify_url", notify_url);
        packageParams.put("out_trade_no", out_trade_no);
        packageParams.put("spbill_create_ip", spbill_create_ip);
        packageParams.put("total_fee", String.valueOf(total_fee));
        packageParams.put("trade_type", trade_type);
        RequestHandler reqHandler = new RequestHandler(request, response);
        reqHandler.init(appId, appsecret, partnerKey);
        String sign = reqHandler.createSign(packageParams);
        String xml = "<xml>" +
                "<appid>" + appId + "</appid>" +
                "<attach>" + attach + "</attach>" +
                "<body>" + body + "</body>" +
                "<mch_id>" + mch_id + "</mch_id>" +
                "<nonce_str>" + nonce_str + "</nonce_str>" +
                "<notify_url>" + notify_url + "</notify_url>" +
                "<out_trade_no>" + out_trade_no + "</out_trade_no>" +
                "<spbill_create_ip>" + spbill_create_ip + "</spbill_create_ip>" +
                "<total_fee>" + total_fee + "</total_fee>" +
                "<trade_type>" + trade_type + "</trade_type>" +
                "<sign>" + sign + "</sign>" +
                "</xml>";
        String allParameters = "";
        try {
            allParameters = reqHandler.genPackage(packageParams);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String prepay_id = "";
        try {
            prepay_id = new GetWxOrderno().getPayNo(createOrderURL, xml);
            if (prepay_id.equals("")) {
                retMsgJson.put("msg", "error");
                return new ReturnMap(false, "支付错误");
            }
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        //获取到prepayid后对以下字段进行签名最终发送给app
        SortedMap<String, String> finalpackage = new TreeMap<String, String>();
        String timestamp = Sha1Util.getTimeStamp();
        finalpackage.put("appid", appId);
        finalpackage.put("timestamp", timestamp);
        finalpackage.put("noncestr", nonce_str);
        finalpackage.put("partnerid", partner);
        finalpackage.put("package", "Sign=WXPay");
        finalpackage.put("prepayid", prepay_id);
        String finalsign = reqHandler.createSign(finalpackage);
        retMsgJson.put("appid", appId);
        retMsgJson.put("timestamp", timestamp);
        retMsgJson.put("noncestr", nonce_str);
        retMsgJson.put("partnerid", partner);
        retMsgJson.put("prepayid", prepay_id);
        retMsgJson.put("package", "Sign=WXPay");
        retMsgJson.put("sign", finalsign);
        return new ReturnMap("请求成功", retMsgJson);
    }


    //微信异步通知
    @RequestMapping(value = "/weixin_notify", method = RequestMethod.POST)
    @ResponseBody
    public void weixin_notify(HttpServletRequest request, HttpServletResponse response) throws Exception {
        //读取参数
        InputStream inputStream;
        StringBuffer sb = new StringBuffer();
        inputStream = request.getInputStream();
        String s;
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        while ((s = in.readLine()) != null) {
            sb.append(s);
        }
        in.close();
        inputStream.close();
        //解析xml成map
        Map<String, String> m = new HashMap<String, String>();
        m = XMLUtil.doXMLParse(sb.toString());
        //过滤空 设置 TreeMap
        SortedMap<Object, Object> packageParams = new TreeMap<Object, Object>();
        Iterator it = m.keySet().iterator();
        while (it.hasNext()) {
            String parameter = (String) it.next();
            String parameterValue = m.get(parameter);
            String v = "";
            if (null != parameterValue) {
                v = parameterValue.trim();
            }
            packageParams.put(parameter, v);
        }

        // 账号信息
        String key = partnerKey; // key
        //判断签名是否正确
        if (PayCommonUtil.isTenpaySign("UTF-8", packageParams, key)) {
            String resXml = "";
            String out_trade_no = (String) packageParams.get("out_trade_no");
            if ("SUCCESS".equals((String) packageParams.get("result_code"))) {
                // 这里是支付成功
                String mch_id = (String) packageParams.get("mch_id");
                String openid = (String) packageParams.get("openid");
                String is_subscribe = (String) packageParams.get("is_subscribe");
                out_trade_no = (String) packageParams.get("out_trade_no");
                String total_fee = (String) packageParams.get("total_fee");
                synchronized (this) {
                    boolean isSuccess = ordersService.alipay_success(out_trade_no, Orders.WECHAT);
                    if (!isSuccess) {
                        resXml = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>"
                                + "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
                    } else {
                        resXml = "<xml>" + "<return_code><![CDATA[SUCCESS]]></return_code>"
                                + "<return_msg><![CDATA[OK]]></return_msg>" + "</xml> ";
                    }
                }
            } else {
                ordersService.weixinpay_fail(out_trade_no, Orders.WECHAT);
                resXml = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>"
                        + "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
            }
            BufferedOutputStream out = new BufferedOutputStream(
                    response.getOutputStream());
            out.write(resXml.getBytes());
            out.flush();
            out.close();
        } else {

        }
    }

    @RequestMapping(value = "weixin_orderquery", method = RequestMethod.POST)
    @ResponseBody
    public String orderquery(@RequestBody Orders orders, HttpServletRequest request, HttpServletResponse response) {
        //Orders orders1 = ordersMapper.selectByOrderno(orders.getOrderno());
        JSONObject retMsgJson = new JSONObject();
        //金额转化为分为单位
        String currTime = TenpayUtil.getCurrTime();
        //8位日期
        String strTime = currTime.substring(8, currTime.length());
        //四位随机数
        String strRandom = TenpayUtil.buildRandom(4) + "";
        //10位序列号,可以自行调整。
        String strReq = strTime + strRandom;
        //商户号
        String mch_id = partner;
        //随机数
        String nonce_str = strReq;
        //商户订单号
        String out_trade_no = orders.getOrderno();//时间戳
        //对以下字段进行签名
        SortedMap<String, String> packageParams = new TreeMap<String, String>();
        packageParams.put("appid", appId);
        packageParams.put("mch_id", mch_id);
        packageParams.put("nonce_str", nonce_str);
        packageParams.put("out_trade_no", out_trade_no);
        RequestHandler reqHandler = new RequestHandler(request, response);
        reqHandler.init(appId, appsecret, partnerKey);
        String sign = reqHandler.createSign(packageParams);
        String xml = "<xml>" +
                "<appid>" + appId + "</appid>" +
                "<mch_id>" + mch_id + "</mch_id>" +
                "<nonce_str>" + nonce_str + "</nonce_str>" +
                "<out_trade_no>" + out_trade_no + "</out_trade_no>" +
                "<sign>" + sign + "</sign>" +
                "</xml>";
        String allParameters = "";
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
        String result = sendpost.sendPost(orderQueryUrl, xml);
        xmltomap xmlParam1 = new xmltomap();
        Map map = null;
        try {
            map = xmlParam1.doXMLParse(result);
        } catch (Exception e) {
            e.printStackTrace();
        }

        /**
         *
         SUCCESS—支付成功
         REFUND—转入退款
         NOTPAY—未支付
         CLOSED—已关闭
         REVOKED—已撤销（刷卡支付）
         USERPAYING--用户支付中
         PAYERROR--支付失败(其他原因，如银行返回失败)
         */
        return (String) map.get("trade_state");
    }
}