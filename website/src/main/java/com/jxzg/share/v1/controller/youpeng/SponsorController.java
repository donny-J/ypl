package com.jxzg.share.v1.controller.youpeng;


import com.jxzg.share.domain.meeting.Sponsor;
import com.jxzg.share.querypage.QueryObject;
import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.returndomain.ReturnList;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.service.v1.ISponsorService;
import com.jxzg.share.service.v1.IUsertokenService;
import com.jxzg.share.toolUtil.FileUploadUtil;
import com.jxzg.share.toolUtil.JudgeRequestDeviceUtil;
import com.jxzg.share.toolUtil.SameUrlData;
import com.jxzg.share.vo.IdAndToken;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * 主办方
 */
@Controller
@CrossOrigin
@RequestMapping(value = "v1")
public class SponsorController {
    private static final Logger log = LoggerFactory.getLogger(SponsorController.class);

    @Autowired
    private ISponsorService sponsorService;
    @Autowired
    private IUsertokenService usertokenService;

    @Value("${pic.sponsorlogourl}")
    private String sponsorlogourl;
    @Value("${pic.sponsorlogo}")
    private String sponsorlogo;

    /**
     * 主办方保存
     *
     * @param sponsor
     * @return
     */
    @RequestMapping(value = "sponsor_save", method = RequestMethod.POST)
    @ResponseBody
    public ReturnMap sponsorsave(@RequestBody Sponsor sponsor) {
        ReturnMap result = null;
        try {
            result = sponsorService.insert(sponsor);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 上传主办方头像upload_sponsorpic
     */
    @RequestMapping(value = "upload_sponsorpic", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> upload_sponsorpic(MultipartFile file, HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> map = new HashMap<>();
        try {
            String uuid = UUID.randomUUID().toString();
            String orgFileName = file.getOriginalFilename();
            String fileName = uuid + "."
                    + FilenameUtils.getExtension(orgFileName);
            String fileNamepc = uuid + "_pc" + "."
                    + FilenameUtils.getExtension(orgFileName);
            String fileNamemove = uuid + "_move" + "."
                    + FilenameUtils.getExtension(orgFileName);

            String name = FileUploadUtil.saveFile(file, sponsorlogo, fileName);
            CommonsMultipartFile cf = (CommonsMultipartFile) file;
            DiskFileItem fi = (DiskFileItem) cf.getFileItem();
            File f = fi.getStoreLocation();
            File toPicPc = new File(sponsorlogo + "/" + fileNamepc);
            Thumbnails.of(f).scale(1.0).toFile(toPicPc);
            File toPicmove = new File(sponsorlogo + "/" + fileNamemove);
            Thumbnails.of(f).scale(1.0).toFile(toPicmove);
            if (JudgeRequestDeviceUtil.check(request, response)) {
                map.put("url", sponsorlogourl + fileNamemove);
            } else {
                map.put("url", sponsorlogourl + fileNamepc);
            }
            if (name != null) {
                map.put("success", true);
                map.put("msg", "上传成功!");
            } else if (name == null) {
                map.put("success", false);
                map.put("msg", "上传失败!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    /**
     * 主办方列表
     *
     * @return
     */
    @RequestMapping(value = "sponsor_list", method = RequestMethod.POST)
    @ResponseBody
    public ReturnList sponsorlist(@RequestBody QueryObject qo) {
        ReturnList returnList = null;
        try {
            returnList = sponsorService.selectSponsorAll(qo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnList;
    }


    /**
     * 主办方删除
     *
     * @param sponsor
     * @return
     */
    @RequestMapping(value = "sponsor_del", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult sponsordel(@RequestBody Sponsor sponsor) {
        JSONResult result = null;
        try {
            result = sponsorService.deleteByOid(sponsor);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 主办方编辑
     *
     * @param sponsor
     * @return
     */
    @RequestMapping(value = "sponsor_update", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult sponsorupdate(@RequestBody Sponsor sponsor) {
        JSONResult result = null;
        try {
            result = sponsorService.sponsorupdate(sponsor);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 主办方详情
     */
    @RequestMapping(value = "sponsor_particulars", method = RequestMethod.POST)
    @ResponseBody
    public ReturnMap sponsor_particulars(@RequestBody IdAndToken idAndToken) {//主办方id,token
        ReturnMap result = null;
        try {
            result = sponsorService.sponsor_particulars(idAndToken.getMid(), idAndToken.getToken());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
