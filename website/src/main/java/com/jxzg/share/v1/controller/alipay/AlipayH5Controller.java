package com.jxzg.share.v1.controller.alipay;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.jxzg.share.pay.alipay.AlipayConfig;
import com.jxzg.share.domain.pay.Orders;
import com.jxzg.share.mapper.OrdersMapper;
import com.jxzg.share.returndomain.JSONResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * Created by 00818Wolf on 2017/5/22.
 */
@Controller
@CrossOrigin
@RequestMapping(value = "v1")
public class AlipayH5Controller {

    @Autowired
    private OrdersMapper ordersMapper;
    @Value("${alipay.sellerId}")
    private String sellId;
    @Value("${alipay.baseUrl}")
    private String alipayBaseUrl;
    @Value("${alipay.appid}")
    private String alipayAppid;
    @Value("${alipay.privateKey}")
    private String privateKey;
    @Value("${alipay.publicKey}")
    private String publicKey;
    @Value("${alipay.notifyUrl}")
    private String notifyUrl;
    @Value("${alipay.h5ReturnUrl}")
    private String h5ReturnUrl;


    /**
     * 支付宝PC下单接口
     *
     * @throws IOException
     */
    @RequestMapping(value = "alipay_h5", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult alipay_pc(@RequestBody Orders orders, HttpServletResponse httpResponse) throws IOException {
        Orders orders1 = ordersMapper.selectByOrderno(orders.getOrderno());
        AlipayClient alipayClient = new DefaultAlipayClient(alipayBaseUrl, alipayAppid, privateKey, "JSON", AlipayConfig.input_charset, publicKey, "RSA2"); //获得初始化的AlipayClient
        //获得初始化的AlipayClient
        AlipayTradeWapPayRequest alipayRequest = new AlipayTradeWapPayRequest();//创建API对应的request
        alipayRequest.setNotifyUrl(notifyUrl);//在公共参数中设置回跳和通知地址
        alipayRequest.setReturnUrl(h5ReturnUrl);
        String body = null;
        if (orders1.getOrdertype() == Orders.ORDERTYPE_GENERAL || orders1.getOrdertype() == Orders.ORDERTYPE_TEAM) {
            body = "门票订单";
        } else if (orders1.getOrdertype() == Orders.ORDERTYPE_NOTE) {
            body = "短信订单";
        } else {
            body = "打赏订单";
        }
        alipayRequest.setBizContent("{" +
                "    \"out_trade_no\":\"" + orders.getOrderno() + "\"," +
                "    \"total_amount\":" + orders1.getMoney() + "," +
                "    \"subject\":\"" + body + "\"," +
                "    \"seller_id\":\"" + sellId + "\"," +
                "    \"product_code\":\"QUICK_WAP_PAY\"" +
                "  }");//填充业务参数
        String form = null; //调用SDK生成表单
        try {
            form = alipayClient.pageExecute(alipayRequest).getBody();
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        httpResponse.setContentType("text/html;charset=" + AlipayConfig.input_charset);
        httpResponse.getWriter().write(form);//直接将完整的表单html输出到页面
        httpResponse.getWriter().flush();
        httpResponse.getWriter().close();
        return new JSONResult("订单正常");
    }
}
