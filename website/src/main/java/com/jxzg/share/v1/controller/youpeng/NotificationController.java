package com.jxzg.share.v1.controller.youpeng;

import com.jxzg.share.domain.other.Notification;
import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.service.v1.INotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 消息控制器(站内信,短信)
 */
@Controller
@CrossOrigin
@RequestMapping(value = "v1")
public class NotificationController {
    @Autowired
    private INotificationService notificationService;

    /**
     * 发送消息
     * @return
     */
    @RequestMapping(value = "send_notification",method = RequestMethod.POST)
    @ResponseBody
    public JSONResult send_notification(@RequestBody Notification notification) {
        JSONResult result = null;
        try {
            result = notificationService.send_notification(notification);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
