/**
 * feiniu.com Inc.
 * Copyright (c) 2013-2014 All Rights Reserved.
 */
package com.jxzg.share.v1.Interceptor;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

/**
 * <B>Description:</B> <br>
 * <B>Create on:</B> 2014年12月12日 上午11:06:12<br>
 *
 * @author yuan.qin
 * @version 1.0
 */
public class SystemProcessTimerFilter implements Filter {

    private static final Logger log = LoggerFactory.getLogger(SystemProcessTimerFilter.class);
    
    public static final String SID = "sessionId";

    /** 
     * @see Filter#destroy()
     */
    @Override
    public void destroy() {
    }

    /** 
     * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
                         				throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
     	//sessionId
     	String sessionId = UUID.randomUUID().toString();
     	MDC.put(SID, sessionId);
     	
     	if(log.isDebugEnabled()){
     		log.debug("ip["+getRemoteHost(req)+"] request (" + req.getRequestURI() + ") ");
     	}
     	
     	try{
     		chain.doFilter(request, response);
     	}finally{
     		MDC.remove(SID);
     	}

    }

    /** 
     * @see Filter#init(FilterConfig)
     */
    @Override
    public void init(FilterConfig arg0) throws ServletException {
    	
    }
    
    /**
	 * 获取IP地址
	 * @param request
	 * @return
	 */
	private String getRemoteHost(HttpServletRequest request){
		String ip = request.getHeader("x-forwarded-for");
		if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
			ip = request.getHeader("Proxy-Client-IP");
		}
		if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
			ip = request.getRemoteAddr();
		}
		return ip.equals("0:0:0:0:0:0:0:1")?"127.0.0.1":ip;
	}

}
