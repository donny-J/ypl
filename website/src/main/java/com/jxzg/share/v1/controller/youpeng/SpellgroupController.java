package com.jxzg.share.v1.controller.youpeng;

import com.jxzg.share.domain.meeting.Spellgroup;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.service.v1.IMeetingApplyService;
import com.jxzg.share.service.v1.IMeetingService;
import com.jxzg.share.vo.Spellgroup.SpellgroupBody;
import com.jxzg.share.vo.Spellgroup.SpellgroupVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 拼团相关
 */
@Controller
@CrossOrigin
@RequestMapping(value = "v1")
public class SpellgroupController {
    @Autowired
    private IMeetingService meetingService;
    @Autowired
    private IMeetingApplyService meetingApplyService;

    /**
     * 创建会议(组团)
     */
    @RequestMapping(value = "cluster_meeting", method = RequestMethod.POST)
    @ResponseBody
    public ReturnMap cluster_meeting(@RequestBody SpellgroupBody body) {
        ReturnMap result = null;
        try {
            result = meetingService.cluster_meeting(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 拼团报名
     */
    @RequestMapping(value = "spellgroup_open", method = RequestMethod.POST)
    @ResponseBody
    public ReturnMap spellgroup_open(@RequestBody SpellgroupVO spellgroupVO) {
        ReturnMap result = null;
        try {
            result = meetingApplyService.Spellgroup_open(spellgroupVO);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 组团详情
     */
    @RequestMapping(value = "spellgroup_details", method = RequestMethod.POST)
    @ResponseBody
    public ReturnMap spellgroup_details(@RequestBody Spellgroup spellgroup) throws Exception {
        ReturnMap result = null;
        try {
            result = meetingService.spellgroup_details(spellgroup);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
