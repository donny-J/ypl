package com.jxzg.share.v1.controller.youpeng;

import com.jxzg.share.querypage.MeetingQueryObject;
import com.jxzg.share.returndomain.ReturnList;
import com.jxzg.share.service.v1.IMeetingService;
import com.jxzg.share.service.v1.IMeetingpicService;
import com.jxzg.share.toolUtil.JudgeRequestDeviceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * 会议图片类型显示
 */
@Controller
@CrossOrigin
@RequestMapping(value = "v1")
public class MeetingPicController {
    @Autowired
    private IMeetingpicService meetingpicService;
    @Autowired
    private IMeetingService meetingService;

    /**
     * 会议图片类型列表
     */
    @RequestMapping(value = "meetingpictypelist", method = RequestMethod.POST)
    @ResponseBody
    public ReturnList meetingpictypelist(@RequestBody MeetingQueryObject qo, HttpServletRequest request, HttpServletResponse response) {
        ReturnList result = null;
        try {
            if (JudgeRequestDeviceUtil.check(request, response)) {//yidong
                qo.setPicstatus(0);
                result = meetingpicService.meetingpictypelist(qo);
            } else {//pc
                qo.setPicstatus(1);
                result = meetingpicService.meetingpictypelist(qo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    /**
     * 轮播图
     */
    @RequestMapping(value = "carousel_figure", method = RequestMethod.GET)
    @ResponseBody
    public ReturnList Carouselfigure() {
        List result = new ArrayList();
        try {
            result = meetingService.selectCarouselfigure();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ReturnList(result, "成功");
    }

}
