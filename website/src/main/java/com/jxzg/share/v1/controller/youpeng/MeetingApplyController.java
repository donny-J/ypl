package com.jxzg.share.v1.controller.youpeng;

import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.returndomain.ReturnList;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.service.v1.IMeetingApplyService;
import com.jxzg.share.service.v1.ISignfieldService;
import com.jxzg.share.vo.CreateOrderVO;
import com.jxzg.share.vo.IdAndToken;
import com.jxzg.share.vo.Spellgroup.SpellgroupVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 会议报名
 */
@Controller
@CrossOrigin
@RequestMapping(value = "v1")
public class MeetingApplyController {
    @Autowired
    private IMeetingApplyService meetingApplyService;
    @Autowired
    private ISignfieldService signfieldService;

    /**
     * 会议报名首页
     *
     * @param midAndToken
     * @return
     */
    @RequestMapping(value = "meetingapply_list", method = RequestMethod.POST)
    @ResponseBody
    public ReturnMap meetingapply_list(@RequestBody IdAndToken midAndToken) {
        ReturnMap result = null;
        try {
            result = meetingApplyService.meetingapplylist(midAndToken.getToken(), midAndToken.getMid());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    /**
     * 会议报名单行文本,多行文本,单选题,多选题
     */
    @RequestMapping(value = "apply_message", method = RequestMethod.POST)
    @ResponseBody
    public ReturnList apply_message(@RequestBody IdAndToken midAndToken) {
        ReturnList result = null;
        try {
            result = meetingApplyService.applymessage(midAndToken.getToken(), midAndToken.getMid());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 获取发票信息
     */
    @RequestMapping(value = "get_invoices", method = RequestMethod.POST)
    @ResponseBody
    public ReturnMap get_Invoices(@RequestBody IdAndToken midAndToken) {
        ReturnMap result = null;
        try {
            result = meetingApplyService.get_Invoices(midAndToken.getToken(), midAndToken.getMid());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 停止报名
     */
    @RequestMapping(value = "stop_meetingapply", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult stop_meetingapply(@RequestBody IdAndToken midAndToken) {
        JSONResult result = null;
        try {
            result = meetingApplyService.stop_meetingapply(midAndToken.getToken(), midAndToken.getMid());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 普通会议报名
     *
     * @param orders
     * @return
     */
    @RequestMapping(value = "create_order", method = RequestMethod.POST)
    @ResponseBody
    public ReturnMap create_order(@RequestBody CreateOrderVO orders) {
        ReturnMap result = null;
        try {
            result = signfieldService.create_order(orders);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
