package com.jxzg.share.v1.controller.youpeng;

import com.jxzg.share.domain.other.Token;
import com.jxzg.share.domain.other.User;
import com.jxzg.share.domain.other.Userprofile;
import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.service.v1.IUserService;
import com.jxzg.share.service.v1.IUserprofileService;
import com.jxzg.share.service.v1.IUsertokenService;
import com.jxzg.share.toolUtil.*;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * ios端个人资料完善
 */
@Controller
@CrossOrigin
@RequestMapping(value = "v1")
public class PersonController {

    private final Logger logger= LoggerFactory.getLogger(this.getClass());

    @Autowired
    private IUserprofileService userprofileService;

    @Autowired
    private IUserService userService;

    @Autowired
    private IUsertokenService usertokenService;


    @Value("${pic.userheadurl}")
    private String userheadurl;
    @Value("${pic.userhead}")
    private String userhead;

    //用户个人资料显示
    @RequestMapping(value = "personlist", method = RequestMethod.POST)
    @ResponseBody
    public ReturnMap personList(@RequestBody Token token) {
        ReturnMap result = null;
        try {
            result = userprofileService.personList(token.getToken());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    //上传头像
    @RequestMapping(value = "headimage", method = RequestMethod.POST)
    @ResponseBody
    public ReturnMap headimage(MultipartFile file, HttpServletRequest request, HttpServletResponse response) {
        Map map = new HashMap();
        try {
            String uuid = UUID.randomUUID().toString();
            String orgFileName = file.getOriginalFilename();
            String fileName = uuid + "."
                    + FilenameUtils.getExtension(orgFileName);
            String fileNamepc = uuid + "_pc" + "."
                    + FilenameUtils.getExtension(orgFileName);
            String fileNamemove = uuid + "_move" + "."
                    + FilenameUtils.getExtension(orgFileName);

            String name = FileUploadUtil.saveFile(file, userhead, fileName);
            CommonsMultipartFile cf = (CommonsMultipartFile) file;
            DiskFileItem fi = (DiskFileItem) cf.getFileItem();
            File f = fi.getStoreLocation();
            File toPicPc = new File(userhead + "/" + fileNamepc);
            Thumbnails.of(f).scale(1.0).toFile(toPicPc);
            File toPicmove = new File(userhead + "/" + fileNamemove);
            Thumbnails.of(f).scale(1.0).toFile(toPicmove);
            if (JudgeRequestDeviceUtil.check(request, response)) {
                map.put("url", userheadurl + fileNamemove);
            } else {
                map.put("url", userheadurl + fileNamepc);
            }
        } catch (Exception e) {
            logger.error("headimage:"+e.getMessage(),e);
        }
        return new ReturnMap("上传成功", map);
    }

    //修改头像
    @RequestMapping(value = "update_headimage", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult update_headimage(@RequestBody User user) {
        JSONResult result = null;
        try {
            result = userService.updateHeadimage(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    //昵称
    @RequestMapping(value = "nickname", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult nickname(@RequestBody Userprofile userprofile) {
        JSONResult jsonResult = null;
        try {
            jsonResult = userprofileService.updateNickname(userprofile.getNickname(), userprofile.getToken());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonResult;
    }

    //姓名
    @RequestMapping(value = "realname", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult realname(@RequestBody Userprofile userprofile) {
        JSONResult jsonResult = null;
        try {
            if (userprofile.getRealname() == null) {
                return new JSONResult(false, "空");
            }
            jsonResult = userprofileService.updateRealname(userprofile.getRealname(), userprofile.getToken());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonResult;
    }

    //性别
    @RequestMapping(value = "gender", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult gender(@RequestBody Userprofile userprofile) {
        JSONResult jsonResult = null;
        try {
            jsonResult = userprofileService.updateGender(userprofile.getGender(), userprofile.getToken());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonResult;
    }

    //证件
    @RequestMapping(value = "idcardtype", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult idcardtype(@RequestBody Userprofile userprofile) {
        JSONResult jsonResult = null;
        try {
            jsonResult = userprofileService.updateIdcardtype(userprofile.getIdcardtype(), userprofile.getIdcard(), userprofile.getToken());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonResult;
    }

    //所在城市
    @RequestMapping(value = "address", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult address(@RequestBody Userprofile userprofile) {
        JSONResult jsonResult = null;
        try {
            if (userprofile == null) {
                return new JSONResult(false, "地址为空");
            }
            jsonResult = userprofileService.updateAddress(userprofile.getToken(), userprofile.getProvice(), userprofile.getCity(), userprofile.getDist());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonResult;
    }
}
