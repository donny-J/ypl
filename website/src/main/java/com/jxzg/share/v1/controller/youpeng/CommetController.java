package com.jxzg.share.v1.controller.youpeng;

import com.jxzg.share.domain.other.Commet;
import com.jxzg.share.mapper.CommetMapper;
import com.jxzg.share.mapper.MeetingMapper;
import com.jxzg.share.querypage.CommetQueryObject;
import com.jxzg.share.returndomain.ReturnPage;
import com.jxzg.share.service.v1.ICommetService;
import com.jxzg.share.service.v1.IMeetingService;
import com.jxzg.share.returndomain.JSONResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 会议评论与点赞
 */
@Controller
@CrossOrigin
@RequestMapping(value = "v1")
public class CommetController {
    @Autowired
    private ICommetService commetService;
    @Autowired
    private IMeetingService meetingService;
    @Autowired
    private MeetingMapper meetingMapper;
    @Autowired
    private CommetMapper commetMapper;


    //评论
    @RequestMapping(value = "commet_save", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult commetsave(@RequestBody Commet commet) {
        JSONResult json = null;
        try {
            json = commetService.insert(commet);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    //回复
    @RequestMapping(value = "commet_reply", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult commetreply(@RequestBody Commet commet) {
        JSONResult json = null;
        try {
            json = commetService.commetreply(commet);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    //评论列表
    @RequestMapping(value = "commet_list", method = RequestMethod.POST)
    @ResponseBody
    public ReturnPage commetlist(@RequestBody CommetQueryObject qo) {
        ReturnPage result = null;
        try {
            result = commetService.commetlist(qo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    //点赞
    @RequestMapping(value = "commet_like", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult commetlike(@RequestBody Commet commet) {
        JSONResult json = null;
        try {
            json = commetService.commetlike(commet);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }
}
