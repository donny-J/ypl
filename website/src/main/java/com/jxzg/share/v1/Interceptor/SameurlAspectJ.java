package com.jxzg.share.v1.Interceptor;

import com.jxzg.share.domain.meeting.Sponsor;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.service.v1.IEhcacheService;
import com.jxzg.share.toolUtil.ApplicationContextUtil;
import com.jxzg.share.toolUtil.JsonUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Created by 00914Donny on 2017/6/6.
 */
@Aspect
@Component
public class SameurlAspectJ {

    private static final Logger log = LoggerFactory.getLogger(SameurlAspectJ.class);
    @Autowired
    private IEhcacheService ehcacheService;

    //注册
    public static final String REGISTER_AROUND = "execution(* com.jxzg.share.v1.controller.youpeng.LoginController.register(..))";
    //验证码
    public static final String VERITYCODE_AROUND = "execution(* com.jxzg.share.v1.controller.youpeng.VerifyCodeController.sendVeritfyCode(..))";
    //添加主办方
    public static final String SPONSORSAVE_AROUND = "execution(* com.jxzg.share.v1.controller.youpeng.SponsorController.sponsorsave(..))";
    //创建会议(发活动)
    public static final String ADDMETTING_AROUND = "execution(* com.jxzg.share.v1.controller.youpeng.MeetingController.addmetting(..))";
    //创建会议(组团)
    public static final String CLUSTER_METTING_AROUND = "execution(* com.jxzg.share.v1.controller.youpeng.SpellgroupController.cluster_meeting(..))";
    //拼团报名
    public static final String SPELLGROUP_OPEN_AROUND = "execution(* com.jxzg.share.v1.controller.youpeng.SpellgroupController.spellgroup_open(..))";


//    @Before(EDP)    //spring中Before通知
//    public void logBefore() {
//        System.out.println("logBefore:现在时间是:"+new Date());
//    }

//    @After(EDP)    //spring中After通知
//    public void logAfter() {
//        System.out.println("logAfter:现在时间是:"+new Date());
//    }

    /**
     * spring中的环绕增强
     *
     * @param joinPoint
     * @return
     */
    @Around(REGISTER_AROUND)
    public Object registerAround(ProceedingJoinPoint joinPoint) {
        return execMethodAround(joinPoint, REGISTER_AROUND);
    }

    @Around(VERITYCODE_AROUND)
    public Object verityCodeAround(ProceedingJoinPoint joinPoint) {
        return execMethodAround(joinPoint, VERITYCODE_AROUND);
    }

    @Around(SPONSORSAVE_AROUND)
    public Object sponsorSaveAround(ProceedingJoinPoint joinPoint) {
        return execMethodAround(joinPoint, ADDMETTING_AROUND);
    }

    @Around(ADDMETTING_AROUND)
    public Object addMeetingAround(ProceedingJoinPoint joinPoint) {
        return execMethodAround(joinPoint, ADDMETTING_AROUND);
    }

    @Around(CLUSTER_METTING_AROUND)
    public Object clusterMeetingAround(ProceedingJoinPoint joinPoint) {
        return execMethodAround(joinPoint, CLUSTER_METTING_AROUND);
    }

    @Around(SPELLGROUP_OPEN_AROUND)
    public Object spellgroupOpenAround(ProceedingJoinPoint joinPoint) {
        return execMethodAround(joinPoint, SPELLGROUP_OPEN_AROUND);
    }


    /**
     * 环绕增强具体实现
     *
     * @param joinPoint
     * @param methodName
     * @return
     */
    private Object execMethodAround(ProceedingJoinPoint joinPoint, String methodName) {
        log.debug("check the same url of " + methodName);
        Object[] args = joinPoint.getArgs();
        Object obj = null;
        if (repeatDataValidator(args, methodName)) {
            return obj;
        }
        try {
            obj = joinPoint.proceed(args);
        } catch (Throwable e) {
            log.error(e.getMessage(), e);
        }
        return obj;
    }

    /**
     * 检验指定时间内是否有重复的提交
     *
     * @param objs
     * @param lock
     * @return
     */
    private boolean repeatDataValidator(Object[] objs, Object lock) {
        if (objs != null && objs.length == 1) {
            String keyUrl = JsonUtil.convertObjectToJson(objs[0]);
            synchronized (lock) {
                String returnValue = ehcacheService.getUrlFromCache(keyUrl);
                // 缓存中不存在
                if (returnValue == null) {
                    ehcacheService.putUrlToCache(keyUrl);
                } else {
                    log.debug("url in ehcache" + keyUrl);
                    return true;
                }
            }
        }
        return false;
    }
}
