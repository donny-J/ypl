package com.jxzg.share.v1.controller.youpeng;


import com.jxzg.share.domain.other.Token;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.service.v1.IUserService;
import com.jxzg.share.service.v1.IUserprofileService;
import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.vo.PasswordVO;
import com.jxzg.share.vo.VerifyCodeVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 密码相关.
 */
@Controller
@CrossOrigin
@RequestMapping(value = "v1")
public class PasswordController {
    @Autowired
    private IUserprofileService userprofileService;

    @Autowired
    private IUserService userService;

    /**
     * 忘记密码
     */
    @RequestMapping(value = "forgetpassword", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult ForgetPassword(@RequestBody VerifyCodeVO forgetpassword) {
        JSONResult jsonResult = null;
        try {
            if (forgetpassword.getLoginname() == null || forgetpassword.getVerifyCode() == null) {
                return new JSONResult(false, "密码或验证码错误!");
            }
            jsonResult = userprofileService.ForgetPassword(forgetpassword.getLoginname(), forgetpassword.getLoginpassword(), forgetpassword.getVerifyCode());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonResult;
    }

    /**
     * 更换手机号
     */
    @RequestMapping(value = "change_tel", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult change_tel(@RequestBody VerifyCodeVO forgetpassword) {
        JSONResult jsonResult = null;
        try {
            jsonResult = userprofileService.change_tel(forgetpassword);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonResult;
    }


    /**
     * 修改密码
     */
    @RequestMapping(value = "changepassword", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult ChangePassword(@RequestBody PasswordVO password) {
        JSONResult json = null;
        try {
            json = userService.updatePassword(password.getToken(), password.getNewloginpassword());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    //验证密码
    @RequestMapping(value = "verify_password", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult verify_password(@RequestBody PasswordVO oldpassword) {
        JSONResult result = null;
        try {
            result = userService.verify_password(oldpassword);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 设置支付密码
     */
    @RequestMapping(value = "paypassword", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult SetApplyPassword(@RequestBody PasswordVO password) {
        JSONResult json = null;
        try {
            json = userService.SetApplyPassword(password.getToken(), password.getNewloginpassword());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    /**
     * 验证支付密码
     */
    @RequestMapping(value = "verifypaypassword", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult verifypaypassword(@RequestBody PasswordVO password) {
        JSONResult json = null;
        try {
            json = userService.verifypaypassword(password.getToken(), password.getNewloginpassword());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    /**
     * 检验是否设置支付密码
     */
    @RequestMapping(value = "checkpaypassword", method = RequestMethod.POST)
    @ResponseBody
    public ReturnMap checkpaypassword(@RequestBody Token token) {
        ReturnMap json = null;
        try {
            json = userService.checkpaypassword(token);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    /**
     * 检验支付密码短信
     */
    @RequestMapping(value = "checkpaypassword_msg", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult checkpaypassword_msg(@RequestBody VerifyCodeVO verifyCode) {
        JSONResult json = null;
        try {
            json = userService.checkpaypassword_msg(verifyCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }
}
