package com.jxzg.share.v1.controller.youpeng;

import com.jxzg.share.domain.other.HistoryRecord;
import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.service.v1.IHistoryRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 历史记录
 */
@Controller
@CrossOrigin
@RequestMapping(value = "v1")
public class HistoryRecordController {
    @Autowired
    private IHistoryRecordService historyRecordService;
    @RequestMapping(value = "historyrecord_save",method = RequestMethod.POST)
    public JSONResult historyrecord_save(@RequestBody HistoryRecord historyRecord){
        JSONResult result = null;
        try {
            result = historyRecordService.historyrecord_save(historyRecord);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
