package com.jxzg.share.v1.controller.youpeng;

import com.jxzg.share.domain.other.Token;
import com.jxzg.share.service.v1.IUserService;
import com.jxzg.share.service.v1.IUsertokenService;
import com.jxzg.share.service.v1.IVerifylogService;
import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.vo.SendNoteVO;
import com.jxzg.share.vo.SendVerifyVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 短信和邮箱验证
 */
@Controller
@CrossOrigin
@RequestMapping(value = "v1")
public class VerifyCodeController {
    @Autowired
    private IVerifylogService verifylogService;
    @Autowired
    private IUserService userService;
    @Autowired
    private IUsertokenService usertokenService;

    /**
     * 发送手机验证码
     * type 1:注册  2:忘记密码 3:更换手机号
     */
    @RequestMapping(value = "sendVerifyCode", method = RequestMethod.POST)
    @ResponseBody
    private JSONResult sendVeritfyCode(@RequestBody SendVerifyVO verify) {
        JSONResult jsonResult = null;
        try {
            jsonResult = verifylogService.sendVerifyCode(verify.getLoginname(), verify.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonResult;
    }

    /**
     * 短信剩余条数
     */
    @RequestMapping(value = "remainmsg", method = RequestMethod.POST)
    @ResponseBody
    private Map<String, Object> remainmsg(@RequestBody Token token) {
        Map<String, Object> result = new HashMap<>();
        try {
            result = userService.selectremainmsg(token.getToken());
        } catch (Exception e) {
            result.put("success", false);
            result.put("msg", "查询失败");
            e.printStackTrace();
        }
        return result;
    }
}
