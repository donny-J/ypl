package com.jxzg.share.v1.controller.youpeng;

import com.jxzg.share.domain.pay.Orderdtl;
import com.jxzg.share.domain.pay.Orders;
import com.jxzg.share.querypage.IncomeDetailQueryObject;
import com.jxzg.share.returndomain.ReturnList;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.service.v1.IOrdersService;
import com.jxzg.share.service.v1.ISignfieldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 订单信息
 */
@Controller
@CrossOrigin
@RequestMapping(value = "v1")
public class OrdersController {
    @Autowired
    private IOrdersService ordersService;
    @Autowired
    private ISignfieldService signfieldService;


    /**
     * 收支明细-全部
     */
    @RequestMapping(value = "income_detail_all", method = RequestMethod.POST)
    @ResponseBody
    public ReturnList income_detail_all(@RequestBody IncomeDetailQueryObject qo) {
        ReturnList result = null;
        try {
            result = ordersService.income_detail_all(qo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    /**
     * 收入明细
     */
    @RequestMapping(value = "income_detail", method = RequestMethod.POST)
    @ResponseBody
    public ReturnList income_detail(@RequestBody IncomeDetailQueryObject qo) {
        ReturnList result = null;
        try {
            result = ordersService.income_detail(qo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 支出明细
     */
    @RequestMapping(value = "outcome_detail", method = RequestMethod.POST)
    @ResponseBody
    public ReturnList outcome_detail(@RequestBody IncomeDetailQueryObject qo) {
        ReturnList result = null;
        try {
            result = ordersService.outcome_detail(qo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 收支明细二级
     */
    @RequestMapping(value = "income_detail_dtl", method = RequestMethod.POST)
    @ResponseBody
    public ReturnMap income_detail_dtl(@RequestBody Orders orders) {
        ReturnMap result = null;
        try {
            result = ordersService.income_detail_dtl(orders);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    //打赏
    @RequestMapping(value = "play_tour ", method = RequestMethod.POST)
    @ResponseBody
    public ReturnMap play_tour(@RequestBody Orderdtl orders) {
        ReturnMap result = null;
        try {
            result = ordersService.play_tour(orders);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 购买短信
     */
    @RequestMapping(value = "buy_msg", method = RequestMethod.POST)
    @ResponseBody
    public ReturnMap buy_msg(@RequestBody Orders orders) {
        ReturnMap result = null;
        try {
            result = ordersService.buy_msg(orders);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
