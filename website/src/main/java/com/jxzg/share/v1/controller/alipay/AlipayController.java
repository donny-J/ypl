package com.jxzg.share.v1.controller.alipay;


import com.jxzg.share.domain.pay.Orders;
import com.jxzg.share.mapper.MeetingticketMapper;
import com.jxzg.share.mapper.OrderdtlMapper;
import com.jxzg.share.mapper.OrdersMapper;
import com.jxzg.share.pay.alipay.AlipayConfig;
import com.jxzg.share.service.v1.IOrdersService;
import com.jxzg.share.pay.alipay.AlipayNotify;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.*;


/**
 * 支付宝支付
 */
@Controller
@RequestMapping(value = "v1")
public class AlipayController {

    private static final Logger log = LoggerFactory.getLogger(AlipayController.class);


    @Value("${alipay.appid}")
    private String appId;
    @Value("${alipay.sellerId}")
    private String sellId;


    @Autowired
    private IOrdersService ordersService;
    @Autowired
    private OrdersMapper ordersMapper;
    @Autowired
    private OrderdtlMapper orderdtlMapper;
    @Autowired
    private MeetingticketMapper meetingticketMapper;


    @Autowired
    private AlipayNotify alipayNotify;

    /**
     * 支付宝回异步调接口
     *
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "getPayNotify", method = {RequestMethod.POST})
    @ResponseBody
    public String getPayNotify(HttpServletRequest request) throws Exception {
        Map<String, String> params = new HashMap<String, String>();
        Map requestParams = request.getParameterMap();
        Date date = new Date();
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
            //valueStr = new String(valueStr.getBytes("ISO-8859-1"), "gbk");
            params.put(name, valueStr);
        }

        //获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以下仅供参考)//
        //商户订单号
        String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"), "UTF-8");
        //支付宝交易号
        String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"), "UTF-8");

        Orders orders = ordersMapper.selectByOrderno(out_trade_no);
        if (orders == null) {
            return "out_trade_no fail";
        }
        String total_amount = new String(request.getParameter("total_amount").getBytes("ISO-8859-1"), "UTF-8");
        if (!total_amount.equals(orders.getMoney().toString())) {
            return "total_amount fail";
        }
        String appid = new String(request.getParameter("app_id").getBytes("ISO-8859-1"), "UTF-8");
        if (!appid.equals(appId)) {
            return "appid fail";
        }
        String seller_id = new String(request.getParameter("seller_id").getBytes("ISO-8859-1"), "UTF-8");
        if (!seller_id.equals(sellId)) {
            return "seller_id fail";
        }
        //交易状态
        String trade_status = new String(request.getParameter("trade_status").getBytes("ISO-8859-1"), "UTF-8");
        //异步通知ID
        String notify_id = request.getParameter("notify_id");
        //sign
        String sign = request.getParameter("sign");
        if (notify_id != "" && notify_id != null) {
            if (alipayNotify.verifyResponse(notify_id).equals("true"))//判断成功之后使用getResponse方法判断是否是支付宝发来的异步通知。
            {
                if (alipayNotify.getSignVeryfy(params, sign)) {//使用支付宝公钥验签
                    if (trade_status.equals("TRADE_FINISHED") || trade_status.equals("TRADE_SUCCESS")) {
                        try {
                            boolean isSuccess = ordersService.alipay_success(out_trade_no, Orders.ALIPAY);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    return "success";
                } else {//验证签名失败
                    return "sign fail";
                }
            } else {//验证是否来自支付宝的通知失败
                return "response fail";
            }
        } else {
            return "no notify message";
        }
    }
}