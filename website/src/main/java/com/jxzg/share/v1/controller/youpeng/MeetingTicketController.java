package com.jxzg.share.v1.controller.youpeng;

import com.jxzg.share.querypage.MeetingTicketQueryObject;
import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.returndomain.ReturnList;
import com.jxzg.share.service.v1.IMeetingticketService;
import com.jxzg.share.service.v1.IMyticketService;
import com.jxzg.share.vo.MyticketVO;
import com.jxzg.share.vo.SendTicketVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by 00818Wolf on 2017/3/15.
 */
@Controller
@CrossOrigin
@RequestMapping(value = "v1")
public class MeetingTicketController {
    @Autowired
    private IMeetingticketService meetingticketService;
    @Autowired
    private IMyticketService myticketService;

    /**
     * 我的参加_列表
     */
    @RequestMapping(value = "myjoinmeeting", method = RequestMethod.POST)
    @ResponseBody
    public ReturnList myticket(@RequestBody MeetingTicketQueryObject qo) {
        ReturnList result = null;
        try {
            result = myticketService.myticket(qo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 我的参加_多张票
     */
    @RequestMapping(value = "mymanyticket", method = RequestMethod.POST)
    @ResponseBody
    public ReturnList mymanyticket(@RequestBody MyticketVO myticket) {
        ReturnList result = null;
        try {
            result = myticketService.mymanyticket(myticket);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 赠送门票
     */
    @RequestMapping(value = "sendticket", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult sendticket(@RequestBody SendTicketVO sendTicket) {//门票id和token
        JSONResult result = null;
        try {
            result = myticketService.sendticket(sendTicket);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}