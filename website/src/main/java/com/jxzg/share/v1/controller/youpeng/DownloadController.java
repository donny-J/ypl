package com.jxzg.share.v1.controller.youpeng;

import com.jxzg.share.domain.other.SysParam;
import com.jxzg.share.dto.DownloadResult;
import com.jxzg.share.enums.ConstantStateEnum;
import com.jxzg.share.mapper.*;
import com.jxzg.share.dto.BaseResult;
import com.jxzg.share.querypage.SysParamModQueryObject;
import com.jxzg.share.service.v1.ISysParamService;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.List;


/**
 * Provide mobile client download APP link
 * You need to maintain your version number
 */
@Controller
@CrossOrigin
@RequestMapping(value = "v1/download")
public class DownloadController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private final String CHARACTER_ENCODE_UTF_8 = "utf-8";
    private final String CHARACTER_ENCODE_ISO_88591 = "ISO-8859-1";

    @Autowired
    private ISysParamService sysParamService;

    @Value("${android.version.code}")
    private String androidVersionCode;
    @Value("${android.app.downloadlink}")
    private String downloadLink;
    @Value("${android.app.absolute.path}")
    private String appAbsolutePath;


    @RequestMapping(value = "/check/app/info", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult checkAppInfo(HttpServletRequest request) {
        DownloadResult result = new DownloadResult();
        SysParamModQueryObject queryObject = new SysParamModQueryObject();
        queryObject.setParamName(androidVersionCode);
        try {
            //TODO cache
            List<SysParam> list = sysParamService.getSysParamsByCon(queryObject);
            if (list.size() == 1) {
                SysParam sysParam = list.get(0);
                result.setVersionCode(sysParam.getParamValue());
                result.setDownloadLink(downloadLink);
                if (sysParam.getParamClass() == 1)
                    result.setIsForceDownload(true);
                else
                    result.setIsForceDownload(false);
                result.setErrorCode(ConstantStateEnum.SUCCESS.getErrorCode());
                result.setMsg(ConstantStateEnum.SUCCESS.getErrMsg());
                return result;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        result.setErrorCode(ConstantStateEnum.INNER_ERROR.getErrorCode());
        result.setMsg(ConstantStateEnum.INNER_ERROR.getErrMsg());
        return result;
    }

    @RequestMapping(value = "/android/app", method = RequestMethod.GET)
    public ResponseEntity<byte[]> downloadApp(HttpServletRequest request) throws Exception {
        File file = new File(appAbsolutePath);
        //处理显示中文文件名的问题
        String fileName = new String(file.getName().getBytes(CHARACTER_ENCODE_UTF_8), CHARACTER_ENCODE_ISO_88591);
        //设置请求头内容,告诉浏览器代开下载窗口
        HttpHeaders headers = new HttpHeaders();
        headers.setContentDispositionFormData("attachment", fileName);
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file),
                headers, HttpStatus.CREATED);
    }


}