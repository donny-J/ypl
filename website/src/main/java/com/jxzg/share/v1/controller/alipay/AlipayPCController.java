package com.jxzg.share.v1.controller.alipay;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.jxzg.share.domain.pay.Orders;
import com.jxzg.share.mapper.OrdersMapper;
import com.jxzg.share.returndomain.JSONResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;

import com.jxzg.share.pay.alipay.AlipayConfig;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * Created by 00818Wolf on 2017/5/9.
 */
@Controller
@CrossOrigin
@RequestMapping(value = "v1")
public class AlipayPCController {
    @Autowired
    private OrdersMapper ordersMapper;


    @Value("${alipay.appid}")
    private String appId;
    @Value("${alipay.baseUrl}")
    private String alipayBaseUrl;
    @Value("${alipay.appid}")
    private String alipayAppid;
    @Value("${alipay.privateKey}")
    private String privateKey;
    @Value("${alipay.publicKey}")
    private String publicKey;
    @Value("${alipay.notifyUrl}")
    private String notifyUrl;
    @Value("${alipay.returnUrl}")
    private String returnUrl;

    /**
     * 支付宝PC下单接口
     *
     * @param orders
     * @param response
     * @throws IOException
     */
    @RequestMapping(value = "alipay_pc", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult alipay_pc(@RequestBody Orders orders, HttpServletResponse response) throws IOException {
        Orders orders1 = ordersMapper.selectByOrderno(orders.getOrderno());
        if (orders.getOrderno() == null || orders1 == null) {
            return new JSONResult(false, "订单状态异常");
        }
        if (orders1.getStatus() != Orders.ORDER_STATUS_NO_PAY) {
            return new JSONResult(false, "订单超时");
        }
        AlipayClient alipayClient = new DefaultAlipayClient(alipayBaseUrl, appId, privateKey, "JSON", AlipayConfig.input_charset, publicKey, "RSA2"); //获得初始化的AlipayClient
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();//创建API对应的request
        alipayRequest.setReturnUrl(returnUrl);
        alipayRequest.setNotifyUrl(notifyUrl);//在公共参数中设置回跳和通知地址
        BigDecimal money = orders1.getMoney();
        String body = null;
        if (orders1.getOrdertype() == Orders.ORDERTYPE_GENERAL || orders1.getOrdertype() == Orders.ORDERTYPE_TEAM) {
            body = "门票订单";
        } else if (orders1.getOrdertype() == Orders.ORDERTYPE_NOTE) {
            body = "短信订单";
        } else {
            body = "打赏订单";
        }
        alipayRequest.setBizContent("{" +
                "    \"out_trade_no\":\"" + orders1.getOrderno() + "\"," +
                "    \"product_code\":\"FAST_INSTANT_TRADE_PAY\"," +
                "    \"total_amount\":\"" + money + "\"," +
                "    \"subject\":\"" + body + "\"," +
                "    \"passback_params\":\"merchantBizType%3d3C%26merchantBizNo%3d2016010101111\"," +
                "    \"extend_params\":{" +
                "    \"sys_service_provider_id\":\"2088511833207846\"" +
                "    }" +
                "  }");//填充业务参数
        String form = "";
        try {
            form = alipayClient.pageExecute(alipayRequest).getBody(); //调用SDK生成表单
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        response.setContentType("text/html;charset=" + AlipayConfig.input_charset);
        response.getWriter().write(form);//直接将完整的表单html输出到页面
        response.getWriter().flush();
        response.getWriter().close();
        return new JSONResult("订单正常");
    }

    @RequestMapping(value = "alipay_query", method = RequestMethod.POST)
    @ResponseBody
    public String alipay_query(@RequestBody Orders orders) {
        AlipayClient alipayClient = new DefaultAlipayClient(alipayBaseUrl, appId, privateKey, "json", AlipayConfig.input_charset, publicKey, "RSA2");
        AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();
        request.setBizContent("{" +
                "    \"out_trade_no\":\"" + orders.getOrderno() + "\"," +
                "    \"trade_no\":\"\"" +
                "  }");
        AlipayTradeQueryResponse response = null;
        try {
            response = alipayClient.execute(request);
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return response.getTradeStatus();
    }
}
