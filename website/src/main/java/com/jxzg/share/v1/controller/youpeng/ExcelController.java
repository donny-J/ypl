package com.jxzg.share.v1.controller.youpeng;

import com.jxzg.share.domain.meeting.*;
import com.jxzg.share.domain.other.User;
import com.jxzg.share.domain.other.Userprofile;
import com.jxzg.share.domain.other.Usertoken;
import com.jxzg.share.domain.pay.Orderdtl;
import com.jxzg.share.domain.pay.Orders;
import com.jxzg.share.mapper.*;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.vo.IdAndToken;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * Excel导出
 */
@Controller
@CrossOrigin
@RequestMapping(value = "v1")
public class ExcelController {
    @Autowired
    private UsertokenMapper usertokenMapper;
    @Autowired
    private MeetingMapper meetingMapper;
    @Autowired
    private MyticketMapper myticketMapper;
    @Autowired
    private SignuserinfoMapper signuserinfoMapper;
    @Autowired
    private SignfieldMapper signfieldMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserprofileMapper userprofileMapper;
    @Autowired
    private OrdersMapper ordersMapper;

    @Autowired
    private OrderdtlMapper orderdtlMapper;
    @Autowired
    private InvoicesMapper invoicesMapper;
    @Value("${excel}")
    private String excel;
    @Value("${excelurl}")
    private String excelurl;

    @RequestMapping(value = "excelexport", method = RequestMethod.POST)
    @ResponseBody
    public ReturnMap excelexport(@RequestBody IdAndToken idAndToken, HttpServletRequest request) {
        Map<String, Object> result = new HashMap();
        try {
            Usertoken usertoken = usertokenMapper.selectUidBytoken(idAndToken.getToken());
            if (usertoken.getToken() == null || idAndToken.getMid() == 0) {
                return new ReturnMap(false, "导出失败!");
            }
            Meeting meeting = meetingMapper.selectMeetingByMid(idAndToken.getMid());
            //创建HSSFWorkbook对象(excel的文档对象)
            HSSFWorkbook wb = new HSSFWorkbook();
            //建立新的sheet对象（excel的表单）
            HSSFSheet sheet = wb.createSheet(meeting.getName() + "活动数据统计表");
            HSSFSheet sheet2 = wb.createSheet(meeting.getName() + "发票信息表");

            //在sheet里创建第一行，参数为行索引(excel的行)，可以是0～65535之间的任何一个
            HSSFRow row1 = sheet.createRow(0);
            HSSFRow rows1 = sheet2.createRow(0);

            //创建单元格（excel的单元格，参数为列索引，可以是0～255之间的任何一个
            HSSFCell cell = row1.createCell(0);
            HSSFCell cell1 = rows1.createCell(0);
            //设置单元格内容
            cell.setCellValue(meeting.getName() + "活动数据统计表");
            cell1.setCellValue(meeting.getName() + "发票信息表");
            //合并单元格CellRangeAddress构造参数依次表示起始行，截至行，起始列， 截至列
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 3));
            sheet2.addMergedRegion(new CellRangeAddress(0, 0, 0, 3));
            //在sheet里创建第二行
            HSSFRow row2 = sheet.createRow(1);
            HSSFRow rows2 = sheet2.createRow(1);
            List<Myticket> mytickets = myticketMapper.selectAll(idAndToken.getMid());
            //创建单元格并设置单元格内容
            row2.createCell(0).setCellValue("序号");
            row2.createCell(1).setCellValue("购票人账号");
            row2.createCell(2).setCellValue("购票人姓名");
            row2.createCell(3).setCellValue("参会人手机号");
            row2.createCell(4).setCellValue("交易时间");
            row2.createCell(5).setCellValue("门票数量");
            row2.createCell(6).setCellValue("交易金额");
            row2.createCell(7).setCellValue("签到状态");
            //创建单元格并设置单元格内容
            rows2.createCell(0).setCellValue("序号");
            rows2.createCell(1).setCellValue("用户名称");
            rows2.createCell(2).setCellValue("用户电话");
            rows2.createCell(3).setCellValue("发票类型");
            rows2.createCell(4).setCellValue("发票抬头");
            rows2.createCell(5).setCellValue("纳税人识别码");
            rows2.createCell(6).setCellValue("注册地址");
            rows2.createCell(7).setCellValue("注册电话");
            rows2.createCell(8).setCellValue("开户银行");
            rows2.createCell(9).setCellValue("银行账户");
            rows2.createCell(10).setCellValue("邮寄地址");
            rows2.createCell(11).setCellValue("收件人姓名");
            rows2.createCell(12).setCellValue("收件人电话");
            rows2.createCell(13).setCellValue("订单金额");
            List<Invoices> invoicess = invoicesMapper.selectByMid(idAndToken.getMid());
            if (invoicess != null) {
                int i = 0;
                for (Invoices invoices : invoicess) {
                    Orders orderdtl = ordersMapper.selectByOrderno(invoices.getOrderno());
                    if (orderdtl.getStatus() == Orderdtl.ORDER_STATUS_YES_PAY) {
                        HSSFRow row3 = sheet2.createRow(i + 2);
                        row3.createCell(0).setCellValue(i + 1);//序号
                        Userprofile userprofile = userprofileMapper.selectByUid(invoices.getUid());
                        User user = userMapper.selectByUid(invoicess.get(i).getUid());
                        if (userprofile.getRealname() != null) {
                            row3.createCell(1).setCellValue(userprofile.getRealname());
                        } else {
                            row3.createCell(1).setCellValue("未设置");
                        }
                        if (user.getLoginname() != null) {
                            row3.createCell(2).setCellValue(user.getLoginname());
                        } else {
                            row3.createCell(2).setCellValue("未设置");
                        }

                        if (invoicess.get(i).getInvoicetype() == 0) {
                            row3.createCell(3).setCellValue("无发票");
                        } else if (invoicess.get(i).getInvoicetype() == 1) {
                            row3.createCell(3).setCellValue("增值税普通发票");
                        } else {
                            row3.createCell(3).setCellValue("增值税专用发票");
                        }
                        row3.createCell(4).setCellValue(invoices.getTitle());
                        row3.createCell(5).setCellValue(invoices.getTaxcode());
                        row3.createCell(6).setCellValue(invoices.getRegaddress());
                        row3.createCell(7).setCellValue(invoices.getRegtel());
                        row3.createCell(8).setCellValue(invoices.getBankname());
                        row3.createCell(9).setCellValue(invoices.getBankcard());
                        row3.createCell(10).setCellValue(invoices.getAddress());
                        row3.createCell(11).setCellValue(invoices.getRealname());
                        row3.createCell(12).setCellValue(invoices.getTel());
                        row3.createCell(13).setCellValue(orderdtl.getMoney().toString());
                        i++;
                    }
                }
            }
            List<Signfield> ss1 = signfieldMapper.selectSignfieldName(idAndToken.getMid(), "单行文本");
            List<Signfield> ss2 = signfieldMapper.selectSignfieldName(idAndToken.getMid(), "多行文本");
            List<Signfield> ss3 = signfieldMapper.selectSignfieldName(idAndToken.getMid(), "单选项");
            List<Signfield> ss4 = signfieldMapper.selectSignfieldName(idAndToken.getMid(), "多选项");
            //在sheet里创建第三行
            for (int i = 0; i < mytickets.size(); i++) {
                HSSFRow row3 = sheet.createRow(i + 2);
                row3.createCell(0).setCellValue(i + 1);//序号
                Userprofile userprofile = userprofileMapper.selectByUid(mytickets.get(i).getUid());
                User user = userMapper.selectByUid(mytickets.get(i).getUid());
                if (user.getLoginname() != null) {
                    row3.createCell(1).setCellValue(user.getLoginname());
                }
                if (user.getLoginname() == null) {
                    row3.createCell(1).setCellValue(userprofile.getNickname());
                }
                Signuserinfo s1 = signuserinfoMapper.selectById(mytickets.get(i).getId(), Signuserinfo.FIELD_NAME);
                Orderdtl orderdtl = orderdtlMapper.selectByTicketid(mytickets.get(i).getId());
                row3.createCell(2).setCellValue(s1.getFieldval());//参会人姓名
                Signuserinfo s2 = signuserinfoMapper.selectById(mytickets.get(i).getId(), Signuserinfo.FIELE_PHONE);
                row3.createCell(3).setCellValue(s2.getFieldval());//参会人电话
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
                String startTime = sdf.format(orderdtl.getUpdatetime());
                row3.createCell(4).setCellValue(startTime);//交易时间
                row3.createCell(5).setCellValue(orderdtl.getAmount());//门票数量
                row3.createCell(6).setCellValue(orderdtl.getMoney().toString());//门票金额
                if (mytickets.get(i).getNo() == 0) {
                    row3.createCell(7).setCellValue("未签到");//签到状态
                } else {
                    row3.createCell(7).setCellValue("已签到");//签到状态
                }
                List<Signuserinfo> s7 = signuserinfoMapper.selectSignType(mytickets.get(i).getId(), Signuserinfo.ONE_CHOICE);//单选项
                List<Signuserinfo> s5 = signuserinfoMapper.selectSignType(mytickets.get(i).getId(), Signuserinfo.ONE_TEXT);
                List<Signuserinfo> s6 = signuserinfoMapper.selectSignType(mytickets.get(i).getId(), Signuserinfo.MANY_TEXT);
                List<String> s8 = signuserinfoMapper.selectChoiceFieldid(mytickets.get(i).getId(), Signuserinfo.MANY_CHOICE);
                int d = 0;
                if (s5 != null) {
                    for (Signuserinfo signuserinfo : s5) {
                        row2.createCell(8 + d).setCellValue(signuserinfo.getFieldid());
                        row3.createCell(8 + d).setCellValue(signuserinfo.getFieldval());
                        d++;
                    }
                }
                int a = d;
                if (s6 != null) {
                    for (Signuserinfo signuserinfo : s6) {
                        row2.createCell(8 + a).setCellValue(signuserinfo.getFieldid());
                        row3.createCell(8 + a).setCellValue(signuserinfo.getFieldval());
                        a++;
                    }
                }
                int b = a;
                if (s7 != null) {
                    for (Signuserinfo signuserinfo : s7) {
                        row2.createCell(8 + b).setCellValue(signuserinfo.getFieldid());
                        row3.createCell(8 + b).setCellValue(signuserinfo.getFieldval());
                        b++;
                    }
                }
                int c = b;
                if (s8 != null) {
                    for (String fieldid : s8) {
                        row2.createCell(8 + c).setCellValue(fieldid);
                        List<String> fieldval = signuserinfoMapper.selectChoiceFieldval(mytickets.get(i).getId(), fieldid, Signuserinfo.MANY_CHOICE);
                        StringBuilder sb = new StringBuilder(60);
                        for (String s : fieldval) {
                            sb.append(s + " , ");
                        }
                        sb.replace(sb.length() - 2, sb.length(), "");
                        row3.createCell(8 + c).setCellValue(String.valueOf(sb));
                        c++;
                    }
                }
            }
            FileOutputStream output = null;
            try {
                String uuid = UUID.randomUUID().toString();
                output = new FileOutputStream(excel + "/" + uuid + ".xls");
                result.put("url", excelurl + uuid + ".xls");
                wb.write(output);
                output.flush();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ReturnMap("导出成功!", result);
    }
}