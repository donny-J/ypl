package com.jxzg.share.v1.controller.alipay;

import com.jxzg.share.pay.alipay.AlipayConfig;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:application.xml")
public class AlipayTransferControllerTest {

    @Autowired
    AlipayTransferController alipayTransferController;

    @Autowired
    AlipayController alipayController;

    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }


    /**
     * Method: alipayApi(HttpServletRequest request, HttpServletResponse response, AlipayConfig alipayConfig)
     */
    @Test
    public void testAlipayApi() throws Exception {
        alipayController.getPayNotify(null);
    }


} 
