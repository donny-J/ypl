package com.jxzg.share.v1.controller.youpeng;

import com.jxzg.share.dto.BaseResult;
import com.jxzg.share.toolUtil.JsonUtil;
import com.jxzg.share.v1.controller.alipay.AlipayTransferController;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:application.xml")
public class DownloadControllerTest {


    @Autowired
    DownloadController downloadController;

    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }

    /**
     * Method: checkAppInfo(HttpServletRequest request)
     */
    @Test
    public void testCheckAppInfo() throws Exception {
        BaseResult baseResult = downloadController.checkAppInfo(null);
        System.out.println(JsonUtil.convertObjectToJson(baseResult));
    }
}
