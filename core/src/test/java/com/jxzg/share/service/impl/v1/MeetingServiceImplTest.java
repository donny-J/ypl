package com.jxzg.share.service.impl.v1;

import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.service.v1.IMeetingService;
import com.jxzg.share.toolUtil.DateUtil;
import com.jxzg.share.toolUtil.JsonUtil;
import com.jxzg.share.vo.ActivityMeeting.CreatMeeting;
import com.jxzg.share.vo.ActivityMeeting.MeetingBody;
import com.jxzg.share.vo.AddressVO;
import com.jxzg.share.vo.CreatticketVO;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:application.xml")
public class MeetingServiceImplTest {
    @Autowired
    IMeetingService meetingService;

    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }

    /**
     * Method: addmetting(MeetingBody body)
     */
    @Test
    public void testAddmetting() throws Exception {
//        System.out.println(new SimpleDateFormat("yyyy-MM-dd").parse("2017-06-23").getTime());
        MeetingBody meetingbody = new MeetingBody();
        CreatMeeting creatMeeting = new CreatMeeting();
        AddressVO addressVO = new AddressVO();

        meetingbody.setToken("cda1b54f-46b9-4eef-83c6-27ebcbefbd42");
        meetingbody.setBody(creatMeeting);
        creatMeeting.setAddress(addressVO);
        creatMeeting.setPic("http://img.jxcollege.com/library/fashion/fashion_move6.jpg");
        creatMeeting.setMeetingname("test durpliate primary key");
        creatMeeting.setMeetingstarttime(1498060800);
        creatMeeting.setMeetingendtime(1498147200);
        creatMeeting.setSid(Arrays.asList(10,10));
        creatMeeting.setIntroduction("");
        creatMeeting.setIsopen(1);

        List<CreatticketVO> creatticketVOS = new ArrayList<>();
        CreatticketVO creatticketVO = new CreatticketVO();
        creatticketVO.setName("activity test duplicate primary key");
        creatticketVO.setPrice(new BigDecimal(0.01));
        creatticketVO.setAmount(100);
        creatticketVO.setBegintime(1498060800);
        creatticketVO.setEndtime(1498147200);
        creatticketVO.setMincount(1);
        creatticketVO.setMaxcount(1);
        creatticketVO.setDescription("没啥说明");
        creatMeeting.setMeetingticket(creatticketVOS);
        creatticketVOS.add(creatticketVO);

        addressVO.setProvice("江苏省");
        addressVO.setCity("南京市");
        addressVO.setDist("秦淮区");
        addressVO.setSite("园东路1000号");
        addressVO.setLat(new BigDecimal(0.00));
        addressVO.setLng(new BigDecimal(0.00));
        ReturnMap result = meetingService.addmetting(meetingbody);
        System.out.println(JsonUtil.convertObjectToJson(result));
    }
}
