package com.jxzg.share.service.impl.v1;

import com.jxzg.share.pay.alipay.AlipayNotify;
import com.jxzg.share.pay.refund.Alirefund;
import com.jxzg.share.querypage.IncomeDetailQueryObject;
import com.jxzg.share.returndomain.ReturnList;
import com.jxzg.share.service.v1.IOrdersService;
import com.jxzg.share.toolUtil.JsonUtil;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * OrdersServiceImpl Tester.
 *
 * @author <Authors name>
 * @version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:application.xml")
public class OrdersServiceImplTest {

    @Autowired
    private IOrdersService ordersService;

    @Autowired
    private AlipayNotify alipayNotify;

    @Autowired
    private Alirefund alirefund;


    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }

    //36268e3a-4124-4342-9ffe-910dec4d4bd6(10000)
    //36268e3a-4124-4342-9ffe-910dec4d8db9(10001)

    /**
     * 测试用户全部支出(count=3,买了两张票+一张退款)
     *
     * @return
     */
    private IncomeDetailQueryObject testOutputTypeNull() {
        IncomeDetailQueryObject queryObject = new IncomeDetailQueryObject();
        queryObject.setToken("36268e3a-4124-4342-9ffe-910dec4d4bd6");
        queryObject.setStatus(1);
        queryObject.setCurrentpage(1);
        return queryObject;
    }

    /**
     * 测试用户支出(拼团门票)(count=2,报名订单此处包含退款是因为退款的这条记录曾经是支付成功的记录)
     *
     * @return
     */
    private IncomeDetailQueryObject testOutputType1() {
        IncomeDetailQueryObject queryObject = new IncomeDetailQueryObject();
        queryObject.setToken("36268e3a-4124-4342-9ffe-910dec4d4bd6");
        queryObject.setStatus(1);
        queryObject.setCurrentpage(1);
        queryObject.setType(1);
        return queryObject;
    }

    /**
     * 测试用户支出(退款)
     *
     * @return
     */
    private IncomeDetailQueryObject testOutputType5() {
        IncomeDetailQueryObject queryObject = new IncomeDetailQueryObject();
        queryObject.setToken("36268e3a-4124-4342-9ffe-910dec4d4bd6");
        queryObject.setStatus(1);
        queryObject.setCurrentpage(1);
        queryObject.setType(5);
        return queryObject;
    }

    /**
     * 测试用户支出(打赏)
     *
     * @return
     */
    private IncomeDetailQueryObject testOutputType2() {
        IncomeDetailQueryObject queryObject = new IncomeDetailQueryObject();
        queryObject.setToken("36268e3a-4124-4342-9ffe-910dec4d8db9");
        queryObject.setStatus(1);
        queryObject.setCurrentpage(1);
        queryObject.setType(2);
        queryObject.setPagesize(8);
        return queryObject;
    }

    /**
     * 测试用户支出(短信)
     *
     * @return
     */
    private IncomeDetailQueryObject testOutputType6() {
        IncomeDetailQueryObject queryObject = new IncomeDetailQueryObject();
        queryObject.setToken("36268e3a-4124-4342-9ffe-910dec4d4bd6");
        queryObject.setStatus(1);
        queryObject.setCurrentpage(1);
        queryObject.setType(6);
        return queryObject;
    }


    /**
     * 测试收入(type=null,全部收入)
     * 记录较多，可以测试分页
     * @return
     */
    private IncomeDetailQueryObject testIncomeTypeNull() {
        IncomeDetailQueryObject queryObject = new IncomeDetailQueryObject();
        queryObject.setToken("36268e3a-4124-4342-9ffe-910dec4d4bd6");
        queryObject.setStatus(2);
        queryObject.setCurrentpage(1);
        return queryObject;
    }

    /**
     * 测试报名订单的收入
     * @return
     */
    private IncomeDetailQueryObject testIncomeType1() {
        IncomeDetailQueryObject queryObject = new IncomeDetailQueryObject();
        queryObject.setToken("36268e3a-4124-4342-9ffe-910dec4d4bd6");
        queryObject.setStatus(2);
        queryObject.setType(1);
        queryObject.setCurrentpage(1);
        return queryObject;
    }
    /**
     * 测试打赏收入
     * @return
     */
    private IncomeDetailQueryObject testIncomeType2() {
        IncomeDetailQueryObject queryObject = new IncomeDetailQueryObject();
        queryObject.setToken("36268e3a-4124-4342-9ffe-910dec4d4bd6");
        queryObject.setStatus(2);
        queryObject.setCurrentpage(1);
        queryObject.setPagesize(10);
        queryObject.setType(2);
        return queryObject;
    }

    @Test
    public void testTotalIncomeAndOutput() throws Exception {
        //构造数据(执行初始化脚本)
        IncomeDetailQueryObject queryObject = testOutputType6();
        ReturnList returnList = ordersService.totalIncomeAndOutput(queryObject);
        System.out.println(JsonUtil.convertObjectToJson(returnList));
    }

    @Test
    public void testImg() throws Exception {
        saveImage();
    }

    public static InputStream getInputStream() {
        InputStream inputStream = null;
        HttpURLConnection httpURLConnection = null;
        try {
            URL url = new URL("https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=3944680232,2054173354&fm=26&gp=0.jpg");
            if (url != null) {
                httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setConnectTimeout(3000);
                httpURLConnection.setRequestMethod("GET");
                int responseCode = httpURLConnection.getResponseCode();
                if (responseCode == 200) {
                    inputStream = httpURLConnection.getInputStream();

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inputStream;
    }

    public static void saveImage() {
        InputStream inputStream = getInputStream();
        FileOutputStream fileOutputStream = null;
        byte[] data = new byte[1024];
        int len = 0;
        try {
            fileOutputStream = new FileOutputStream("D:\\2.jpg");
            while ((len = inputStream.read(data)) != -1) {
                fileOutputStream.write(data, 0, len);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                inputStream.close();
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
