package com.jxzg.share.querypage;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by 00914Donny on 2017/6/14.
 */
@Getter
@Setter
public class MeetingModQueryObject {
    private Integer uid;//用户id
    private Integer status;//会议状态

    //extends property
    private List<Integer> statuslist;//会议状态集合
}
