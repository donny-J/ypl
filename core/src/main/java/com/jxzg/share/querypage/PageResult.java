package com.jxzg.share.querypage;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Setter
@Getter@ToString
public class PageResult {
    private List listData;//结果集
    private Integer currentpage;//当前页
    private Integer pagesize;//页面大小
    private Integer totalcount;//总条数
    private Integer totalpage;//总页数
    private Integer prevpage;//上一页
    private Integer nextpage;//下一页

    public Integer getTotalPage() {
        return totalpage == 0 ? 1 : totalpage;
    }

    public PageResult(List listData, Integer pagesize, Integer currentpage, Integer totalcount) {
        this.listData = listData;
        this.pagesize = pagesize;
        this.currentpage = currentpage;
        this.totalcount = totalcount;
        this.totalpage = totalcount % pagesize == 0 ? totalcount / pagesize : totalcount / pagesize + 1;
        this.prevpage = currentpage - 1 > 1 ? currentpage - 1 : 1;
        this.nextpage = currentpage + 1 < totalpage ? currentpage + 1 : totalpage;
    }
}
