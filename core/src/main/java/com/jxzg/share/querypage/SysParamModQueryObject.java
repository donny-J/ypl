package com.jxzg.share.querypage;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by 00914Donny on 2017/6/14.
 */
@Getter
@Setter
public class SysParamModQueryObject {
    private Integer id;
    private String paramName;
    private Integer currentPage;
    private Integer pageSize;
}
