package com.jxzg.share.querypage;

import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by 00818Wolf on 2017/3/23.
 */
@Getter
@Setter
public class MeetingTicketQueryObject extends Token {
    private Integer uid;
    private Integer currentpage = 1;
    private Integer pagesize = 100;
    private Integer start;

    public Integer getStart() {
        return (currentpage - 1) * pagesize;
    }

    private List<Integer> meetingtype;
    private List<Integer> meetingstatus;
    private List<Integer> mids;
}
