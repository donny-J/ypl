package com.jxzg.share.querypage;

import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by 00818Wolf on 2017/2/16.
 */
@Getter@Setter
public class CommetQueryObject extends Token{
    private Integer currentpage = 1;
    private Integer pagesize = 100;
    private Integer start;
    public Integer getStart() {
        return (currentpage-1)*pagesize;
    }
    private int mid;
}
