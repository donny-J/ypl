package com.jxzg.share.querypage;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

/**
 * Created by 00914Donny on 2017/6/13.
 */
@Getter
@Setter
public class OrderModQueryObject {
    private Integer uid;
    private Integer currentPage;
    private Integer pageSize;
    private Integer orderType;//orders->ordertype
    private Integer productType;//orderdtl->producttype
    private Integer ordtlStatus;//orderdtl->status
    private Integer orderStatus;//orders->status 0 未支付 1 支付完成 -1 退款成功 -2 订单失败
    private Date startTime;
    private Date endTime;
    //extends condition
    public Integer getStart() {
        return (currentPage - 1) * pageSize;
    }
    private List<Integer> mids;//会议号码集合
    private List<String> ordernos;//订单号码集合
    private List<Integer> orderTypes;//订单类型
    private List<Integer> orderStatuslist;//订单状态

}
