package com.jxzg.share.querypage;

import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

/**
 * 订单明细
 */
@Getter
@Setter
public class IncomeDetailQueryObject extends Token {
    private Integer uid;
    private Integer currentpage = 1;
    private Integer pagesize = 10;
    private Integer start;

    public Integer getStart() {
        return (currentpage - 1) * pagesize;
    }

    private Integer type;
    private Integer mid;
    private Integer status;
    private Date begintimedate;
    private Date endtimedate;
    private Long begintime;
    private Long endtime;
    private List<Integer> mids;
}
