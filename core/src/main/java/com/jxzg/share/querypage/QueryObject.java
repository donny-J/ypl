package com.jxzg.share.querypage;

import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter@Setter
public class QueryObject extends Token {
    private String content;
    private Integer uid ;
    private Integer currentpage = 1;
    private Integer pagesize = 10;
    private Integer start;
    public Integer getStart() {
        return (currentpage-1)*pagesize;
    }

    private List<Integer> sids;
    private List<Integer> mids;
}
