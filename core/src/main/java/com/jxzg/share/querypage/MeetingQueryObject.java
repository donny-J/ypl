package com.jxzg.share.querypage;

import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by 00818Wolf on 2017/3/19.
 */
@Getter
@Setter
public class MeetingQueryObject extends Token {
    private Integer uid;
    private Integer currentpage = 1;
    private Integer pagesize = 100;
    private Integer start;

    public Integer getStart() {
        return (currentpage - 1) * pagesize;
    }

    private List<Integer> meetingtype;
    private List<Integer> status;
    private List<Integer> isopen;
    private String content;
    private int type;//图片类型
    private int picstatus;
    private String city;
    private BigDecimal lat;
    private BigDecimal lng;
}
