package com.jxzg.share.vo.ActivityMeeting;

import com.jxzg.share.vo.AddressVO;
import com.jxzg.share.vo.CreatticketVO;
import com.jxzg.share.vo.FillField;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * 创建会议
 */
@Getter
@Setter
public class CreatMeeting {
    private int scale;
    private int isopen;//是否公开
    private int comment;//评论开关
    private String pic;
    private String meetingname;
    private String introduction;
    private long meetingstarttime;
    private long meetingendtime;
    private AddressVO address;
    private List<Integer> sid = new ArrayList<>();
    private List<String> tag = new ArrayList();
    private List<FillField> fillFields = new ArrayList<>();
    private List<CreatticketVO> meetingticket = new ArrayList<>();
    private CreatticketVO spellgroupticket;
    private int invoiceflag;
}
