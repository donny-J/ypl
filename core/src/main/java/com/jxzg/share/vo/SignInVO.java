package com.jxzg.share.vo;

import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by 00818Wolf on 2017/5/19.
 */
@Getter
@Setter
public class SignInVO extends Token{
    public static final int TYPE_CODE_ZERO = 0;
    public static final int TYPE_CODE_ONE = 1;
    private int mid;
    private String code;
    private int type;//0:扫码  1:签到码签到
}
