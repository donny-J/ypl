package com.jxzg.share.vo;

import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;

/**
 * 付款成功
 */
@Getter@Setter
public class ApplyTrue extends Token{
    public static final int STATUS_FALSE=0;
    public static final int STATUS_TRUE=1;
    private String orderno;//订单号
    private int status;//状态,0:付款成功,1付款失败
    private int mid;
}
