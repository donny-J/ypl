package com.jxzg.share.vo;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by 00818Wolf on 2017/3/3.
 */
@Getter@Setter
public class OrderVO {
    public static final int TICKETNUM = 1;
    private BigDecimal ticketprice;
    private String ticketname;
    private List<FileOrderVO> fieldmsg;
}
