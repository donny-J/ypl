package com.jxzg.share.vo;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * Created by 00818Wolf on 2017/3/22.
 */
@Getter@Setter
public class AddressVO {
    private String provice;//省份
    private String city;//城市
    private String dist;//县区
    private String site;//详细地址
    private BigDecimal lat;
    private BigDecimal lng;
}
