package com.jxzg.share.vo.Spellgroup;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * Created by 00818Wolf on 2017/3/23.
 */
@Getter@Setter
public class ClusterTicket {
    private String name;
    private BigDecimal price;
    private int amount;
    private long tkendtime;
}
