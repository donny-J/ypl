package com.jxzg.share.vo;

import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by 00818Wolf on 2017/2/9.
 */
@Getter@Setter
public class IdAndToken extends Token{
    private int mid;
}
