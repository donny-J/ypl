package com.jxzg.share.vo;

import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 保证验证码,手机号和发送时间
 * @author lenovo
 *
 */
@Setter@Getter
public class VerifyCodeVO extends Token{
	private String verifyCode;//验证码
	private String loginname;//手机号
	private Date sendDate;//发送时间
	private String loginpassword;//登录密码
	private BigDecimal lat;//位置维度
	private BigDecimal lng;//位置精度
	public VerifyCodeVO() {
	}
	public VerifyCodeVO(String verifyCode,String loginname,Date sendDate) {
		this.verifyCode=verifyCode;
		this.loginname=loginname;
		this.sendDate=sendDate;
	}
}
