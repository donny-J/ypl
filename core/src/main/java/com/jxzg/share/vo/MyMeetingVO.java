package com.jxzg.share.vo;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 我的活动
 */
@Getter@Setter
public class MyMeetingVO {
    private String sponsor;//
    private String meetingpic;//
    private String meetingname;//
    private Date starttime;//
    private String city;
    private int number;
    private BigDecimal sumprice;
    private int status;//
    private String sponlogo;//
}
