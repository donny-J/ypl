package com.jxzg.share.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * 单行文本,多行文本,单选,多选
 */
@Getter@Setter
public class FillField {
    private int type;//0:姓名 1:电话 2.单文本 3.多文本 4.单选项 5.多选项
    private String content;
    private String[] options;
    private int  isrequire;//0.选填 1.必填
}
