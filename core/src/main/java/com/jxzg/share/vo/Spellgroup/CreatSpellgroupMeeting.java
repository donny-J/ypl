package com.jxzg.share.vo.Spellgroup;

import com.jxzg.share.vo.AddressVO;
import com.jxzg.share.vo.FillField;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * 创建会议
 */
@Getter@Setter
public class CreatSpellgroupMeeting {
    private int scale;//拼团才会有,拼团类型
    private int isopen;//是否公开
    private int comment;//评论开关
    private String pic;
    private String meetingname;
    private String introduction;
    private long meetingstarttime;
    private long meetingendtime;
    private AddressVO address;
    private List<Integer> sid = new ArrayList<>();
    private String[] tag;
    private List<FillField> fillFields = new ArrayList<>();
    private ClusterTicket ticket;
    private int  invoiceflag;
}
