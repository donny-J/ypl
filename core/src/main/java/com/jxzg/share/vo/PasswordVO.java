package com.jxzg.share.vo;

import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;

/**
 * 修改密码
 */
@Getter@Setter
public class PasswordVO extends Token{
    private String oldloginpassword;
    private String newloginpassword;
}
