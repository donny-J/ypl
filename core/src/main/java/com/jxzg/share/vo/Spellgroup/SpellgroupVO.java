package com.jxzg.share.vo.Spellgroup;

import com.jxzg.share.domain.meeting.Invoices;
import com.jxzg.share.domain.other.Token;
import com.jxzg.share.vo.FileOrderVO;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by 00818Wolf on 2017/3/26.
 */
@Getter
@Setter
public class SpellgroupVO extends Token {
    private Integer tid;
    private Integer amount;
    private BigDecimal ticketprice;
    private Integer mid;
    private String ticketname;
    private Invoices invoices;
    private List<FileOrderVO> fieldmsg;
}
