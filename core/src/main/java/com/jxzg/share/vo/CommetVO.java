package com.jxzg.share.vo;

import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;

/**
 * 点赞论
 */
@Getter@Setter
public class CommetVO extends Token{
    private String content;//评论内容
}
