package com.jxzg.share.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * Created by 00818Wolf on 2017/3/20.
 */
@Getter@Setter@ToString
public class WeixinLoginVO {
    public static final int LOGINTYPE_WEIXIN = 0;
    public static final int LOGINTYPE_PC = 1;//PC
    public static final int LOGINTYPE_MOVE = 2;//移动
    public static final int LOGINTYPE_WEIXIN_MP = 3;//微信公众号


    private String nickname;//昵称
    private String city;//城市
    private String country;//
    private String headimgurl;//头像
    private String province;//身份
    private int sex;//性别
    private String unionid;//微信特有
    private String openid;//用户唯一标识
    private String accessToken;//当前token
    private String refreshToken;//刷新的token
    private long expiration;//token刷新时间
    private String scope;
    private BigDecimal lat;
    private BigDecimal lng;
    private int type;
}
