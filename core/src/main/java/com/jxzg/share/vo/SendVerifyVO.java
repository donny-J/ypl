package com.jxzg.share.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * 发送验证码
 */
@Getter@Setter
public class SendVerifyVO {
    private String loginname;
    private int type;
}
