package com.jxzg.share.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by 00818Wolf on 2017/3/3.
 */
@Getter
@Setter
public class FileOrderVO {
    private String fieldname;
    private String fieldval;
    private List<String> fieldvals;
    private int type;
}
