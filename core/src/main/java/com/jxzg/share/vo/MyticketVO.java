package com.jxzg.share.vo;

import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;

/**
 * 我的门票
 */
@Getter
@Setter
public class MyticketVO extends Token{
    private int tid;
    private int ticketnum;
    private int mid;
}
