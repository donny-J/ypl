package com.jxzg.share.vo;

import com.jxzg.share.domain.meeting.Invoices;
import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 00818Wolf on 2017/3/3.
 */
@Getter
@Setter
public class CreateOrderVO extends Token {
    private int ordertype;
    private int amount;//总数
    private BigDecimal money;//订单总额
    private int mid;
    private Invoices invoices;
    private List<OrderVO> tickets = new ArrayList<>();
}
