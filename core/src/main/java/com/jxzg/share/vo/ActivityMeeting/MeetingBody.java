package com.jxzg.share.vo.ActivityMeeting;

import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by 00818wolf on 2017/1/16.
 */
@Getter@Setter
public class MeetingBody extends Token{
    private int mid;
    private int meetingtype;
    private CreatMeeting body;
}
