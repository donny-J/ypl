package com.jxzg.share.vo;

import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;

/**
 * 赠送门票
 */
@Getter@Setter
public class SendTicketVO extends Token{
    private int tid;
    private String loginname;
}
