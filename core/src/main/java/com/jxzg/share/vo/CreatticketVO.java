package com.jxzg.share.vo;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * Created by 00818Wolf on 2017/3/22.
 */
@Getter
@Setter
public class  CreatticketVO {
    private String name;
    private BigDecimal price;
    private int amount;
    private long begintime;
    private long endtime;
    private int mincount;
    private int maxcount;
    private String description;
}
