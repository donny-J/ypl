package com.jxzg.share.vo;

import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 00818Wolf on 2017/5/22.
 */
@Getter@Setter
public class SendNoteVO extends Token {
    private String code;
    private List<String> phoneNumber = new ArrayList<>();
    private Integer mid;
    private Integer type;
    private Long sendtime;
}
