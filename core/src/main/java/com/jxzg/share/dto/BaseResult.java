package com.jxzg.share.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * Created by 00914Donny on 2017/6/23.
 * Common returned attributes
 */
@Getter
@Setter
@ToString
public class BaseResult{
    private String msg;
    private int errorCode;
}
