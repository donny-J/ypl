package com.jxzg.share.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by 00914Donny on 2017/6/23.
 * The data transfer object returned by the download controller
 */
@Getter
@Setter
@ToString
public class DownloadResult extends BaseResult {
    private String versionCode;
    private String versionDesc;
    private String downloadLink;
    private Boolean isForceDownload;
}
