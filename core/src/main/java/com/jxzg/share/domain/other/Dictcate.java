package com.jxzg.share.domain.other;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 字典类别
 */
@Getter
@Setter
public class Dictcate extends Token{
    private int id;//类别id
    private String description;//描述
    private int status;//状态
    private Date createtime;//创建时间
    private Date updatetime;//更新时间
    private String creater;//创造者
    private String operator;//操作者
}
