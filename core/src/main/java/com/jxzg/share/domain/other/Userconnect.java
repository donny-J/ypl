package com.jxzg.share.domain.other;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 第三方登录授权
 */
@Getter
@Setter
public class Userconnect {
    public static final int LOGIN_WRONG = 0;//登录失败
    public static final int LOGIN_SUCCESS = 1;//登录成功

    public static final int LOGINTYPE_WEIXIN = 0;
    public static final int LOGINTYPE_PC = 1;//PC
    public static final int LOGINTYPE_MOVE = 2;//移动
    public static final int LOGINTYPE_WEIXIN_MP = 3;//微信公众号

    private int id;
    private int uid;//用户id
    private int type;//
    private String accesstoken;//登录token
    private String refreshtoken;//刷新token
    private String thirdid;//第三方id,openid
    private String unionid;//微信特有
    private Date createtime;
    private Date updatetime;
    private int status;
    private BigDecimal lat;
    private BigDecimal lng;
}
