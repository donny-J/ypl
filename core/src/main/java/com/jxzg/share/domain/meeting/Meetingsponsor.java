package com.jxzg.share.domain.meeting;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by 00818Wolf on 2017/3/6.
 */
@Getter@Setter
public class Meetingsponsor {
    private int mid;//会议id
    private int oid;//主办方id
    private int uid;//创办者id
    private int status;
    private Date updatetime;
    private Date createtime;
}
