package com.jxzg.share.domain.meeting;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 我的门票
 */
@Getter
@Setter@ToString
public class Myticket {
    public static final int MYTICKET_NO_NO = 0;//未签到
    public static final int MYTICKET_NO_YES = 1;//已签到

    public static final int ORDER_STATUS_NO_PAY = 0;//未支付
    public static final int ORDER_STATUS_YES_PAY = 1;//支付完成
    public static final int ORDER_STATUS_FAIL = -1;//1支付失败
    public static final int ORDER_STATUS_OUTTIME = 3;//退款成功

    public static final int TICKETTYPE_MYTICKET = 0;//自己的票
    public static final int TICKETTYPE_SENDTICKET = 1;//赠送的票

    private int id;
    private int uid;//用户id
    private int mid;//会议id
    private int no;//是否签到
    private String signcode;//签到码
    private String qcode;//票据二维码
    private int tickettype;//票务类型
    private int status;//状态
    private String createip;//门票名称
    private int ticketid;
    private String orderno;//订单编号
    private Date createtime;
    private Date updatetime;
    private BigDecimal lat;
    private BigDecimal lng;
}
