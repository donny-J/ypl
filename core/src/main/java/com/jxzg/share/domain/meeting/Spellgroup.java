package com.jxzg.share.domain.meeting;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * 拼团报名信息
 */
@Getter
@Setter
@ToString
public class Spellgroup {
    public static final int TYPE_HEADER = 0;//团长
    public static final int TYPE_MEMBER = 1;//团员

    public static final int STATUS_STAY = 0;//待付款
    public static final int STATUS_SUCCESS_A = 1;//待成团
    public static final int STATUS_SUCCESS = 2;//拼团成功
    public static final int STATUS_OUTTIME = 3;//拼团失败(退款成功,或者是免费)
    public static final int STATUS_FAIL = -1;//拼团失败待退款
    public static final int STATUS_TIME = 4;//付款超时
    /**
     * 待付款-->付款成功检查团是否满员-->如果没满为付款成功待成团,如果满员为拼团成功
     * 如果当前时间>拼团结束时间并且拼团状态处于付款成功待成团(满足退款要求状态改变为拼团超时代退款)
     * 拼团会议结束时,检查该会议付款成功待成团(满足退款要求状态改变为拼团超时代退款)
     * 拼团状态  1:待付款  2:付款成功待成团  3:拼团成功  4:拼团超时代退款
     */
    private int id;
    private int mid;
    private int uid;//用户id
    private int tid;//团长id,如果自己是团长就是0
    private int type;//0:团长,1:团员
    private int status;
    private Date createtime;
    private Date updatetime;//拼团结束时间
    private int ticketid;  //门票id
}
