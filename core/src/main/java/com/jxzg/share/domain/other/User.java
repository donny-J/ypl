package com.jxzg.share.domain.other;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 用户基础信息(注册后初始化信息)
 */
@Getter
@Setter
public class User extends Token {
    public final static int STATUS_NOONE = 0;//不是第一次登录
    public final static int STATUS_ONE = 1;//是第一次登录

    public final static int EMAIL_NOVERIFY = 0;//未验证
    public final static int EMAIL_VERIFY = 1;//已验证

    public static final BigDecimal BALANCE_ZERO = new BigDecimal(0.00);//初始值
    public static final int CREDITS_ZERO = 0;//初始积分
    public static final int LV_ZERO = 0;//用户等级,默认为0
    public static final long SEND_VERIFYCODE_INTERVAL = 90;//短信发送时间间隔

    private int uid;//用户ID
    private String loginname;//登录名
    private String loginpassword;//登录密码
    private int lv;//用户等级(预留)
    private int status = User.STATUS_ONE;//状态
    private String mobile;//手机号码
    private String email;//邮箱
    private int emailstatus;//邮箱状态
    private int credits;//积分
    private BigDecimal balance = User.BALANCE_ZERO;//余额
    private int remainmsg;//短信条数
    private String regip;//注册IP
    private Date regtime;//注册时间
    private String avatar;//头像
    private String paypassword;//支付密码
    private int recommandid;//推荐人id
    private BigDecimal lat;//位置维度
    private BigDecimal lng;//位置精度

    public String getAvatar() {
        return avatar == null ? "" : avatar;
    }
}