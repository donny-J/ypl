package com.jxzg.share.domain.meeting;

import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 增票记录
 */
@Getter@Setter
public class SendTicketRecord extends Token {
    private int id;
    private int uid;//赠票人id
    private int mid;
    private int sid;//被增票人id
    private int tid;//门票id
    private int status;
    private Date createtime;
    private Date updatetime;
}
