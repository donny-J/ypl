package com.jxzg.share.domain.other;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by 00818wolf on 2017/1/16.
 */
@Getter@Setter
public class Token {
    protected String token;
}
