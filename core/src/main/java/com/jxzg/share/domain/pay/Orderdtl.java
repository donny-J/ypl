package com.jxzg.share.domain.pay;

import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单
 */
@Getter
@Setter
public class Orderdtl extends Token {
    public static final int PRODUCTTYPE_TICKET = 1;//门票
    public static final int PRODUCTTYPE_AWARD = 2;//打赏
    public static final int PRODUCTTYPE_RECHARGE = 3;//充值
    public static final int PRODUCTTYPE_REFLECT = 4;//提现
    public static final int PRODUCTTYPE_REFUND = 5;//退款
    public static final int PRODUCTTYPE_NOTE = 6;//短信

    public static final int ORDER_STATUS_NO_PAY = 0;//未支付
    public static final int ORDER_STATUS_YES_PAY = 1;//支付成功
    public static final int ORDER_STATUS_FAIL = -1;//1退款成功
    public static final int ORDER_STATUS_OVERTIME = -2;//支付失败

    private Long orderdtlid;//订单id
    private String orderno;//订单号(只有下单后才会有订单号)
    private int uid;//用户id
    private int mid;//会议id
    private int producttype;//商品类型
    private int status;//状态默认为0,待支付，1已下单，-1支付失败，-2超时关闭
    private int productid;//商品id
    private int amount;//数量
    private BigDecimal money;//总额
    private BigDecimal price;//单价
    private Date createtime;
    private Date updatetime;
}
