package com.jxzg.share.domain.other;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;


/**
 * 会议评论点赞
 */
@Getter
@Setter
@ToString
public class Commet extends Token {
    public static final int COMMET_TOURIST = -1;//游客

    public static final int STATUS_LIKE_NO = 0;//未点赞
    public static final int STATUS_LIKE_YES = 1;//已点赞

    public static final int TYPE_COMMET = 0;//评论
    public static final int TYPE_REPLY = 1;//回复
    public static final int TYPE_LIKE = 2;//点赞

    public static final int LIKECOUNT = 0;//初始点赞数0
    private Long id;
    private int uid;//用户id,如果是游客id=-1
    private int mid;//会议id
    private int type;//类型
    private int likecount;//点赞数
    private String content;//内容
    private Date createtime;//时间
    private int status;//状态
    private Date updatetime;//更新时间
    private String signip;//签到ip
    private BigDecimal lat;//位置维度
    private BigDecimal lng;//位置精度
    private Long pid;//父id
}
