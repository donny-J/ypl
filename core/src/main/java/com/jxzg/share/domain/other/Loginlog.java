package com.jxzg.share.domain.other;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 登录日志表
 */
@Getter@Setter
public class Loginlog extends Token{
    public static final int LOGIN_WRONG=0;//登录失败
    public static final int LOGIN_SUCCESS=1;//登录成功

    public static final int LOGINTYPR_PC=0;//PCweb
    public static final int LOGINTYPR_PC_MOVEN=1;//PC移动
    public static final int LOGINTYPR_IOS=2;//iOS
    public static final int LOGINTYPR_ANDROID=3;//Android
    public static final int LOGINTYPR_WEIXIN=4;//微信

    private int id;//用户ID
    private String loginname;//登录名
    private String loginpassword;//登录密码
    private Date logintime;//登录时间
    private int logintype;//登录类型
    private String loginip;//登录ip
    private int status;//状态
    private BigDecimal lat;
    private BigDecimal lng;
    private String extinfo;//扩展信息可以存放用户系统信息，平台，浏览器等信息
}
