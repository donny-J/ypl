package com.jxzg.share.domain.meeting;

/**
 * 主办方信息.
 */

import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Sponsor extends Token {
    public static final int STATUS_NORMAL = 0;//正常状态
    public static final int STATUS_DEL = 1;//删除状态

    private int uid;//用户id
    private int oid;//主办方id
    private String name;//主办方名称
    private String logo;//主办方logo
    private String tel;//电话
    private int qq;//QQ
    private String email;//邮箱
    private String website;//网站
    private String introduction;//主办方简介
    private int status;
}
