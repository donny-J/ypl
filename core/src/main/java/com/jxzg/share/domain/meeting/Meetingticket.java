package com.jxzg.share.domain.meeting;

import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 会议门票
 */
@Getter
@Setter
public class Meetingticket extends Token {
    public static final int STATUS_STOCK=1;//售完
    public static final int STATUS_HAND=2;//售票中
    public static final BigDecimal PRICE = new BigDecimal("0.00");
    public static final int AMOUNT_INFINITY = -1;//无穷大的意思

    public static final int MINCOUNT=1;//最少购票
    private int mid;//会议id
    private int ticdketid;//门票id
    private String name;//门票名称
    private BigDecimal price=PRICE;//门票价格
    private int amount;//门票数量
    private int remaincount;//剩余数量
    private Date begintime;//开始时间
    private Date endtime;//结束时间
    private int mincount=Meetingticket.MINCOUNT;//最少买?
    private int maxcount;//最多买?
    private String description;//描述
    private int status;//状态
    private Date createtime;//时间
    private Date updatetime;//更新时间
}
