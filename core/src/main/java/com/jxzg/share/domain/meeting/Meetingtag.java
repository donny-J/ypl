package com.jxzg.share.domain.meeting;

import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 会议标签
 */
@Getter@Setter
public class Meetingtag extends Token {
    private int tagid;//标签id
    private int mid;//所属会议id
    private String tag;//标签
    private int status;//状态
    private Date createtime;//创建时间
}
