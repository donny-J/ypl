package com.jxzg.share.domain.other;

import java.util.Date;

public class SysParam {
    private Integer id;

    private String paramName;

    private String paramValue;

    private String paramDiscription;

    private Integer paramClass;

    private Date createTime;

    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName == null ? null : paramName.trim();
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue == null ? null : paramValue.trim();
    }

    public String getParamDiscription() {
        return paramDiscription;
    }

    public void setParamDiscription(String paramDiscription) {
        this.paramDiscription = paramDiscription == null ? null : paramDiscription.trim();
    }

    public Integer getParamClass() {
        return paramClass;
    }

    public void setParamClass(Integer paramClass) {
        this.paramClass = paramClass;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}