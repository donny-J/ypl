package com.jxzg.share.domain.other;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


/**
 * 用户资料数据表
 */
@Getter
@Setter
@ToString
public class Userprofile extends Token {
    public static final int SEX_MAN = 1;//男
    public static final int SEX_WOMAN = 0;//女

    public static final int IDCARDTYPE_CARD = 0;//身份证
    public static final int IDCARDTYPE_PROT = 1;//护照

    private int uid;//用户id
    private String realname;//真实姓名
    private String nickname;//昵称
    private int gender = Userprofile.SEX_MAN;//性别
    private String position;//职位
    private int idcardtype;//证件类型
    private String idcard;//证件号码
    private String organizationname;//机构名称
    private int organizationtype;//机构类型
    private int organizationsize;//机构规模
    private String provice;//居住省份
    private String city;//居住城市
    private String dist;//居住行政区/县
    private String address;//地址
    private int worked;//工作年限
    private String personalintroduction;//个人信息简介

    public String getRealname() {
        return realname == null ? "" : realname;
    }
}
