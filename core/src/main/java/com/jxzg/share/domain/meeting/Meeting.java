package com.jxzg.share.domain.meeting;

import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;


/**
 * 会议相关
 */
@Getter
@Setter
@ToString
public class Meeting extends Token {
    public static final int MEETING_STATUS_ACTICITY_NO = 0;//报名未开始
    public static final int MEETING_STATUS_ACTIVITY_APPLY = 1;//报名中
    public static final int MEETING_STATUS_ACTIVITY_YES = 2;//活动中
    public static final int MEETING_STATUS_ACTIVITY_END = 3;//已结束

    public static final int MEETING_STATUS_ACTIVITY_STOP = 4;//报名结束(售票时间到,票售完,停止报名)

    public static final int MEETING_STATUS_ACTIVITY_DEL = -1;//删除状态

    public static final int MEETINGTYPE_ACTIVITY = 0;//发活动
    public static final int MEETINGTYPEA_TEAM = 1;//发团购

    public static final int MSGFLAG_OFF = 0;//短信关
    public static final int MSGFLAG_ON = 1;//短信开

    public static final int EMAIL_OFF = 0;//邮件关
    public static final int EMAIL_ON = 1;//邮件开
    public static final int COMMET_OFF = 0;//评论关
    public static final int COMMET_ON = 1;//评论开

    public static final int MSGCOUNT_NUM = 0;//默认短信条数为0

    public static final int LIKECOUNT = 0;//初始点赞数0
    public static final int COMMENTCOUNT = 0;//初始评论数0

    public static final int ISOPEN_OFF = 0;//不公开
    public static final int ISOPEN_ON = 1;//公开

    public static final int MEETING_SCALE_TWO = 2;//会议规模(2人团)
    public static final int MEETING_SCALE_THREE = 3;//会议规模(3人团)
    public static final int MEETING_SCALE_FIVE = 5;//会议规模(5人团)

    private int mid;//会议id
    private int uid;//所属人
    private int oid;//所属机构
    private int status;//状态
    private int scale;//规模
    private Date starttime;//会议开始时间
    private Date endtime;//会议结束时间
    private String address;//会议地点
    private String introduction;//会议简介
    private String schedule;//日程
    private String guest;//嘉宾
    private String ticketinstruction;//门票说明
    private int meetingtype;//会议类型
    private int likecount;//点赞数
    private int commentcount;//评论数
    private int commentflag;//评论开关
    private String sign;//电子签订协议
    private int isopen;//公开与否
    private Date createtime;//创建时间
    private Date updatetime;//更新时间
    private int emailflag;//浏览数
    private BigDecimal lat;
    private BigDecimal lng;
    private int msgflag;//短信开关
    private int msgcount;//短信数量
    private int viewnum;//浏览数
    private String pic;//广告图
    private String name;//会议名称
    private int soldticket;//已售门票数量
    private int purchaseduser;//已购票账号数
    private int invoiceflag;//0:不开 1:普通发票 2:增值发票 3:2种都提供
}
