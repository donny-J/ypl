package com.jxzg.share.domain.other;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 消息表
 */
@Getter@Setter
public class Notification extends Token {
    public static final int NTYPE_MESSAGE = 1;//短信
    public static final int NTYPE_NOTE = 2;//站内信

    public static final int MSGTYPE_SYSTEM=1;//系统消息
    public static final int MSGTYPE_ACATIVITY=2;//活动消息

    public static final int STATUS_INIT=0;//初始化
    public static final int STATUS_INFROM=1;//已通知
    public static final int STATUS_PUSH=2;//已推送
    public static final int STATUS_LOOK_OVER=9;//已查看

    private Long nid;
    private int ntype;//'通知类型，1：短信，2：站内短信
    private int msgtype;// '消息类型，1：系统消息；2：活动消息'
    private String msgtitle;//'消息标题，邮件通知类型需要'
    private String content;//'消息内容，具体业务定'
    private String msgfrom;// '消息来源，系统发送为0,非0位消息发送者'
    private int uid;//接收消息的用户
    private String target;//接收消息用户的电话
    private int status;// '0：初始化；1：已通知；2：已推送；9：已查看
    private Date createtime;
    private Date updatetime;//发送时间
}
