package com.jxzg.share.domain.other;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 登录令牌
 */
@Getter@Setter
public class Usertoken extends Token{
    public static final int LOGINTYPE_WEIXIN = 0;
    public static final int LOGINTYPE_PC = 1;//PC
    public static final int LOGINTYPE_MOVE = 2;//移动
    public static final int LOGINTYPE_WEIXIN_MP = 3;//微信公众号

    private Long id;
    private int uid;//用户id
    private String loginname;//登录名
    private String token;//token
    private Date logintime;//登录时间
    private int logintype;//登录类型
    private String loginip;//登录ip
    private int status;//状态
    private BigDecimal lat;//位置维度
    private BigDecimal lng;//位置精度
    private Date expiretime;//终止时间
}
