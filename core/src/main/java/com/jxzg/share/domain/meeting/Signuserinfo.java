package com.jxzg.share.domain.meeting;

import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 报名者信息
 */
@Getter
@Setter
public class Signuserinfo extends Token {
    public static final int ONE_TEXT = 2;//单行文本
    public static final int MANY_TEXT = 3;//多行文本
    public static final int ONE_CHOICE = 4;//单选项
    public static final int MANY_CHOICE = 5;//多选项

    public static final int FIELD_NAME = 0;//名称
    public static final int FIELE_PHONE = 1;//电话

    private Long id;
    private int sid;//报名id
    private String fieldid;//扩展字段id
    private String fieldval;//扩展字段值
    private int status;//状态
    private Date createtime;//创建时间
    private Date updatetime;//更新时间
}
