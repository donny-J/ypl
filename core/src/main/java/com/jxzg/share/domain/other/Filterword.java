package com.jxzg.share.domain.other;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * 过滤字
 */
@Getter@Setter
public class Filterword implements Serializable {
    public static final int STATUS_FILTERWORD = 0;//过滤字
    public static final int STATUS_HOTWORD = 1;//关键字

    private int id;
    private String word;//词语
    private int statue;//状态
    private Date createtime;//添加时间
    private Date updatetime;//修改时间
    private String adduser;//添加人
    private String modifyuser; //修改人
}
