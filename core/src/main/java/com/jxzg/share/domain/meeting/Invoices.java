package com.jxzg.share.domain.meeting;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 发票
 */
@Getter
@Setter
public class Invoices {
    public static final int STATUS_ONESELF = 0;//自己领取
    public static final int STATUS_ADDRESS = 1;//邮寄
    //public static final int STATUS_NO = -1;//不需要发票

    public static final int INVOICETYPE_NO = 0;//不提供
    public static final int INVOICETYPE_COMMON = 1;//普通发票
    public static final int INVOICETYPE_APPRECIATION = 2;//增值
    public static final int INVOICETYPE_TWO = 3;//两种都提供

    private int id;
    private int uid;
    private long oid;//门票id
    private int invoicetype;//发票类型 0:不提供发票 1:普通发票 2:增值发票 3:两种都提供
    private String title;//发票抬头
    private String taxcode;//纳税人识别码
    private String regaddress;//注册地址
    private String regtel;//注册电话
    private String bankname;//开户银行
    private String bankcard;//银行帐号
    private String realname;//姓名
    private String tel;//电话
    private String address;//地址
    private int status;//状态
    private String orderno;
    private Date createtime;//创建时间
    private Date updatetime;//更新时间
}
