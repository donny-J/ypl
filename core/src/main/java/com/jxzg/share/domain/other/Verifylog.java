package com.jxzg.share.domain.other;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 验证日志表.
 */
@Getter@Setter
public class Verifylog extends Token{
    public static final int STATUS_LOSER=0;//验证失败
    public static final int STATUS_SUCCESS=1;//验证成功
    public static final int TYPE_PHONE=0;//手机验证

    private int id;
    private int uid;//用户ID
    private int type;//验证类型
    private String content;//验证内容
    private int status;//状态
    private Date createtime;//创建时间
    private Date updatetime;//更新时间
}
