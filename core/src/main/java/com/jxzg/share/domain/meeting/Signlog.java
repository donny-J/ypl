package com.jxzg.share.domain.meeting;

import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 签到记录
 */
@Getter
@Setter
public class Signlog extends Token {
    public static final int TYPE_CODE_ZERO = 0;
    public static final int TYPE_CODE_ONE = 1;

    public static final int STATUS_FAIL = 0;
    public static final int STATUS_SUCCESS = 1;

    private Long nid;//签到id
    private int mid;//会议id
    private int ntype;//签到类型 0:扫码  1:签到码签到
    private String content;//签到内容
    private int uid;//用户id
    private String target;//签到目标
    private int status;//状态
    private Date createtime;//时间
    private Date updatetime;//更新时间
    private String extinfo;//签到扩展信息
    private String signip;//签到ip
    private BigDecimal lat;//位置维度
    private BigDecimal lng;//位置精度
}
