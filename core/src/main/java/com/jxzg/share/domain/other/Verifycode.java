package com.jxzg.share.domain.other;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
@Getter@Setter
public class Verifycode {
    public static final int ONE = 1;//注册
    public static final int TWO = 2;//忘记密码
    public static final int THREE=3;//更改手机号
    public static final int PAY=4;//验证支付密码

    private Integer id;

    private String loginname;

    private String verifycode;

    private Integer type;

    private Date createtime;

    private Date updatetime;
}