package com.jxzg.share.domain.pay;

import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by 00818Wolf on 2017/5/20.
 * 提现记录
 */
@Getter@Setter
public class Withdraw extends Token {
    private int id;
    private int uid;
    private int type;//提现类型 '1.支付宝 2.微信 3.银联',
    private String name;//账号名
    private String account;//账号
    private int status;
    private Date createtime;
    private Date updatetime;
    private BigDecimal lat;
    private BigDecimal lng;
}
