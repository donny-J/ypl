package com.jxzg.share.domain.other;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 收藏和关注
 */
@Getter@Setter
public class Collectattention extends Token{
    public static final int TYPE_COLLECT=0;//收藏
    public static final int TYPE_ATTENTION=1;//关注
    public static final int STATUS_ZERO=0;//未登录
    public static final int STATUS_DEL = -1;//删除状态

    private int id;
    private int uid;//用户id
    private int type;//0会议1主办方
    private int smid;//会议或者主办方id
    private int status;//状态
    private Date updatetime;//更新时间
    private Date createtime;//创建时间
}
