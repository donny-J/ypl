package com.jxzg.share.domain.meeting;

import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * 报名字段
 */
@Getter@Setter@ToString
public class Signfield extends Token {
    public static final int ISREQUIRE_NO = 0;//选填
    public static final int ISREQUIRE_YES = 1;//必填

    private Long id;
    private int mid;//会议id
    private String fieldname;//字段名
    private String fieldcode;//字段编码
    private String fieldval;//字段值
    private String fielddescription;//字段描述
    private int  isrequire;//必选
    private Date createtime;//创建时间
    private Date updatetime;//更新时间
}
