package com.jxzg.share.domain.meeting;

import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 *会议广告图
 */
@Getter
@Setter
public class Meetingpic extends Token {
    public static final int PIC_SW_TYPE = 0;//商务会议
    public static final int PIC_JH_TYPE = 1;//聚会娱乐
    public static final int PIC_YD_TYPE = 2;//运动健身
    public static final int PIC_QZ_TYPE = 3;//亲子幼教
    public static final int PIC_WH_TYPE = 4;//文化艺术
    public static final int PIC_LY_TYPE = 5;//旅游户外
    public static final int PIC_YC_TYPE = 6;//演出赛事

    public static final int PIC_STATUS_NO= 0 ;//默认(平台提供的图片)
    public static final int PIC_STATUS_YES= 1 ;//用户上传了图片
    private Long id;
    private int mid;//会议id
    private int type;//图片类型
    private String url;//图片路径
    private String spec;//图片规格
    private Date createtime;//创建时间
    private Date datetime;//创建时间
    private Date updatetime;//更新时间
    private int status;//状态
}
