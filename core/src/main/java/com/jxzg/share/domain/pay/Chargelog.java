package com.jxzg.share.domain.pay;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by 00818Wolf on 2017/5/20.
 * '内部账务日志表';
 */
@Getter
@Setter
public class Chargelog {
    public static final int BUY_TICKET = 0;
    public static final int TOPUP = 1;
    public static final int EMPLOY = 2;
    public static final int REFUND = 3;

    public static final int ALIPAY = 1;

    public static final int WEIXIN = 2;

    private long id;
    private String orderno;
    private int uid;
    private int type;//账务类型，0：购票,打赏增加；1：充值；2：使用' 3:拼团退款
    private int method;//0：支付宝；1：微信',
    private String billno;
    private BigDecimal amount;//金额
    private BigDecimal currentamount;//当前金额
    private BigDecimal remainamount;//充值成功后金额
    private Date createtime;
    private Date updatetime;
    private int status;//状态，0初始化，1提交支付，2成功，-2失败',
    private String remarks;//备注
    private String extinfo;
}
