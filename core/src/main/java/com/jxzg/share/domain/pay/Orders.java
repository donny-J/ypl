package com.jxzg.share.domain.pay;

import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单支付信息
 */
@Getter
@Setter
public class Orders extends Token {
    public static final int ALIPAY = 0;//支付宝
    public static final int WECHAT = 1;//微信

    public static final int ORDER_STATUS_NO_PAY = 0;//未支付
    public static final int ORDER_STATUS_YES_PAY = 1;//支付完成
    public static final int ORDER_STATUS_FAIL = -1;//1退款成功
    public static final int ORDER_STATUS_OVERTIME = -2;//订单失败

    public static final int ORDERTYPE_GENERAL = 0;//普通订单
    public static final int ORDERTYPE_TEAM = 1;//拼团订单
    public static final int ORDERTYPE_ELSE = 2;//打赏
    public static final int PRODUCTTYPE_RECHARGE = 3;//充值
    public static final int PRODUCTTYPE_REFLECT = 4;//提现
    public static final int PRODUCTTYPE_REFUND = 5;//退款
    public static final int ORDERTYPE_NOTE = 6;//短信

    private Long orderid;//订单ID
    private String orderno;//订单号
    private int uid;//用户ID
    private int ordertype;//订单类型0:普通订单 1拼团订单
    private int status;//订单状态默认为0，1完成，-1支付失败，-2超时关闭
    private int amount;//数量
    private BigDecimal money;//总额
    private int paymethd;//支付方式
    private String payinfo;//支付信息
    private Date createtime;//创建时间
    private Date updatetime;//订单交易成功时间或订单失败时间
    private int invoiceflag;//默认为0未开，1已开（申请）
}
