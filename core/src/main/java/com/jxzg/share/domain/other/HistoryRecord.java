package com.jxzg.share.domain.other;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 历史搜索记录
 */
@Getter
@Setter
public class HistoryRecord extends Token{
    public static final int UID_TOURIST = -1;
    public static final int STATUS_TOURIST = 0;//游客
    public static final int STATUS_USER = 1;//用户
    private int id;
    private int uid;//如果是游客为-1
    private int status;
    private String content;
    private BigDecimal lat;
    private BigDecimal lng;
    private String ip;
    private Date createtime;
    private Date updatetime;
}
