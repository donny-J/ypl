package com.jxzg.share.domain.meeting;

import com.jxzg.share.domain.other.Token;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 我参加的会议
 */
@Getter
@Setter
public class MyMeeting extends Token {
    private int id;//
    private int uid;//用户id
    private int mid;//会议id
    private int no;//会议位置编号
    private int status;//状态
    private Date createtime;//创建时间
    private Date updatetime;//更新时间
    private String createip;//创建ip
}
