package com.jxzg.share.domain.pay;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by 00818Wolf on 2017/5/20.
 * 收款(退款)管理
 *
 */
@Getter@Setter
public class Payee {
    private int id;
    private int uid;
    private int type;//'1.支付宝  2.微信  3.银联',
    private int isdefault;//0:收款 1:退款
    private String name;//账号名
    private String account;//账号 || 退款金额
    private int status;//1:成功  0:失败
    private Date createtime;
    private Date updatetime;
}