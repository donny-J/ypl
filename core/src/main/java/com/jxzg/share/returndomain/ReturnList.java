package com.jxzg.share.returndomain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 00818Wolf on 2017/1/21.
 */
@Getter@Setter@ToString
public class ReturnList {
    private boolean success = true;
    private String msg;
    private int errorCode;
    private Integer totalCount;
    private List result = new ArrayList();
    public ReturnList(List result,String msg) {
        this.result = result;
        this.msg = msg;
    }
    public ReturnList(List result,String msg,int totalCount) {
        this.totalCount = totalCount;
        this.result = result;
        this.msg = msg;
    }
    public ReturnList(String msg,boolean success) {
        this.success = success;
        this.msg = msg;
    }

    public ReturnList(boolean success, String msg, int errorCode) {
        this.success = success;
        this.msg = msg;
        this.errorCode = errorCode;
    }
    public ReturnList(boolean success,List result, String msg, int errorCode) {
        this.success = success;
        this.result=result;
        this.msg = msg;
        this.errorCode = errorCode;
    }

    public ReturnList(boolean success,List result, String msg, int errorCode,Integer totalCount) {
        this.success = success;
        this.result=result;
        this.msg = msg;
        this.errorCode = errorCode;
        this.totalCount=totalCount;
    }

    public ReturnList() {
    }
}
