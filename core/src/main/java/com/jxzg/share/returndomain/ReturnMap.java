package com.jxzg.share.returndomain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;

/**
 * Creaed by 00818Wolf on 2017/1/12.
 */
@Getter
@Setter
@ToString
public class ReturnMap {
    private boolean success = true;
    private String msg;
    private Map<String,Object> result = new HashMap();

    public ReturnMap() {
    }
    public ReturnMap(String msg, Map<String,Object> result) {
        this.msg = msg;
        this.result = result;
    }
    public ReturnMap(Boolean success,String msg) {
        this.success=success;
        this.msg = msg;
    }
}
