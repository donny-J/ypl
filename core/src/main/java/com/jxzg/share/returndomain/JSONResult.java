package com.jxzg.share.returndomain;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter@Setter@ToString
public class JSONResult {
    //成功的状态设置为true
    public JSONResult(boolean success, String msg) {
        this.success = success;
        this.msg = msg;
    }
    private boolean success = true;
    private String msg;
    public JSONResult(String msg) {
        this.msg = msg;
    }
}
