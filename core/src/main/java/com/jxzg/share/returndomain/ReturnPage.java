package com.jxzg.share.returndomain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Crated by 00818Wolf on 2017/1/12.
 */
@Getter@Setter@ToString
public class ReturnPage {
    private int totalCount;//总条数
    private boolean success = true;
    private String msg;
    private List<Map<String,Object>> result = new ArrayList();
    public ReturnPage(int totalCount,List<Map<String,Object>> result,String msg) {
        this.totalCount = totalCount;
        this.result = result;
        this.msg = msg;
    }
    public ReturnPage(List<Map<String,Object>> result,String msg) {
        this.result = result;
        this.msg = msg;
    }
    public ReturnPage(boolean success,String msg) {
        this.success = success;
        this.msg = msg;
    }
}
