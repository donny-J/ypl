package com.jxzg.share.service.impl.v1;

import com.jxzg.share.domain.meeting.MyMeeting;
import com.jxzg.share.domain.other.Usertoken;
import com.jxzg.share.mapper.MymeetingMapper;
import com.jxzg.share.mapper.UsertokenMapper;
import com.jxzg.share.returndomain.ReturnList;
import com.jxzg.share.service.v1.IMyMeetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by 00818Wolf on 2017/1/3.
 */
@Service
public class MyMeetingServiceImpl implements IMyMeetingService {
    @Autowired
    private MymeetingMapper mymeetingMapper;
    @Autowired
    private UsertokenMapper usertokenMapper;

    @Override
    public ReturnList mymeeting(String token) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(token);
        if(token == null || usertoken==null){
            return new ReturnList("请重新登录",false);
        }
        List<MyMeeting> myMeetings = mymeetingMapper.selectMeetingByUid(usertoken.getUid());
        return new ReturnList(myMeetings,"成功!");
    }
}
