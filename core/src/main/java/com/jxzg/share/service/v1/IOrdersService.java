package com.jxzg.share.service.v1;

import com.jxzg.share.domain.pay.Orderdtl;
import com.jxzg.share.domain.pay.Orders;
import com.jxzg.share.querypage.IncomeDetailQueryObject;
import com.jxzg.share.returndomain.ReturnList;
import com.jxzg.share.returndomain.ReturnMap;

import java.util.Date;

/**
 * 订单管理
 */
public interface IOrdersService {


    /**
     * 收支明细
     *
     * @return
     */
    ReturnList income_detail(IncomeDetailQueryObject qo);

    /**
     * 收支明细二级
     *
     * @return
     */
    ReturnMap income_detail_dtl(Orders orders);

    ReturnMap buy_msg(Orders orders);

    ReturnMap play_tour(Orderdtl orderdtl);

    /**
     * 微信,支付宝支付完成
     */
    boolean alipay_success(String out_trade_no,int type);

    /**
     * 微信支付回调失败
     * @param out_trade_no
     */
    void weixinpay_fail(String out_trade_no,int type);

    void updateByOrderno(int orderStatusFail, String orderno, Date date);

    ReturnList outcome_detail(IncomeDetailQueryObject qo);

    ReturnList income_detail_all(IncomeDetailQueryObject qo);

    ReturnList totalIncomeAndOutput(IncomeDetailQueryObject qo);
}
