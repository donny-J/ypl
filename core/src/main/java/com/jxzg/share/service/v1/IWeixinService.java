package com.jxzg.share.service.v1;

import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.vo.WeixinLoginVO;

/**
 * 微信相关
 */
public interface IWeixinService {
    /**
     * 微信登录
     * @param userconnect
     * @return
     */
    ReturnMap weixin_login(WeixinLoginVO userconnect);

    /**
     * 微信公众号登录
     * @param userInfo
     * @return
     */
    ReturnMap weixinmp_login(WeixinLoginVO userInfo);
}
