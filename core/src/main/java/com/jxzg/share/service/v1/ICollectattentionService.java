package com.jxzg.share.service.v1;

import com.jxzg.share.domain.other.Collectattention;
import com.jxzg.share.querypage.QueryObject;
import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.returndomain.ReturnList;

/**
 * Created by 00818Wolf on 2017/2/14.
 */
public interface ICollectattentionService {

    /**
     * 收藏
     * @param
     */
    JSONResult collect_save(Collectattention collectattention);

    /**
     * 关注
     * @param
     */
    JSONResult attention_save(Collectattention collectattention);

    /**
     * 我的收藏
     * @return
     */
    ReturnList mycollection(QueryObject qo);

    /**
     * 我的关注
     * @return
     */
    ReturnList myattention(QueryObject qo);
}
