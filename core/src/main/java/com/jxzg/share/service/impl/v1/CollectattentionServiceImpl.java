package com.jxzg.share.service.impl.v1;

import com.jxzg.share.domain.meeting.Meeting;
import com.jxzg.share.domain.meeting.Sponsor;
import com.jxzg.share.domain.other.Collectattention;
import com.jxzg.share.domain.other.Usertoken;
import com.jxzg.share.mapper.*;
import com.jxzg.share.querypage.QueryObject;
import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.returndomain.ReturnList;
import com.jxzg.share.service.v1.ICollectattentionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

import static com.jxzg.share.domain.other.Collectattention.TYPE_COLLECT;

/**
 * Created by 00818Wolf on 2017/2/14.
 */
@Service
public class CollectattentionServiceImpl implements ICollectattentionService {
    @Autowired
    private CollectattentionMapper collectattentionMapper;
    @Autowired
    private UsertokenMapper usertokenMapper;
    @Autowired
    private MeetingMapper meetingMapper;
    @Autowired
    private MeetingticketMapper meetingticketMapper;
    @Autowired
    private SponsorMapper sponsorMapper;

    @Override
    public JSONResult collect_save(Collectattention collectattention) {
        if (collectattention.getToken() == null || collectattention.getSmid() == 0) {
            return new JSONResult(false, "请重新登录!");
        }
        Usertoken usertoken = usertokenMapper.selectUidBytoken(collectattention.getToken());
        if (usertoken == null) {
            return new JSONResult(false, "请重新登录!");
        }
        Integer count = collectattentionMapper.IsCollectAttentionNum(collectattention.getSmid(), usertoken.getUid(), TYPE_COLLECT);
        if (count != 0) {
            collectattentionMapper.deleteCollectattention(collectattention.getSmid(), usertoken.getUid(), TYPE_COLLECT);
            return new JSONResult("取消收藏成功!");
        }
        Date date = new Date();
        Collectattention ca = new Collectattention();
        ca.setUid(usertoken.getUid());
        ca.setCreatetime(date);
        ca.setUpdatetime(date);
        ca.setSmid(collectattention.getSmid());
        ca.setType(TYPE_COLLECT);
        collectattentionMapper.insert(ca);
        return new JSONResult("收藏成功!");
    }

    @Override
    public JSONResult attention_save(Collectattention collectattention) {
        if (collectattention.getToken() == null || collectattention.getSmid() == 0) {
            return new JSONResult(false, "请重新登录!");
        }
        Usertoken usertoken = usertokenMapper.selectUidBytoken(collectattention.getToken());
        if (usertoken == null) {
            return new JSONResult(false, "请重新登录!");
        }
        Sponsor sponsor = sponsorMapper.selectByOid(collectattention.getSmid());
        Integer count = collectattentionMapper.IsCollectAttentionNum(collectattention.getSmid(), usertoken.getUid(), Collectattention.TYPE_ATTENTION);
        if (count != 0) {
            collectattentionMapper.deleteCollectattention(collectattention.getSmid(), usertoken.getUid(), Collectattention.TYPE_ATTENTION);
            return new JSONResult("取消关注成功!");
        }
        if (sponsor.getStatus() == Sponsor.STATUS_DEL) {
            return new JSONResult(false, "关注失败!该主办方已被删除");
        }
        Date date = new Date();
        Collectattention ca = new Collectattention();
        ca.setUid(usertoken.getUid());
        ca.setCreatetime(date);
        ca.setUpdatetime(date);
        ca.setSmid(collectattention.getSmid());
        ca.setType(Collectattention.TYPE_ATTENTION);
        collectattentionMapper.insert(ca);
        return new JSONResult("关注成功!");
    }

    @Override
    public ReturnList mycollection(QueryObject qo) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(qo.getToken());
        if (qo.getToken() == null || usertoken == null) {
            return new ReturnList("请重新登录!", false);
        }
        List result = new ArrayList();
        List<Integer> mids = collectattentionMapper.selectCollectAttentionCount(Collectattention.TYPE_COLLECT, usertoken.getUid());
        if (mids.size() == 0) {
            return new ReturnList(result, "列表获取成功!", 0);
        }
        qo.setMids(mids);
        int count = meetingMapper.selectNameLikeCount(qo);
        if (count > 0) {
            List<Meeting> meetings = meetingMapper.selectNameLike(qo);
            for (Meeting meeting : meetings) {
                Map<String, Object> map = new HashMap<>();
                BigDecimal ticket = meetingticketMapper.selectMeetingTicket(meeting.getMid());
                map.put("mid", meeting.getMid());
                map.put("name", meeting.getName());
                map.put("starttime", meeting.getStarttime());
                map.put("pic", meeting.getPic());
                String address = meeting.getAddress();
                String[] str = address.split("-");
                map.put("address", str[1]);
                map.put("ticket", ticket);
                result.add(map);
            }
            return new ReturnList(result, "列表获取成功!", count);
        }
        return new ReturnList(result, "列表获取成功!", 0);
    }

    @Override
    public ReturnList myattention(QueryObject qo) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(qo.getToken());
        if (qo.getToken() == null || usertoken == null) {
            return new ReturnList("请重新登录!", false);
        }
        List result = new ArrayList();
        List<Integer> sids = collectattentionMapper.selectCollectAttentionCount(Collectattention.TYPE_ATTENTION, usertoken.getUid());
        if (sids.size() == 0) {
            return new ReturnList(result, "列表获取成功!", 0);
        }
        qo.setSids(sids);
        int count = sponsorMapper.selectByNameLikeCount(qo);
        if (count > 0) {
            List<Sponsor> sponsors = sponsorMapper.selectByNameLike(qo);
            for (Sponsor sponsor : sponsors) {
                Map<String, Object> map = new HashMap<>();
                map.put("name", sponsor.getName());
                map.put("logo", sponsor.getLogo());
                map.put("status", Collectattention.TYPE_ATTENTION);
                map.put("id", sponsor.getOid());
                result.add(map);
            }
            return new ReturnList(result, "列表获取成功!", count);
        }
        return new ReturnList(result, "列表获取成功!", 0);
    }
}
