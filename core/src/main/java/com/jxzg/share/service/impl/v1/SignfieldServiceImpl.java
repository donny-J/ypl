package com.jxzg.share.service.impl.v1;

import com.jxzg.share.domain.meeting.*;
import com.jxzg.share.domain.other.Usertoken;
import com.jxzg.share.domain.pay.Orderdtl;
import com.jxzg.share.domain.pay.Orders;
import com.jxzg.share.mapper.*;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.service.v1.ISignfieldService;
import com.jxzg.share.toolUtil.DateUtil;
import com.jxzg.share.toolUtil.OrdernoUtil;
import com.jxzg.share.toolUtil.QRcodeImageUtil;
import com.jxzg.share.toolUtil.RandomNum;
import com.jxzg.share.vo.CreateOrderVO;
import com.jxzg.share.vo.FileOrderVO;
import com.jxzg.share.vo.OrderVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;

import static org.apache.ibatis.ognl.DynamicSubscript.mid;

/**
 * Created by 00818Wolf on 2017/1/12.
 */
@Service
public class SignfieldServiceImpl implements ISignfieldService {
    @Autowired
    private SignfieldMapper signfieldMapper;
    @Autowired
    private SignuserinfoMapper signuserinfoMapper;
    @Autowired
    private OrdersMapper ordersMapper;
    @Autowired
    private OrderdtlMapper orderdtlMapper;
    @Autowired
    private UsertokenMapper usertokenMapper;
    @Autowired
    private MyticketMapper myticketMapper;
    @Autowired
    private MeetingticketMapper meetingticketMapper;
    @Autowired
    private MeetingMapper meetingMapper;
    @Autowired
    private InvoicesMapper invoicesMapper;
    @Value("${pic.baseUrl}")
    private String picBaseUrl;

    @Value("${pic.qrcode}")
    private String qrcodeRelativeUrl;




    @Override
    public synchronized ReturnMap create_order(CreateOrderVO orders) {
        Map<String, Object> map = new HashMap<>();
        Usertoken usertoken = usertokenMapper.selectUidBytoken(orders.getToken());
        if (orders.getTickets() == null || orders.getMid() == 0 || orders.getToken() == null || usertoken == null) {
            return new ReturnMap(false, "请重新登录!");
        }
        Map<String, Integer> m = new HashMap<String, Integer>();
        List<String> ticketnames = new ArrayList<>();
        for (OrderVO s : orders.getTickets()) {
            ticketnames.add(s.getTicketname());
        }
        for (String str : ticketnames) {
            m.put(str, m.get(str) == null ? 1 : m.get(str) + 1);
        }
        for (String str : ticketnames) {
            Integer iscount = meetingticketMapper.selectIsRemaincount(orders.getMid(), str, m.get(str));
            Meetingticket meetingticket = meetingticketMapper.selectTicketByName(str, orders.getMid());
            if (meetingticket.getRemaincount() != Meetingticket.AMOUNT_INFINITY) {
                if (iscount == 0) {
                    return new ReturnMap(false, "购票失败!余票不足");
                }
            }
        }
        Date date = new Date();
        for (OrderVO orderVO : orders.getTickets()) {
            Meetingticket meetingticket = meetingticketMapper.selectTicketByName(orderVO.getTicketname(), orders.getMid());
            if (date.getTime() > meetingticket.getEndtime().getTime()) {
                return new ReturnMap(false, "购票失败!售票时间到");
            }
            if (date.getTime() < meetingticket.getBegintime().getTime()) {
                return new ReturnMap(false, "购票失败!售票时间未到");
            }

        }
        Meeting meeting = meetingMapper.selectMeetingByMid(orders.getMid());
        if (meeting.getStatus() != 1) {
            return new ReturnMap(false, "报名失败!请核对报名时间");
        }
        String orderno = OrdernoUtil.getOrderno() + usertoken.getUid();
        //生成订单
        Orders co = new Orders();
        co.setOrderno(orderno);
        co.setUid(usertoken.getUid());
        co.setOrdertype(Orders.ORDERTYPE_GENERAL);
        if (orders.getMoney().compareTo(Meetingticket.PRICE) == 0) {//如果订单总额是0,支付成功
            co.setStatus(Orders.ORDER_STATUS_YES_PAY);
        } else {
            co.setStatus(Orders.ORDER_STATUS_NO_PAY);
        }
        co.setAmount(orders.getAmount());
        co.setMoney(orders.getMoney());
        co.setCreatetime(date);
        try {
            co.setUpdatetime(DateUtil.twentylater(20));
        } catch (Exception e) {
            e.printStackTrace();
        }
        ordersMapper.insert(co);
        //生成发票
        if (orders.getInvoices().getInvoicetype() != 0) {
            Invoices invoices = orders.getInvoices();
            invoices.setCreatetime(date);
            invoices.setUpdatetime(date);
            invoices.setUid(usertoken.getUid());
            invoices.setOid(orders.getMid());
            invoices.setStatus(invoices.getStatus());
            invoices.setInvoicetype(invoices.getInvoicetype());
            invoices.setTitle(invoices.getTitle());
            invoices.setTaxcode(invoices.getTaxcode());
            invoices.setRegaddress(invoices.getRegaddress());
            invoices.setRegtel(invoices.getRegtel());
            invoices.setBankname(invoices.getBankname());
            invoices.setBankcard(invoices.getBankcard());
            invoices.setRealname(invoices.getRealname());
            invoices.setTel(invoices.getTel());
            invoices.setAddress(invoices.getAddress());
            invoices.setOrderno(orderno);
            invoicesMapper.insert(invoices);
        }
        //保存信息(单行文本...)
        List<OrderVO> orderlist = orders.getTickets();
        for (OrderVO orderVO : orderlist) {
            Meetingticket meetingticket = meetingticketMapper.selectTicketByName(orderVO.getTicketname(), orders.getMid());
            //票的余量减少
            if (meetingticket.getRemaincount() != Meetingticket.AMOUNT_INFINITY) {
                meetingticketMapper.updateTicketNum(meetingticket.getName(), orders.getMid(), OrderVO.TICKETNUM);
            }
        }
        for (OrderVO orderVO : orderlist) {
            Myticket myticket = new Myticket();
            //生成我的门票
            myticket.setUid(usertoken.getUid());
            myticket.setMid(orders.getMid());
            if (orders.getMoney().compareTo(Meetingticket.PRICE) == 0) {//如果订单总额是0,支付成功
                myticket.setStatus(Myticket.ORDER_STATUS_YES_PAY);
            } else {
                myticket.setStatus(Myticket.ORDER_STATUS_NO_PAY);
            }
            myticket.setUpdatetime(date);
            myticket.setNo(Myticket.MYTICKET_NO_NO);
            myticket.setTickettype(Myticket.TICKETTYPE_MYTICKET);
            myticket.setCreateip(orderVO.getTicketname());
            //myticket.setTicketid(mee);
            myticket.setOrderno(orderno);
            myticket.setCreatetime(date);
            myticketMapper.insert(myticket);
            String code = co.getOrderno() + ":" + myticket.getId() + ":" + meeting.getMid() + usertoken.getUid();
            String qcode = QRcodeImageUtil.getQRcodePath(code,picBaseUrl,qrcodeRelativeUrl);//二维码,订单号生成
            String signcode = null;
            int count = 1;
            while (count > 0) {
                signcode = RandomNum.getRandomnum();
                int amount = myticketMapper.selectSignCode(signcode, meeting.getMid());
                if (amount > 0) {
                    count = 1;
                } else {
                    count = 0;
                }
            }
            myticketMapper.updateQcode(myticket.getId(), qcode, signcode);
            Orderdtl orderdtl = new Orderdtl();
            orderdtl.setUid(usertoken.getUid());
            orderdtl.setMid(orders.getMid());
            orderdtl.setProducttype(Orderdtl.PRODUCTTYPE_TICKET);
            if (orders.getMoney().compareTo(Meetingticket.PRICE) == 0) {//如果订单总额是0,支付成功
                meetingMapper.updateSoldticketAdd(orders.getMid(), 1);
                orderdtl.setStatus(Orderdtl.ORDER_STATUS_YES_PAY);
            } else {
                orderdtl.setStatus(Orderdtl.ORDER_STATUS_NO_PAY);
            }
            orderdtl.setProductid(myticket.getId());
            orderdtl.setAmount(OrderVO.TICKETNUM);
            orderdtl.setOrderno(orderno);
            orderdtl.setPrice(orderVO.getTicketprice());
            orderdtl.setMoney(orderVO.getTicketprice());
            orderdtl.setCreatetime(date);
            try {
                orderdtl.setUpdatetime(DateUtil.twentylater(20));
            } catch (Exception e) {
                e.printStackTrace();
            }
            orderdtlMapper.insert(orderdtl);
            for (FileOrderVO fieldmsg : orderVO.getFieldmsg()) {
                if (fieldmsg.getType() != Signuserinfo.MANY_CHOICE) {
                    Signuserinfo signuserinfo = new Signuserinfo();
                signuserinfo.setSid(myticket.getId());
                signuserinfo.setFieldid(fieldmsg.getFieldname());
                signuserinfo.setFieldval(fieldmsg.getFieldval());
                signuserinfo.setStatus(fieldmsg.getType());
                signuserinfo.setCreatetime(date);
                signuserinfo.setUpdatetime(date);
                signuserinfoMapper.insert(signuserinfo);
            }
                if (fieldmsg.getType() == Signuserinfo.MANY_CHOICE) {//多选项
                    for (String vo : fieldmsg.getFieldvals()) {
                        Signuserinfo sgi = new Signuserinfo();
                        sgi.setSid(myticket.getId());
                        sgi.setFieldid(fieldmsg.getFieldname());
                        sgi.setFieldval(vo);
                        sgi.setStatus(fieldmsg.getType());
                        sgi.setCreatetime(date);
                        sgi.setUpdatetime(date);
                        signuserinfoMapper.insert(sgi);
                    }
                }
            }
        }
        map.put("orderno", co.getOrderno());
        map.put("mid", meeting.getMid());
        return new ReturnMap("订单提交成功!", map);
    }
}
