package com.jxzg.share.service.impl.v1;

import com.jxzg.share.domain.pay.Chargelog;
import com.jxzg.share.mapper.ChargelogMapper;
import com.jxzg.share.service.v1.IChargelogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by 00818wolf on 2017/5/27.
 */
@Service("chargelogService")
public class ChargelogServiceImpl implements IChargelogService{
    @Autowired
    private ChargelogMapper chargelogMapper;
    @Override
    public void insert(Chargelog chargelog) {
        chargelogMapper.insert(chargelog);
    }
}
