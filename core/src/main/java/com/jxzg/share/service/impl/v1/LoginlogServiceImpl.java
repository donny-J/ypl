package com.jxzg.share.service.impl.v1;

import com.jxzg.share.domain.other.Loginlog;
import com.jxzg.share.domain.other.User;
import com.jxzg.share.domain.other.Userprofile;
import com.jxzg.share.domain.other.Usertoken;
import com.jxzg.share.mapper.LoginlogMapper;
import com.jxzg.share.mapper.UserMapper;
import com.jxzg.share.mapper.UsertokenMapper;
import com.jxzg.share.mapper.VerifycodeMapper;
import com.jxzg.share.pay.alipay.MD5;
import com.jxzg.share.querypage.PageResult;
import com.jxzg.share.querypage.QueryObject;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.service.v1.ILoginlogService;
import com.jxzg.share.service.v1.IUserprofileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class LoginlogServiceImpl implements ILoginlogService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private LoginlogMapper loginlogMapper;

    @Autowired
    private UsertokenMapper usertokenMapper;

    @Autowired
    private IUserprofileService userprofileService;

    @Autowired
    private VerifycodeMapper veMapper;

    @Override
    public ReturnMap login(Loginlog userinfo, String ip) {
        if (userinfo.getLoginpassword() == null || userinfo.getLoginname() == null) {
            return new ReturnMap(false, "账号和密码不能为空!");
        }
        Date date = new Date();
        Map<String, Object> map = new HashMap<>();
        String password = MD5.encode(userinfo.getLoginpassword());
        //创建Iplog对象
        Loginlog iplog = new Loginlog();
        iplog.setLogintime(date);
        iplog.setLoginname(userinfo.getLoginname());
        iplog.setLoginpassword(password);
        iplog.setLoginip(ip);
        iplog.setLogintype(userinfo.getLogintype());
        iplog.setLng(userinfo.getLng());
        iplog.setLat(userinfo.getLat());
        User user = userMapper.selectUserByPhone(userinfo.getLoginname(), password);
        if (user != null) {
            iplog.setStatus(Loginlog.LOGIN_SUCCESS);
            loginlogMapper.insert(iplog);
        } else {
            iplog.setStatus(Loginlog.LOGIN_WRONG);
            loginlogMapper.insert(iplog);
            return new ReturnMap(false, "用户不存在或密码错误!");
        }
        String token = UUID.randomUUID().toString();
        Usertoken usertoken = new Usertoken();
        usertoken.setLoginip(ip);
        usertoken.setUid(user.getUid());
        usertoken.setLat(userinfo.getLat());
        usertoken.setLng(userinfo.getLng());
        usertoken.setLoginname(user.getLoginname());
        usertoken.setLogintime(date);
        usertoken.setLogintype(Usertoken.LOGINTYPE_MOVE);
        usertoken.setToken(token);
        //usertoken.setExpiretime(DateUtil.getExpiretime());//终止时间,一个月后
        Usertoken usertoken1 = usertokenMapper.selectByUidAndType(user.getUid(), Usertoken.LOGINTYPE_MOVE);
        if (usertoken1 != null) {
            usertokenMapper.deleteByUid(user.getUid(), Usertoken.LOGINTYPE_MOVE);
        }
        usertokenMapper.insert(usertoken);
        if (user.getStatus() == 1) {
            map.put("fl", true);
            //初始化个人资料信息
            Userprofile userprofile = new Userprofile();
            userprofile.setUid(user.getUid());
            userprofile.setNickname("未设置");
            userprofile.setRealname("未设置");
            userprofile.setAddress("未设置");
            userprofileService.insert(userprofile);
            userMapper.updateStatus(user.getUid());
        } else if (user.getStatus() == 0) {
            map.put("fl", false);
        }
        map.put("token", token);
        if (user.getLoginname() != null) {
            map.put("loginname", user.getLoginname());
        } else {
            map.put("loginname", "");
        }
        return new ReturnMap("登录成功!", map);
    }

    @Override
    public ReturnMap pclogin(Loginlog userinfo, String ip) {
        if (userinfo.getLoginpassword() == null || userinfo.getLoginname() == null) {
            return new ReturnMap(false, "账号和密码不能为空!");
        }
        Date date = new Date();
        Map<String, Object> map = new HashMap<>();
        String password = MD5.encode(userinfo.getLoginpassword());
        User user = userMapper.selectUserByPhone(userinfo.getLoginname(), password);
        //创建Iplog对象
        Loginlog iplog = new Loginlog();
        iplog.setLogintime(date);
        iplog.setLoginname(userinfo.getLoginname());
        iplog.setLoginpassword(password);
        iplog.setLoginip(ip);
        iplog.setLogintype(Loginlog.LOGINTYPR_PC);
        iplog.setLng(userinfo.getLng());
        iplog.setLat(userinfo.getLat());
        if (user != null) {
            iplog.setStatus(Loginlog.LOGIN_SUCCESS);
            loginlogMapper.insert(iplog);
        } else {
            iplog.setStatus(Loginlog.LOGIN_WRONG);
            loginlogMapper.insert(iplog);
            return new ReturnMap(false, "用户不存在或密码错误!");
        }
        if (user.getStatus() == 1) {
            map.put("fl", true);
            //初始化个人资料信息
            Userprofile userprofile = new Userprofile();
            userprofile.setUid(user.getUid());
            userprofile.setNickname("未设置");
            userprofile.setRealname("未设置");
            userprofile.setAddress("未设置");
            userprofileService.insert(userprofile);
            userMapper.updateStatus(user.getUid());
        } else if (user.getStatus() == 0) {
            map.put("fl", false);
        }
        String token = UUID.randomUUID().toString();
        Usertoken usertoken = new Usertoken();
        usertoken.setLoginip(ip);
        usertoken.setUid(user.getUid());
        usertoken.setLat(userinfo.getLat());
        usertoken.setLng(userinfo.getLng());
        usertoken.setLoginname(user.getLoginname());
        usertoken.setLogintime(date);
        usertoken.setLogintype(Usertoken.LOGINTYPE_PC);
        usertoken.setToken(token);
        //usertoken.setExpiretime(DateUtil.getExpiretime());//终止时间,一个月后
        Usertoken usertoken1 = usertokenMapper.selectByUidAndType(user.getUid(), Usertoken.LOGINTYPE_PC);
        if (usertoken1 != null) {
            usertokenMapper.deleteByUid(user.getUid(), Usertoken.LOGINTYPE_PC);
        }
        usertokenMapper.insert(usertoken);
        map.put("token", token);
        if (user.getLoginname() != null) {
            map.put("loginname", user.getLoginname());
        } else {
            map.put("loginname", "");
        }
        return new ReturnMap("登录成功!", map);
    }

    @Override
    public boolean validate(String loginname, String verifyCode, int type) {
        if (loginname == null || verifyCode == null) {
            throw new RuntimeException("账户名或密码错误!!!");
        }
        int count = veMapper.selectByLoginname(loginname, verifyCode, type);
        if (count > 0) {
            return false;
        }
        return true;
    }

    @Override
    public PageResult query(QueryObject qo) {
        int count = loginlogMapper.queryForCount(qo);
        if (count > 0) {
            List<Loginlog> result = loginlogMapper.query(qo);
            return new PageResult(result, qo.getPagesize(), qo.getCurrentpage(), count);
        }
        return new PageResult(new ArrayList<Loginlog>(), qo.getPagesize(), 1, 0);
    }
}