package com.jxzg.share.service.v1;

import com.jxzg.share.querypage.PageResult;
import com.jxzg.share.querypage.QueryObject;
import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.vo.SendNoteVO;

import java.math.BigDecimal;

public interface IVerifylogService {
    /**
     * 注册
     */
    ReturnMap register(String loginname, String loginpassword, String verifyCode, String ip, BigDecimal lat, BigDecimal lng);

    /**
     *发送验证码
     */
    JSONResult sendVerifyCode(String loginname,int type);

    /**
     * 分页查询
     */
    PageResult query(QueryObject qo);

    /**
     * pc注册
     */
    ReturnMap register_pc(String loginname, String loginpassword, String verifyCode, String ip, BigDecimal lat, BigDecimal lng);

    /**
     * 给参会人发送短信
     * @param sendNote
     * @return
     */
    JSONResult send_note(SendNoteVO sendNote);
}
