package com.jxzg.share.service.v1;

import com.jxzg.share.domain.other.Userprofile;
import com.jxzg.share.querypage.PageResult;
import com.jxzg.share.querypage.QueryObject;
import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.vo.VerifyCodeVO;

/**
 * 个人资料
 */
public interface IUserprofileService {
    /**
     * 保存用户的个人信息
     */
    void insert(Userprofile userprofile);

    /**
     * 修改密码
     */
    JSONResult ForgetPassword(String loginname, String loginpassword, String verifyCode);

    /**
     * 分页查询
     */
    PageResult query(QueryObject qo);

    /**
     * 修改昵称
     *
     * @param nickname
     */
    JSONResult updateNickname(String nickname, String token);

    /**
     * 修改真实姓名
     *
     * @param realname
     */
    JSONResult updateRealname(String realname, String token);

    /**
     * 修改性别
     *
     * @param gender
     */
    JSONResult updateGender(int gender, String token);

    /**
     * 修改证件号码
     *
     * @param idcardtype
     * @param idcard
     */
    JSONResult updateIdcardtype(int idcardtype, String idcard, String token);

    /**
     * 修改地址
     *
     * @param provice
     * @param city
     * @param dist
     */
    JSONResult updateAddress(String token, String provice, String city, String dist);


    /**
     * 查询用户资料
     *
     * @param uid 用户uid
     * @return
     */
    Userprofile selectByUid(int uid);

    ReturnMap personList(String token);

    /**
     * 更换手机号
     *
     * @param forgetpassword
     * @return
     */
    JSONResult change_tel(VerifyCodeVO forgetpassword);
}
