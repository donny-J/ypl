package com.jxzg.share.service.v1;

import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.vo.CreateOrderVO;

/**
 * 会议文本
 */
public interface ISignfieldService {

    /**
     * 提交订单
     *
     * @param orders
     * @return
     */
    ReturnMap create_order(CreateOrderVO orders);
}
