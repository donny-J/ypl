package com.jxzg.share.service.impl.v1;

import com.jxzg.share.domain.meeting.Meeting;
import com.jxzg.share.domain.meeting.Sponsor;
import com.jxzg.share.domain.other.Collectattention;
import com.jxzg.share.domain.other.Usertoken;
import com.jxzg.share.mapper.*;
import com.jxzg.share.querypage.MeetingQueryObject;
import com.jxzg.share.querypage.SearchQueryObject;
import com.jxzg.share.returndomain.ReturnList;
import com.jxzg.share.service.v1.ISearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by 00818Wolf on 2017/3/13.
 */
@Service
public class SearchServiceImpl implements ISearchService {
    @Autowired
    private MeetingMapper meetingMapper;
    @Autowired
    private SponsorMapper sponsorMapper;
    @Autowired
    private MeetingticketMapper meetingticketMapper;
    @Autowired
    private CollectattentionMapper collectattentionMapper;
    @Autowired
    private UsertokenMapper usertokenMapper;
    @Autowired
    private MeetingtagMapper meetingtagMapper;

    @Override
    public ReturnList searchMeeting(SearchQueryObject qo) {
        List resultmeeting = new ArrayList();
        List<Integer> mids = meetingtagMapper.selectByTag(qo);
        if (mids.size()==0) {
            qo.setMids(null);
        }else{
            qo.setMids(mids);
        }
        int count = meetingMapper.SearchqueryForCount(qo);
        if (count > 0) {
            List<Meeting> meetings = meetingMapper.SearcgQuery(qo);
            for (Meeting meeting : meetings) {
                Map<String, Object> map = new HashMap<>();
                BigDecimal price = meetingticketMapper.selectMeetingTicket(meeting.getMid());
                String url = meeting.getPic();
                if (meeting.getAddress() != null) {
                    String[] address = meeting.getAddress().split("-");
                    map.put("address", address[1]);
                }
                map.put("starttime", meeting.getStarttime());
                map.put("price", price);
                map.put("name", meeting.getName());
                map.put("pic", url);
                map.put("mid", meeting.getMid());
                resultmeeting.add(map);
            }
            return new ReturnList(resultmeeting, "查询成功!", count);
        }
        return new ReturnList("没有结果!", true);
    }

    @Override
    public ReturnList searchSponsor(MeetingQueryObject qo) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(qo.getToken());
        List resultsponsor = new ArrayList();
        int ct = sponsorMapper.queryForCount(qo);
        if (ct > 0) {
            List<Sponsor> sponsors = sponsorMapper.selectSponsorByContent(qo);
            for (Sponsor sponsor : sponsors) {
                Map<String, Object> map = new HashMap<>();
                map.put("sponsorname", sponsor.getName());
                map.put("sponsorlogo", sponsor.getLogo());
                map.put("sponsorid", sponsor.getOid());
                if (qo.getToken() != null && usertoken != null) {
                    int count = collectattentionMapper.IsCollectAttentionNum(sponsor.getOid(), usertoken.getUid(), Collectattention.TYPE_ATTENTION);
                    map.put("attention", count);
                } else {
                    map.put("attention", 0);
                }
                resultsponsor.add(map);
            }
            return new ReturnList(resultsponsor, "查询成功!", ct);
        }
        return new ReturnList("没有结果!", false);
    }
}
