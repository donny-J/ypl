package com.jxzg.share.service.v1;

import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.vo.SignInVO;

/**
 * Created by 00818Wolf on 2017/5/19.
 */
public interface ISignInService {
    JSONResult signin(SignInVO signInVO);
}
