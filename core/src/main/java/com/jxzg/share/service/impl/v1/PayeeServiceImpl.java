package com.jxzg.share.service.impl.v1;

import com.jxzg.share.domain.pay.Payee;
import com.jxzg.share.mapper.PayeeMapper;
import com.jxzg.share.service.v1.IPayeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by 00818wolf on 2017/5/27.
 */
@Service("payeeService")
public class PayeeServiceImpl implements IPayeeService {
    @Autowired
    private PayeeMapper payeeMapper;
    @Override
    public void insert(Payee payee) {
        payeeMapper.insert(payee);
    }
}
