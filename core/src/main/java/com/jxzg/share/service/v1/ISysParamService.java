package com.jxzg.share.service.v1;

import com.jxzg.share.domain.other.SysParam;
import com.jxzg.share.domain.pay.Payee;
import com.jxzg.share.querypage.SysParamModQueryObject;

import java.util.List;

/**
 * Created by 00818wolf on 2017/5/27.
 */
public interface ISysParamService {
    List<SysParam> getSysParamsByCon(SysParamModQueryObject queryObject);
}
