package com.jxzg.share.service.impl.v1;

import com.jxzg.share.domain.meeting.Meeting;
import com.jxzg.share.domain.meeting.Myticket;
import com.jxzg.share.domain.meeting.Spellgroup;
import com.jxzg.share.domain.other.User;
import com.jxzg.share.domain.other.Usertoken;
import com.jxzg.share.domain.pay.Chargelog;
import com.jxzg.share.domain.pay.Orderdtl;
import com.jxzg.share.domain.pay.Orders;
import com.jxzg.share.enums.ConstantStateEnum;
import com.jxzg.share.enums.OrderStateEnum;
import com.jxzg.share.mapper.*;
import com.jxzg.share.querypage.IncomeDetailQueryObject;
import com.jxzg.share.querypage.MeetingModQueryObject;
import com.jxzg.share.querypage.MeetingQueryObject;
import com.jxzg.share.querypage.OrderModQueryObject;
import com.jxzg.share.returndomain.ReturnList;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.service.v1.IOrdersService;
import com.jxzg.share.toolUtil.DateUtil;
import com.jxzg.share.toolUtil.OrdernoUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * Created by 00818wolf on 2017/1/16.
 */
@Service("ordersService")
public class OrdersServiceImpl implements IOrdersService {

    private static final Logger logger = LoggerFactory.getLogger(OrdersServiceImpl.class);
    @Autowired
    private OrdersMapper ordersMapper;
    @Autowired
    private UsertokenMapper usertokenMapper;
    @Autowired
    private MeetingMapper meetingMapper;
    @Autowired
    private OrderdtlMapper orderdtlMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private MyticketMapper myticketMapper;
    @Autowired
    private MeetingticketMapper meetingticketMapper;
    @Autowired
    private SpellgroupMapper spellgroupMapper;
    @Autowired
    private ChargelogMapper chargelogMapper;


    @Override
    public ReturnList income_detail_all(IncomeDetailQueryObject qo) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(qo.getToken());
        if (qo.getToken() == null || usertoken == null) {
            return new ReturnList("请重新登录!", false);
        }
        try {
            qo.setEndtimedate(DateUtil.longToDate(qo.getEndtime()));
            qo.setBegintimedate(DateUtil.longToDate(qo.getBegintime()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (qo.getStart() == 1) {
            ReturnList returnList = outcome_detail(qo);
            return returnList;
        } else if (qo.getStart() == 2) {
            ReturnList returnList = income_detail(qo);
            return returnList;
        } else {
            ReturnList returnList = outcome_detail(qo);
            List list = returnList.getResult();
            ReturnList returnList1 = income_detail(qo);
            List list1 = returnList1.getResult();
            list.addAll(list1);
            return new ReturnList(list, "查询成功!", list.size());
        }
    }

    /**
     * 全部收支明细
     * //返回 订单号	交易时间	交易类型	交易金额	交易方式
     *
     * @param qo
     * @return
     */
    @Override
    public ReturnList totalIncomeAndOutput(IncomeDetailQueryObject qo) {
        //缺少token
        if (StringUtils.isEmpty(qo.getToken()))
            return new ReturnList(false, ConstantStateEnum.MISSING_TOKEN.getErrMsg(), ConstantStateEnum.MISSING_TOKEN.getErrorCode());
        //校验status 0:全部 1.支出 2.收入
        if (StringUtils.isEmpty(qo.getStatus()))
            return new ReturnList(false, OrderStateEnum.MISSING_STATUS.getErrMsg(), OrderStateEnum.MISSING_STATUS.getErrorCode());
        if (!Arrays.asList(0, 1, 2).contains(qo.getStatus()))
            return new ReturnList(false, OrderStateEnum.INVALID_STATUS.getErrMsg(), OrderStateEnum.INVALID_STATUS.getErrorCode());
        //校验 currentpage, pagesize
        if (qo.getCurrentpage() != null && qo.getCurrentpage() <= 0)
            qo.setCurrentpage(1);
        if (qo.getPagesize() != null && qo.getPagesize() <= 0)
            return new ReturnList(false, ConstantStateEnum.INVALID_PAGESIZE.getErrMsg(), ConstantStateEnum.INVALID_PAGESIZE.getErrorCode());
        //type null:全部 1:报名订单 2:打赏 3:充值 4:提现 5:退款 6:短信
        if (!StringUtils.isEmpty(qo.getType()))
            if (!Arrays.asList(1, 2, 3, 4, 5, 6).contains(qo.getType()))
                return new ReturnList(false, OrderStateEnum.INVALID_TYPE.getErrMsg(), OrderStateEnum.INVALID_TYPE.getErrorCode());
        //无效token
        Usertoken usertoken = usertokenMapper.selectUidBytoken(qo.getToken());
        if (StringUtils.isEmpty(usertoken))
            return new ReturnList(false, ConstantStateEnum.INVALID_TOKEN.getErrMsg(), ConstantStateEnum.INVALID_TOKEN.getErrorCode());
        List<Orders> orders = new ArrayList<>();
        Integer totalCount = 0;//返回的总记录数
        try {
            Integer type = qo.getType(), uid = usertoken.getUid(), currentPage = qo.getCurrentpage(), pageSize = qo.getPagesize();
            Date startTime = null, endTime = null;
            if (!StringUtils.isEmpty(qo.getBegintime()))
                startTime = DateUtil.longToDate(qo.getBegintime());
            if (!StringUtils.isEmpty(qo.getEndtime()))
                endTime = DateUtil.longToDate(qo.getEndtime());
            if (qo.getStatus() == 1) { //查找用户支出 type=null 1 2 4 5 6
                if (type == null) {//综合1,2,4,5,6
                    //查找用户创建的会议(会议状态  1报名中,2活动中,3已结束,4报名结束)
                    List<Meeting> meetings = getMeetingsByUid(uid, Arrays.asList(Meeting.MEETING_STATUS_ACTIVITY_APPLY, Meeting.MEETING_STATUS_ACTIVITY_YES, Meeting.MEETING_STATUS_ACTIVITY_END, Meeting.MEETING_STATUS_ACTIVITY_STOP));
                    List<String> ordernosByMeetings = getOrdernosByMeetings(meetings);
                    if (ordernosByMeetings.size() > 0) {
                        orders = getAllOrders(uid, startTime, endTime, ordernosByMeetings, currentPage, pageSize, -1);
                        totalCount = getAllOrdersCount(uid, startTime, endTime, ordernosByMeetings, currentPage, pageSize, -1);
                    } else {
                        orders = getOutputOrdersByCon(uid, null, startTime, endTime, null, Arrays.asList(Orders.ORDERTYPE_GENERAL, Orders.ORDERTYPE_TEAM, Orders.PRODUCTTYPE_REFUND, Orders.ORDERTYPE_ELSE, Orders.PRODUCTTYPE_REFLECT, Orders.ORDERTYPE_NOTE), Arrays.asList(Orders.ORDER_STATUS_YES_PAY, Orders.ORDER_STATUS_FAIL), null, currentPage, pageSize);
                        totalCount = getOutputOrdersCountByCon(uid, null, startTime, endTime, null, Arrays.asList(Orders.ORDERTYPE_GENERAL, Orders.ORDERTYPE_TEAM, Orders.PRODUCTTYPE_REFUND, Orders.ORDERTYPE_ELSE, Orders.PRODUCTTYPE_REFLECT, Orders.ORDERTYPE_NOTE), Arrays.asList(Orders.ORDER_STATUS_YES_PAY, Orders.ORDER_STATUS_FAIL), null, currentPage, pageSize);
                    }
                } else if (type == 1) {   //报名订单(普通订单和拼团订单和退款订单,其中退款订单要记录曾经使用哪种方式支付的)
                    orders = getOutputOrdersByCon(uid, null, startTime, endTime, null, Arrays.asList(Orders.ORDERTYPE_GENERAL, Orders.ORDERTYPE_TEAM, Orders.PRODUCTTYPE_REFUND), Arrays.asList(Orders.ORDER_STATUS_YES_PAY, Orders.ORDER_STATUS_FAIL), null, currentPage, pageSize);
                    totalCount = getOutputOrdersCountByCon(uid, null, startTime, endTime, null, Arrays.asList(Orders.ORDERTYPE_GENERAL, Orders.ORDERTYPE_TEAM, Orders.PRODUCTTYPE_REFUND), Arrays.asList(Orders.ORDER_STATUS_YES_PAY, Orders.ORDER_STATUS_FAIL), null, currentPage, pageSize);
                } else if (type == 5) {  //退款(用户作为主办方时的退款)
                    //查找用户创建的会议(会议状态  1报名中,2活动中,3已结束,4报名结束)
                    List<Meeting> meetings = getMeetingsByUid(uid, Arrays.asList(Meeting.MEETING_STATUS_ACTIVITY_APPLY, Meeting.MEETING_STATUS_ACTIVITY_YES, Meeting.MEETING_STATUS_ACTIVITY_END, Meeting.MEETING_STATUS_ACTIVITY_STOP));
                    List<String> ordernos = getOrdernosByMeetings(meetings);
                    if (ordernos.size() > 0) {
                        orders = getOutputOrdersByCon(null, Orders.PRODUCTTYPE_REFUND, startTime, endTime, Orders.ORDER_STATUS_FAIL, null, null, ordernos, currentPage, pageSize);
                        totalCount = getOutputOrdersCountByCon(null, Orders.PRODUCTTYPE_REFUND, startTime, endTime, Orders.ORDER_STATUS_FAIL, null, null, ordernos, currentPage, pageSize);
                    }
                } else if (type == 2 || type == 4 || type == 6) {  //打赏 提现 短信
                    orders = getOutputOrdersByCon(uid, type, startTime, endTime, Orders.ORDER_STATUS_YES_PAY, null, null, null, currentPage, pageSize);
                    totalCount = getOutputOrdersCountByCon(uid, type, startTime, endTime, Orders.ORDER_STATUS_YES_PAY, null, null, null, currentPage, pageSize);
                }
                return new ReturnList(true, ordersToMap(orders, 0), ConstantStateEnum.SUCCESS.getErrMsg(), ConstantStateEnum.SUCCESS.getErrorCode(), totalCount);
            } else if (qo.getStatus() == 2) {//查找用户收入 type=null 1 2 3
                List<Meeting> meetings = getMeetingsByUid(uid, Arrays.asList(Meeting.MEETING_STATUS_ACTIVITY_APPLY, Meeting.MEETING_STATUS_ACTIVITY_YES, Meeting.MEETING_STATUS_ACTIVITY_END, Meeting.MEETING_STATUS_ACTIVITY_STOP));
                List<String> ordernosByMeetings = getOrdernosByMeetings(meetings);
                if (type == null) { //全部收入
                    if (ordernosByMeetings.size() > 0) {
                        orders = getAllOrders(uid, startTime, endTime, ordernosByMeetings, currentPage, pageSize, 1);
                        totalCount = getAllOrdersCount(uid, startTime, endTime, ordernosByMeetings, currentPage, pageSize, 1);
                    } else {
                        orders = getIncomeOrdersByCon(uid, startTime, endTime, Orders.PRODUCTTYPE_RECHARGE, Orders.ORDER_STATUS_YES_PAY, null, null, null, currentPage, pageSize);//获取用户充值收入
                        totalCount = getIncomeOrdersCountByCon(uid, startTime, endTime, Orders.PRODUCTTYPE_RECHARGE, Orders.ORDER_STATUS_YES_PAY, null, null, null, currentPage, pageSize);
                    }
                } else if (type == 1) {//报名订单(退款的也要包含进来)
                    //根据会议拿出订单号
                    if (ordernosByMeetings.size() > 0) {
                        orders = getIncomeOrdersByCon(null, startTime, endTime, null, null, ordernosByMeetings, Arrays.asList(Orders.ORDERTYPE_GENERAL, Orders.ORDERTYPE_TEAM, Orders.PRODUCTTYPE_REFUND), Arrays.asList(Orders.ORDER_STATUS_YES_PAY, Orders.ORDER_STATUS_FAIL), currentPage, pageSize);
                        totalCount = getIncomeOrdersCountByCon(null, startTime, endTime, null, null, ordernosByMeetings, Arrays.asList(Orders.ORDERTYPE_GENERAL, Orders.ORDERTYPE_TEAM, Orders.PRODUCTTYPE_REFUND), Arrays.asList(Orders.ORDER_STATUS_YES_PAY, Orders.ORDER_STATUS_FAIL), currentPage, pageSize);
                    }
                } else if (type == 2) {//打赏
                    //根据订单号查找打赏收入
                    if (ordernosByMeetings.size() > 0) {
                        orders = getIncomeOrdersByCon(null, startTime, endTime, Orders.ORDERTYPE_ELSE, Orders.ORDER_STATUS_YES_PAY, ordernosByMeetings, null, null, currentPage, pageSize);
                        totalCount = getIncomeOrdersCountByCon(null, startTime, endTime, Orders.ORDERTYPE_ELSE, Orders.ORDER_STATUS_YES_PAY, ordernosByMeetings, null, null, currentPage, pageSize);
                    }
                } else if (type == 3) {//充值
                    orders = getIncomeOrdersByCon(uid, startTime, endTime, Orders.PRODUCTTYPE_RECHARGE, Orders.ORDER_STATUS_YES_PAY, null, null, null, currentPage, pageSize);//获取用户充值收入
                    totalCount = getIncomeOrdersCountByCon(uid, startTime, endTime, Orders.PRODUCTTYPE_RECHARGE, Orders.ORDER_STATUS_YES_PAY, null, null, null, currentPage, pageSize);//获取用户充值收入;
                }
                return new ReturnList(true, ordersToMap(orders, 1), ConstantStateEnum.SUCCESS.getErrMsg(), ConstantStateEnum.SUCCESS.getErrorCode(), totalCount);
            } else {//收入+支出
                return new ReturnList(false, OrderStateEnum.UNSUPPORTED_FUNCTION.getErrMsg(), OrderStateEnum.UNSUPPORTED_FUNCTION.getErrorCode());
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(ConstantStateEnum.INNER_ERROR.getErrMsg(), e);
        }
    }

    /**
     * 获取用户会议
     *
     * @param uid
     * @return
     */
    private List<Meeting> getMeetingsByUid(Integer uid, List<Integer> meetingStatusList) {
        MeetingModQueryObject queryObject = new MeetingModQueryObject();
        queryObject.setUid(uid);
        queryObject.setStatuslist(meetingStatusList);
        List<Meeting> meetings = meetingMapper.getMeetingsByCon(queryObject);
        return meetings;
    }

    /**
     * 根据条件查找用户支出
     *
     * @param uid       用户id
     * @param orderType 1:拼团  2:打赏  4:提现  6:短信
     * @param startTime
     * @param endTime
     * @param status    订单明细的状态
     * @param ordernos  订单号 集合
     * @return
     * @Param status(orders&ordersdtl) 0 未支付 1 支付成功 -1 退款成功  -2 支付失败
     */
    private List<Orders> getOutputOrdersByCon(Integer uid, Integer orderType, Date startTime, Date endTime, Integer status, List<Integer> orderTypes, List<Integer> orderStatus, List<String> ordernos, Integer currentPage, Integer pageSize) {
        OrderModQueryObject queryObject = createOrderModQuery(uid, startTime, endTime, orderType, status, ordernos, orderTypes, orderStatus, currentPage, pageSize);
        List<Orders> ordersList = ordersMapper.getOrdersByCon(queryObject);
        return ordersList;
    }

    /**
     * 根据条件查找订单总数
     *
     * @param uid
     * @param orderType
     * @param startTime
     * @param endTime
     * @param status
     * @param orderTypes
     * @param orderStatus
     * @param ordernos
     * @param currentPage
     * @param pageSize
     * @return
     */
    private Integer getOutputOrdersCountByCon(Integer uid, Integer orderType, Date startTime, Date endTime, Integer status, List<Integer> orderTypes, List<Integer> orderStatus, List<String> ordernos, Integer currentPage, Integer pageSize) {
        OrderModQueryObject queryObject = createOrderModQuery(uid, startTime, endTime, orderType, status, ordernos, orderTypes, orderStatus, currentPage, pageSize);
        Integer totalCount = ordersMapper.getOrdersCountByCon(queryObject);
        return totalCount;
    }

    /**
     * 根据条件查找收入
     *
     * @param uid
     * @param startTime
     * @param endTime
     * @param orderType      2.打赏    3:充值
     * @param status(orders) 0 未支付 1 支付成功 -1 退款成功  -2 支付失败
     * @param ordernos       订单号 集合
     * @param orderTypes     订单类型集合
     * @param orderstatus    订单状态集合
     * @return
     */
    private List<Orders> getIncomeOrdersByCon(Integer uid, Date startTime, Date endTime, Integer orderType, Integer status, List<String> ordernos, List<Integer> orderTypes, List<Integer> orderstatus, Integer currentPage, Integer pageSize) {
        OrderModQueryObject queryObject = createOrderModQuery(uid, startTime, endTime, orderType, status, ordernos, orderTypes, orderstatus, currentPage, pageSize);
        List<Orders> ordersList = ordersMapper.getOrdersByCon(queryObject);
        return ordersList;
    }

    /**
     * @param uid
     * @param startTime
     * @param endTime
     * @param orderType
     * @param status
     * @param ordernos
     * @param orderTypes
     * @param orderstatus
     * @param currentPage
     * @param pageSize
     * @return
     */
    private Integer getIncomeOrdersCountByCon(Integer uid, Date startTime, Date endTime, Integer orderType, Integer status, List<String> ordernos, List<Integer> orderTypes, List<Integer> orderstatus, Integer currentPage, Integer pageSize) {
        OrderModQueryObject queryObject = createOrderModQuery(uid, startTime, endTime, orderType, status, ordernos, orderTypes, orderstatus, currentPage, pageSize);
        Integer totalCount = ordersMapper.getOrdersCountByCon(queryObject);
        return totalCount;
    }


    /**
     * 查找用户所有收入(支出/收入)
     *
     * @param direction -1 支出 0 全部 1 收入
     */
    private List<Orders> getAllOrders(Integer uid, Date startTime, Date endTime, List<String> ordernos, Integer currentPage, Integer pageSize, Integer direction) {
        List<Orders> orders = new ArrayList<>();
        OrderModQueryObject queryObject = createOrderModQuery(uid, startTime, endTime, null, null, ordernos, null, null, currentPage, pageSize);
        if (direction == 1)
            orders = ordersMapper.getIncomeOrdersAll(queryObject);
        else
            orders = ordersMapper.getOutputOrdersAll(queryObject);
        return orders;
    }

    private Integer getAllOrdersCount(Integer uid, Date startTime, Date endTime, List<String> ordernos, Integer currentPage, Integer pageSize, Integer direction) {
        Integer totalCount = 0;
        OrderModQueryObject queryObject = createOrderModQuery(uid, startTime, endTime, null, null, ordernos, null, null, currentPage, pageSize);
        if (direction == 1)
            totalCount = ordersMapper.getIncomeOrdersAllCount(queryObject);
        else
            totalCount = ordersMapper.getOutputOrdersAllCount(queryObject);
        return totalCount;
    }


    /**
     * 取出会议相关的订单号
     *
     * @param meetings
     * @return
     */
    private List<String> getOrdernosByMeetings(List<Meeting> meetings) {
        List<String> ordernos = new ArrayList<>();
        List<Integer> mids = new ArrayList<>();
        OrderModQueryObject queryObject = new OrderModQueryObject();
        if (meetings != null && meetings.size() > 0)
            for (Meeting meeting : meetings)
                mids.add(meeting.getMid());
        if (mids.size() > 0) {
            queryObject.setMids(mids);
            List<Orderdtl> orderdtls = orderdtlMapper.getOrderdtlByCon(queryObject);
            if (orderdtls != null && orderdtls.size() > 0)
                for (Orderdtl odtl : orderdtls)
                    ordernos.add(odtl.getOrderno());
        }
        return ordernos;
    }

    /**
     * @param orders
     * @param direct
     * @return
     */
    private List<Map> ordersToMap(List<Orders> orders, int direct) {
        List<Map> list = new ArrayList<>();
        for (Orders o : orders) {
            Map<String, Object> map = new HashMap<>();
            map.put("ordertype", o.getOrdertype());
            map.put("ordertime", o.getCreatetime());
            map.put("ordermoney", o.getMoney());
            map.put("orderno", o.getOrderno());
            map.put("paymethd", o.getPaymethd());
            map.put("orderid", o.getOrderid());
            map.put("orderstatus", direct);
            list.add(map);
        }
        return list;
    }

    /**
     * @param uid
     * @param startTime
     * @param endTime
     * @param orderType      2.打赏    3:充值
     * @param status(orders) 0 未支付 1 支付成功 -1 退款成功  -2 支付失败
     * @param ordernos       订单号 集合
     * @param orderTypes     订单类型集合
     * @param orderstatus    订单状态集合
     * @param currentPage
     * @param pageSize
     * @return
     */
    private OrderModQueryObject createOrderModQuery(Integer uid, Date startTime, Date endTime, Integer orderType, Integer status, List<String> ordernos, List<Integer> orderTypes, List<Integer> orderstatus, Integer currentPage, Integer pageSize) {
        OrderModQueryObject queryObject = new OrderModQueryObject();
        if (uid != null)
            queryObject.setUid(uid);
        if (startTime != null)
            queryObject.setStartTime(startTime);
        if (endTime != null)
            queryObject.setEndTime(endTime);
        if (orderType != null)
            queryObject.setOrderType(orderType);
        if (status != null)
            queryObject.setOrderStatus(status);
        if (ordernos != null)
            queryObject.setOrdernos(ordernos);
        if (orderTypes != null)
            queryObject.setOrderTypes(orderTypes);
        if (orderstatus != null)
            queryObject.setOrderStatuslist(orderstatus);
        if (currentPage != null)
            queryObject.setCurrentPage(currentPage);
        if (pageSize != null)
            queryObject.setPageSize(pageSize);
        return queryObject;
    }

    @Override
    public ReturnList income_detail(IncomeDetailQueryObject qo) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(qo.getToken());
        if (qo.getToken() == null || usertoken == null) {
            return new ReturnList("请重新登录!", false);
        }
        List<Integer> mids = meetingMapper.selectMidsIncomeDetail(usertoken.getUid());
        List list = new ArrayList();
        if (mids.size() == 0) {
            return new ReturnList(list, "查询成功!", 0);
        } else {
            qo.setMids(mids);
        }
        qo.setUid(null);
        try {
            if (qo.getBegintime() != null) {
                qo.setBegintimedate(DateUtil.longToDate(qo.getBegintime()));
                qo.setEndtimedate(DateUtil.longToDate(qo.getEndtime()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        int totalCount = orderdtlMapper.selectByQueryCount(qo);
        if (totalCount > 0) {
            List<Orderdtl> orderdtls = orderdtlMapper.selectByQuery(qo);
            for (Orderdtl orderdtl : orderdtls) {
                Orders orders = ordersMapper.selectByOrderno(orderdtl.getOrderno());
                Map<String, Object> map = new HashMap<>();
                if (orders.getOrdertype() == Orders.ORDERTYPE_GENERAL || orders.getOrdertype() == Orders.ORDERTYPE_TEAM) {
                    map.put("ordertype", 1);
                } else {
                    map.put("ordertype", orders.getOrdertype());
                }
                map.put("ordertime", orders.getUpdatetime());
                map.put("orderno", orders.getOrderno());
                map.put("paymethd", orders.getPaymethd());
                map.put("ordermoney", orders.getMoney());
                map.put("orderid", orders.getOrderid());
                map.put("orderstatus", 1);
                list.add(map);
            }
        }
        return new ReturnList(list, "查询成功!", totalCount);
    }

    @Override
    public ReturnList outcome_detail(IncomeDetailQueryObject qo) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(qo.getToken());
        if (qo.getToken() == null || usertoken == null) {
            return new ReturnList("请重新登录!", false);
        }
        List list = new ArrayList();
        List<Integer> mids = meetingMapper.selectMidsOutcomeDetail(usertoken.getUid());
        qo.setUid(usertoken.getUid());
        if (mids.size() == 0) {
            qo.setMids(null);
        } else {
            qo.setMids(mids);
        }
        try {
            if (qo.getBegintime() != null) {
                qo.setBegintimedate(DateUtil.longToDate(qo.getBegintime()));
                qo.setEndtimedate(DateUtil.longToDate(qo.getEndtime()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        int count = orderdtlMapper.selectOutcomeDetailCount(qo);
        if (count > 0) {
            List<Orderdtl> orderss = orderdtlMapper.selectOutcomeDetail(qo);
            for (Orderdtl orderdtl : orderss) {
                Orders orders = ordersMapper.selectByOrderno(orderdtl.getOrderno());
                Map<String, Object> map = new HashMap<>();
                if (orders.getOrdertype() == Orders.ORDERTYPE_GENERAL || orders.getOrdertype() == Orders.ORDERTYPE_TEAM) {
                    map.put("ordertype", 1);
                } else {
                    map.put("ordertype", orders.getOrdertype());
                }
                map.put("ordertime", orders.getUpdatetime());
                map.put("orderno", orders.getOrderno());
                map.put("paymethd", orders.getPaymethd());
                map.put("ordermoney", orders.getMoney());
                map.put("orderid", orders.getOrderid());
                map.put("orderstatus", 0);
                list.add(map);
            }
            return new ReturnList(list, "查询成功!", count);
        }
        return new ReturnList(new ArrayList(), "查询成功!", 0);
    }

    @Override
    public ReturnMap income_detail_dtl(Orders orders) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(orders.getToken());
        if (orders.getOrderid() == null || orders.getToken() == null || usertoken == null) {
            return new ReturnMap(false, "请重新登录!");
        }
        Orders orders1 = ordersMapper.selectByOrderid(orders.getOrderid());
        Map<String, Object> map = new HashMap<>();
        if (orders1.getOrdertype() == Orders.ORDERTYPE_GENERAL || orders1.getOrdertype() == Orders.ORDERTYPE_TEAM) {
            map.put("ordertype", 1);
        } else {
            map.put("ordertype", orders1.getOrdertype());
        }
        map.put("ordertime", orders1.getUpdatetime());
        map.put("ordermoney", orders1.getMoney());
        map.put("orderno", orders1.getOrderno());
        map.put("paymethd", orders1.getPaymethd());
        if (orders1.getOrdertype() == Orders.ORDERTYPE_NOTE) {//短信
            map.put("notenum", orders1.getAmount());
        }
        return new ReturnMap("查询成功!", map);
    }

    @Override
    public boolean alipay_success(String out_trade_no, int type) {
        Date date = new Date();
        Orders orders = ordersMapper.selectByOrderno(out_trade_no);
        if (orders.getOrdertype() == Orders.ORDERTYPE_GENERAL) {//如果是普通会议订单
            // 改变订单的状态,查看票的数量修改票的状态
            ordersMapper.updateOrderStatusType(Orders.ORDER_STATUS_YES_PAY, out_trade_no, date, type);
            orderdtlMapper.updateByOrderno(Orderdtl.ORDER_STATUS_YES_PAY, Orderdtl.PRODUCTTYPE_TICKET, out_trade_no, date);
            //余票减少
            List<Myticket> myticket = myticketMapper.selectByOrderno(out_trade_no);
            for (Myticket myticket1 : myticket) {
                myticketMapper.updateStatus(myticket1.getId(), Myticket.ORDER_STATUS_YES_PAY, date);
            }
            //会议发布者余额增加
            Orderdtl orderdtl = orderdtlMapper.selectOneByOrderno(out_trade_no);
            Meeting meeting = meetingMapper.selectMeetingByMid(orderdtl.getMid());
            meetingMapper.updateSoldticketAdd(meeting.getMid(), myticket.size());
            User user = userMapper.selectByUid(meeting.getUid());
            Chargelog cl = new Chargelog();
            cl.setOrderno(out_trade_no);
            cl.setUid(meeting.getUid());
            cl.setType(Chargelog.BUY_TICKET);
            cl.setMethod(type);
            cl.setBillno(out_trade_no);
            cl.setAmount(orders.getMoney());
            cl.setCurrentamount(user.getBalance());
            cl.setRemainamount(user.getBalance().add(orders.getMoney()));
            cl.setCreatetime(date);
            cl.setUpdatetime(date);
            cl.setRemarks("普通门票");
            chargelogMapper.insert(cl);
            userMapper.updateBalance(meeting.getUid(), orders.getMoney());
            return true;
        } else if (orders.getOrdertype() == Orders.ORDERTYPE_TEAM) { //如果是拼团会议订单
            ordersMapper.updateOrderStatusType(Orders.ORDER_STATUS_YES_PAY, out_trade_no, date, type);
            orderdtlMapper.updateByOrderno(Orderdtl.ORDER_STATUS_YES_PAY, Orderdtl.PRODUCTTYPE_TICKET, out_trade_no, date);
            Orderdtl orderdtl = orderdtlMapper.selectOneByOrderno(out_trade_no);
            List<Myticket> myticket = myticketMapper.selectByOrderno(out_trade_no);
            Meeting meeting = meetingMapper.selectMeetingByMid(orderdtl.getMid());
            for (Myticket myticket1 : myticket) {
                myticketMapper.updateStatus(myticket1.getId(), Myticket.ORDER_STATUS_YES_PAY, date);
                Spellgroup spellgroup = spellgroupMapper.selectByTicketid(myticket1.getId());
                meetingMapper.updateSoldticketAdd(meeting.getMid(), 1);
                if (spellgroup.getTid() == 0) {
                    spellgroupMapper.updateStatusOne(spellgroup.getId(), Spellgroup.STATUS_SUCCESS_A);
                } else {
                    int count = spellgroupMapper.selectCountByStatus(spellgroup.getTid(), meeting.getMid(), Spellgroup.STATUS_SUCCESS_A);
                    if (meeting.getScale() - count == 1) {
                        spellgroupMapper.updateStatusMany(spellgroup.getTid(), Spellgroup.STATUS_SUCCESS, Spellgroup.STATUS_SUCCESS_A);
                        spellgroupMapper.updateStatusOne(spellgroup.getId(), Spellgroup.STATUS_SUCCESS);
                    } else {
                        spellgroupMapper.updateStatusOne(spellgroup.getId(), Spellgroup.STATUS_SUCCESS_A);
                    }
                }
            }
            User user = userMapper.selectByUid(meeting.getUid());
            Chargelog cl = new Chargelog();
            cl.setOrderno(out_trade_no);
            cl.setUid(meeting.getUid());
            cl.setType(Chargelog.BUY_TICKET);
            cl.setMethod(type);
            cl.setBillno(out_trade_no);
            cl.setAmount(orders.getMoney());
            cl.setCurrentamount(user.getBalance());
            cl.setRemainamount(user.getBalance().add(orders.getMoney()));
            cl.setCreatetime(date);
            cl.setUpdatetime(date);
            cl.setRemarks("拼团门票");
            chargelogMapper.insert(cl);
            userMapper.updateBalance(meeting.getUid(), orders.getMoney());
            return true;
        } else if (orders.getOrdertype() == Orders.ORDERTYPE_ELSE) {//如果是打赏订单
            ordersMapper.updateOrderStatusType(Orders.ORDER_STATUS_YES_PAY, out_trade_no, date, type);
            orderdtlMapper.updateByOrderno(Orderdtl.ORDER_STATUS_YES_PAY, Orderdtl.PRODUCTTYPE_AWARD, out_trade_no, date);
            Orderdtl orderdtl = orderdtlMapper.selectOneByOrderno(out_trade_no);
            Meeting meeting = meetingMapper.selectMeetingByMid(orderdtl.getMid());
            User user = userMapper.selectByUid(meeting.getUid());
            Chargelog cl = new Chargelog();
            cl.setOrderno(out_trade_no);
            cl.setUid(meeting.getUid());
            cl.setType(Chargelog.BUY_TICKET);
            cl.setMethod(type);
            cl.setBillno(out_trade_no);
            cl.setAmount(orders.getMoney());
            cl.setCurrentamount(user.getBalance());
            cl.setRemainamount(user.getBalance().add(orders.getMoney()));
            cl.setCreatetime(date);
            cl.setUpdatetime(date);
            cl.setRemarks("打赏");
            chargelogMapper.insert(cl);
            userMapper.updateBalance(meeting.getUid(), orders.getMoney());
            return true;
        } else if (orders.getOrdertype() == Orders.ORDERTYPE_NOTE) {//如果是短信订单
            ordersMapper.updateOrderStatusType(Orders.ORDER_STATUS_YES_PAY, out_trade_no, date, type);
            orderdtlMapper.updateStatus(Orderdtl.ORDER_STATUS_YES_PAY, out_trade_no, date);
            userMapper.updateMsg(orders.getUid(), orders.getAmount());
            return true;
        }
        return true;
    }

    @Override
    public ReturnMap buy_msg(Orders orders) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(orders.getToken());
        if (orders.getAmount() == 0 || orders.getMoney() == null || orders.getToken() == null || usertoken == null) {
            return new ReturnMap(false, "请重新登录!");
        }
        Date date = new Date();
        String orderno = OrdernoUtil.getOrderno() + usertoken.getUid();
        Orders orders1 = new Orders();
        orders1.setOrderno(orderno);
        orders1.setUid(usertoken.getUid());
        orders1.setOrdertype(Orders.ORDERTYPE_NOTE);
        orders1.setStatus(Orders.ORDER_STATUS_NO_PAY);
        orders1.setAmount(orders.getAmount());
        orders1.setMoney(orders.getMoney());
        orders1.setCreatetime(date);
        orders1.setUpdatetime(date);
        ordersMapper.insert(orders1);
        Orderdtl ord = new Orderdtl();
        ord.setOrderno(orderno);
        ord.setUid(usertoken.getUid());
        ord.setProducttype(Orderdtl.PRODUCTTYPE_NOTE);
        ord.setStatus(Orderdtl.ORDER_STATUS_NO_PAY);
        ord.setAmount(orders.getAmount());
        ord.setMoney(orders.getMoney());
        ord.setPrice(orders.getMoney());
        ord.setCreatetime(date);
        ord.setUpdatetime(date);
        orderdtlMapper.insert(ord);
        Map<String, Object> map = new HashMap();
        map.put("orderno", orderno);
        return new ReturnMap("购买短信订单生成成功!", map);
    }

    @Override
    public ReturnMap play_tour(Orderdtl orderdtl) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(orderdtl.getToken());
        if (orderdtl.getMid() == 0 || orderdtl.getMoney() == null || orderdtl.getToken() == null || usertoken == null) {
            return new ReturnMap(false, "请重新登录!");
        }
        Date date = new Date();
        String orderno = OrdernoUtil.getOrderno() + usertoken.getUid();
        Orders ords = new Orders();
        ords.setOrderno(orderno);
        ords.setUid(usertoken.getUid());
        ords.setOrdertype(Orders.ORDERTYPE_ELSE);
        ords.setStatus(Orders.ORDER_STATUS_NO_PAY);
        ords.setMoney(orderdtl.getMoney());
        ords.setCreatetime(date);
        ords.setUpdatetime(date);
        ordersMapper.insert(ords);
        Orderdtl ord = new Orderdtl();
        ord.setOrderno(orderno);
        ord.setUid(usertoken.getUid());
        ord.setMid(orderdtl.getMid());
        ord.setProducttype(Orderdtl.PRODUCTTYPE_AWARD);
        ord.setStatus(Orderdtl.ORDER_STATUS_NO_PAY);
        ord.setMoney(orderdtl.getMoney());
        ord.setPrice(orderdtl.getMoney());
        ord.setCreatetime(date);
        ord.setUpdatetime(date);
        orderdtlMapper.insert(ord);
        Map<String, Object> map = new HashMap();
        map.put("orderno", orderno);
        return new ReturnMap("打赏订单生成成功!", map);
    }


    @Override
    public void weixinpay_fail(String out_trade_no, int type) {
        Date date = new Date();
        Orders orders = ordersMapper.selectByOrderno(out_trade_no);
        // 改变订单的状态,查看票的数量修改票的状态
        ordersMapper.updateOrderStatusType(Orders.ORDER_STATUS_FAIL, orders.getOrderno(), date, type);
        List<Orderdtl> orderdtls = orderdtlMapper.selectByOrderno(out_trade_no);
        int count = orderdtlMapper.selectByOrdernoCount(out_trade_no);
        for (Orderdtl orderdtl : orderdtls) {
            orderdtlMapper.updateStatus(Orders.ORDER_STATUS_FAIL, orders.getOrderno(), date);
        }
    }

    @Override
    public void updateByOrderno(int orderStatusFail, String orderno, Date date) {
        ordersMapper.updateStatus(orderStatusFail, orderno, date);
    }
}
