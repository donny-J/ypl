package com.jxzg.share.service.impl.v1;

import com.jxzg.share.domain.meeting.Meetingticket;
import com.jxzg.share.mapper.MeetingticketMapper;
import com.jxzg.share.service.v1.IMeetingticketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * Created by 00818Wolf on 2016/12/26.
 */
@Service
public class MeetingticketServiceImpl implements IMeetingticketService {
    @Autowired
    private MeetingticketMapper meetingticketMapper;

    @Override
    public int insert(Meetingticket record) {
        return meetingticketMapper.insert(record);
    }

    @Override
    public  BigDecimal selectMeetingTicketPic(int mid) {
        return meetingticketMapper.selectMeetingTicket(mid);
    }
}
