package com.jxzg.share.service.impl.v1;

import com.jxzg.share.domain.meeting.Spellgroup;
import com.jxzg.share.mapper.SpellgroupMapper;
import com.jxzg.share.service.v1.ISpellgroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by 00818Wolf on 2017/6/6.
 */
@Service("spellgroupService")
public class SpellgroupServiceImpl implements ISpellgroupService {
    @Autowired
    private SpellgroupMapper spellgroupMapper;
    @Override
    public Spellgroup selectByTicketid(int id) {
        Spellgroup spellgroup = spellgroupMapper.selectByTicketid(id);
        return spellgroup;
    }
}
