package com.jxzg.share.service.impl.v1;

import com.jxzg.share.domain.meeting.Meeting;
import com.jxzg.share.domain.other.Commet;
import com.jxzg.share.domain.other.User;
import com.jxzg.share.domain.other.Userprofile;
import com.jxzg.share.domain.other.Usertoken;
import com.jxzg.share.mapper.*;
import com.jxzg.share.querypage.CommetQueryObject;
import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.returndomain.ReturnPage;
import com.jxzg.share.service.v1.ICommetService;
import com.jxzg.share.service.v1.IFilterwordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 评论点赞
 */
@Service
public class CommetServiceImpl implements ICommetService {
    @Autowired
    private CommetMapper commetMapper;
    @Autowired
    private UsertokenMapper usertokenMapper;
    @Autowired
    private UserprofileMapper userprofileMapper;
    @Autowired
    private MeetingMapper meetingMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private IFilterwordService filterwordService;

    @Override
    public JSONResult insert(Commet commet) {
        if (commet.getMid() == 0 || commet.getContent() == null) {
            return new JSONResult(false, "数据异常,请重新登录!");
        }
        Meeting meeting = meetingMapper.selectMeetingByMid(commet.getMid());
        if (meeting.getCommentflag() == Meeting.COMMET_OFF) {
            return new JSONResult(false, "该活动，不允许评论");
        }
        //判断用户是否登录
        Date date = new Date();
        Commet com = new Commet();
        if (commet.getToken() == null) {
            com.setUid(Commet.COMMET_TOURIST);
        } else {
            Usertoken usertoken = usertokenMapper.selectUidBytoken(commet.getToken());
            if (usertoken == null) {
                return new JSONResult(false, "请重新登录!");
            }
            com.setUid(usertoken.getUid());
        }
        com.setCreatetime(date);
        com.setUpdatetime(date);
        com.setContent(commet.getContent());
        com.setMid(commet.getMid());
        com.setLat(commet.getLat());
        com.setLng(commet.getLng());
        com.setType(Commet.TYPE_COMMET);
        commetMapper.insert(com);
        meetingMapper.updateCommetCount(commet.getMid());
        return new JSONResult("评论成功!");
    }

    @Override
    public JSONResult commetreply(Commet commet) {
        if (commet.getId() == null || commet.getMid() == 0 || commet.getContent() == null || commet.getToken() == null) {
            return new JSONResult(false, "数据异常,请重新登录!");
        }
        Usertoken usertoken = usertokenMapper.selectUidBytoken(commet.getToken());
        if (usertoken == null) {
            return new JSONResult(false, "请重新登录!");
        }
        Meeting meeting = meetingMapper.selectMeetingByMid(commet.getMid());
        if (meeting.getCommentflag() == Meeting.COMMET_OFF) {
            return new JSONResult(false, "该活动，不允许评论");
        }
        Date date = new Date();
        Commet com = new Commet();
        com.setCreatetime(date);
        com.setUid(usertoken.getUid());
        com.setUpdatetime(date);
        com.setContent(commet.getContent());
        com.setMid(commet.getMid());
        com.setLat(commet.getLat());
        com.setLng(commet.getLng());
        com.setPid(commet.getId());
        com.setType(Commet.TYPE_REPLY);
        commetMapper.insert(com);
        return new JSONResult("回复成功");
    }

    @Override
    public JSONResult commetlike(Commet commet) {
        if (commet.getId() == null || commet.getMid() == 0 || commet.getToken() == null) {
            return new JSONResult(false, "数据异常,请重新登录!");
        }
        Usertoken usertoken = usertokenMapper.selectUidBytoken(commet.getToken());
        if (usertoken == null) {
            return new JSONResult(false, "请重新登录!");
        }
        Date date = new Date();
        Commet com = new Commet();
        com.setCreatetime(date);
        com.setUid(usertoken.getUid());
        com.setUpdatetime(date);
        com.setMid(commet.getMid());
        com.setStatus(Commet.STATUS_LIKE_YES);
        com.setLat(commet.getLat());
        com.setLng(commet.getLng());
        com.setType(Commet.TYPE_LIKE);
        com.setPid(commet.getId());
        commetMapper.insert(com);
        commetMapper.updateCommetLikeCount(commet.getId());//评论点赞数+1,改变点赞状态
        return new JSONResult("点赞成功！");
    }

    @Override
    public ReturnPage commetlist(CommetQueryObject qo) {
        List<Map<String, Object>> result = new ArrayList();
        int totalCount = commetMapper.queryForCount(qo);
        if (totalCount > 0) {
            Meeting meeting = meetingMapper.selectMeetingByMid(qo.getMid());
            List<Commet> commets = commetMapper.query(qo);
            for (Commet commet : commets) {
                if (commet.getType() == Commet.TYPE_COMMET) {//评论
                    Map<String, Object> map = new HashMap<>();
                    if (commet.getUid() == -1) {//游客的评论
                        map.put("commetcontent", commet.getContent());
                        map.put("commetcreatetime", commet.getCreatetime());
                        map.put("id", commet.getId());
                        map.put("uid", commet.getUid());
                        Usertoken usertoken = usertokenMapper.selectUidBytoken(qo.getToken());
                        if (qo.getToken() == null || usertoken == null) {
                            map.put("praise", Commet.STATUS_LIKE_NO);
                        } else {
                            int count = commetMapper.selectCommetByMidAndUid(commet.getId(), Commet.TYPE_LIKE, usertoken.getUid());
                            map.put("praise", count);
                        }
                        map.put("commetnickname", "游客");
                        map.put("likecount", commet.getLikecount());
                        if (commet.getType() == Commet.TYPE_COMMET) {
                            List<Commet> commetreply = commetMapper.selectReplyByid(commet.getId(), Commet.TYPE_REPLY);
                            List<Map<String, Object>> replys = new ArrayList();
                            if (commetreply.size() > 0) {
                                for (Commet reply : commetreply) {
                                    Map<String, Object> mapreply = new HashMap<>();
                                    Userprofile userprofile = userprofileMapper.selectByUid(reply.getUid());
                                    User user = userMapper.selectCommetUser(reply.getUid());
                                    mapreply.put("replynickname", userprofile.getNickname());
                                    mapreply.put("replyavatar", user.getAvatar());
                                    mapreply.put("replycontent", reply.getContent());
                                    mapreply.put("replycreatetime", reply.getCreatetime());
                                    replys.add(mapreply);
                                }
                                map.put("replys", replys);
                            }
                            map.put("replys", replys);
                        }
                    } else {
                        Userprofile userprofile = userprofileMapper.selectByUid(commet.getUid());
                        User user = userMapper.selectCommetUser(commet.getUid());
                        map.put("commetcontent", commet.getContent());
                        map.put("commetcreatetime", commet.getCreatetime());
                        map.put("id", commet.getId());
                        map.put("uid", commet.getUid());
                        map.put("likecount", commet.getLikecount());//点赞数
                        Usertoken usertoken = usertokenMapper.selectUidBytoken(qo.getToken());
                        if (qo.getToken() == null || usertoken == null) {
                            map.put("praise", Commet.STATUS_LIKE_NO);
                        } else {
                            int count = commetMapper.selectCommetByMidAndUid(commet.getId(), Commet.TYPE_LIKE, usertoken.getUid());
                            map.put("praise", count);
                        }
                        map.put("commetnickname", userprofile.getNickname());
                        map.put("commetavatar", user.getAvatar());
                        List<Commet> commetreply = commetMapper.selectReplyByid(commet.getId(), Commet.TYPE_REPLY);
                        List<Map<String, Object>> replys = new ArrayList();
                        if (commetreply.size() > 0) {
                            for (Commet reply : commetreply) {
                                Userprofile userpfreply = userprofileMapper.selectByUid(reply.getUid());
                                User userreply = userMapper.selectCommetUser(reply.getUid());
                                Map<String, Object> mapreply = new HashMap<>();
                                mapreply.put("replynickname", userpfreply.getNickname());
                                mapreply.put("replyavatar", userreply.getAvatar());
                                mapreply.put("replycontent", reply.getContent());
                                mapreply.put("replycreatetime", reply.getCreatetime());
                                replys.add(mapreply);
                            }
                            map.put("replys", replys);
                        }
                        map.put("replys", replys);
                    }
                    result.add(map);
                }
            }
        } else {
            return new ReturnPage(result, "没有评论");
        }
        return new ReturnPage(totalCount, result, "评论列表");
    }
}
