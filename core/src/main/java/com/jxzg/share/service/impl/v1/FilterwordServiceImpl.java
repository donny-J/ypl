package com.jxzg.share.service.impl.v1;

import com.jxzg.share.domain.other.Filterword;
import com.jxzg.share.mapper.FilterwordMapper;
import com.jxzg.share.returndomain.ReturnList;
import com.jxzg.share.service.v1.IFilterwordService;
import com.jxzg.share.toolUtil.SensitiveInit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by 00818Wolf on 2017/2/20.
 */
@Service
public class FilterwordServiceImpl implements IFilterwordService{
    @Autowired
    private FilterwordMapper filterwordMapper;

    @Override
    public Map initfilterword() {
        List<Filterword> filterwords = filterwordMapper.selectAll();
        SensitiveInit sensitiveInit = new SensitiveInit();
        List<Filterword> list = new ArrayList();
        for (Filterword filterword : filterwords) {
            filterword.setWord(filterword.getWord());
            list.add(filterword);
        }
        Map f = sensitiveInit.initKeyWord(list);
        return f;
    }

    @Override
    public ReturnList filterword() {
        List result = new ArrayList();
        List<Filterword> filterwords = filterwordMapper.selecthotword(Filterword.STATUS_HOTWORD);
        for (Filterword filterword : filterwords) {
            result.add(filterword.getWord());
        }
        return new ReturnList(result,"查询成功!");
    }
}
