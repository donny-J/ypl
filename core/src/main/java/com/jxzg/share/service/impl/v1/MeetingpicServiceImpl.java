package com.jxzg.share.service.impl.v1;

import com.jxzg.share.domain.meeting.Meetingpic;
import com.jxzg.share.mapper.MeetingpicMapper;
import com.jxzg.share.querypage.MeetingQueryObject;
import com.jxzg.share.returndomain.ReturnList;
import com.jxzg.share.service.v1.IMeetingpicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by 00818Wolf on 2016/12/22.
 */
@Service
public class MeetingpicServiceImpl implements IMeetingpicService {
    @Autowired
    private MeetingpicMapper meetingpicMapper;

    @Override
    public void insert(Meetingpic meetingpic) {
        meetingpicMapper.insert(meetingpic);
    }

    @Override
    public ReturnList meetingpictypelist(MeetingQueryObject qo) {
        int count = meetingpicMapper.queryForCount(qo);
        List result = new ArrayList();
        if (count > 0) {
            List<Meetingpic> meetingpics = meetingpicMapper.query(qo);
            for (Meetingpic meetingpic : meetingpics) {
                Map<String, Object> map = new HashMap<>();
                String url = meetingpic.getUrl();
                map.put("url", url);
                result.add(map);
            }
        }
        return new ReturnList(result, "查看成功!", count);
    }
}