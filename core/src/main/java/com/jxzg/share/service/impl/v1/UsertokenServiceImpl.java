package com.jxzg.share.service.impl.v1;

import com.jxzg.share.domain.other.Usertoken;
import com.jxzg.share.mapper.UsertokenMapper;
import com.jxzg.share.service.v1.IUsertokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by 00818Wolf on 2016/12/28.
 */
@Service
public class UsertokenServiceImpl implements IUsertokenService{
    @Autowired
    private UsertokenMapper usertokenMapper;

    public void insert(Usertoken usertoken) {
        if(usertoken!=null){
            usertokenMapper.insert(usertoken);
        }
    }

    @Override
    public Usertoken selectUidBytoken(String token) {
        return usertokenMapper.selectUidBytoken(token);
    }

    @Override
    public void updateExpiretime(Usertoken usertoken) {
        usertokenMapper.updateExpiretime(usertoken);
    }

}
