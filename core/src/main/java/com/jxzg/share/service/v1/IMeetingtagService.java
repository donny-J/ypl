package com.jxzg.share.service.v1;

import com.jxzg.share.domain.meeting.Meetingtag;

/**
 * 会议标签
 */
public interface IMeetingtagService {
    /**
     * 添加标签
     * @param meetingtag
     */
    void insert(Meetingtag meetingtag);
}
