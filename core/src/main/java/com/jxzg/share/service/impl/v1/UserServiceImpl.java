package com.jxzg.share.service.impl.v1;

import com.jxzg.share.domain.other.Token;
import com.jxzg.share.domain.other.User;
import com.jxzg.share.domain.other.Usertoken;
import com.jxzg.share.domain.other.Verifycode;
import com.jxzg.share.mapper.UserMapper;
import com.jxzg.share.mapper.UsertokenMapper;
import com.jxzg.share.mapper.VerifycodeMapper;
import com.jxzg.share.pay.alipay.MD5;
import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.service.v1.IUserService;
import com.jxzg.share.vo.PasswordVO;
import com.jxzg.share.vo.VerifyCodeVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 00818Wolf on 2016/12/28.
 */
@Service("userService")
public class UserServiceImpl implements IUserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UsertokenMapper usertokenMapper;
    @Autowired
    private VerifycodeMapper verifycodeMapper;

    @Override
    public JSONResult updatePassword(String token, String newloginpassword) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(token);
        if (newloginpassword == null || token == null || usertoken == null) {
            return new JSONResult(false, "请重新登录!");
        }
        String password = MD5.encode(newloginpassword);
        int upnum = userMapper.updatePassword(password, usertoken.getUid());
        if (upnum == 0) {
            return new JSONResult(false, "修改密码失败!");
        }
        return new JSONResult("密码修改成功!");
    }

    @Override
    //@CacheEvict(value="myCache", key="'personList'+#user.token")
    public JSONResult updateHeadimage(User user) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(user.getToken());
        if (user.getToken() == null || usertoken.getToken() == null) {
            return new JSONResult(false, "请重新登录!");
        }
        userMapper.updateHeadimage(user.getAvatar(), usertoken.getUid());
        return new JSONResult(true, "头像修改成功!");
    }

    @Override
    public Map<String, Object> selectremainmsg(String token) {
        Map<String, Object> result = new HashMap<>();
        Usertoken usertoken = usertokenMapper.selectUidBytoken(token);
        if (token == null || usertoken == null) {
            result.put("msg", "请重新登录");
            result.put("success", false);
            return result;
        }
        int remainmsg = userMapper.selectremainmsg(usertoken.getUid());
        result.put("remainmsg", remainmsg);
        result.put("success", true);
        result.put("msg", "剩余短信条数");
        return result;
    }


    @Override
    public JSONResult verify_password(PasswordVO oldpassword) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(oldpassword.getToken());
        if (oldpassword.getOldloginpassword() == null || oldpassword.getToken() == null || usertoken == null) {
            return new JSONResult(false, "请重新登录!");
        }
        String password = MD5.encode(oldpassword.getOldloginpassword());
        int count = userMapper.selectByPassword(password, usertoken.getUid());
        if (count > 0) {
            return new JSONResult(true, "密码验证成功");
        }
        return new JSONResult(false, "密码错误");
    }

    @Override
    public ReturnMap mywallet(String token) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(token);
        if (token == null || usertoken == null) {
            return new ReturnMap(false, "请重新登录!");
        }
        User user = userMapper.selectUser(usertoken.getUid());
        Map<String, Object> map = new HashMap<>();
        map.put("remainmsg", user.getRemainmsg());
        map.put("balance", user.getBalance());
        return new ReturnMap("查看成功!", map);
    }

    @Override
    public JSONResult SetApplyPassword(String token, String newloginpassword) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(token);
        if (newloginpassword == null || token == null || usertoken == null) {
            return new JSONResult(false, "请重新登录!");
        }
        String password = MD5.encode(newloginpassword);
        int count = userMapper.updateApplyPassword(usertoken.getUid(), password);
        if (count > 0) {
            return new JSONResult("设置成功!");
        } else {
            return new JSONResult(false, "设置失败!");
        }
    }

    @Override
    public JSONResult verifypaypassword(String token, String newloginpassword) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(token);
        if (newloginpassword == null || token == null || usertoken == null) {
            return new JSONResult(false, "请重新登录!");
        }
        String password = MD5.encode(newloginpassword);
        int count = userMapper.selectverifypaypassword(usertoken.getUid(), password);
        if (count > 0) {
            return new JSONResult("验证正确!");
        } else {
            return new JSONResult(false, "密码不正确!");
        }
    }

    @Override
    public ReturnMap checkpaypassword(Token token) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(token.getToken());
        Map<String, Object> map = new HashMap<>();
        if (token.getToken() == null || usertoken == null) {
            return new ReturnMap(false, "请重新登录!");
        }
        String paypassword = userMapper.selectisPaypassword(usertoken.getUid());
        if (paypassword == null) {
            map.put("status", 0);
            return new ReturnMap("没有设置支付密码", map);
        } else {
            map.put("status", 1);
            return new ReturnMap("设置过支付密码", map);
        }
    }

    @Override
    public JSONResult checkpaypassword_msg(VerifyCodeVO verifyCode) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(verifyCode.getToken());
        if (verifyCode.getLoginname() == null || verifyCode.getVerifyCode() == null ||
                verifyCode.getToken() == null || usertoken == null) {
            return new JSONResult(false, "请重新登录!");
        }
        int count = verifycodeMapper.selectByLoginname(verifyCode.getLoginname(), verifyCode.getVerifyCode(), Verifycode.PAY);
        if (count > 0) {
            return new JSONResult(true, "验证码正确!");
        }
        return new JSONResult(false, "验证码错误!");
    }

    @Override
    public void updateBalance(int uid, BigDecimal multiply) {
        userMapper.updateBalance(uid,multiply);
    }

    @Override
    public User selectByUid(int uid) {
        User user = userMapper.selectByUid(uid);
        return user;
    }
}
