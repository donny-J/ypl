package com.jxzg.share.service.v1;

import com.jxzg.share.returndomain.ReturnList;

import java.util.Map;

/**
 * Created by 00818Wolf on 2017/2/20.
 */
public interface IFilterwordService {
    /**
     * 初始化敏感字库
     */
    Map initfilterword();

    /**
     * 热门词搜索
     * @return
     */
    ReturnList filterword();

}
