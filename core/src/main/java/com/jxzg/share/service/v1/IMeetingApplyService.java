package com.jxzg.share.service.v1;

import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.returndomain.ReturnList;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.vo.Spellgroup.SpellgroupVO;

/**
 * 会议报名
 */
public interface IMeetingApplyService {
    /**
     * 会议报名列表
     *
     * @param mid
     */
    ReturnMap meetingapplylist(String token, int mid);

    /**
     * 会议报名，单行文本,多行文本,单选题,多选题
     *
     * @param token
     * @param mid
     */
    ReturnList applymessage(String token, int mid);

    /**
     * 停止报名
     *
     * @param token
     * @param mid
     * @return
     */
    JSONResult stop_meetingapply(String token, int mid);

    /**
     * 拼团报名
     *
     * @param spellgroupVO
     * @return
     */
    ReturnMap Spellgroup_open(SpellgroupVO spellgroupVO);

    ReturnMap get_Invoices(String token, int mid);
}
