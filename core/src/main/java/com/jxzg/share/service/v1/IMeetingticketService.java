package com.jxzg.share.service.v1;

import com.jxzg.share.domain.meeting.Meetingticket;

import java.math.BigDecimal;

/**
 * 会议门票
 */
public interface IMeetingticketService {
    int insert(Meetingticket record);

    /**
     * 查询会议的价格
     * @param mid
     * @return
     */
    BigDecimal selectMeetingTicketPic(int mid);
}
