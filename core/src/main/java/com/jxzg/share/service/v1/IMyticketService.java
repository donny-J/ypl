package com.jxzg.share.service.v1;

import com.jxzg.share.domain.meeting.Myticket;
import com.jxzg.share.querypage.CommetQueryObject;
import com.jxzg.share.querypage.MeetingTicketQueryObject;
import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.returndomain.ReturnList;
import com.jxzg.share.vo.IdAndToken;
import com.jxzg.share.vo.MyticketVO;
import com.jxzg.share.vo.SendTicketVO;

import java.util.List;

/**
 * Created by 00818Wolf on 2017/3/8.
 */
public interface IMyticketService {

    /**
     * 我的门票列表
     * @return
     */
    ReturnList myticket(MeetingTicketQueryObject qo);

    /**
     * 赠送门票
     * @return
     */
    JSONResult sendticket(SendTicketVO sendTicket);

    /**
     * 活动数据
     * @return
     */
    ReturnList activity_data(CommetQueryObject qo);

    /**
     * 我参加的,多张票
     * @return
     */
    ReturnList mymanyticket(MyticketVO myticket);

    List<Myticket> selectByOrderno(String orderno);
}
