package com.jxzg.share.service.impl.v1;

import com.jxzg.share.domain.meeting.*;
import com.jxzg.share.domain.other.User;
import com.jxzg.share.domain.other.Userprofile;
import com.jxzg.share.domain.other.Usertoken;
import com.jxzg.share.domain.pay.Orderdtl;
import com.jxzg.share.domain.pay.Orders;
import com.jxzg.share.mapper.*;
import com.jxzg.share.querypage.CommetQueryObject;
import com.jxzg.share.querypage.MeetingTicketQueryObject;
import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.returndomain.ReturnList;
import com.jxzg.share.service.v1.IMyticketService;
import com.jxzg.share.vo.IdAndToken;
import com.jxzg.share.vo.MyticketVO;
import com.jxzg.share.vo.SendTicketVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by 00818Wolf on 2017/3/8.
 */
@Service("myticketService")
public class MyticketServiceImpl implements IMyticketService {
    @Autowired
    private MyticketMapper myticketMapper;
    @Autowired
    private UsertokenMapper usertokenMapper;
    @Autowired
    private MeetingMapper meetingMapper;
    @Autowired
    private SignuserinfoMapper signuserinfoMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private OrderdtlMapper orderdtlMapper;
    @Autowired
    private UserprofileMapper userprofileMapper;
    @Autowired
    private OrdersMapper ordersMapper;
    @Autowired
    private SendTicketRecordMapper sendTicketRecordMapper;
    @Autowired
    private SpellgroupMapper spellgroupMapper;
    @Autowired
    private MeetingticketMapper meetingticketMapper;

    @Override
    public ReturnList myticket(MeetingTicketQueryObject qo) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(qo.getToken());
        if (qo.getToken() == null || usertoken == null) {
            return new ReturnList("请重新登录!", false);
        }
        List<Integer> mids = meetingMapper.selectQoMid(qo);
        int count = 0;
        List result = new ArrayList();
        List<Myticket> mytickets = null;
        if (mids.size() != 0) {
            qo.setMids(mids);
            qo.setUid(usertoken.getUid());
            count = myticketMapper.queryMyTicketCount(qo);
            int sendTicketCount = myticketMapper.selectSendTicketCount(qo);
            count += sendTicketCount;
            mytickets = myticketMapper.selectByUids(qo);
            List<Myticket> sendTickets = myticketMapper.selectSendTickets(qo);
            mytickets.addAll(sendTickets);
        } else {
            return new ReturnList(result, "成功!", 0);
        }
        if (count > 0) {
            for (Myticket myticket : mytickets) {
                Meeting meeting = meetingMapper.selectMeetingByMid(myticket.getMid());
                int countnnum = Meeting.MEETINGTYPEA_TEAM;//发团购和增票都是1
                if (meeting.getMeetingtype() == Meeting.MEETINGTYPE_ACTIVITY && myticket.getTickettype() != Myticket.TICKETTYPE_SENDTICKET) {
                    countnnum = myticketMapper.selectCountByMid(myticket.getMid(), myticket.getUid(), myticket.getOrderno(), Myticket.TICKETTYPE_MYTICKET);
                }
                Map<String, Object> map = new HashMap<>();
                map.put("meetingname", meeting.getName());
                map.put("meetingmid", meeting.getMid());
                map.put("meetingstarttime", meeting.getStarttime());
                map.put("meetingtype", meeting.getMeetingtype());
                map.put("meetingendtime", meeting.getEndtime());
                map.put("signstatus", myticket.getNo());
                String ss = meeting.getAddress();
                String[] s = ss.split("-");
                map.put("meetingaddress", s[1]);
                Date date = new Date();
                if (meeting.getStatus() == Meeting.MEETING_STATUS_ACTICITY_NO || meeting.getStatus() == Meeting.MEETING_STATUS_ACTIVITY_APPLY) {
                    map.put("meetingstatus", meeting.getStatus());
                    map.put("countdown", meeting.getStarttime().getTime() - date.getTime());
                } else {
                    map.put("meetingstatus", meeting.getStatus());
                }
                map.put("qcode", myticket.getQcode());
                if (meeting.getMeetingtype() == Meeting.MEETINGTYPEA_TEAM) {
                    Spellgroup spellgroup = spellgroupMapper.selectByTicketid(myticket.getId());
                    if (spellgroup != null) {
                        map.put("spellgroupstatus", spellgroup.getStatus());
                        if (spellgroup.getType() == Spellgroup.TYPE_HEADER) {
                            map.put("spellgroupid", spellgroup.getId());
                        } else {
                            map.put("spellgroupid", spellgroup.getTid());
                        }
                    }
                }
                map.put("ticketnum", countnnum);
                map.put("tickettype", myticket.getTickettype());
                map.put("tid", myticket.getId());
                result.add(map);
            }
            return new ReturnList(result, "成功!", count);
        }
        return new ReturnList(result, "成功!", 0);
    }

    @Override
    public ReturnList mymanyticket(MyticketVO mt) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(mt.getToken());
        if (mt.getMid() == 0 || mt.getTid() == 0 || mt.getToken() == null || usertoken == null) {
            return new ReturnList("请重新登录!", false);
        }
        List result = new ArrayList();
        Myticket myticket = myticketMapper.selectByPrimaryKey(mt.getTid());
        Meeting meeting = meetingMapper.selectMeetingByMid(myticket.getMid());
        Date date = new Date();
        if (mt.getTicketnum() == 1) {
            Signuserinfo signuserinfo = signuserinfoMapper.selectById(myticket.getId(), Signuserinfo.FIELD_NAME);
            Meetingticket meetingticket = meetingticketMapper.selectByPrimaryKey(myticket.getTicketid());
            Map<String, Object> map = new HashMap<>();
            map.put("meetingname", meeting.getName());
            map.put("meetingmid", meeting.getMid());
            map.put("meetingstarttime", meeting.getStarttime());
            map.put("meetingtype", meeting.getMeetingtype());
            map.put("meetingendtime", meeting.getEndtime());
            String ss = meeting.getAddress();
            String[] s = ss.split("-");
            map.put("meetingaddress", s[1]);
            map.put("qcode", myticket.getQcode());
            map.put("signcode", myticket.getSigncode());
            map.put("ticketname", myticket.getCreateip());//门票名称
            map.put("username", signuserinfo.getFieldval());//参会人名称
            map.put("signstatus", myticket.getNo());
            map.put("tickettype", myticket.getTickettype());
            map.put("tid", mt.getTid());
            if (meeting.getMeetingtype() == Meeting.MEETINGTYPEA_TEAM) {
                Spellgroup spellgroup = spellgroupMapper.selectByTicketid(mt.getTid());
                int count = 0;
                if (spellgroup.getTid() == 0) {
                    count = spellgroupMapper.selectCountByTid(spellgroup.getId(), meeting.getMid());
                } else {
                    count = spellgroupMapper.selectCountByTid(spellgroup.getTid(), meeting.getMid());
                }
                if (spellgroup.getType() == Spellgroup.TYPE_HEADER) {
                    map.put("spellgroupid", spellgroup.getId());
                    map.put("spellgroupcount", meeting.getScale() - count);
                } else {
                    map.put("spellgroupid", spellgroup.getTid());
                    map.put("spellgroupcount", meeting.getScale() - count);
                }
                map.put("spellgroupstatus", spellgroup.getStatus());
                map.put("spellgrouptime", meeting.getUpdatetime().getTime() - date.getTime());
            }
            if (meeting.getStatus() == Meeting.MEETING_STATUS_ACTICITY_NO || meeting.getStatus() == Meeting.MEETING_STATUS_ACTIVITY_APPLY) {
                map.put("meetingstatus", meeting.getStatus());
                map.put("countdown", meeting.getStarttime().getTime() - date.getTime());
            } else {
                map.put("meetingstatus", meeting.getStatus());
            }
            result.add(map);
        } else {
            List<Myticket> mytickets = myticketMapper.selectByOrderno(myticket.getOrderno());
            for (Myticket mts : mytickets) {
                Signuserinfo signuserinfo = signuserinfoMapper.selectById(mts.getId(), Signuserinfo.FIELD_NAME);
                Meetingticket meetingticket = meetingticketMapper.selectByPrimaryKey(mts.getTicketid());
                Map<String, Object> map = new HashMap<>();
                map.put("meetingname", meeting.getName());
                map.put("meetingmid", meeting.getMid());
                map.put("meetingstarttime", meeting.getStarttime());
                map.put("meetingtype", meeting.getMeetingtype());
                map.put("meetingendtime", meeting.getEndtime());
                String ss = meeting.getAddress();
                String[] s = ss.split("-");
                map.put("meetingaddress", s[1]);
                map.put("qcode", mts.getQcode());
                map.put("signcode", mts.getSigncode());
                map.put("ticketname", mts.getCreateip());//门票名称
                map.put("username", signuserinfo.getFieldval());//参会人名称
                map.put("tickettype", mts.getTickettype());
                map.put("tid", mts.getId());
                if (meeting.getStatus() == Meeting.MEETING_STATUS_ACTICITY_NO || meeting.getStatus() == Meeting.MEETING_STATUS_ACTIVITY_APPLY) {
                    map.put("meetingstatus", meeting.getStatus());
                    map.put("countdown", meeting.getStarttime().getTime() - date.getTime());
                } else {
                    map.put("meetingstatus", meeting.getStatus());
                }
                result.add(map);
            }
        }
        return new ReturnList(result, "查看成功!", mt.getTicketnum());
    }

    @Override
    public JSONResult sendticket(SendTicketVO sendTicket) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(sendTicket.getToken());
        if (sendTicket.getTid() == 0 || sendTicket.getLoginname() == null || sendTicket.getToken() == null || usertoken == null) {
            return new JSONResult(false, "请重新登陆!");
        }
        User user = userMapper.selectByLoginname(sendTicket.getLoginname());//被赠送的人
        if (user == null) {
            return new JSONResult(false, "赠票失败!该用户尚未注册!");
        }
        if (user.getUid() == usertoken.getUid()) {
            return new JSONResult(false, "赠票失败!不能送给自己!");
        }
        Myticket myticket = myticketMapper.selectByPrimaryKey(sendTicket.getTid());
        if (myticket.getNo() == Myticket.MYTICKET_NO_YES) {
            return new JSONResult(false, "赠票失败!票已签到!");
        }
        Date date = new Date();
        myticketMapper.updateByPrimaryKey(sendTicket.getTid(), user.getUid());
        SendTicketRecord std = new SendTicketRecord();
        std.setUid(usertoken.getUid());
        std.setMid(myticket.getMid());
        std.setSid(user.getUid());
        std.setTid(sendTicket.getTid());
        std.setCreatetime(date);
        std.setUpdatetime(date);
        sendTicketRecordMapper.insert(std);
        return new JSONResult("赠票成功!");
    }

    @Override
    public ReturnList activity_data(CommetQueryObject qo) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(qo.getToken());
        if (qo.getMid() == 0 || qo.getToken() == null || usertoken == null) {
            return new ReturnList("请重新登陆!", false);
        }
        List returnlist = new ArrayList();
        int amount = orderdtlMapper.selectActivityCount(qo);
        if (amount > 0) {
            List<Orderdtl> orderdtls = orderdtlMapper.selectByMid(qo);
            for (Orderdtl orderdtl : orderdtls) {
                Map<String, Object> map = new HashMap<>();
                Orders orders = ordersMapper.selectByOrderno(orderdtl.getOrderno());
                Userprofile userprofile = userprofileMapper.selectByUid(orders.getUid());
                User user = userMapper.selectByUid(orders.getUid());
                int count = orderdtlMapper.selectCountByOrderno(orders.getOrderno());
                map.put("loginname", user.getLoginname());
                map.put("nickname", userprofile.getNickname());
                map.put("avatar", user.getAvatar());
                map.put("time", orders.getUpdatetime());
                map.put("ticketcount", count);
                List<Orderdtl> orderdtls1 = orderdtlMapper.selectByOrderno(orderdtl.getOrderno());
                List list = new ArrayList();
                for (Orderdtl orderdtl1 : orderdtls1) {
                    Myticket mt = myticketMapper.selectByPrimaryKey(orderdtl1.getProductid());
                    Map<String, Object> smap = new HashMap<>();
                    Signuserinfo sn = signuserinfoMapper.selectById(mt.getId(), Signuserinfo.FIELD_NAME);
                    Signuserinfo sp = signuserinfoMapper.selectById(mt.getId(), Signuserinfo.FIELE_PHONE);
                    smap.put("name", sn.getFieldval());
                    smap.put("tel", sp.getFieldval());
                    smap.put("ticketname", mt.getCreateip());
                    smap.put("tocketstatus", mt.getNo());
                    list.add(smap);
                    map.put("order", list);
                }
                returnlist.add(map);
            }
            return new ReturnList(returnlist, "成功", amount);
        }
        return new ReturnList(returnlist, "成功", 0);
    }

    @Override
    public List<Myticket> selectByOrderno(String orderno) {
        return myticketMapper.selectByOrderno(orderno);
    }
}