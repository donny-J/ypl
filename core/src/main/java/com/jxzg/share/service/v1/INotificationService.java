package com.jxzg.share.service.v1;

import com.jxzg.share.domain.other.Notification;
import com.jxzg.share.returndomain.JSONResult;

/**
 * Created by 00818Wolf on 2017/3/3.
 */
public interface INotificationService {

    /**
     * 站内信
     * @param notification
     * @return
     */
    JSONResult send_notification(Notification notification);

}
