package com.jxzg.share.service.v1;

import com.jxzg.share.domain.other.Token;
import com.jxzg.share.domain.other.User;
import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.vo.PasswordVO;
import com.jxzg.share.vo.VerifyCodeVO;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 用户相关
 */
public interface IUserService {

    /**
     * 当前密码正确,修改密码
     * @param newloginpassword 新密码
     */
    JSONResult updatePassword(String token,String newloginpassword);


    /**
     * 修改头像
     */
    JSONResult updateHeadimage(User user);

    /**
     * 短信剩余条数
     * @param token
     * @return
     */
    Map<String, Object> selectremainmsg(String token);


    /**
     * 我的钱包
     * @param token
     * @return
     */
    ReturnMap mywallet(String token);

    /**
     * 验证原密码
     * @param oldpassword
     * @return
     */
    JSONResult verify_password(PasswordVO oldpassword);

    JSONResult SetApplyPassword(String token, String newloginpassword);

    JSONResult verifypaypassword(String token, String newloginpassword);

    ReturnMap checkpaypassword(Token token);

    JSONResult checkpaypassword_msg(VerifyCodeVO verifyCode);

    void updateBalance(int uid, BigDecimal multiply);

    User selectByUid(int uid);
}
