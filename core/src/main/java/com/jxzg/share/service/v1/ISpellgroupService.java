package com.jxzg.share.service.v1;

import com.jxzg.share.domain.meeting.Spellgroup;
import org.springframework.stereotype.Service;

/**
 * Created by 00818Wolf on 2017/6/6.
 */
public interface ISpellgroupService {
    Spellgroup selectByTicketid(int id);
}
