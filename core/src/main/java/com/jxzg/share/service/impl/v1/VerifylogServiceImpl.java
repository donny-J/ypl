package com.jxzg.share.service.impl.v1;

import com.jxzg.share.domain.meeting.Myticket;
import com.jxzg.share.domain.meeting.Signuserinfo;
import com.jxzg.share.domain.other.*;
import com.jxzg.share.mapper.*;
import com.jxzg.share.pay.alipay.MD5;
import com.jxzg.share.querypage.PageResult;
import com.jxzg.share.querypage.QueryObject;
import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.service.v1.ILoginlogService;
import com.jxzg.share.service.v1.IVerifylogService;
import com.jxzg.share.toolUtil.DateUtil;
import com.jxzg.share.toolUtil.JuheDemo;
import com.jxzg.share.vo.SendNoteVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

import static com.jxzg.share.toolUtil.JuheDemo.getRequest2;

@Service
public class VerifylogServiceImpl implements IVerifylogService {
    @Autowired
    private VerifylogMapper verifylogMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private ILoginlogService loginlogService;
    @Autowired
    private VerifycodeMapper verifycodeMapper;
    @Autowired
    private UsertokenMapper usertokenMapper;
    @Autowired
    private UserprofileMapper userprofileMapper;
    @Autowired
    private SignuserinfoMapper signuserinfoMapper;
    @Autowired
    private MyticketMapper myticketMapper;
    @Autowired
    private NotificationMapper notificationMapper;

    public ReturnMap register(String loginname, String loginpassword, String verifyCode, String ip, BigDecimal lat, BigDecimal lng) {
        if (loginname == null || loginpassword == null || verifyCode == null) {
            return new ReturnMap(false, "用户名,密码和验证码不能为空!");
        }
        //判断用户是否存在
        int count = userMapper.countByLoginname(loginname);
        if (count > 0) {
            return new ReturnMap(false, "用户名已经存在!");
        }
        String uuid = UUID.randomUUID().toString();
        if (loginlogService.validate(loginname, verifyCode, 1)) {
            //如果存在
            Verifylog vl = new Verifylog();
            vl.setCreatetime(new Date());
            vl.setStatus(Verifylog.STATUS_LOSER);
            vl.setType(Verifylog.TYPE_PHONE);
            vl.setUpdatetime(new Date());
            vl.setContent(verifyCode);//验证码
            verifylogMapper.insert(vl);
            return new ReturnMap(false, "验证码输入错误!");
        } else {
            //初始化用户信息
            User user = new User();
            user.setBalance(User.BALANCE_ZERO);
            user.setCredits(User.CREDITS_ZERO);
            user.setLv(User.LV_ZERO);
            user.setEmailstatus(User.EMAIL_NOVERIFY);
            user.setLoginname(loginname);
            user.setMobile(loginname);
            String password = MD5.encode(loginpassword);
            user.setLoginpassword(password);
            user.setRegtime(new Date());
            user.setRegip(ip);
            user.setRemainmsg(User.CREDITS_ZERO);
            user.setLng(lng);
            user.setLat(lat);
            userMapper.insert(user);
            Userprofile userprofile = new Userprofile();
            userprofile.setUid(user.getUid());
            userprofile.setNickname("未设置");
            userprofile.setRealname("未设置");
            userprofile.setAddress("未设置");
            userprofileMapper.insert(userprofile);
            userMapper.updateStatus(user.getUid());
            //验证码验证成功后,保存验证日志
            //验证码校验
            Verifylog vl = new Verifylog();
            vl.setCreatetime(new Date());
            vl.setStatus(Verifylog.STATUS_SUCCESS);
            vl.setType(Verifylog.TYPE_PHONE);
            vl.setUpdatetime(new Date());
            vl.setUid(user.getUid());
            verifylogMapper.insert(vl);
            Usertoken usertoken = new Usertoken();
            usertoken.setLoginip(ip);
            usertoken.setLat(lat);
            usertoken.setLng(lng);
            usertoken.setUid(user.getUid());
            usertoken.setLoginname(user.getLoginname());
            usertoken.setLogintime(new Date());
            usertoken.setLogintype(Usertoken.LOGINTYPE_MOVE);
            usertoken.setToken(uuid);
            usertoken.setExpiretime(DateUtil.getExpiretime());//终止时间,一个月后
            usertokenMapper.insert(usertoken);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("token", uuid);
        map.put("f1", true);
        return new ReturnMap("注册成功!", map);
    }

    @Override
    public ReturnMap register_pc(String loginname, String loginpassword, String verifyCode, String ip, BigDecimal lat, BigDecimal lng) {
        if (loginname == null || loginpassword == null || verifyCode == null) {
            return new ReturnMap(false, "用户名,密码和验证码不能为空!");
        }
        //判断用户是否存在
        int count = userMapper.countByLoginname(loginname);
        if (count > 0) {
            return new ReturnMap(false, "用户名已经存在!");
        }
        String uuid = UUID.randomUUID().toString();
        if (loginlogService.validate(loginname, verifyCode, 1)) {
            //如果存在
            Verifylog vl = new Verifylog();
            vl.setCreatetime(new Date());
            vl.setStatus(Verifylog.STATUS_LOSER);
            vl.setType(Verifylog.TYPE_PHONE);
            vl.setUpdatetime(new Date());
            vl.setContent(verifyCode);//验证码
            verifylogMapper.insert(vl);
            return new ReturnMap(false, "验证码输入错误!");
        } else {
            //初始化用户信息
            User user = new User();
            user.setBalance(User.BALANCE_ZERO);
            user.setCredits(User.CREDITS_ZERO);
            user.setLv(User.LV_ZERO);
            user.setEmailstatus(User.EMAIL_NOVERIFY);
            user.setLoginname(loginname);
            user.setMobile(loginname);
            String password = MD5.encode(loginpassword);
            user.setLoginpassword(password);
            user.setRegtime(new Date());
            user.setRegip(ip);
            user.setRemainmsg(User.CREDITS_ZERO);
            user.setLng(lng);
            user.setLat(lat);
            userMapper.insert(user);
            Userprofile userprofile = new Userprofile();
            userprofile.setUid(user.getUid());
            userprofile.setNickname("未设置");
            userprofile.setRealname("未设置");
            userprofile.setAddress("未设置");
            userprofileMapper.insert(userprofile);
            userMapper.updateStatus(user.getUid());
            //验证码验证成功后,保存验证日志
            //验证码校验
            Verifylog vl = new Verifylog();
            vl.setCreatetime(new Date());
            vl.setStatus(Verifylog.STATUS_SUCCESS);
            vl.setType(Verifylog.TYPE_PHONE);
            vl.setUpdatetime(new Date());
            vl.setUid(user.getUid());
            verifylogMapper.insert(vl);
            Usertoken usertoken = new Usertoken();
            usertoken.setLoginip(ip);
            usertoken.setLat(lat);
            usertoken.setLng(lng);
            usertoken.setUid(user.getUid());
            usertoken.setLoginname(user.getLoginname());
            usertoken.setLogintime(new Date());
            usertoken.setLogintype(Usertoken.LOGINTYPE_PC);
            usertoken.setToken(uuid);
            usertoken.setExpiretime(DateUtil.getExpiretime());//终止时间,一个月后
            usertokenMapper.insert(usertoken);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("token", uuid);
        map.put("f1", true);
        return new ReturnMap("注册成功!", map);
    }

    @Override
    public JSONResult sendVerifyCode(String loginname, int type) {
        if (loginname == null) {
            return new JSONResult(false, "账号异常,请重新登录");
        }
        User user = userMapper.selectByLoginname(loginname);
        if (type == 1) {
            if (user != null) {
                return new JSONResult(false, "该账号已存在!");
            }
        } else if (type == 2) {//忘记密码
            if (user == null) {
                return new JSONResult(false, "该账号不存在!");
            }
        } else if (type == 3) {
            if (user != null) {
                return new JSONResult(false, "该账号已存在!");
            }
        }
        int c = (int) (Math.random() * (9999 - 1000 + 1)) + 1000;
        String code = String.valueOf(c);
        getRequest2(loginname, code, "31530");
        Verifycode ve = new Verifycode();
        ve.setCreatetime(new Date());
        ve.setLoginname(loginname);
        ve.setVerifycode(code);
        ve.setUpdatetime(new Date());
        ve.setCreatetime(new Date());
        ve.setType(type);
        verifycodeMapper.deleteByloginname(loginname, type);
        verifycodeMapper.insert(ve);
        return new JSONResult("发送成功!");
    }

    @Override
    public PageResult query(QueryObject qo) {
        int count = verifylogMapper.queryForCount(qo);
        if (count > 0) {
            List<Verifylog> result = verifylogMapper.query(qo);
            return new PageResult(result, qo.getPagesize(), qo.getCurrentpage(), count);
        }
        return new PageResult(new ArrayList<Loginlog>(), qo.getPagesize(), 1, 0);
    }

    @Override
    public JSONResult send_note(SendNoteVO sendNote) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(sendNote.getToken());
        if (sendNote.getToken() == null || usertoken == null) {
            return new JSONResult(false, "请重新登录!");
        }
        Date date = new Date();
        if (sendNote.getMid() != null) {
            User user = userMapper.selectByUid(usertoken.getUid());
            List<Myticket> mytickets = myticketMapper.selectByMidAndStatus(sendNote.getMid(), Myticket.ORDER_STATUS_YES_PAY);
            if (user.getRemainmsg() < mytickets.size()) {
                return new JSONResult(false, "短信余额不足,请充值!");
            }
            for (Myticket myticket : mytickets) {
                Signuserinfo signuserinfo = signuserinfoMapper.selectById(myticket.getId(), Signuserinfo.FIELE_PHONE);
                if (sendNote.getSendtime() != null) {
                    Notification notification = new Notification();
                    notification.setNtype(Notification.NTYPE_MESSAGE);
                    notification.setMsgtype(Notification.MSGTYPE_ACATIVITY);
                    notification.setContent(sendNote.getCode());
                    notification.setMsgfrom(String.valueOf(user.getUid()));
                    notification.setUid(myticket.getUid());
                    notification.setTarget(signuserinfo.getFieldval());
                    notification.setCreatetime(date);
                    try {
                        notification.setUpdatetime(DateUtil.longToDate(sendNote.getSendtime()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    notification.setStatus(Notification.STATUS_INIT);
                    notificationMapper.insert(notification);
                } else {
                    System.out.println(signuserinfo.getFieldval());
                    JuheDemo.getRequest2(signuserinfo.getFieldval(), sendNote.getCode(), "31530");
                    Notification notification = new Notification();
                    notification.setNtype(Notification.NTYPE_MESSAGE);
                    notification.setMsgtype(Notification.MSGTYPE_ACATIVITY);
                    notification.setContent(sendNote.getCode());
                    notification.setMsgfrom(String.valueOf(user.getUid()));
                    notification.setUid(myticket.getUid());
                    notification.setTarget(signuserinfo.getFieldval());
                    notification.setCreatetime(date);
                    notification.setUpdatetime(date);
                    //notification.setStatus(Notification.STATUS_INIT);
                    notificationMapper.insert(notification);
                    userMapper.updateMsg(usertoken.getUid(), -1);
                }
            }
        }
        return new JSONResult("发送成功!");
    }
}
