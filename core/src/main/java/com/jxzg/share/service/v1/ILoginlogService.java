package com.jxzg.share.service.v1;

import com.jxzg.share.domain.other.Loginlog;
import com.jxzg.share.querypage.PageResult;
import com.jxzg.share.querypage.QueryObject;
import com.jxzg.share.returndomain.ReturnMap;

/**
 * 登录
 */
public interface ILoginlogService {
    /**
     * 登录
     */
    ReturnMap login(Loginlog userinfo, String ip);

    /**
     * 验证码校验
     * @param loginname
     * @param verifyCode
     * @return
     */
    boolean validate(String loginname, String verifyCode,int type);

    /**
     * 查询所有+分页
     */
    PageResult query(QueryObject qo);

    ReturnMap pclogin(Loginlog userinfo, String remoteAddr);
}
