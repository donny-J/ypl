package com.jxzg.share.service.impl.v1;

import com.jxzg.share.domain.other.SysParam;
import com.jxzg.share.domain.pay.Chargelog;
import com.jxzg.share.mapper.ChargelogMapper;
import com.jxzg.share.mapper.SysParamMapper;
import com.jxzg.share.querypage.SysParamModQueryObject;
import com.jxzg.share.service.v1.IChargelogService;
import com.jxzg.share.service.v1.ISysParamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by 00818wolf on 2017/5/27.
 */
@Service
public class SysParamServiceImpl implements ISysParamService {
    @Autowired
    private SysParamMapper sysParamMapper;

    @Override
    public List<SysParam> getSysParamsByCon(SysParamModQueryObject queryObject) {
        return sysParamMapper.getSysParamsByCon(queryObject);
    }
}
