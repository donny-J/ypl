package com.jxzg.share.service.v1;

import com.jxzg.share.domain.pay.Payee;

/**
 * Created by 00818wolf on 2017/5/27.
 */
public interface IPayeeService {
    void insert(Payee payee);
}
