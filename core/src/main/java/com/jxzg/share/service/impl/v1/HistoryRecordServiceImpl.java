package com.jxzg.share.service.impl.v1;

import com.jxzg.share.domain.other.HistoryRecord;
import com.jxzg.share.domain.other.Usertoken;
import com.jxzg.share.mapper.HistoryRecordMapper;
import com.jxzg.share.mapper.UsertokenMapper;
import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.service.v1.IHistoryRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by 00818Wolf on 2017/3/28.
 */
@Service
public class HistoryRecordServiceImpl implements IHistoryRecordService{
    @Autowired
    private HistoryRecordMapper historyRecordMapper;
    @Autowired
    private UsertokenMapper usertokenMapper;
    @Override
    public JSONResult historyrecord_save(HistoryRecord historyRecord) {
        HistoryRecord hr = new HistoryRecord();
        if(historyRecord.getToken()==null){
            hr.setUid(HistoryRecord.UID_TOURIST);
            hr.setStatus(HistoryRecord.STATUS_TOURIST);
        }else{
            Usertoken usertoken = usertokenMapper.selectUidBytoken(historyRecord.getToken());
            hr.setUid(usertoken.getUid());
            hr.setStatus(HistoryRecord.STATUS_USER);
        }
        Date date = new Date();
        hr.setCreatetime(date);
        hr.setUpdatetime(date);
        hr.setIp(historyRecord.getIp());
        hr.setLat(historyRecord.getLat());
        hr.setLng(historyRecord.getLng());
        hr.setContent(historyRecord.getContent());
        historyRecordMapper.insert(hr);
        return new JSONResult("保存成功!");
    }
}
