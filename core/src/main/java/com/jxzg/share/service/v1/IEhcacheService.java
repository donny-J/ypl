package com.jxzg.share.service.v1;

/**
 * Created by 00914Donny on 2017/6/3.
 */
public interface IEhcacheService {
    public String getUrlFromCache(String url);
    public String putUrlToCache(String url);

}
