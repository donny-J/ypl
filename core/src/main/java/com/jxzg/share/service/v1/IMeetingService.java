package com.jxzg.share.service.v1;

import com.jxzg.share.domain.meeting.Meeting;
import com.jxzg.share.domain.meeting.Spellgroup;
import com.jxzg.share.querypage.MeetingQueryObject;
import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.returndomain.ReturnList;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.vo.ActivityMeeting.MeetingBody;
import com.jxzg.share.vo.IdAndToken;
import com.jxzg.share.vo.Spellgroup.SpellgroupBody;

import java.util.List;

/**
 * 会议
 */
public interface IMeetingService {


    /**
     * 创建会议(活动)
     *
     * @return
     */
    ReturnMap addmetting(MeetingBody meeting);

    /**
     * 创建会议(组团会议)
     *
     * @param body
     * @return
     */
    ReturnMap cluster_meeting(SpellgroupBody body);

    /**
     * 轮播图
     *
     * @return
     */
    List selectCarouselfigure();

    /**
     * 我的发布
     *
     * @return
     */
    ReturnList mymeeting(MeetingQueryObject qo);

    /**
     * 拼团详情
     *
     * @param spellgroup
     * @return
     */
    ReturnMap spellgroup_details(Spellgroup spellgroup);

    /**
     * 会议显示列表(推荐)
     *
     * @return
     */
    ReturnList selectMeetingList(MeetingQueryObject qo);

    /**
     * 会议显示列表(关注)
     *
     * @param qo
     * @return
     */
    ReturnList meeting_attention(MeetingQueryObject qo);

    /**
     * 会议显示(附近)
     *
     * @param qo
     * @return
     */
    ReturnList meeting_nearby(MeetingQueryObject qo);

    /**
     * 编辑会议
     *
     * @param idAndToken
     * @return
     */
    ReturnMap update_meeting_see(IdAndToken idAndToken);

    JSONResult update_meeting(MeetingBody meetingBody);

    JSONResult del_meeting(IdAndToken idAndToken);

    Meeting selectMeetingByMid(int mid);
}
