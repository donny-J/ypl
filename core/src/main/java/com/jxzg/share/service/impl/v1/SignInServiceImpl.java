package com.jxzg.share.service.impl.v1;

import com.jxzg.share.domain.meeting.Meeting;
import com.jxzg.share.domain.meeting.Myticket;
import com.jxzg.share.domain.meeting.Signlog;
import com.jxzg.share.domain.other.Usertoken;
import com.jxzg.share.mapper.MeetingMapper;
import com.jxzg.share.mapper.MyticketMapper;
import com.jxzg.share.mapper.SignlogMapper;
import com.jxzg.share.mapper.UsertokenMapper;
import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.service.v1.ISignInService;
import com.jxzg.share.vo.SignInVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by 00818Wolf on 2017/5/19.
 */
@Service
public class SignInServiceImpl implements ISignInService {
    @Autowired
    private UsertokenMapper usertokenMapper;
    @Autowired
    private MyticketMapper myticketMapper;
    @Autowired
    private SignlogMapper signlogMapper;
    @Autowired
    private MeetingMapper meetingMapper;

    @Override
    public JSONResult signin(SignInVO signInVO) {
        if (signInVO.getToken() == null || signInVO.getCode() == null || signInVO.getMid() == 0) {
            return new JSONResult(false, "账号异常!请重新输入");
        }
        Usertoken usertoken = usertokenMapper.selectUidBytoken(signInVO.getToken());
        if (usertoken.getToken() == null) {
            return new JSONResult(false, "账号异常!请重新输入");
        }
        Date date = new Date();
        if (signInVO.getType() == SignInVO.TYPE_CODE_ZERO) {
            String signcode = signInVO.getCode();
            String[] code = signcode.split(":");
            int ticketid = Integer.parseInt(code[1]);
            Myticket myticket = myticketMapper.selectByPrimaryKey(ticketid);
            Meeting meeting = meetingMapper.selectMeetingByMid(myticket.getMid());
            if (myticket.getMid() != signInVO.getMid() || meeting.getUid() != usertoken.getUid()) {
                return new JSONResult(false, "签到异常,请联系主办方!");
            }
            if (myticket != null) {
                if (myticket.getNo() == Myticket.MYTICKET_NO_YES) {
                    return new JSONResult(false, "该门票已经签到!");
                }
                myticketMapper.updateSignStatus(ticketid);
                Signlog signlog = new Signlog();
                signlog.setMid(myticket.getMid());
                signlog.setNtype(SignInVO.TYPE_CODE_ZERO);
                signlog.setContent(signInVO.getCode());
                signlog.setUid(myticket.getUid());
                signlog.setTarget(meeting.getName());
                signlog.setStatus(Signlog.STATUS_SUCCESS);
                signlog.setCreatetime(date);
                signlog.setUpdatetime(date);
                signlogMapper.insert(signlog);
                return new JSONResult("签到成功!");
            } else {
                return new JSONResult(false, "签到码错误!");
            }
        } else {
            String signcode = signInVO.getCode();
            Myticket myticket = myticketMapper.selectBySignCode(signcode,signInVO.getMid());
            if (myticket == null) {
                return new JSONResult(false, "签到码错误!");
            }
            Meeting meeting = meetingMapper.selectMeetingByMid(myticket.getMid());
            if (meeting.getUid() != usertoken.getUid()) {
                return new JSONResult(false, "签到异常,请联系主办方!");
            }
            if (myticket.getNo() == Myticket.MYTICKET_NO_YES) {
                return new JSONResult(false, "该门票已经签到!");
            }
            myticketMapper.updateSignStatus(myticket.getId());
            Signlog signlog = new Signlog();
            signlog.setMid(myticket.getMid());
            signlog.setNtype(SignInVO.TYPE_CODE_ZERO);
            signlog.setContent(signInVO.getCode());
            signlog.setUid(myticket.getUid());
            signlog.setTarget(meeting.getName());
            signlog.setStatus(Signlog.STATUS_SUCCESS);
            signlog.setCreatetime(date);
            signlog.setUpdatetime(date);
            signlogMapper.insert(signlog);
            return new JSONResult("签到成功!");
        }
    }
}
