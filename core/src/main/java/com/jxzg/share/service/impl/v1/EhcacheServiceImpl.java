package com.jxzg.share.service.impl.v1;

import com.jxzg.share.service.v1.IEhcacheService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * Created by 00914Donny on 2017/6/3.
 */
@Service
public class EhcacheServiceImpl implements IEhcacheService{

    private static final Logger log = LoggerFactory.getLogger(EhcacheServiceImpl.class);

    @Override
    @Cacheable(value="urlCache",key="'sameUrlCache'+#url")
    public String getUrlFromCache(String url) {
        log.debug("url not in ehcache"+url);
        return null;
    }

    @Override
    @CachePut(value="urlCache",key="'sameUrlCache'+#url")
    public String putUrlToCache(String url) {
        log.debug("add key to ehcache"+url);
        return url;
    }


}
