package com.jxzg.share.service.impl.v1;

import com.jxzg.share.domain.meeting.Meetingtag;
import com.jxzg.share.mapper.MeetingtagMapper;
import com.jxzg.share.service.v1.IMeetingtagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by 00818Wolf on 2017/1/3.
 */
@Service
public class MeetingtagServiceImpl implements IMeetingtagService {
    @Autowired
    private MeetingtagMapper meetingtagMapper;
    @Override
    public void insert(Meetingtag meetingtag) {
        meetingtagMapper.insert(meetingtag);
    }
}
