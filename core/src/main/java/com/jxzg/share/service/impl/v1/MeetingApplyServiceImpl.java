package com.jxzg.share.service.impl.v1;

import com.jxzg.share.domain.meeting.*;
import com.jxzg.share.domain.other.Collectattention;
import com.jxzg.share.domain.other.User;
import com.jxzg.share.domain.other.Userprofile;
import com.jxzg.share.domain.other.Usertoken;
import com.jxzg.share.domain.pay.Orderdtl;
import com.jxzg.share.domain.pay.Orders;
import com.jxzg.share.mapper.*;
import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.returndomain.ReturnList;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.service.v1.IMeetingApplyService;
import com.jxzg.share.toolUtil.DateUtil;
import com.jxzg.share.toolUtil.OrdernoUtil;
import com.jxzg.share.toolUtil.QRcodeImageUtil;
import com.jxzg.share.toolUtil.RandomNum;
import com.jxzg.share.vo.FileOrderVO;
import com.jxzg.share.vo.Spellgroup.SpellgroupVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;


/**
 * 会议报名
 */
@Service
public class MeetingApplyServiceImpl implements IMeetingApplyService {
    @Autowired
    private MeetingMapper meetingMapper;
    @Autowired
    private UsertokenMapper usertokenMapper;
    @Autowired
    private MeetingticketMapper meetingticketMapper;
    @Autowired
    private SignfieldMapper signfieldMapper;
    @Autowired
    private MeetingsponsorMapper meetingsponsorMapper;
    @Autowired
    private SponsorMapper sponsorMapper;
    @Autowired
    private CollectattentionMapper collectattentionMapper;
    @Autowired
    private OrdersMapper ordersMapper;
    @Autowired
    private UserprofileMapper userprofileMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private OrderdtlMapper orderdtlMapper;
    @Autowired
    private MyticketMapper myticketMapper;
    @Autowired
    private SignuserinfoMapper signuserinfoMapper;
    @Autowired
    private SpellgroupMapper spellgroupMapper;
    @Autowired
    private InvoicesMapper invoicesMapper;

    @Value("${pic.baseUrl}")
    private String picBaseUrl;

    @Value("${pic.qrcode}")
    private String qrcodeRelativeUrl;


    @Override

    public ReturnMap meetingapplylist(String token, int mid) {
        if (mid == 0) {
            return new ReturnMap(false, "账号异常,请重新登录!");
        }
        Date date = new Date();
        Meeting meeting = meetingMapper.selectMeetingByMid(mid);
        Map<String, Object> map = new HashMap<>();
        if (meeting.getStatus() == Meeting.MEETING_STATUS_ACTIVITY_DEL) {
            return new ReturnMap(false, "此活动已经被删除!");
        }
        Usertoken usertoken = usertokenMapper.selectUidBytoken(token);
        if (token != null && usertoken != null) {
            map.put("ticketstatus", meeting.getStatus());
            Date date1 = meetingticketMapper.selectMinTime(meeting.getMid());
            if (meeting.getStatus() == Meeting.MEETING_STATUS_ACTICITY_NO && date1.getTime() > date.getTime()) {
                map.put("counttime", date1.getTime() - date.getTime());
            }
            map.put("meetingtype", meeting.getMeetingtype());
            map.put("pic", meeting.getPic());
            map.put("starttime", meeting.getStarttime());
            map.put("endtime", meeting.getEndtime());
            String as = meeting.getAddress();
            String[] address = as.split("-");
            map.put("lat", meeting.getLat());
            map.put("lng", meeting.getLng());
            if (address[0].equals(address[1])) {
                map.put("address", address[1] + "-" + address[2] + "-" + address[3]);
            } else {
                map.put("address", meeting.getAddress());
            }
            map.put("soldticket", meeting.getSoldticket());
            map.put("commentflag", meeting.getCommentflag());
            map.put("meetingname", meeting.getName());
            BigDecimal price = meetingticketMapper.selectMeetingTicket(mid);
            map.put("price", price);
            map.put("viewnum", meeting.getViewnum());
            //门票
            List<Map<String, Object>> tickets = new ArrayList();
            List<Meetingticket> meetingtickets = meetingticketMapper.selectAllMeetingTicket(mid);
            for (Meetingticket meetingticket : meetingtickets) {
                Map<String, Object> ticketmap = new HashMap<>();
                ticketmap.put("name", meetingticket.getName());
                ticketmap.put("price", meetingticket.getPrice());
                ticketmap.put("amount", meetingticket.getAmount());
                ticketmap.put("price", meetingticket.getPrice());
                ticketmap.put("remaincount", meetingticket.getRemaincount());
                ticketmap.put("begintime", meetingticket.getBegintime());
                ticketmap.put("endtime", meetingticket.getEndtime());
                ticketmap.put("mincount", meetingticket.getMincount());
                ticketmap.put("maxcount", meetingticket.getMaxcount());
                ticketmap.put("description", meetingticket.getDescription());
                tickets.add(ticketmap);
            }
            map.put("tickets", tickets);
            //如果是拼团随机增加2条拼团信息
            if (meeting.getMeetingtype() == Meeting.MEETINGTYPEA_TEAM) {
                List<Spellgroup> spellgroups = spellgroupMapper.selectRandom(meeting.getMid());
                Collections.shuffle(spellgroups);
                List randomlist = new ArrayList();
                map.put("scale", meeting.getScale());//拼团规模
                int amount = 0;
                int count = 0;
                for (Spellgroup spellgroup : spellgroups) {
                    if (spellgroup.getTid() == 0) {
                        count = spellgroupMapper.selectApplyById(spellgroup.getId(), meeting.getMid());
                    } else {
                        count = spellgroupMapper.selectApplyById(spellgroup.getTid(), meeting.getMid());
                    }
                    if (count < meeting.getScale()) {
                        Map<String, Object> randommap = new HashMap<>();
                        User user = userMapper.selectByUid(spellgroup.getUid());
                        Userprofile userprofile = userprofileMapper.selectByUid(spellgroup.getUid());
                        if (userprofile.getNickname() != null) {
                            randommap.put("nickname", userprofile.getNickname());
                        } else {
                            randommap.put("nickname", user.getLoginname());
                        }
                        randommap.put("avatar", user.getAvatar());
                        randommap.put("scale", meeting.getScale());
                        randommap.put("countdown", spellgroup.getUpdatetime().getTime() - date.getTime());
                        randommap.put("surplus", meeting.getScale() - count);
                        randommap.put("groupid", spellgroup.getId());
                        randomlist.add(randommap);
                        amount++;
                    }
                    if (amount == 2) {
                        break;
                    }
                }
                map.put("randomn", randomlist);
            }
            //主办方,关注
            List<Meetingsponsor> sponsors = meetingsponsorMapper.selectByMid(mid);
            List<Map<String, Object>> listsponsor = new ArrayList<>();
            for (Meetingsponsor sponsoroid : sponsors) {
                Map<String, Object> newmap = new HashMap<>();
                Sponsor sponsor = sponsorMapper.selectByPrimaryKey(sponsoroid.getOid());
                newmap.put("sid", sponsoroid.getOid());
                int count = collectattentionMapper.IsCollectAttentionNum(sponsoroid.getOid(), usertoken.getUid(), Collectattention.TYPE_ATTENTION);//关注
                newmap.put("sponsorname", sponsor.getName());
                newmap.put("sponsorlogo", sponsor.getLogo());
                newmap.put("sponsorintroduction", sponsor.getIntroduction());
                newmap.put("sponsortel", sponsor.getTel());
                newmap.put("sponsorqq", sponsor.getQq());
                newmap.put("sponsorwebsite", sponsor.getWebsite());
                newmap.put("sponsoremail", sponsor.getEmail());
                newmap.put("attention", count);
                listsponsor.add(newmap);
            }
            map.put("sponsors", listsponsor);
            Integer count = collectattentionMapper.IsCollectAttentionNum(mid, usertoken.getUid(), Collectattention.TYPE_COLLECT);//收藏
            map.put("collect", count);
            map.put("ownerid", meeting.getUid());
            //会议详情
            map.put("introduction", meeting.getIntroduction());
            //打赏数
            int playnum = orderdtlMapper.selectProducttype(meeting.getMid(), Orderdtl.PRODUCTTYPE_AWARD);
            map.put("playnum", playnum);
        } else {
            map.put("ticketstatus", meeting.getStatus());
            Date date1 = meetingticketMapper.selectMinTime(meeting.getMid());
            if (meeting.getStatus() == Meeting.MEETING_STATUS_ACTICITY_NO && date1.getTime() > date.getTime()) {
                map.put("counttime", date1.getTime() - date.getTime());
            }
            map.put("meetingtype", meeting.getMeetingtype());
            map.put("pic", meeting.getPic());
            map.put("starttime", meeting.getStarttime());
            map.put("endtime", meeting.getEndtime());
            String as = meeting.getAddress();
            map.put("lat", meeting.getLat());
            map.put("lng", meeting.getLng());
            String[] address = as.split("-");
            if (address[0].equals(address[1])) {
                map.put("address", address[1] + "-" + address[2] + "-" + address[3]);
            } else {
                map.put("address", meeting.getAddress());
            }
            map.put("soldticket", meeting.getSoldticket());
            map.put("commentflag", meeting.getCommentflag());
            map.put("meetingname", meeting.getName());
            map.put("viewnum", meeting.getViewnum());
            //门票
            List<Map<String, Object>> tickets = new ArrayList();
            List<Meetingticket> meetingtickets = meetingticketMapper.selectAllMeetingTicket(mid);
            for (Meetingticket meetingticket : meetingtickets) {
                Map<String, Object> ticketmap = new HashMap<>();
                ticketmap.put("name", meetingticket.getName());
                ticketmap.put("amount", meetingticket.getAmount());
                ticketmap.put("remaincount", meetingticket.getRemaincount());
                ticketmap.put("begintime", meetingticket.getBegintime());
                ticketmap.put("price", meetingticket.getPrice());
                ticketmap.put("endtime", meetingticket.getEndtime());
                ticketmap.put("mincount", meetingticket.getMincount());
                ticketmap.put("maxcount", meetingticket.getMaxcount());
                ticketmap.put("description", meetingticket.getDescription());
                tickets.add(ticketmap);
            }
            BigDecimal price = meetingticketMapper.selectMeetingTicket(mid);
            map.put("price", price);
            map.put("tickets", tickets);
            //如果是拼团随机增加2条拼团信息
            if (meeting.getMeetingtype() == Meeting.MEETINGTYPEA_TEAM) {
                List<Spellgroup> spellgroups = spellgroupMapper.selectRandom(meeting.getMid());
                Collections.shuffle(spellgroups);
                List randomlist = new ArrayList();
                map.put("scale", meeting.getScale());//拼团规模
                int amount = 0;
                int count = 0;
                for (Spellgroup spellgroup : spellgroups) {
                    if (spellgroup.getTid() == 0) {
                        count = spellgroupMapper.selectApplyById(spellgroup.getId(), meeting.getMid());
                    } else {
                        count = spellgroupMapper.selectApplyById(spellgroup.getTid(), meeting.getMid());
                    }
                    if (count < meeting.getScale()) {
                        Map<String, Object> randommap = new HashMap<>();
                        User user = userMapper.selectByUid(spellgroup.getUid());
                        Userprofile userprofile = userprofileMapper.selectByUid(spellgroup.getUid());
                        if (userprofile.getNickname() != null) {
                            randommap.put("nickname", userprofile.getNickname());
                        } else {
                            randommap.put("nickname", user.getLoginname());
                        }
                        randommap.put("avatar", user.getAvatar());
                        randommap.put("scale", meeting.getScale());
                        randommap.put("countdown", spellgroup.getUpdatetime().getTime() - date.getTime());
                        randommap.put("surplus", meeting.getScale() - count);
                        randommap.put("groupid", spellgroup.getId());
                        randomlist.add(randommap);
                        amount++;
                    }
                    if (amount == 2) {
                        break;
                    }
                }
                map.put("randomn", randomlist);
            }
            //主办方,关注
            List<Meetingsponsor> sponsors = meetingsponsorMapper.selectByMid(mid);
            List<Map<String, Object>> listsponsor = new ArrayList<>();
            for (Meetingsponsor sponsoroid : sponsors) {
                Map<String, Object> newmap = new HashMap<>();
                Sponsor sponsor = sponsorMapper.selectByPrimaryKey(sponsoroid.getOid());
                newmap.put("sid", sponsoroid.getOid());
                newmap.put("sponsorname", sponsor.getName());
                newmap.put("sponsorlogo", sponsor.getLogo());
                newmap.put("sponsorintroduction", sponsor.getIntroduction());
                newmap.put("sponsortel", sponsor.getTel());
                newmap.put("sponsorqq", sponsor.getQq());
                newmap.put("sponsorwebsite", sponsor.getWebsite());
                newmap.put("sponsoremail", sponsor.getEmail());
                newmap.put("attention", Collectattention.STATUS_ZERO);
                newmap.put("status", sponsor.getStatus());
                listsponsor.add(newmap);
            }
            map.put("sponsors", listsponsor);
            //收藏
            map.put("collect", Collectattention.STATUS_ZERO);
            //会议详情
            map.put("introduction", meeting.getIntroduction());
            map.put("ownerid", meeting.getUid());
            //打赏数
            int playnum = orderdtlMapper.selectProducttype(meeting.getMid(), Orderdtl.PRODUCTTYPE_AWARD);
            map.put("playnum", playnum);
        }
        meetingMapper.updateMeetingAddflow(mid);
        ReturnMap result = new ReturnMap("成功", map);
        return result;
    }


    @Override
    //@Cacheable(value="myCache", key="'applymessage'+#mid")
    public ReturnList applymessage(String token, int mid) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(token);
        if (mid == 0 || token == null || usertoken == null) {
            return new ReturnList("请重新登录!", false);
        }
        List<Map<String, Object>> mapList = new ArrayList<>();
        List<Signfield> signfield3 = signfieldMapper.selectSignfieldMid(mid);
        for (Signfield signfield : signfield3) {
            Map<String, Object> newmap = new HashMap<>();
            List onechoiceval = new ArrayList();
            if (signfield.getFielddescription().equals("单选项")) {
                List<String> signfieldval = signfieldMapper.selectSignfieldVal(signfield.getFieldname(), signfield.getFielddescription(), mid);
                for (String s : signfieldval) {
                    onechoiceval.add(s);
                }
                newmap.put("options", onechoiceval);
                newmap.put("type", 4);
            }
            if (signfield.getFielddescription().equals("多选项")) {
                List<String> signfieldval = signfieldMapper.selectSignfieldVal(signfield.getFieldname(), signfield.getFielddescription(), mid);
                for (String s : signfieldval) {
                    onechoiceval.add(s);
                }
                newmap.put("options", onechoiceval);
                newmap.put("type", 5);
            }
            if (signfield.getFielddescription().equals("姓名")) {
                newmap.put("type", 0);
            } else if (signfield.getFielddescription().equals("电话")) {
                newmap.put("type", 1);
            } else if (signfield.getFielddescription().equals("单行文本")) {
                newmap.put("type", 2);
            } else if (signfield.getFielddescription().equals("多行文本")) {
                newmap.put("type", 3);
            }
            long sid = signfieldMapper.selectSignfieldSid(mid, signfield.getFieldname());
            newmap.put("sid", sid);
            newmap.put("mid", signfield.getMid());
            newmap.put("fieldname", signfield.getFieldname());
            newmap.put("isrequire", signfield.getIsrequire());
            mapList.add(newmap);
        }
        ReturnList result = new ReturnList(mapList, "成功");
        return result;
    }

    @Override
    public ReturnMap get_Invoices(String token, int mid) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(token);
        if (mid == 0 || token == null || usertoken == null) {
            return new ReturnMap(false, "请重新登录!");
        }
        Meeting meeting = meetingMapper.selectMeetingByMid(mid);
        Userprofile userprofile = userprofileMapper.selectByUid(usertoken.getUid());
        User user = userMapper.selectByUid(usertoken.getUid());
        Map<String, Object> map = new HashMap<>();
        if (userprofile.getRealname() == null) {
            map.put("realname", "");
        } else {
            map.put("realname", userprofile.getRealname());
        }
        if (user.getLoginname() == null) {
            map.put("loginname", "");
        } else {
            map.put("loginname", user.getLoginname());
        }
        map.put("invoiceflag", meeting.getInvoiceflag());
        //真实姓名电话号码
        return new ReturnMap("获取发票信息!", map);
    }

    @Override
    public JSONResult stop_meetingapply(String token, int mid) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(token);
        if (mid == 0 || token == null || usertoken == null) {
            return new JSONResult(false, "请重新登录!");
        }
        Meeting meeting = meetingMapper.selectMeetingByMid(mid);
        if (meeting.getMeetingtype() == Meeting.MEETINGTYPEA_TEAM) {//如果是拼团会议
            Meetingticket meetingticket = meetingticketMapper.selectByMid(mid);
            if (meetingticket.getPrice().compareTo(Meetingticket.PRICE) == 0) {
                spellgroupMapper.updateStatusByMid(meeting.getMid(), Spellgroup.STATUS_OUTTIME, Spellgroup.STATUS_SUCCESS_A);
                int count = spellgroupMapper.selectCountStatus(meeting.getMid(), Spellgroup.STATUS_SUCCESS_A);
                meetingticketMapper.updateTicketNumByid(meetingticket.getTicdketid(), mid, count * (-1));
            } else {
                spellgroupMapper.updateStatusByMid(meeting.getMid(), Spellgroup.STATUS_FAIL, Spellgroup.STATUS_SUCCESS_A);
            }
        }
        int count = meetingMapper.selectMeetingByUidMid(usertoken.getUid(), mid);
        if (count > 0) {
            meetingMapper.updateMeetingStatus(mid, Meeting.MEETING_STATUS_ACTIVITY_STOP);
            return new JSONResult("活动停止报名!");
        }
        return new JSONResult(false, "没有修改权限!");
    }

    @Override
    public synchronized ReturnMap Spellgroup_open(SpellgroupVO spellgroupVO) {
        List<FileOrderVO> fileOrderVO = spellgroupVO.getFieldmsg();
        if (fileOrderVO.size() == 0 || fileOrderVO == null) {
            return new ReturnMap(false, "账号异常,请重新登录!");
        }
        Map<String, Object> map = new HashMap<>();
        Usertoken usertoken = usertokenMapper.selectUidBytoken(spellgroupVO.getToken());
        if (spellgroupVO.getTicketname() == null || spellgroupVO.getTicketprice() == null || spellgroupVO.getAmount() == null || spellgroupVO.getMid() == 0 || spellgroupVO.getToken() == null || usertoken == null) {
            return new ReturnMap(false, "请重新登录!");
        }
        Date date = new Date();
        Meetingticket meetingticket = meetingticketMapper.selectByMid(spellgroupVO.getMid());
        if (date.getTime() > meetingticket.getEndtime().getTime()) {
            return new ReturnMap(false, "购票失败!售票时间到");
        }
        if (date.getTime() < meetingticket.getBegintime().getTime()) {
            return new ReturnMap(false, "购票失败!售票时间未到");
        }
        Meeting meeting = meetingMapper.selectMeetingByMid(spellgroupVO.getMid());
        //去参团
        if (spellgroupVO.getTid() != null) {
            int count = spellgroupMapper.selectApplyById(spellgroupVO.getTid(), spellgroupVO.getMid());
            Spellgroup s = spellgroupMapper.selectById(spellgroupVO.getTid());
            if (meeting.getScale() == count || s.getStatus() != Spellgroup.STATUS_SUCCESS_A || meetingticket.getRemaincount() == 0) {
                return new ReturnMap(false, "购票失败,此团已满!");
            }
        }
        if (spellgroupVO.getTid() == null && meetingticket.getRemaincount() != -1) {
            int amount = meetingticketMapper.selectMaxAmount(meeting.getMid());
            if (meeting.getMeetingtype() == Meeting.MEETINGTYPEA_TEAM && amount < meeting.getScale()) {
                return new ReturnMap(false, "余票不足开团!");
            }
        }
        //生成订单
        String orderno = OrdernoUtil.getOrderno() + usertoken.getUid();
        Orders co = new Orders();
        co.setOrderno(orderno);
        co.setUid(usertoken.getUid());
        co.setOrdertype(Orders.ORDERTYPE_TEAM);
        if (spellgroupVO.getTicketprice().compareTo(Meetingticket.PRICE) == 0) {
            co.setStatus(Orders.ORDER_STATUS_YES_PAY);
        } else {
            co.setStatus(Orders.ORDER_STATUS_NO_PAY);
        }
        co.setAmount(spellgroupVO.getAmount());
        co.setMoney(spellgroupVO.getTicketprice());
        co.setCreatetime(date);
        try {
            co.setUpdatetime(DateUtil.twentylater(20));
        } catch (Exception e) {
            e.printStackTrace();
        }
        ordersMapper.insert(co);
        Myticket myticket = new Myticket();
        myticket.setUid(usertoken.getUid());
        myticket.setMid(spellgroupVO.getMid());
        myticket.setTickettype(Myticket.TICKETTYPE_MYTICKET);
        if (spellgroupVO.getTicketprice().compareTo(Meetingticket.PRICE) == 0) {
            myticket.setStatus(Myticket.ORDER_STATUS_YES_PAY);
        } else {
            myticket.setStatus(Myticket.ORDER_STATUS_NO_PAY);
        }
        myticket.setCreateip(spellgroupVO.getTicketname());
        myticket.setTicketid(meetingticket.getTicdketid());
        myticket.setOrderno(orderno);
        myticket.setCreatetime(date);
        myticket.setNo(Myticket.MYTICKET_NO_NO);
        try {
            myticket.setUpdatetime(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        myticketMapper.insert(myticket);
        String code = co.getOrderno() + ":" + myticket.getId() + ":" + meeting.getMid() + usertoken.getUid();
        String qcode = QRcodeImageUtil.getQRcodePath(code,picBaseUrl,qrcodeRelativeUrl);//二维码,订单号生成
        String signcode = null;
        int count1 = 1;
        while (count1 > 0) {
            signcode = RandomNum.getRandomnum();
            int amount = myticketMapper.selectSignCode(signcode, meeting.getMid());
            if (amount > 0) {
                count1 = 1;
            } else {
                count1 = 0;
            }
        }
        myticketMapper.updateQcode(myticket.getId(), qcode, signcode);
        //生成发票
        if (spellgroupVO.getInvoices().getInvoicetype() != 0) {
            Invoices invoices = spellgroupVO.getInvoices();
            invoices.setCreatetime(date);
            invoices.setUpdatetime(date);
            invoices.setUid(usertoken.getUid());
            invoices.setOid(spellgroupVO.getMid());
            invoices.setStatus(invoices.getStatus());
            invoices.setOrderno(orderno);
            if (invoices.getStatus() == Invoices.STATUS_ONESELF) {
                invoices.setAddress("自取");
            }
            invoicesMapper.insert(invoices);
        }
        Orderdtl orderdtl = new Orderdtl();
        orderdtl.setUid(usertoken.getUid());
        orderdtl.setMid(spellgroupVO.getMid());
        orderdtl.setProducttype(Orderdtl.PRODUCTTYPE_TICKET);
        if (spellgroupVO.getTicketprice().compareTo(Meetingticket.PRICE) == 0) {
            orderdtl.setStatus(Orderdtl.ORDER_STATUS_YES_PAY);
        } else {
            orderdtl.setStatus(Orderdtl.ORDER_STATUS_NO_PAY);
        }
        orderdtl.setAmount(1);
        orderdtl.setOrderno(orderno);
        orderdtl.setPrice(spellgroupVO.getTicketprice());
        orderdtl.setMoney(spellgroupVO.getTicketprice());
        orderdtl.setCreatetime(date);
        orderdtl.setProductid(myticket.getId());
        try {
            orderdtl.setUpdatetime(DateUtil.twentylater(20));
        } catch (Exception e) {
            e.printStackTrace();
        }
        orderdtlMapper.insert(orderdtl);
        //拼团信息
        Spellgroup spellgroup = new Spellgroup();
        if (spellgroupVO.getTicketprice().compareTo(Meetingticket.PRICE) == 0) {
            meetingMapper.updateSoldticketAdd(meeting.getMid(), 1);
            if (spellgroupVO.getTid() == null) {//团长
                spellgroup.setTid(Spellgroup.TYPE_HEADER);
                spellgroup.setType(Spellgroup.TYPE_HEADER);
                spellgroup.setStatus(Spellgroup.STATUS_SUCCESS_A);
                try {
                    spellgroup.setUpdatetime(DateUtil.twentylater(1440));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                int count = spellgroupMapper.selectApplyById(spellgroupVO.getTid(), spellgroupVO.getMid());
                if (meeting.getScale() == count) {
                    return new ReturnMap(false, "拼团失败!此团已满");
                }
                if (meeting.getScale() - count == 1) {
                    spellgroup.setStatus(Spellgroup.STATUS_SUCCESS);
                    spellgroupMapper.updateStatusMany(spellgroupVO.getTid(), Spellgroup.STATUS_SUCCESS, Spellgroup.STATUS_SUCCESS_A);
                } else {
                    spellgroup.setStatus(Spellgroup.STATUS_SUCCESS_A);
                }
                spellgroup.setTid(spellgroupVO.getTid());
                spellgroup.setType(Spellgroup.TYPE_MEMBER);
                try {
                    Spellgroup spellgroup1 = spellgroupMapper.selectById(spellgroupVO.getTid());
                    spellgroup.setUpdatetime(spellgroup1.getUpdatetime());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            spellgroup.setTicketid(myticket.getId());
            spellgroup.setMid(spellgroupVO.getMid());
            spellgroup.setUid(usertoken.getUid());
            spellgroup.setCreatetime(date);
            spellgroupMapper.insert(spellgroup);
        } else {
            if (spellgroupVO.getTid() == null) {//团长
                spellgroup.setTid(0);
                spellgroup.setType(Spellgroup.TYPE_HEADER);
                try {
                    spellgroup.setUpdatetime(DateUtil.twentylater(1440));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                spellgroup.setTid(spellgroupVO.getTid());
                spellgroup.setType(Spellgroup.TYPE_MEMBER);
                Spellgroup spellgroup1 = spellgroupMapper.selectById(spellgroupVO.getTid());
                spellgroup.setUpdatetime(spellgroup1.getUpdatetime());
            }
            spellgroup.setStatus(Spellgroup.STATUS_STAY);
            spellgroup.setMid(spellgroupVO.getMid());
            spellgroup.setUid(usertoken.getUid());
            spellgroup.setCreatetime(date);
            spellgroup.setTicketid(myticket.getId());
            spellgroupMapper.insert(spellgroup);
        }
        meetingticketMapper.updateTicketNum(meetingticket.getName(), meeting.getMid(), 1);
        for (FileOrderVO fieldmsg : spellgroupVO.getFieldmsg()) {
            if (fieldmsg.getType() != Signuserinfo.MANY_CHOICE) {
                Signuserinfo signuserinfo = new Signuserinfo();
                signuserinfo.setSid(myticket.getId());
                signuserinfo.setFieldid(fieldmsg.getFieldname());
                signuserinfo.setFieldval(fieldmsg.getFieldval());
                signuserinfo.setStatus(fieldmsg.getType());
                signuserinfo.setCreatetime(date);
                signuserinfo.setUpdatetime(date);
                signuserinfoMapper.insert(signuserinfo);
            }
            if (fieldmsg.getType() == Signuserinfo.MANY_CHOICE) {//多选项
                for (String vo : fieldmsg.getFieldvals()) {
                    Signuserinfo sgi = new Signuserinfo();
                    sgi.setSid(myticket.getId());
                    sgi.setFieldid(fieldmsg.getFieldname());
                    sgi.setFieldval(vo);
                    sgi.setStatus(fieldmsg.getType());
                    sgi.setCreatetime(date);
                    sgi.setUpdatetime(date);
                    signuserinfoMapper.insert(sgi);
                }
            }
        }
        if (spellgroupVO.getTid() == null) {//团长
            map.put("tid", spellgroup.getId());
        } else {
            map.put("tid", spellgroupVO.getTid());
        }
        map.put("orderno", co.getOrderno());
        map.put("mid", meeting.getMid());
        return new ReturnMap("拼团报名成功!", map);
    }
}
