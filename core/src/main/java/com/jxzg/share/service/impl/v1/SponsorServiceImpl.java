package com.jxzg.share.service.impl.v1;

import com.jxzg.share.domain.meeting.Meeting;
import com.jxzg.share.domain.meeting.Sponsor;
import com.jxzg.share.domain.other.Collectattention;
import com.jxzg.share.domain.other.Usertoken;
import com.jxzg.share.mapper.*;
import com.jxzg.share.querypage.QueryObject;
import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.returndomain.ReturnList;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.service.v1.ISponsorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 00818Wolf on 2016/12/26.
 */
@Service
public class SponsorServiceImpl implements ISponsorService {
    @Autowired
    private SponsorMapper sponsorMapper;
    @Autowired
    private UsertokenMapper usertokenMapper;
    @Autowired
    private MeetingsponsorMapper meetingsponsorMapper;
    @Autowired
    private MeetingMapper meetingMapper;
    @Autowired
    private MeetingticketMapper meetingticketMapper;
    @Autowired
    private CollectattentionMapper collectattentionMapper;

    @Override
    //@CacheEvict(value="myCache", key="'selectSponsorAll'+#sponsor.token")
    public ReturnMap insert(Sponsor sponsor) {
        if (sponsor.getName() == null || sponsor.getTel() == null) {
            return new ReturnMap(false, "账号异常!请重新登录");
        }
        Usertoken usertoken = usertokenMapper.selectUidBytoken(sponsor.getToken());
        if (sponsor.getToken() == null || usertoken == null) {
            return new ReturnMap(false, "请重新登录!");
        }

        Sponsor sponsor1 = sponsorMapper.selectByNameOne(usertoken.getUid(), sponsor.getName());
        if (sponsor1 != null) {
            return new ReturnMap(false, "该主办方已存在");
        }
        sponsor.setUid(usertoken.getUid());
        sponsor.setStatus(Sponsor.STATUS_NORMAL);
        sponsorMapper.insert(sponsor);
        Map<String, Object> map = new HashMap<>();
        map.put("sid", sponsor.getOid());
        return new ReturnMap("主办方保存成功!", map);
    }

    @Override
    //@Cacheable(value="myCache", key="'selectSponsorAll'+#qo.token")
    public ReturnList selectSponsorAll(QueryObject qo) {
        List result = new ArrayList();
        Usertoken usertoken = usertokenMapper.selectUidBytoken(qo.getToken());
        if (qo.getToken() == null || usertoken == null) {
            return new ReturnList("请重新登录!", false);
        }
        qo.setUid(usertoken.getUid());
        int count = sponsorMapper.selectAllCount(qo);
        if (count > 0) {
            List<Sponsor> sponsors = sponsorMapper.selectAll(qo);
            for (Sponsor sponsor : sponsors) {
                Map<String, Object> map = new HashMap<>();
                map.put("uid", sponsor.getUid());
                map.put("email", sponsor.getEmail());
                map.put("introduction", sponsor.getIntroduction());
                map.put("logo", sponsor.getLogo());
                map.put("name", sponsor.getName());
                map.put("qq", sponsor.getQq());
                map.put("tel", sponsor.getTel());
                map.put("website", sponsor.getWebsite());
                map.put("oid", sponsor.getOid());
                result.add(map);
            }
            return new ReturnList(result, "查询成功!", count);
        }
        return new ReturnList(result, "查询成功!", 0);
    }

    @Override
    //@CacheEvict(value="myCache", key="'selectSponsorAll'+#sponsor.token")
    public JSONResult deleteByOid(Sponsor sponsor) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(sponsor.getToken());
        if (sponsor.getOid() == 0 || sponsor.getToken() == null || usertoken == null) {
            return new JSONResult(false, "请重新登录");
        }
        sponsorMapper.updateStatus(sponsor.getOid(), Sponsor.STATUS_DEL);
        collectattentionMapper.updateByStatus(sponsor.getOid(), Collectattention.STATUS_DEL);
        return new JSONResult(true, "删除成功!");
    }

    @Override
    //@CacheEvict(value="myCache", key="'selectSponsorAll'+#sponsor.token")
    public JSONResult sponsorupdate(Sponsor sponsor) {
        if (sponsor.getName() == null || sponsor.getTel() == null) {
            return new JSONResult(false, "账号异常!请重新登录");
        }
        Usertoken usertoken = usertokenMapper.selectUidBytoken(sponsor.getToken());
        if (sponsor.getToken() == null || usertoken == null) {
            return new JSONResult(false, "请重新登录");
        }
        sponsor.setUid(usertoken.getUid());
        sponsorMapper.updateByPrimaryKey(sponsor);
        return new JSONResult("主办方编辑成功!");
    }

    @Override
    public ReturnMap sponsor_particulars(int id, String token) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(token);
        Map<String, Object> spmap = new HashMap<>();
        List meetingresult = new ArrayList();
        //举办过的会议
        Sponsor sponsor = sponsorMapper.selectByOid(id);
        if (sponsor.getStatus() == Sponsor.STATUS_DEL) {
            return new ReturnMap(false, "该主办方已被删除!");
        }
        List<Integer> mids = meetingsponsorMapper.selectMids(id);
        if (mids.size() > 0) {
            List<Meeting> meetings = meetingMapper.selectBymis(mids);
            for (Meeting mt : meetings) {
                Map<String, Object> meetingmap = new HashMap<>();
                BigDecimal price = meetingticketMapper.selectMeetingTicket(mt.getMid());
                meetingmap.put("mid", mt.getMid());
                meetingmap.put("meetingpic", mt.getPic());
                meetingmap.put("meetingname", mt.getName());
                meetingmap.put("meetingtime", mt.getStarttime());
                meetingmap.put("meetingaddress", mt.getAddress());
                meetingmap.put("price", price);
                meetingresult.add(meetingmap);
            }
            spmap.put("meeting", meetingresult);
        } else {
            spmap.put("meeting", meetingresult);
        }
        //主办方详情
        Map<String, Object> map = new HashMap<>();
        map.put("uid", sponsor.getUid());
        map.put("sid", sponsor.getOid());
        map.put("sponsorintroduction", sponsor.getIntroduction());
        map.put("sponsorlogo", sponsor.getLogo());
        map.put("sponsorname", sponsor.getName());
        map.put("sponsortel", sponsor.getTel());
        map.put("sponsorstatus", sponsor.getStatus());
        if (token == null || usertoken == null) {
            map.put("attention", 0);
        } else {
            int count = collectattentionMapper.IsCollectAttentionNum(id, usertoken.getUid(), Collectattention.TYPE_ATTENTION);
            map.put("attention", count);
        }
        int attentionnum = collectattentionMapper.selectCollectAttentionNum(Collectattention.TYPE_ATTENTION, id);
        map.put("attentionnum", attentionnum);
        spmap.put("sponsor", map);
        return new ReturnMap("成功!", spmap);
    }
}
