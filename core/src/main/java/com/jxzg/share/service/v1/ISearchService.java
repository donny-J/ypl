package com.jxzg.share.service.v1;

import com.jxzg.share.querypage.MeetingQueryObject;
import com.jxzg.share.querypage.SearchQueryObject;
import com.jxzg.share.returndomain.ReturnList;

/**
 * Created by 00818Wolf on 2017/3/13.
 */
public interface ISearchService {
    /**
     * 查询主办方
     * @param qo
     * @return
     */
    ReturnList searchSponsor(MeetingQueryObject qo);

    /**
     * 查询会议
     * @param qo
     * @return
     */
    ReturnList searchMeeting(SearchQueryObject qo);
}
