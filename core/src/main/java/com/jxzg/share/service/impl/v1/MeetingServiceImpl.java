package com.jxzg.share.service.impl.v1;

import com.jxzg.share.domain.meeting.*;
import com.jxzg.share.domain.other.Collectattention;
import com.jxzg.share.domain.other.User;
import com.jxzg.share.domain.other.Userprofile;
import com.jxzg.share.domain.other.Usertoken;
import com.jxzg.share.domain.pay.Orderdtl;
import com.jxzg.share.mapper.*;
import com.jxzg.share.querypage.MeetingQueryObject;
import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.returndomain.ReturnList;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.service.v1.IFilterwordService;
import com.jxzg.share.service.v1.IMeetingService;
import com.jxzg.share.toolUtil.DateUtil;
import com.jxzg.share.toolUtil.PhoneDimUtil;
import com.jxzg.share.vo.ActivityMeeting.MeetingBody;
import com.jxzg.share.vo.CreatticketVO;
import com.jxzg.share.vo.FillField;
import com.jxzg.share.vo.IdAndToken;
import com.jxzg.share.vo.Spellgroup.ClusterTicket;
import com.jxzg.share.vo.Spellgroup.SpellgroupBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

import static org.aspectj.bridge.Version.getTime;

/**
 * Created by 00818Wolf on 2016/12/15.
 */
@Service("meetingService")
public class MeetingServiceImpl implements IMeetingService {
    @Autowired
    private MeetingMapper meetingMapper;
    @Autowired
    private UsertokenMapper usertokenMapper;
    @Autowired
    private SignfieldMapper signfieldMapper;
    @Autowired
    private MeetingticketMapper meetingticketMapper;
    @Autowired
    private CommetMapper commetMapper;
    @Autowired
    private MeetingsponsorMapper meetingsponsorMapper;
    @Autowired
    private SponsorMapper sponsorMapper;
    @Autowired
    private SpellgroupMapper spellgroupMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserprofileMapper userprofileMapper;
    @Autowired
    private MyticketMapper myticketMapper;
    @Autowired
    private OrdersMapper ordersMapper;
    @Autowired
    private CollectattentionMapper collectattentionMapper;
    @Autowired
    private InvoicesMapper invoicesMapper;
    @Autowired
    private MeetingtagMapper meetingtagMapper;
    @Autowired
    private IFilterwordService filterwordService;
    @Autowired
    private OrderdtlMapper orderdtlMapper;


    @Override
    //@CacheEvict(value = "myCache", key = "'mymeeting'+#body.token")
    public ReturnMap addmetting(MeetingBody body) {
        if (body.getBody().getPic() == null || body.getBody().getMeetingname() == null || body.getBody().getMeetingstarttime() == 0 || body.getBody().getMeetingendtime() == 0
                || body.getBody().getAddress().getProvice() == null || body.getBody().getAddress().getCity() == null || body.getBody().getAddress().getDist() == null
                || body.getBody().getAddress().getSite() == null || body.getBody().getSid() == null) {
            return new ReturnMap(false, "请重新登录");
        }
        if (body.getBody().getAddress().getProvice() == "" || body.getBody().getAddress().getCity() == "" || body.getBody().getAddress().getDist() == ""
                || body.getBody().getAddress().getSite() == "") {
            return new ReturnMap(false, "请核对您填的地址信息");
        }
        Date date = new Date();
        Usertoken usertoken = usertokenMapper.selectUidBytoken(body.getToken());
        if (body.getToken() == null || usertoken == null) {
            return new ReturnMap(false, "请重新登录");
        }
        if (body.getBody().getMeetingstarttime() > body.getBody().getMeetingendtime()) {
            return new ReturnMap(false, "开始时间不能小于结束时间");
        }
        List<CreatticketVO> register = body.getBody().getMeetingticket();
        if (register.size() > 1) {
            List<String> names = new ArrayList<>();
            for (CreatticketVO creatticketVO : register) {
                names.add(creatticketVO.getName());
            }
            if (names.size() != new HashSet<Object>(names).size()) {
                return new ReturnMap(false, "门票名称不能重复");
            }
        }
        //会议基础信息
        Meeting meeting = new Meeting();
        meeting.setIsopen(body.getBody().getIsopen());
        meeting.setPic(body.getBody().getPic());
        //保存会议地址
        String address = body.getBody().getAddress().getProvice() + "-" + body.getBody().getAddress().getCity() + "-" + body.getBody().getAddress().getDist() + "-" + body.getBody().getAddress().getSite();
        meeting.setAddress(address);
        try {
            meeting.setEndtime(DateUtil.longToDate(body.getBody().getMeetingendtime()));
            meeting.setStarttime(DateUtil.longToDate(body.getBody().getMeetingstarttime()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        meeting.setLat(body.getBody().getAddress().getLat());
        meeting.setLng(body.getBody().getAddress().getLng());
        meeting.setUpdatetime(date);
        meeting.setUid(usertoken.getUid());
        meeting.setIntroduction(body.getBody().getIntroduction());
        meeting.setMeetingtype(Meeting.MEETINGTYPE_ACTIVITY);
        meeting.setName(body.getBody().getMeetingname());
        meeting.setInvoiceflag(body.getBody().getInvoiceflag());
        if (body.getBody().getMeetingticket().size() == 0) {
            meeting.setStatus(Meeting.MEETING_STATUS_ACTIVITY_APPLY);
        } else {
            List list = new ArrayList();
            for (CreatticketVO meetingticket : body.getBody().getMeetingticket()) {
                list.add(meetingticket.getBegintime());
            }
            long mintime = (long) Collections.max(list);
            if (date.getTime() < mintime) {
                meeting.setStatus(Meeting.MEETING_STATUS_ACTICITY_NO);
            } else if (date.getTime() > mintime && date.getTime() < body.getBody().getMeetingstarttime()) {
                meeting.setStatus(Meeting.MEETING_STATUS_ACTIVITY_APPLY);
            }
        }
        //开启会议评论
        meeting.setCommentflag(body.getBody().getComment());
        meeting.setCreatetime(date);
        meetingMapper.insert(meeting);
        //主办方
        for (Integer oid : body.getBody().getSid()) {
            Meetingsponsor meetingsponsor = new Meetingsponsor();
            meetingsponsor.setUid(usertoken.getUid());
            meetingsponsor.setMid(meeting.getMid());
            meetingsponsor.setUpdatetime(date);
            meetingsponsor.setCreatetime(date);
            meetingsponsor.setOid(oid);
            meetingsponsorMapper.insert(meetingsponsor);
        }
        //会议标签
        if (body.getBody().getTag() != null) {
            for (String tag : body.getBody().getTag()) {
                Meetingtag meetingtag = new Meetingtag();
                meetingtag.setCreatetime(date);
                meetingtag.setTag(tag);
                meetingtag.setMid(meeting.getMid());
                meetingtagMapper.insert(meetingtag);
            }
        }

        //门票
        if (body.getBody().getMeetingticket().size() == 0) {
            Meetingticket meetingticket = new Meetingticket();
            meetingticket.setMid(meeting.getMid());
            meetingticket.setName("免费票");
            meetingticket.setPrice(Meetingticket.PRICE);
            meetingticket.setAmount(Meetingticket.AMOUNT_INFINITY);
            meetingticket.setRemaincount(Meetingticket.AMOUNT_INFINITY);
            meetingticket.setMincount(1);
            meetingticket.setMaxcount(Meetingticket.AMOUNT_INFINITY);
            meetingticket.setStatus(Meetingticket.STATUS_HAND);
            meetingticket.setCreatetime(date);
            meetingticket.setUpdatetime(date);
            try {
                meetingticket.setBegintime(date);
                meetingticket.setEndtime(meeting.getStarttime());//活动开始时间
            } catch (Exception e) {
                e.printStackTrace();
            }
            meetingticketMapper.insert(meetingticket);
        } else {
            for (CreatticketVO ticket : register) {
                Meetingticket meetingticket = new Meetingticket();
                meetingticket.setMid(meeting.getMid());
                if (ticket.getName() == null) {
                    meetingticket.setName("普通票");
                } else {
                    meetingticket.setName(ticket.getName());
                }
                if (ticket.getPrice() == null) {
                    meetingticket.setPrice(Meetingticket.PRICE);
                } else {
                    meetingticket.setPrice(ticket.getPrice());
                }
                if (ticket.getAmount() == 0) {
                    meetingticket.setAmount(Meetingticket.AMOUNT_INFINITY);
                    meetingticket.setRemaincount(Meetingticket.AMOUNT_INFINITY);
                } else {
                    meetingticket.setAmount(ticket.getAmount());
                    meetingticket.setRemaincount(ticket.getAmount());
                }
                try {
                    if (ticket.getEndtime() == 0) {
                        meetingticket.setEndtime(meeting.getStarttime());
                    } else {
                        meetingticket.setEndtime(DateUtil.longToDate(ticket.getEndtime()));
                    }
                    if (ticket.getBegintime() == 0) {
                        meetingticket.setBegintime(date);
                    } else {
                        meetingticket.setBegintime(DateUtil.longToDate(ticket.getBegintime()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (ticket.getMincount() == 0) {
                    meetingticket.setMincount(Meetingticket.MINCOUNT);
                } else {
                    meetingticket.setMincount(ticket.getMincount());
                }
                if (ticket.getMaxcount() == 0) {
                    meetingticket.setMaxcount(Meetingticket.AMOUNT_INFINITY);
                } else {
                    meetingticket.setMaxcount(ticket.getMaxcount());
                }
                meetingticket.setDescription(ticket.getDescription());
                meetingticket.setStatus(Meetingticket.STATUS_HAND);
                meetingticket.setCreatetime(date);
                meetingticket.setUpdatetime(date);
                meetingticketMapper.insert(meetingticket);
            }
        }
        if (body.getBody().getFillFields() != null) {
            for (FillField ff : body.getBody().getFillFields()) {
                if (ff.getType() == 0) {
                    Signfield signfield = new Signfield();
                    signfield.setMid(meeting.getMid());
                    signfield.setFieldname(ff.getContent());
                    signfield.setFieldcode("UTF-8");
                    signfield.setFielddescription("姓名");
                    signfield.setIsrequire(ff.getIsrequire());
                    signfield.setCreatetime(date);
                    signfield.setUpdatetime(date);
                    signfieldMapper.insert(signfield);
                }
                if (ff.getType() == 1) {
                    Signfield signfield = new Signfield();
                    signfield.setMid(meeting.getMid());
                    signfield.setFieldname(ff.getContent());
                    signfield.setFieldcode("UTF-8");
                    signfield.setFielddescription("电话");
                    signfield.setIsrequire(ff.getIsrequire());
                    signfield.setCreatetime(date);
                    signfield.setUpdatetime(date);
                    signfieldMapper.insert(signfield);
                }
                if (ff.getType() == 2) {
                    Signfield signfield = new Signfield();
                    signfield.setMid(meeting.getMid());
                    signfield.setFieldname(ff.getContent());
                    signfield.setFieldcode("UTF-8");
                    signfield.setFielddescription("单行文本");
                    signfield.setIsrequire(ff.getIsrequire());
                    signfield.setCreatetime(date);
                    signfield.setUpdatetime(date);
                    signfieldMapper.insert(signfield);
                }
                if (ff.getType() == 3) {
                    Signfield signfield = new Signfield();
                    signfield.setMid(meeting.getMid());
                    signfield.setFieldname(ff.getContent());
                    signfield.setFieldcode("UTF-8");
                    signfield.setFielddescription("多行文本");
                    signfield.setIsrequire(ff.getIsrequire());
                    signfield.setCreatetime(date);
                    signfield.setUpdatetime(date);
                    signfieldMapper.insert(signfield);
                }
                if (ff.getType() == 4) {
                    for (String op : ff.getOptions()) {
                        Signfield signfield = new Signfield();
                        signfield.setMid(meeting.getMid());
                        signfield.setFieldname(ff.getContent());
                        signfield.setFieldcode("UTF-8");
                        signfield.setFielddescription("单选项");
                        signfield.setFieldval(op);
                        signfield.setIsrequire(ff.getIsrequire());
                        signfield.setCreatetime(date);
                        signfield.setUpdatetime(date);
                        signfieldMapper.insert(signfield);
                    }
                }
                if (ff.getType() == 5) {
                    for (String op : ff.getOptions()) {
                        Signfield signfield = new Signfield();
                        signfield.setMid(meeting.getMid());
                        signfield.setFieldname(ff.getContent());
                        signfield.setFieldcode("UTF-8");
                        signfield.setFielddescription("多选项");
                        signfield.setFieldval(op);
                        signfield.setIsrequire(ff.getIsrequire());
                        signfield.setCreatetime(date);
                        signfield.setUpdatetime(date);
                        signfieldMapper.insert(signfield);
                    }
                }
            }
        }
        Map<String, Object> map = new HashMap<>();
        map.put("mid", meeting.getMid());
        return new ReturnMap("会议创建成功!", map);
    }

    @Override
    public ReturnMap cluster_meeting(SpellgroupBody body) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(body.getToken());
        if (body.getToken() == null || usertoken == null) {
            return new ReturnMap(false, "请重新登录");
        }
        if (body.getBody().getMeetingname() == null || body.getBody().getPic() == null || body.getBody().getMeetingstarttime() == 0
                || body.getBody().getMeetingendtime() == 0 || body.getBody().getAddress().getProvice() == null || body.getBody().getAddress().getCity() == null ||
                body.getBody().getAddress().getDist() == null || body.getBody().getAddress().getSite() == null || body.getBody().getSid() == null || body.getBody().getTicket() == null) {
            return new ReturnMap(false, "请重新登录");
        }
        if (body.getBody().getAddress().getProvice() == "" || body.getBody().getAddress().getCity() == "" || body.getBody().getAddress().getDist() == ""
                || body.getBody().getAddress().getSite() == "") {
            return new ReturnMap(false, "请核对您填的地址信息");
        }
        Date date = new Date();
        //会议基础信息
        Meeting meeting = new Meeting();
        meeting.setIsopen(body.getBody().getIsopen());
        meeting.setPic(body.getBody().getPic());
        //保存会议地址
        String address = body.getBody().getAddress().getProvice() + "-" + body.getBody().getAddress().getCity() + "-" + body.getBody().getAddress().getDist() + "-" + body.getBody().getAddress().getSite();
        meeting.setAddress(address);
        meeting.setScale(body.getBody().getScale());//设置会议规模
        try {
            meeting.setEndtime(DateUtil.longToDate(body.getBody().getMeetingendtime()));
            meeting.setStarttime(DateUtil.longToDate(body.getBody().getMeetingstarttime()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        meeting.setLat(body.getBody().getAddress().getLat());
        meeting.setLng(body.getBody().getAddress().getLng());
        meeting.setUpdatetime(date);
        meeting.setUid(usertoken.getUid());
        meeting.setIntroduction(body.getBody().getIntroduction());
        meeting.setInvoiceflag(body.getBody().getInvoiceflag());
        meeting.setMeetingtype(Meeting.MEETINGTYPEA_TEAM);
        meeting.setName(body.getBody().getMeetingname());
        if (date.getTime() > meeting.getEndtime().getTime()) {//如果当前时间大于活动结束时间,活动已结束
            meeting.setStatus(Meeting.MEETING_STATUS_ACTIVITY_END);
        } else if (date.getTime() < meeting.getStarttime().getTime()) {//如果当前时间小于活动开始时间,报名中
            meeting.setStatus(Meeting.MEETING_STATUS_ACTIVITY_APPLY);
        } else if (date.getTime() > meeting.getStarttime().getTime() && date.getTime() < meeting.getEndtime().getTime()) {//如果当前时间大于活动开始时间小于活动结束时间,活动中
            meeting.setStatus(Meeting.MEETING_STATUS_ACTIVITY_YES);
        }
        meeting.setCreatetime(date);
        //开启会议评论
        meeting.setCommentflag(body.getBody().getComment());
        meetingMapper.insert(meeting);
        //主办方
        if (body.getBody().getSid() != null) {
            for (Integer oid : body.getBody().getSid()) {
                Meetingsponsor meetingsponsor = new Meetingsponsor();
                meetingsponsor.setUid(usertoken.getUid());
                meetingsponsor.setMid(meeting.getMid());
                meetingsponsor.setUpdatetime(date);
                meetingsponsor.setCreatetime(date);
                meetingsponsor.setOid(oid);
                meetingsponsorMapper.insert(meetingsponsor);
            }
        }
        //会议标签
        if (body.getBody().getTag() != null) {
            for (String tag : body.getBody().getTag()) {
                Meetingtag meetingtag = new Meetingtag();
                meetingtag.setCreatetime(date);
                meetingtag.setTag(tag);
                meetingtag.setMid(meeting.getMid());
                meetingtagMapper.insert(meetingtag);
            }
        }
        //门票
        ClusterTicket ticket = body.getBody().getTicket();
        if (ticket.getTkendtime() > meeting.getStarttime().getTime()) {
            return new ReturnMap(false, "请核对你填的时间!");
        }
        Meetingticket meetingticket = new Meetingticket();
        meetingticket.setMid(meeting.getMid());
        meetingticket.setName(ticket.getName());
        meetingticket.setPrice(ticket.getPrice());
        if (ticket.getAmount() == 0) {
            meetingticket.setAmount(Meetingticket.AMOUNT_INFINITY);
            meetingticket.setRemaincount(Meetingticket.AMOUNT_INFINITY);
        } else {
            meetingticket.setAmount(ticket.getAmount());
            meetingticket.setRemaincount(ticket.getAmount());
        }
        try {
            meetingticket.setEndtime(DateUtil.longToDate(body.getBody().getTicket().getTkendtime()));
            meetingticket.setBegintime(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        meetingticket.setMaxcount(Meetingticket.MINCOUNT);
        meetingticket.setMaxcount(Meetingticket.MINCOUNT);
        meetingticket.setStatus(Meetingticket.STATUS_HAND);
        meetingticket.setCreatetime(date);
        meetingticket.setUpdatetime(date);
        meetingticketMapper.insert(meetingticket);
        List<FillField> f = body.getBody().getFillFields();
        for (FillField ff : f) {
            if (ff.getType() == 0) {
                Signfield signfield = new Signfield();
                signfield.setMid(meeting.getMid());
                signfield.setFieldname(ff.getContent());
                signfield.setFieldcode("UTF-8");
                signfield.setFielddescription("姓名");
                signfield.setIsrequire(ff.getIsrequire());
                signfield.setCreatetime(date);
                signfield.setUpdatetime(date);
                signfieldMapper.insert(signfield);
            }
            if (ff.getType() == 1) {
                Signfield signfield = new Signfield();
                signfield.setMid(meeting.getMid());
                signfield.setFieldname(ff.getContent());
                signfield.setFieldcode("UTF-8");
                signfield.setFielddescription("电话");
                signfield.setIsrequire(ff.getIsrequire());
                signfield.setCreatetime(date);
                signfield.setUpdatetime(date);
                signfieldMapper.insert(signfield);
            }
            if (ff.getType() == 2) {
                Signfield signfield = new Signfield();
                signfield.setMid(meeting.getMid());
                signfield.setFieldname(ff.getContent());
                signfield.setFieldcode("UTF-8");
                signfield.setFielddescription("单行文本");
                signfield.setIsrequire(ff.getIsrequire());
                signfield.setCreatetime(date);
                signfield.setUpdatetime(date);
                signfieldMapper.insert(signfield);
            }
            if (ff.getType() == 3) {
                Signfield signfield = new Signfield();
                signfield.setMid(meeting.getMid());
                signfield.setFieldname(ff.getContent());
                signfield.setFieldcode("UTF-8");
                signfield.setFielddescription("多行文本");
                signfield.setIsrequire(ff.getIsrequire());
                signfield.setCreatetime(date);
                signfield.setUpdatetime(date);
                signfieldMapper.insert(signfield);
            }
            if (ff.getType() == 4) {
                for (String op : ff.getOptions()) {
                    Signfield signfield = new Signfield();
                    signfield.setMid(meeting.getMid());
                    signfield.setFieldname(ff.getContent());
                    signfield.setFieldcode("UTF-8");
                    signfield.setFielddescription("单选项");
                    signfield.setFieldval(op);
                    signfield.setIsrequire(ff.getIsrequire());
                    signfield.setCreatetime(date);
                    signfield.setUpdatetime(date);
                    signfieldMapper.insert(signfield);
                }
            }
            if (ff.getType() == 5) {
                for (String op : ff.getOptions()) {
                    Signfield signfield = new Signfield();
                    signfield.setMid(meeting.getMid());
                    signfield.setFieldname(ff.getContent());
                    signfield.setFieldcode("UTF-8");
                    signfield.setFielddescription("多选项");
                    signfield.setFieldval(op);
                    signfield.setIsrequire(ff.getIsrequire());
                    signfield.setCreatetime(date);
                    signfield.setUpdatetime(date);
                    signfieldMapper.insert(signfield);
                }
            }
        }
        Map<String, Object> map = new HashMap<>();
        map.put("mid", meeting.getMid());
        return new ReturnMap("会议创建成功!", map);
    }

    @Override
    public ReturnList mymeeting(MeetingQueryObject qo) {
        List result = new ArrayList();
        Usertoken usertoken = usertokenMapper.selectUidBytoken(qo.getToken());
        if (qo.getToken() == null || usertoken == null) {
            return new ReturnList("请重新登录", false);
        }
        qo.setUid(usertoken.getUid());
        int count = meetingMapper.queryForCount(qo);
        if (count > 0) {
            List<Meeting> meetings = meetingMapper.query(qo);
            for (Meeting meeting : meetings) {
                Map<String, Object> map = new HashMap<>();
                map.put("meetingpic", meeting.getPic());
                map.put("meetingname", meeting.getName());
                map.put("meetingstart", meeting.getStarttime());
                map.put("meetingendtime", meeting.getEndtime());
                map.put("meetingstatus", meeting.getStatus());
                map.put("scalse", meeting.getScale());
                map.put("meetingtype", meeting.getMeetingtype());
                String as = meeting.getAddress();
                String[] address = as.split("-");
                if (address[0].equals(address[1])) {
                    map.put("meetingaddress", address[1]);
                } else {
                    map.put("meetingaddress", address[0] + "-" + address[1]);
                }
                map.put("soldticket", meeting.getSoldticket());
                map.put("mid", meeting.getMid());
                BigDecimal sum = orderdtlMapper.selectSum(meeting.getMid());
                if (sum == null) {
                    map.put("rental", 0);
                } else {
                    map.put("rental", sum);
                }
                List<Meetingsponsor> meetingsponsors = meetingsponsorMapper.selectByMid(meeting.getMid());
                for (Meetingsponsor meetingsponsor : meetingsponsors) {
                    Sponsor sponsor = sponsorMapper.selectByOid(meetingsponsor.getOid());
                    map.put("sponsorname", sponsor.getName());
                    map.put("sponsorlogo", sponsor.getLogo());
                }
                result.add(map);
            }
        }
        return new ReturnList(result, "查看成功!", count);
    }

    @Override
    //@Cacheable(value = "myCache", key = "'selectCarouselfigure'")
    public List selectCarouselfigure() {
        List result = new ArrayList();
        //按照会议报名人数排序取前6张图
        List<Meeting> meetings = meetingMapper.selectPicByNumber();
        for (Meeting meeting : meetings) {
            Map<String, Object> map = new HashMap<>();
            map.put("pic", meeting.getPic());
            map.put("mid", meeting.getMid());
            map.put("name", meeting.getName());
            result.add(map);
        }
        return result;
    }

    @Override
    public ReturnList selectMeetingList(MeetingQueryObject qo) {
        List result = new ArrayList();
        int count = meetingMapper.queryForCountCity(qo);
        if (count > 0) {
            List<Meeting> meetings = meetingMapper.queryCity(qo);
            for (Meeting meeting : meetings) {
                Map<String, Object> map = new HashMap<>();
                BigDecimal price = meetingticketMapper.selectMeetingTicket(meeting.getMid());
                String url = meeting.getPic();
                if (meeting.getAddress() != null) {
                    String[] address = meeting.getAddress().split("-");
                    map.put("address", address[1]);
                }
                map.put("starttime", meeting.getStarttime());
                map.put("price", price);
                map.put("name", meeting.getName());
                if (meeting.getMeetingtype() == Meeting.MEETINGTYPEA_TEAM) {
                    map.put("scale", meeting.getScale());
                }
                map.put("pic", url);
                map.put("mid", meeting.getMid());
                result.add(map);
            }
        }
        return new ReturnList(result, "查询成功!", count);
    }

    @Override
    public ReturnMap spellgroup_details(Spellgroup spellgroup) {
        if (spellgroup.getMid() == 0 || spellgroup.getTid() == 0) {
            return new ReturnMap(false, "数据异常,请重新登录!");
        }
        Meeting meeting = meetingMapper.selectMeetingByMid(spellgroup.getMid());
        Map meetingmap = new HashMap();
        meetingmap.put("meetingname", meeting.getName());
        meetingmap.put("meetingtime", meeting.getStarttime());
        meetingmap.put("pic", meeting.getPic());
        meetingmap.put("scale", meeting.getScale());
        BigDecimal price = meetingticketMapper.selectMeetingTicket(spellgroup.getMid());
        meetingmap.put("price", price);
        Spellgroup spell = spellgroupMapper.selectById(spellgroup.getTid());
        Date date = new Date();
        List<Spellgroup> sp = null;
        if (spell.getStatus() == Spellgroup.STATUS_OUTTIME || spell.getStatus() == Spellgroup.STATUS_FAIL) {
            sp = spellgroupMapper.selectFailByTid(spellgroup.getTid(), spellgroup.getMid());
        } else if (spell.getStatus() == Spellgroup.STATUS_SUCCESS) {//成功
            sp = spellgroupMapper.selectByTid(spellgroup.getTid(), spellgroup.getMid(), Spellgroup.STATUS_SUCCESS);
        } else {//待成团
            sp = spellgroupMapper.selectByTid(spellgroup.getTid(), spellgroup.getMid(), Spellgroup.STATUS_SUCCESS_A);
        }
        meetingmap.put("status", spell.getStatus());
        if (spell.getStatus() == Spellgroup.STATUS_SUCCESS_A) {
            meetingmap.put("surplus", meeting.getScale() - sp.size());
            meetingmap.put("countdown", spell.getUpdatetime().getTime() - date.getTime());
        }
        List list = new ArrayList();
        for (Spellgroup sg : sp) {
            Map<String, Object> map = new HashMap<>();
            User user = userMapper.selectByUid(sg.getUid());
            Userprofile userprofile = userprofileMapper.selectByUid(sg.getUid());
            if (userprofile.getNickname() != null) {
                map.put("nickname", userprofile.getNickname());
            } else {
                map.put("nickname", PhoneDimUtil.PhoneDim(user.getLoginname()));
            }
            map.put("jointime", sg.getCreatetime());
            map.put("type", sg.getType());
            map.put("avatar", user.getAvatar());
            list.add(map);
        }
        meetingmap.put("spelluser", list);
        return new ReturnMap("查看成功!", meetingmap);
    }

    @Override
    public ReturnList meeting_attention(MeetingQueryObject qo) {//关注的
        Usertoken usertoken = usertokenMapper.selectUidBytoken(qo.getToken());
        if (qo.getToken() == null || usertoken == null) {
            return new ReturnList("请重新登录!", false);
        }
        List result = new ArrayList();
        List<Integer> cs = collectattentionMapper.selectCollectAttentionCount(Collectattention.TYPE_ATTENTION, usertoken.getUid());
        if (cs.size() > 0) {
            List<Meetingsponsor> ms = meetingsponsorMapper.selectlistbyoid(cs);
            List listcms = new ArrayList();
            for (Meetingsponsor m : ms) {
                listcms.add(m.getMid());
            }
            List<Meeting> meetings = meetingMapper.selectlistbymid(listcms, qo.getStart(), qo.getPagesize());
            int count = meetingMapper.selectcountbymid(listcms);
            if (count > 0) {
                for (Meeting meeting : meetings) {
                    Map<String, Object> map = new HashMap<>();
                    BigDecimal price = meetingticketMapper.selectMeetingTicket(meeting.getMid());
                    String url = meeting.getPic();
                    if (meeting.getAddress() != null) {
                        String[] address = meeting.getAddress().split("-");
                        map.put("address", address[1]);
                    }
                    map.put("starttime", meeting.getStarttime());
                    map.put("price", price);
                    map.put("name", meeting.getName());
                    if (meeting.getMeetingtype() == Meeting.MEETINGTYPEA_TEAM) {
                        map.put("scale", meeting.getScale());
                    }
                    map.put("pic", url);
                    map.put("mid", meeting.getMid());
                    result.add(map);
                }
            }
            if (count == 0) {
                Map<String, Object> map = new HashMap<>();
                map.put("ismeeting", count);
                result.add(map);
                return new ReturnList(result, "查询成功", count);
            }
            return new ReturnList(result, "查询成功", count);
        }
        return new ReturnList(result, "查询成功", 0);
    }

    @Override
    public ReturnList meeting_nearby(MeetingQueryObject qo) {
        List result = new ArrayList();
        int count = meetingMapper.selectNearCount(qo);
        if (count > 0) {
            List<Meeting> meetings = meetingMapper.selectByLatLng(qo);
            for (Meeting meeting : meetings) {
                Map<String, Object> map = new HashMap<>();
                BigDecimal price = meetingticketMapper.selectMeetingTicket(meeting.getMid());
                String url = meeting.getPic();
                if (meeting.getAddress() != null) {
                    String[] address = meeting.getAddress().split("-");
                    map.put("address", address[1]);
                }
                map.put("starttime", meeting.getStarttime());
                map.put("price", price);
                if (meeting.getMeetingtype() == Meeting.MEETINGTYPEA_TEAM) {
                    map.put("scale", meeting.getScale());
                }
                map.put("name", meeting.getName());
                map.put("pic", url);
                map.put("mid", meeting.getMid());
                result.add(map);
            }
        }
        return new ReturnList(result, "查询成功!", count);
    }

    @Override
    public ReturnMap update_meeting_see(IdAndToken idAndToken) {
        if (idAndToken.getMid() == 0 || idAndToken.getToken() == null) {
            return new ReturnMap(false, "账号异常,请重新登录!");
        }
        Usertoken usertoken = usertokenMapper.selectUidBytoken(idAndToken.getToken());
        if (usertoken == null) {
            return new ReturnMap(false, "请重新登录!");
        }
        Meeting meeting = meetingMapper.selectMeetingByMid(idAndToken.getMid());
        if (meeting.getSoldticket() > 0) {
            return new ReturnMap(false, "此会议不允许修改!");
        }
        Map<String, Object> map = new HashMap<>();
        if (meeting.getMeetingtype() == Meeting.MEETINGTYPEA_TEAM) {
            map.put("scale", meeting.getScale());
        }
        map.put("pic", meeting.getPic());
        map.put("meetingname", meeting.getName());
        map.put("meetingstarttime", meeting.getStarttime());
        map.put("meetingtype", meeting.getMeetingtype());
        map.put("meetingendtime", meeting.getEndtime());
        map.put("introduction", meeting.getIntroduction());
        map.put("isopen", meeting.getIsopen());
        map.put("comment", meeting.getCommentflag());
        map.put("mid", idAndToken.getMid());
        map.put("lat", meeting.getLat());
        map.put("lng", meeting.getLng());
        map.put("invoiceflag", meeting.getInvoiceflag());
        Map<String, Object> addressmap = new HashMap<>();
        String as = meeting.getAddress();
        String[] address = as.split("-");
        if (address.length == 3) {
            addressmap.put("provice", address[0]);
            addressmap.put("city", address[1]);
            addressmap.put("dist", address[2]);
        } else {
            addressmap.put("provice", address[0]);
            addressmap.put("city", address[1]);
            addressmap.put("dist", address[2]);
            addressmap.put("site", address[3]);
        }
        map.put("address", addressmap);
        List<Meetingsponsor> meetingsponsorMappers = meetingsponsorMapper.selectByMid(idAndToken.getMid());
        List<Map<String, Object>> sponsormap = new ArrayList<>();
        for (Meetingsponsor sponsor : meetingsponsorMappers) {
            Sponsor sponsor1 = sponsorMapper.selectByOid(sponsor.getOid());
            Map<String, Object> map1 = new HashMap<>();
            map1.put("sid", sponsor1.getOid());
            map1.put("name", sponsor1.getName());
            map1.put("tel", sponsor1.getTel());
            map1.put("introduction", sponsor1.getIntroduction());
            map1.put("status", sponsor1.getStatus());
            sponsormap.add(map1);
        }
        map.put("sponsors", sponsormap);
        List<String> tag = meetingtagMapper.selectTagByMid(idAndToken.getMid());
        map.put("tag", tag);
        List<Signfield> signfields = signfieldMapper.selectSignfieldMid(idAndToken.getMid());
        List mapList = new ArrayList();
        for (Signfield signfield : signfields) {
            Map<String, Object> newmap = new HashMap<>();
            List onechoiceval = new ArrayList();
            if (signfield.getFielddescription().equals("单选项")) {
                List<String> signfieldval = signfieldMapper.selectSignfieldVal(signfield.getFieldname(), signfield.getFielddescription(), idAndToken.getMid());
                for (String s : signfieldval) {
                    onechoiceval.add(s);
                }
                newmap.put("options", onechoiceval);
                newmap.put("type", 4);
            }
            if (signfield.getFielddescription().equals("多选项")) {
                List<String> signfieldval = signfieldMapper.selectSignfieldVal(signfield.getFieldname(), signfield.getFielddescription(), idAndToken.getMid());
                for (String s : signfieldval) {
                    onechoiceval.add(s);
                }
                newmap.put("options", onechoiceval);
                newmap.put("type", 5);
            }
            if (signfield.getFielddescription().equals("姓名")) {
                newmap.put("type", 0);
            } else if (signfield.getFielddescription().equals("电话")) {
                newmap.put("type", 1);
            } else if (signfield.getFielddescription().equals("单行文本")) {
                newmap.put("type", 2);
            } else if (signfield.getFielddescription().equals("多行文本")) {
                newmap.put("type", 3);
            }
            newmap.put("fieldname", signfield.getFieldname());
            newmap.put("isrequire", signfield.getIsrequire());
            mapList.add(newmap);
        }
        map.put("fillFields", mapList);
        List<Meetingticket> meetingticket = meetingticketMapper.selectAllMeetingTicket(idAndToken.getMid());
        map.put("meetingticket", meetingticket);
        return new ReturnMap("查看成功!", map);
    }

    @Override
    //@CacheEvict(value = "myCache", key = "'applymessage'+#meetingBody.mid")
    public JSONResult update_meeting(MeetingBody meetingBody) {
        if (meetingBody.getBody().getMeetingname() == null || meetingBody.getBody().getPic() == null || meetingBody.getBody().getMeetingstarttime() == 0
                || meetingBody.getBody().getMeetingendtime() == 0 || meetingBody.getBody().getAddress().getProvice() == null || meetingBody.getBody().getAddress().getCity() == null ||
                meetingBody.getBody().getAddress().getDist() == null || meetingBody.getBody().getAddress().getSite() == null || meetingBody.getBody().getSid() == null) {
            return new JSONResult(false, "请重新登录");
        }
        if (meetingBody.getBody().getAddress().getProvice() == "" || meetingBody.getBody().getAddress().getCity() == "" || meetingBody.getBody().getAddress().getDist() == ""
                || meetingBody.getBody().getAddress().getSite() == "") {
            return new JSONResult(false, "请核对您填的地址信息");
        }
        List<CreatticketVO> register = meetingBody.getBody().getMeetingticket();
        if (register.size() > 1) {
            List<String> names = new ArrayList<>();
            for (CreatticketVO creatticketVO : register) {
                names.add(creatticketVO.getName());
            }
            if (names.size() != new HashSet<Object>(names).size()) {
                return new JSONResult(false, "门票名称不能重复");
            }
        }
        Usertoken usertoken = usertokenMapper.selectUidBytoken(meetingBody.getToken());
        if (meetingBody.getToken() == null || usertoken == null) {
            return new JSONResult(false, "请重新登录!");
        }
        Meeting mt = meetingMapper.selectMeetingByMid(meetingBody.getMid());
        if (mt.getSoldticket() > 0) {
            return new JSONResult(false, "此会议不允许修改!");
        }
        Date date = new Date();
        //会议基础信息
        Meeting meeting = new Meeting();
        meeting.setMid(meetingBody.getMid());
        meeting.setIsopen(meetingBody.getBody().getIsopen());
        meeting.setPic(meetingBody.getBody().getPic());
        //保存会议地址
        String address = meetingBody.getBody().getAddress().getProvice() + "-" + meetingBody.getBody().getAddress().getCity() + "-" + meetingBody.getBody().getAddress().getDist() + "-" + meetingBody.getBody().getAddress().getSite();
        meeting.setAddress(address);
        try {
            meeting.setEndtime(DateUtil.longToDate(meetingBody.getBody().getMeetingendtime()));
            meeting.setStarttime(DateUtil.longToDate(meetingBody.getBody().getMeetingstarttime()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        meeting.setScale(meetingBody.getBody().getScale());
        meeting.setLat(meetingBody.getBody().getAddress().getLat());
        meeting.setLng(meetingBody.getBody().getAddress().getLng());
        meeting.setUpdatetime(date);
        meeting.setUid(usertoken.getUid());
        meeting.setIntroduction(meetingBody.getBody().getIntroduction());
        meeting.setName(meetingBody.getBody().getMeetingname());
        meeting.setInvoiceflag(meetingBody.getBody().getInvoiceflag());
        meeting.setStatus(Meeting.MEETING_STATUS_ACTICITY_NO);
        meeting.setCommentflag(meetingBody.getBody().getComment());
        meeting.setCreatetime(date);
        meetingMapper.updateByPrimaryKey(meeting);
        //主办方
        if (meetingBody.getBody().getSid() != null) {
            meetingsponsorMapper.delectByMid(meetingBody.getMid());
            for (Integer oid : meetingBody.getBody().getSid()) {
                Meetingsponsor meetingsponsor = new Meetingsponsor();
                meetingsponsor.setUid(usertoken.getUid());
                meetingsponsor.setMid(meetingBody.getMid());
                meetingsponsor.setUpdatetime(date);
                meetingsponsor.setCreatetime(date);
                meetingsponsor.setOid(oid);
                meetingsponsorMapper.insert(meetingsponsor);
            }
        }
        //会议标签
        meetingtagMapper.delectByMid(meeting.getMid());
        for (String tag : meetingBody.getBody().getTag()) {
            Meetingtag meetingtag = new Meetingtag();
            meetingtag.setCreatetime(date);
            meetingtag.setTag(tag);
            meetingtag.setMid(meetingBody.getMid());
            meetingtagMapper.insert(meetingtag);
        }
        //门票
        if (register != null) {
            meetingticketMapper.delectByMid(meetingBody.getMid());
            for (CreatticketVO ticket : register) {
                Meetingticket meetingticket = new Meetingticket();
                meetingticket.setMid(meetingBody.getMid());
                meetingticket.setName(ticket.getName());
                meetingticket.setPrice(ticket.getPrice());
                if (ticket.getAmount() == 0) {
                    meetingticket.setAmount(Meetingticket.AMOUNT_INFINITY);
                    meetingticket.setRemaincount(Meetingticket.AMOUNT_INFINITY);
                } else {
                    meetingticket.setAmount(ticket.getAmount());
                    meetingticket.setRemaincount(ticket.getAmount());
                }
                try {
                    if (ticket.getBegintime() == 0) {
                        meetingticket.setBegintime(date);
                    } else {
                        meetingticket.setBegintime(DateUtil.longToDate(ticket.getBegintime()));
                    }
                    if (ticket.getEndtime() == 0) {
                        meetingticket.setEndtime(DateUtil.longToDate(meetingBody.getBody().getMeetingstarttime()));
                    } else {
                        meetingticket.setEndtime(DateUtil.longToDate(ticket.getEndtime()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (ticket.getMincount() == 0) {
                    meetingticket.setMincount(1);
                } else {
                    meetingticket.setMincount(ticket.getMincount());
                }
                if (ticket.getMaxcount() == 0) {
                    meetingticket.setMaxcount(Meetingticket.AMOUNT_INFINITY);
                } else {
                    meetingticket.setMaxcount(ticket.getMaxcount());
                }
                meetingticket.setDescription(ticket.getDescription());
                meetingticket.setCreatetime(date);
                meetingticket.setUpdatetime(date);
                meetingticketMapper.insert(meetingticket);
            }
        }
        if (meetingBody.getBody().getFillFields() != null) {
            signfieldMapper.delectByMid(meetingBody.getMid());
            for (FillField ff : meetingBody.getBody().getFillFields()) {
                if (ff.getType() == 0) {
                    Signfield signfield = new Signfield();
                    signfield.setMid(meetingBody.getMid());
                    signfield.setFieldname(ff.getContent());
                    signfield.setFieldcode("UTF-8");
                    signfield.setFielddescription("姓名");
                    signfield.setIsrequire(ff.getIsrequire());
                    signfield.setCreatetime(date);
                    signfield.setUpdatetime(date);
                    signfieldMapper.insert(signfield);
                }
                if (ff.getType() == 1) {
                    Signfield signfield = new Signfield();
                    signfield.setMid(meetingBody.getMid());
                    signfield.setFieldname(ff.getContent());
                    signfield.setFieldcode("UTF-8");
                    signfield.setFielddescription("电话");
                    signfield.setIsrequire(ff.getIsrequire());
                    signfield.setCreatetime(date);
                    signfield.setUpdatetime(date);
                    signfieldMapper.insert(signfield);
                }
                if (ff.getType() == 2) {
                    Signfield signfield = new Signfield();
                    signfield.setMid(meetingBody.getMid());
                    signfield.setFieldname(ff.getContent());
                    signfield.setFieldcode("UTF-8");
                    signfield.setFielddescription("单行文本");
                    signfield.setIsrequire(ff.getIsrequire());
                    signfield.setCreatetime(date);
                    signfield.setUpdatetime(date);
                    signfieldMapper.insert(signfield);
                }
                if (ff.getType() == 3) {
                    Signfield signfield = new Signfield();
                    signfield.setMid(meetingBody.getMid());
                    signfield.setFieldname(ff.getContent());
                    signfield.setFieldcode("UTF-8");
                    signfield.setFielddescription("多行文本");
                    signfield.setIsrequire(ff.getIsrequire());
                    signfield.setCreatetime(date);
                    signfield.setUpdatetime(date);
                    signfieldMapper.insert(signfield);
                }
                if (ff.getType() == 4) {
                    for (String op : ff.getOptions()) {
                        Signfield signfield = new Signfield();
                        signfield.setMid(meetingBody.getMid());
                        signfield.setFieldname(ff.getContent());
                        signfield.setFieldcode("UTF-8");
                        signfield.setFielddescription("单选项");
                        signfield.setFieldval(op);
                        signfield.setIsrequire(ff.getIsrequire());
                        signfield.setCreatetime(date);
                        signfield.setUpdatetime(date);
                        signfieldMapper.insert(signfield);
                    }
                }
                if (ff.getType() == 5) {
                    for (String op : ff.getOptions()) {
                        Signfield signfield = new Signfield();
                        signfield.setMid(meetingBody.getMid());
                        signfield.setFieldname(ff.getContent());
                        signfield.setFieldcode("UTF-8");
                        signfield.setFielddescription("多选项");
                        signfield.setFieldval(op);
                        signfield.setIsrequire(ff.getIsrequire());
                        signfield.setCreatetime(date);
                        signfield.setUpdatetime(date);
                        signfieldMapper.insert(signfield);
                    }
                }
            }
        }
        Map<String, Object> map = new HashMap<>();
        map.put("mid", meeting.getMid());
        return new JSONResult("修改成功");
    }

    @Override
    public JSONResult del_meeting(IdAndToken idAndToken) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(idAndToken.getToken());
        if (idAndToken.getMid() == 0 || idAndToken.getToken() == null || usertoken == null) {
            return new JSONResult(false, "请重新登录!");
        }
        int count = orderdtlMapper.selectCountByMid(idAndToken.getMid());
        Meeting mt = meetingMapper.selectMeetingByMid(idAndToken.getMid());
        if (mt.getSoldticket() > 0 || count > 0) {
            return new JSONResult(false, "此会议不允许删除!");
        }
        int amount = meetingMapper.updateMeetingStatus(mt.getMid(), Meeting.MEETING_STATUS_ACTIVITY_DEL);
        collectattentionMapper.updateByStatus(mt.getMid(), Collectattention.STATUS_DEL);
        return new JSONResult(true, "删除成功");
    }

    @Override
    public Meeting selectMeetingByMid(int mid) {
        Meeting meeting = meetingMapper.selectMeetingByMid(mid);
        return meeting;
    }
}
