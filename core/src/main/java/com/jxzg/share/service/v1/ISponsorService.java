package com.jxzg.share.service.v1;

import com.jxzg.share.domain.meeting.Sponsor;
import com.jxzg.share.querypage.QueryObject;
import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.returndomain.ReturnList;
import com.jxzg.share.returndomain.ReturnMap;

/**
 * 主办方
 */
public interface ISponsorService {
    ReturnMap insert(Sponsor sponsor);

    ReturnList selectSponsorAll(QueryObject qo);

    JSONResult deleteByOid(Sponsor sponsor);

    JSONResult sponsorupdate(Sponsor sponsor);

    ReturnMap sponsor_particulars(int id, String token);
}
