package com.jxzg.share.service.v1;

import com.jxzg.share.domain.other.Commet;
import com.jxzg.share.querypage.CommetQueryObject;
import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.returndomain.ReturnPage;

/**
 * Created by 00818Wolf on 2017/1/3.
 */
public interface ICommetService {
    /**
     * 评论保存
     *
     * @param commet
     */
    JSONResult insert(Commet commet);

    /**
     * 评论回复
     *
     * @param commet
     */
    JSONResult commetreply(Commet commet);

    /**
     * 评论列表
     *
     * @return
     */
    ReturnPage commetlist(CommetQueryObject qo);

    /**
     * 点赞
     *
     * @param commet
     * @return
     */
    JSONResult commetlike(Commet commet);
}
