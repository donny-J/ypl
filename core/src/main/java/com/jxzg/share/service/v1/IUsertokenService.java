package com.jxzg.share.service.v1;

import com.jxzg.share.domain.other.Usertoken;

/**
 * token令牌
 */
public interface IUsertokenService {
    void insert(Usertoken usertoken);

    /**
     * 通过token查找用户
     * @param token
     * @return
     */
    Usertoken selectUidBytoken(String token);


    /**
     * 更新过期时间
     * @param usertoken
     */
    void updateExpiretime(Usertoken usertoken);
}
