package com.jxzg.share.service.impl.v1;

import com.jxzg.share.domain.other.*;
import com.jxzg.share.mapper.*;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.service.v1.IWeixinService;
import com.jxzg.share.toolUtil.DateUtil;
import com.jxzg.share.vo.WeixinLoginVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by 00818Wolf on 2017/3/20.
 */
@Service
public class WeixinServiceImpl implements IWeixinService {
    @Autowired
    private UserconnectMapper userconnectMapper;
    @Autowired
    private LoginlogMapper loginlogMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserprofileMapper userprofileMapper;
    @Autowired
    private UsertokenMapper usertokenMapper;

    @Override
    public ReturnMap weixin_login(WeixinLoginVO weixinLogin) {
        if (weixinLogin.getOpenid() == null) {
            return new ReturnMap(false, "登录失败!");
        }
        Date date = new Date();
        Userconnect userconnect = userconnectMapper.selectByPrimaryKey(weixinLogin.getOpenid());
        if (userconnect == null) {
            Map<String, Object> map = new HashMap<>();
            //初始化用户信息
            User user = new User();
            user.setAvatar(weixinLogin.getHeadimgurl());
            user.setLoginname("未设置");
            userMapper.insert(user);
            Userprofile userprofile = new Userprofile();
            userprofile.setUid(user.getUid());
            userprofile.setNickname(weixinLogin.getNickname());
            userprofile.setProvice(weixinLogin.getProvince());
            userprofile.setCity(weixinLogin.getCity());
            userprofile.setGender(weixinLogin.getSex());
            userprofile.setRealname("未设置");
            userprofile.setAddress("未设置");
            userprofileMapper.insert(userprofile);
            Userconnect unt = new Userconnect();
            unt.setUid(user.getUid());
            unt.setType(weixinLogin.getType());
            unt.setAccesstoken(weixinLogin.getAccessToken());
            unt.setRefreshtoken(weixinLogin.getRefreshToken());
            unt.setThirdid(weixinLogin.getOpenid());
            unt.setUnionid(weixinLogin.getUnionid());
            unt.setCreatetime(date);
            unt.setUpdatetime(date);
            unt.setStatus(Userconnect.LOGIN_SUCCESS);
            unt.setLat(weixinLogin.getLat());
            unt.setLng(weixinLogin.getLng());
            userconnectMapper.insert(unt);
            String token = UUID.randomUUID().toString();
            Usertoken usertoken = new Usertoken();
            //usertoken.setLoginip(ip);
            usertoken.setUid(user.getUid());
            usertoken.setLat(user.getLat());
            usertoken.setLng(user.getLng());
            usertoken.setLoginname(user.getLoginname());
            usertoken.setLogintime(date);
            usertoken.setLogintype(weixinLogin.getType());
            usertoken.setToken(token);
            usertoken.setExpiretime(DateUtil.getExpiretime());//终止时间,一个月后
            Usertoken usertoken1 = usertokenMapper.selectByUidAndType(user.getUid(), weixinLogin.getType());
            if (usertoken1 != null) {
                usertokenMapper.deleteByUid(user.getUid(), weixinLogin.getType());
            }
            usertokenMapper.insert(usertoken);
            Loginlog iplog = new Loginlog();
            iplog.setLogintime(date);
            iplog.setLoginname(weixinLogin.getOpenid());
            iplog.setLoginpassword(weixinLogin.getUnionid());
            iplog.setLogintype(Loginlog.LOGINTYPR_WEIXIN);
            iplog.setLogintime(date);
            iplog.setLng(weixinLogin.getLng());
            iplog.setLat(weixinLogin.getLat());
            iplog.setStatus(Loginlog.LOGIN_SUCCESS);
            loginlogMapper.insert(iplog);
            map.put("f1", true);
            map.put("token", token);
            return new ReturnMap("登录成功!", map);
        } else {
            //如果有,直接登录,生成登录日志,更新token信息
            Map<String, Object> map = new HashMap<>();
            Loginlog iplog = new Loginlog();
            iplog.setLogintime(date);
            iplog.setLoginname(weixinLogin.getOpenid());
            iplog.setLoginpassword(weixinLogin.getUnionid());
            iplog.setLogintype(Loginlog.LOGINTYPR_WEIXIN);
            iplog.setLogintime(date);
            iplog.setLng(weixinLogin.getLng());
            iplog.setLat(weixinLogin.getLat());
            iplog.setStatus(Loginlog.LOGIN_SUCCESS);
            loginlogMapper.insert(iplog);
            String token = UUID.randomUUID().toString();
            User user = userMapper.selectByUid(userconnect.getUid());
            Usertoken usertoken = new Usertoken();
            //usertoken.setLoginip(ip);
            usertoken.setUid(user.getUid());
            usertoken.setLat(user.getLat());
            usertoken.setLng(user.getLng());
            usertoken.setLoginname(user.getLoginname());
            usertoken.setLogintime(date);
            usertoken.setLogintype(weixinLogin.getType());
            usertoken.setToken(token);
            //usertoken.setExpiretime(DateUtil.getExpiretime());//终止时间,一个月后
            Usertoken usertoken1 = usertokenMapper.selectByUidAndType(user.getUid(), weixinLogin.getType());
            if (usertoken1 != null) {
                usertokenMapper.deleteByUid(user.getUid(), weixinLogin.getType());
            }
            usertokenMapper.insert(usertoken);
            map.put("f1", false);
            map.put("token", token);
            return new ReturnMap("登录成功!", map);
        }
    }

    @Override
    public ReturnMap weixinmp_login(WeixinLoginVO weixinLogin) {
        if (weixinLogin.getOpenid() == null) {
            return new ReturnMap(false, "登录失败!");
        }
        Date date = new Date();
        Userconnect userconnect = userconnectMapper.selectByPrimaryKey(weixinLogin.getOpenid());
        if (userconnect == null) {
            Integer uid = userconnectMapper.selectByUnionid(weixinLogin.getUnionid());
            Map<String, Object> map = new HashMap<>();
            if (uid == null) {
                //初始化用户信息
                User user = new User();
                user.setAvatar(weixinLogin.getHeadimgurl());
                user.setLoginname("未设置");
                userMapper.insert(user);
                Userprofile userprofile = new Userprofile();
                userprofile.setUid(user.getUid());
                userprofile.setNickname(weixinLogin.getNickname());
                userprofile.setProvice(weixinLogin.getProvince());
                userprofile.setCity(weixinLogin.getCity());
                userprofile.setGender(weixinLogin.getSex());
                userprofile.setRealname("未设置");
                userprofile.setAddress("未设置");
                userprofileMapper.insert(userprofile);
                Userconnect unt = new Userconnect();
                unt.setUid(user.getUid());
                unt.setType(weixinLogin.getType());
                unt.setAccesstoken(weixinLogin.getAccessToken());
                unt.setRefreshtoken(weixinLogin.getRefreshToken());
                unt.setThirdid(weixinLogin.getOpenid());
                unt.setUnionid(weixinLogin.getUnionid());
                unt.setCreatetime(date);
                unt.setUpdatetime(date);
                unt.setStatus(Userconnect.LOGIN_SUCCESS);
                unt.setLat(weixinLogin.getLat());
                unt.setLng(weixinLogin.getLng());
                userconnectMapper.insert(unt);
                String token = UUID.randomUUID().toString();
                Usertoken usertoken = new Usertoken();
                //usertoken.setLoginip(ip);
                usertoken.setUid(user.getUid());
                usertoken.setLat(user.getLat());
                usertoken.setLng(user.getLng());
                usertoken.setLoginname(user.getLoginname());
                usertoken.setLogintime(date);
                usertoken.setLogintype(weixinLogin.getType());
                usertoken.setToken(token);
                usertoken.setExpiretime(DateUtil.getExpiretime());//终止时间,一个月后
                Usertoken usertoken1 = usertokenMapper.selectByUidAndType(user.getUid(), weixinLogin.getType());
                if (usertoken1 != null) {
                    usertokenMapper.deleteByUid(user.getUid(), weixinLogin.getType());
                }
                usertokenMapper.insert(usertoken);
                Loginlog iplog = new Loginlog();
                iplog.setLogintime(date);
                iplog.setLoginname(weixinLogin.getOpenid());
                iplog.setLoginpassword(weixinLogin.getUnionid());
                iplog.setLogintype(Loginlog.LOGINTYPR_WEIXIN);
                iplog.setLogintime(date);
                iplog.setLng(weixinLogin.getLng());
                iplog.setLat(weixinLogin.getLat());
                iplog.setStatus(Loginlog.LOGIN_SUCCESS);
                loginlogMapper.insert(iplog);
                map.put("f1", true);
                map.put("token", token);
                return new ReturnMap("登录成功!", map);
            } else {
                Userconnect unt = new Userconnect();
                unt.setUid(uid);
                unt.setType(weixinLogin.getType());
                unt.setAccesstoken(weixinLogin.getAccessToken());
                unt.setRefreshtoken(weixinLogin.getRefreshToken());
                unt.setThirdid(weixinLogin.getOpenid());
                unt.setUnionid(weixinLogin.getUnionid());
                unt.setCreatetime(date);
                unt.setUpdatetime(date);
                unt.setStatus(Userconnect.LOGIN_SUCCESS);
                unt.setLat(weixinLogin.getLat());
                unt.setLng(weixinLogin.getLng());
                userconnectMapper.insert(unt);
                Loginlog iplog = new Loginlog();
                iplog.setLogintime(date);
                iplog.setLoginname(weixinLogin.getOpenid());
                iplog.setLoginpassword(weixinLogin.getUnionid());
                iplog.setLogintype(Loginlog.LOGINTYPR_WEIXIN);
                iplog.setLogintime(date);
                iplog.setLng(weixinLogin.getLng());
                iplog.setLat(weixinLogin.getLat());
                iplog.setStatus(Loginlog.LOGIN_SUCCESS);
                loginlogMapper.insert(iplog);
                String token = UUID.randomUUID().toString();
                Usertoken usertoken = new Usertoken();
                //usertoken.setLoginip(ip);
                usertoken.setUid(uid);
                usertoken.setLogintime(date);
                usertoken.setLogintype(weixinLogin.getType());
                usertoken.setToken(token);
                usertoken.setExpiretime(DateUtil.getExpiretime());//终止时间,一个月后
                Usertoken usertoken1 = usertokenMapper.selectByUidAndType(uid, weixinLogin.getType());
                if (usertoken1 != null) {
                    usertokenMapper.deleteByUid(uid, weixinLogin.getType());
                }
                usertokenMapper.insert(usertoken);
                map.put("f1", true);
                map.put("token", token);
                return new ReturnMap("登录成功!", map);
            }
        } else {
            //如果有,直接登录,生成登录日志,更新token信息
            Map<String, Object> map = new HashMap<>();
            Loginlog iplog = new Loginlog();
            iplog.setLogintime(date);
            iplog.setLoginname(weixinLogin.getOpenid());
            iplog.setLoginpassword(weixinLogin.getUnionid());
            iplog.setLogintype(Loginlog.LOGINTYPR_WEIXIN);
            iplog.setLogintime(date);
            iplog.setLng(weixinLogin.getLng());
            iplog.setLat(weixinLogin.getLat());
            iplog.setStatus(Loginlog.LOGIN_SUCCESS);
            loginlogMapper.insert(iplog);
            String token = UUID.randomUUID().toString();
            User user = userMapper.selectByUid(userconnect.getUid());
            Usertoken usertoken = new Usertoken();
            //usertoken.setLoginip(ip);
            usertoken.setUid(user.getUid());
            usertoken.setLat(user.getLat());
            usertoken.setLng(user.getLng());
            usertoken.setLoginname(user.getLoginname());
            usertoken.setLogintime(date);
            usertoken.setLogintype(weixinLogin.getType());
            usertoken.setToken(token);
            //usertoken.setExpiretime(DateUtil.getExpiretime());//终止时间,一个月后
            Usertoken usertoken1 = usertokenMapper.selectByUidAndType(user.getUid(), weixinLogin.getType());
            if (usertoken1 != null) {
                usertokenMapper.deleteByUid(user.getUid(), weixinLogin.getType());
            }
            usertokenMapper.insert(usertoken);
            map.put("f1", false);
            map.put("token", token);
            return new ReturnMap("登录成功!", map);
        }
    }
}
