package com.jxzg.share.service.impl.v1;

import com.jxzg.share.domain.other.*;
import com.jxzg.share.mapper.*;
import com.jxzg.share.pay.alipay.MD5;
import com.jxzg.share.querypage.PageResult;
import com.jxzg.share.querypage.QueryObject;
import com.jxzg.share.returndomain.JSONResult;
import com.jxzg.share.returndomain.ReturnMap;
import com.jxzg.share.service.v1.ILoginlogService;
import com.jxzg.share.service.v1.IUserprofileService;
import com.jxzg.share.vo.VerifyCodeVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 00818Wolf on 2016/12/16.
 */
@Service
public class UserprofileServiceImpl implements IUserprofileService {
    @Autowired
    private UserprofileMapper userprofileMapper;
    @Autowired
    private ILoginlogService loginlogService;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UsertokenMapper usertokenMapper;
    @Autowired
    private CollectattentionMapper collectattentionMapper;
    @Autowired
    private NotificationMapper notificationMapper;
    @Autowired
    private VerifycodeMapper verifycodeMapper;

    @Override
    public void insert(Userprofile userprofile) {
        userprofileMapper.insert(userprofile);
    }

    @Override
    public JSONResult ForgetPassword(String loginname, String loginpassword, String verifyCode) {
        if(loginname == null || loginpassword == null || verifyCode == null){
            return new JSONResult(false, "账号异常!请重新");
        }
        if (!loginlogService.validate(loginname, verifyCode, 2)) {
            User user = userMapper.selectByLoginname(loginname);
            if (user != null) {
                String password = MD5.encode(loginpassword);
                userMapper.updatePassword(password, user.getUid());
            } else {
                return new JSONResult(false, "账号不存在!");
            }
        } else {
            return new JSONResult(false, "验证码错误!");
        }
        return new JSONResult("密码修改成功!");
    }

    @Override
    public PageResult query(QueryObject qo) {
        int count = userprofileMapper.queryForCount(qo);
        if (count > 0) {
            List<Userprofile> result = userprofileMapper.query(qo);
            return new PageResult(result, qo.getPagesize(), qo.getCurrentpage(), count);
        }
        return new PageResult(new ArrayList<Loginlog>(), qo.getPagesize(), 1, 0);
    }

    //@CacheEvict(value="myCache", key="'personList'+#token")
    public JSONResult updateNickname(String nickname, String token) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(token);
        if (nickname == null || token == null || usertoken.getToken() == null) {
            return new JSONResult(false, "请重新登录!");
        }
        int upnum = userprofileMapper.updateNickname(nickname, usertoken.getUid());
        if (upnum == 0) {
            return new JSONResult(false, "昵称修改失败!");
        }
        return new JSONResult("昵称修改成功!");
    }

    @Override
    //@CacheEvict(value="myCache", key="'personList'+#token")
    public JSONResult updateRealname(String realname, String token) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(token);
        if (realname == null || token == null || usertoken == null) {
            return new JSONResult(false, "请重新登录!");
        }
        int upnum = userprofileMapper.updateRealname(realname, usertoken.getUid());
        if (upnum == 0) {
            return new JSONResult(false, "真实修改失败!");
        }
        return new JSONResult("真实修改成功!");
    }

    @Override
    //@CacheEvict(value="myCache", key="'personList'+#token")
    public JSONResult updateGender(int gender, String token) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(token);
        if (token == null || usertoken == null) {
            return new JSONResult(false, "请重新登录!");
        }
        int upnum = userprofileMapper.updateGender(gender, usertoken.getUid());
        if (upnum == 0) {
            return new JSONResult(false, "性别修改失败!");
        }
        return new JSONResult("性别修改成功!");
    }

    @Override
    //@CacheEvict(value="myCache", key="'personList'+#token")
    public JSONResult updateIdcardtype(int idcardtype, String idcard, String token) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(token);
        if (token == null || usertoken == null) {
            return new JSONResult(false, "请重新登录!");
        }
        int upnum = userprofileMapper.updateIdcardtype(idcardtype, idcard, usertoken.getUid());
        if (upnum == 0) {
            return new JSONResult(false, "证件号码修改失败!");
        }
        return new JSONResult("证件号码修改成功!");
    }

    @Override
    //@CacheEvict(value="myCache", key="'personList'+#token")
    public JSONResult updateAddress(String token, String provice, String city, String dist) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(token);
        if (token == null || usertoken == null) {
            return new JSONResult(false, "请重新登录!");
        }
        int upnum = userprofileMapper.updateAddress(provice, city, dist, usertoken.getUid());
        if (upnum == 0) {
            return new JSONResult(false, "地址修改失败!");
        }
        return new JSONResult("地址修改成功!");
    }

    @Override
    public Userprofile selectByUid(int uid) {
        return userprofileMapper.selectByUid(uid);
    }

    @Override
    //@Cacheable(value="myCache", key="'personList'+#token")
    public ReturnMap personList(String token) {
        Map<String, Object> result = new HashMap<>();
        Usertoken usertoken = usertokenMapper.selectUidBytoken(token);
        if (token == null || usertoken == null) {
            return new ReturnMap(true, "请重新登录!");
        }
        Userprofile userprofile = userprofileMapper.selectByUid(usertoken.getUid());
        User user = userMapper.selectImageAndLoginname(usertoken.getUid());
        result.put("avatar", user.getAvatar());
        result.put("loginname", user.getLoginname());
        if (userprofile.getRealname() == null) {
            result.put("realname", "");
        } else {
            result.put("realname", userprofile.getRealname());
        }
        result.put("nickname", userprofile.getNickname());
        result.put("gender", userprofile.getGender());
        result.put("position", userprofile.getPosition());
        result.put("idcardtype", userprofile.getIdcardtype());
        result.put("idcard", userprofile.getIdcard());
        result.put("provice", userprofile.getProvice());
        result.put("city", userprofile.getCity());
        result.put("dist", userprofile.getDist());
        result.put("address", userprofile.getAddress());
        result.put("worked", userprofile.getWorked());
        int attention = collectattentionMapper.selectMyCollectAttentionNum(usertoken.getUid(), Collectattention.TYPE_ATTENTION);
        result.put("attention", attention);//关注数
        int collect = collectattentionMapper.selectMyCollectAttentionNum(usertoken.getUid(), Collectattention.TYPE_COLLECT);
        result.put("collect", collect);//收藏数
        int notification = notificationMapper.selectNotificationNum(usertoken.getUid());
        result.put("notification", 0);//消息数
        return new ReturnMap("查看成功!", result);
    }

    @Override
    public JSONResult change_tel(VerifyCodeVO forgetpassword) {
        Usertoken usertoken = usertokenMapper.selectUidBytoken(forgetpassword.getToken());
        if (forgetpassword.getLoginname() == null || forgetpassword.getVerifyCode() == null || forgetpassword.getToken() == null || usertoken == null) {
            return new JSONResult(false, "请重新登录!");
        }
        if (forgetpassword.getLoginname() != null) {
            int count = userMapper.countByLoginname(forgetpassword.getLoginname());
            if (count > 0) {
                return new JSONResult(false, "该手机号已注册!");
            }
            int ct = verifycodeMapper.selectByLoginname(forgetpassword.getLoginname(), forgetpassword.getVerifyCode(), Verifycode.THREE);
            //修改User和Userprofile中的登录名
            if (ct > 0) {
                userMapper.updateLoginname(forgetpassword.getLoginname(), usertoken.getUid());
                usertokenMapper.updateLoginname(forgetpassword.getLoginname(), usertoken.getUid());
            } else {
                return new JSONResult(false, "手机号或验证码错误!");
            }
        }
        return new JSONResult(true, "手机号更改成功!");
    }
}
