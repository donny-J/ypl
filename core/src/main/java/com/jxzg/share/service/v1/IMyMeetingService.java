package com.jxzg.share.service.v1;

import com.jxzg.share.returndomain.ReturnList;

/**
 * 我的会议
 */
public interface IMyMeetingService {
    ReturnList mymeeting(String token);
}
