package com.jxzg.share.service.v1;

import com.jxzg.share.domain.other.HistoryRecord;
import com.jxzg.share.returndomain.JSONResult;

/**
 * Created by 00818Wolf on 2017/3/28.
 */
public interface IHistoryRecordService {
    /**
     * 搜索历史记录保存
     * @param historyRecord
     * @return
     */
    JSONResult historyrecord_save(HistoryRecord historyRecord);

}
