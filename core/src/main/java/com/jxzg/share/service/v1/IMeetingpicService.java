package com.jxzg.share.service.v1;

import com.jxzg.share.domain.meeting.Meetingpic;
import com.jxzg.share.querypage.MeetingQueryObject;
import com.jxzg.share.returndomain.ReturnList;

/**
 * 会议图片
 */
public interface IMeetingpicService {
    /**
     * 会议图片的保存
     */
    void insert(Meetingpic meetingpic);

    /**
     * 会议图片列表
     */
    ReturnList meetingpictypelist(MeetingQueryObject qo);

}
