package com.jxzg.share.pay.refund;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradeFastpayRefundQueryRequest;
import com.alipay.api.request.AlipayTradeRefundRequest;
import com.alipay.api.response.AlipayTradeFastpayRefundQueryResponse;
import com.alipay.api.response.AlipayTradeRefundResponse;
import com.jxzg.share.domain.meeting.Myticket;
import com.jxzg.share.domain.meeting.Spellgroup;
import com.jxzg.share.domain.pay.Orders;
import com.jxzg.share.mapper.MyticketMapper;
import com.jxzg.share.mapper.SpellgroupMapper;
import com.jxzg.share.pay.alipay.AlipayConfig;
import com.jxzg.share.pay.weixinpc.PayCommonUtil;
import com.jxzg.share.pay.weixinpc.XMLUtil;
import com.jxzg.share.pay.weixinutils.MD5Util;
import com.jxzg.share.pay.weixinutils.TenpayUtil;
import com.jxzg.share.pay.weixinutils.WeixinPayConstants;
import com.jxzg.share.service.v1.IEhcacheService;
import com.jxzg.share.service.v1.IMyticketService;
import com.jxzg.share.service.v1.ISpellgroupService;
import com.jxzg.share.toolUtil.ApplicationContextUtil;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.net.ssl.SSLContext;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.security.KeyStore;
import java.util.*;

import static com.jxzg.share.toolUtil.ApplicationContextUtil.getBean;


/**
 * Created by 00818wolf on 2017/5/26.
 */
@Component
public class Alirefund {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${alipay.baseUrl}")
    private String alipayBaseUrl;

    @Value("${alipay.appid}")
    private String alipayAppid;

    @Value("${alipay.privateKey}")
    private String privateKey;

    @Value("${alipay.publicKey}")
    private String publicKey;

    @Value("${wechat.mpAppid}")
    private String wechatAppid;

    @Value("${wechat.partner}")
    private String wechatPartner;

    @Value("${wechat.partnerkey}")
    private String wechatPartnerKey;


    public boolean aliRefund(Orders orders) {
        AlipayClient alipayClient = new DefaultAlipayClient(alipayBaseUrl, alipayAppid, privateKey, AlipayConfig.DATA_TYPE_JSON, AlipayConfig.input_charset, publicKey, AlipayConfig.sign_type);
        AlipayTradeRefundRequest request = new AlipayTradeRefundRequest();
        request.setBizContent("{" +
                "\"out_trade_no\":\"" + orders.getOrderno() + "\"," +
                "\"refund_amount\":" + orders.getMoney() + "," +
                "\"refund_reason\":\"正常退款\"," +
                "\"out_request_no\":\"HZ01RF001\"," +
                "\"operator_id\":\"OP001\"," +
                "\"store_id\":\"NJ_S_001\"," +
                "\"terminal_id\":\"NJ_T_001\"" +
                "  }");
        AlipayTradeRefundResponse response = null;
        try {
            response = alipayClient.execute(request);
            if (response.isSuccess())//接口调用成功(使用交易号)
                return true;
            else
                return false;
        } catch (AlipayApiException e) {
            logger.error(e.getMessage(), e);
            return false;
        }
    }

    public void aliRefundQuery(Orders orders) {
        AlipayClient alipayClient = new DefaultAlipayClient(alipayBaseUrl, alipayAppid, privateKey, AlipayConfig.DATA_TYPE_JSON, AlipayConfig.input_charset, publicKey, AlipayConfig.sign_type);
        AlipayTradeFastpayRefundQueryRequest request = new AlipayTradeFastpayRefundQueryRequest();
        request.setBizContent("{" +
                "\"out_trade_no\":\"" + orders.getOrderno() + "\"," +
                "\"out_request_no\":\"" + orders.getOrderno() + "\"" +
                "  }");
        AlipayTradeFastpayRefundQueryResponse response = null;
        try {
            response = alipayClient.execute(request);
            System.out.println("response = " + response.getBody());
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
    }

    public boolean weixinrefund(Orders orders) throws Exception {
        Date date = new Date();
        String currTime = TenpayUtil.getCurrTime();
        String strTime = currTime.substring(8, currTime.length());
        //四位随机数
        String strRandom = TenpayUtil.buildRandom(4) + "";
        //10位序列号,可以自行调整。
        String strReq = strTime + strRandom;

        //总金额以分为单位，不带小数点
        String money = orders.getMoney().toString();
        float sessionmoney = Float.parseFloat(money);
        BigDecimal total = orders.getMoney().multiply(new BigDecimal("100"));
        total.toString().replace(".", "");
        String finalmoney = String.format("%.2f", sessionmoney);
        finalmoney = finalmoney.replace(".", "");
        int total_fee = Integer.parseInt(finalmoney);
        SortedMap<Object, Object> parameters = new TreeMap<Object, Object>();
        parameters.put("appid", wechatAppid);//appid  wxf909e2fdfb212ce1
        parameters.put("mch_id", wechatPartner);//商户号
        parameters.put("nonce_str", strReq);
        //在notify_url中解析微信返回的信息获取到 transaction_id，此项不是必填，详细请看上图文档
        //parameters.put("transaction_id", "微信支付订单中调用统一接口后微信返回的 transaction_id");
        parameters.put("out_trade_no", orders.getOrderno());//订单好
        parameters.put("out_refund_no", UUID.randomUUID().toString());//我们自己设定的退款申请号，约束为UK
        parameters.put("total_fee", String.valueOf(total_fee));//单位为分
        parameters.put("refund_fee", String.valueOf(total_fee));//单位为分
        parameters.put("op_user_id", wechatPartner);//操作人员,默认为商户账号
        String sign = PayCommonUtil.createSign("UTF-8", parameters, wechatPartnerKey);
        parameters.put("sign", sign);

        String reuqestXml = getRequestXml(parameters);
        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        FileInputStream instream = new FileInputStream(new File("/var/www/key/apiclient_cert.p12"));
        try {
            keyStore.load(instream, wechatPartner.toCharArray());
        } finally {
            instream.close();
        }

        SSLContext sslcontext = SSLContexts.custom().loadKeyMaterial(keyStore, wechatPartner.toCharArray()).build();
        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                sslcontext,
                new String[]{"TLSv1"},
                null,
                SSLConnectionSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
        CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
        try {

            HttpPost httpPost = new HttpPost("https://api.mch.weixin.qq.com/secapi/pay/refund");//退款接口

            StringEntity reqEntity = new StringEntity(reuqestXml);
            // 设置类型
            reqEntity.setContentType("application/json");
            httpPost.setEntity(reqEntity);
            CloseableHttpResponse response = httpclient.execute(httpPost);
            try {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(entity.getContent(), "UTF-8"));
                    String text = null;
                    StringBuilder sb = new StringBuilder(60);
                    while ((text = bufferedReader.readLine()) != null) {
                        sb.append(text);
                    }
                    Map map = XMLUtil.doXMLParse(String.valueOf(sb));
                    if (map.get("result_code").equals("SUCCESS") && map.get("return_code").equals("SUCCESS")) {
                        return true;
                    } else {
                        return false;
                    }
                }
                EntityUtils.consume(entity);
            } finally {
                response.close();
            }
        } finally {
            httpclient.close();
        }
        return false;
    }


    public String createSign(String charSet, SortedMap<Object, Object> parameters) {
        StringBuffer sb = new StringBuffer();
        Set es = parameters.entrySet();
        Iterator it = es.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            String k = (String) entry.getKey();
            Object v = entry.getValue();
            if (null != v && !"".equals(v)
                    && !"sign".equals(k) && !"key".equals(k)) {
                sb.append(k + "=" + v + "&");
            }
        }
        sb.append("key=" + wechatPartnerKey);
        String sign = MD5Util.MD5Encode(sb.toString(), charSet).toUpperCase();
        return sign;
    }

    public static String getRequestXml(SortedMap<Object, Object> parameters) {
        StringBuffer sb = new StringBuffer();
        sb.append("<xml>");
        Set es = parameters.entrySet();
        Iterator it = es.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            String k = (String) entry.getKey();
            String v = (String) entry.getValue();
            if ("attach".equalsIgnoreCase(k) || "body".equalsIgnoreCase(k) || "sign".equalsIgnoreCase(k)) {
                sb.append("<" + k + ">" + "<![CDATA[" + v + "]]></" + k + ">");
            } else {
                sb.append("<" + k + ">" + v + "</" + k + ">");
            }
        }
        sb.append("</xml>");
        return sb.toString();
    }

}
