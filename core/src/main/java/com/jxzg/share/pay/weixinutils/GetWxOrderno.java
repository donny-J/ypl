package com.jxzg.share.pay.weixinutils;


import com.jxzg.share.pay.weixinutils.http.HttpClientConnectionManager;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.impl.client.DefaultHttpClient;

import java.util.Map;

public class GetWxOrderno {
    public static DefaultHttpClient httpclient;

    static {
        httpclient = new DefaultHttpClient();
        httpclient = (DefaultHttpClient) HttpClientConnectionManager.getSSLInstance(httpclient);
    }


    public static String getPayNo(String url, String xmlParam) throws Exception {
        DefaultHttpClient client = new DefaultHttpClient();
        client.getParams().setParameter(ClientPNames.ALLOW_CIRCULAR_REDIRECTS, true);
        //HttpPost httpost = HttpClientConnectionManager.getPostMethod(url);
        String prepay_id = "";
        try {
            //httpost.setEntity(new StringEntity(xmlParam, "UTF-8"));
            String result = sendpost.sendPost(url, xmlParam);
            xmltomap xmlParam1 = new xmltomap();
            Map map = xmlParam1.doXMLParse(result);
            String return_code = (String) map.get("return_code");
            //System.out.println("return_code = " + return_code);
            prepay_id = (String) map.get("prepay_id");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return prepay_id;
    }
}