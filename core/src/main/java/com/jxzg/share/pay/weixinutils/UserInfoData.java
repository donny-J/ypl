package com.jxzg.share.pay.weixinutils;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by 00818Wolf on 2017/5/17.
 */
@Getter@Setter
public class UserInfoData {
    private String openId;
    private String authToken;
    private String authRefreshToken;
    private String scope;
    private int expiresIn;
    private String name;
    private String icon;
    private String gender;
    private String loginId;
}
