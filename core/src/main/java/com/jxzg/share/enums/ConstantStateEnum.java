package com.jxzg.share.enums;

/**
 * 常用状态枚举
 * Created by 00914Donny on 2017/6/13.
 */
public enum ConstantStateEnum {
    SUCCESS(1,"操作成功"),
    MISSING_TOKEN(-1,"缺少参数token"),
    INVALID_TOKEN(-2,"无效的参数token"),
    INVALID_PAGESIZE(-3,"无效的参数pagesize"),
    INNER_ERROR(-100,"inner error");
    private int errorCode;

    private String errMsg;

    ConstantStateEnum(int errorCode, String errMsg) {
        this.errorCode = errorCode;
        this.errMsg = errMsg;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public static ConstantStateEnum codeOf(int index){
        for(ConstantStateEnum state : values())
            if(state.getErrorCode() == index)
                return state;
        return null;
    }
}
