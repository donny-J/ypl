package com.jxzg.share.enums;

/**
 * Created by 00914Donny on 2017/6/13.
 */
public enum OrderStateEnum {
    /*
     * [-1...-100] also see ConstantStateEnum
     */
    INVALID_STATUS(-101,"无效的参数status"),
    INVALID_TYPE(-102,"无效的参数type"),
    INVALID_METHOD_INVOKE(-103,"传入的参数不允许调用该方法"),
    MISSING_STATUS(-104,"参数status不能为空"),
    UNSUPPORTED_FUNCTION(-105,"尚未实现查询全部的功能");

    private int errorCode;

    private String errMsg;

    OrderStateEnum(int errorCode, String errMsg) {
        this.errorCode = errorCode;
        this.errMsg = errMsg;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public static OrderStateEnum codeOf(int index){
        for(OrderStateEnum state : values())
            if(state.getErrorCode() == index)
                return state;
        return null;
    }
}
