package com.jxzg.share.toolUtil;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;

import java.util.Map;

/**
 * Created by 00914Donny on 2017/6/3.
 */
public class JsonUtil {
    /**
     * 根据json字符串转换成Map
     *
     * @param json
     *            需要转换的json字符串
     * @return Map 转换结果
     */
    public static Map<String, Object> jsonStringToMap(String json) {
        Map<String, Object> map = JSON.parseObject(
                json,new TypeReference<Map<String, Object>>(){} );
        return map;
    }


    /**
     * Convert object to JSON
     * @return JSON text (return null if any exception occurs)
     */
    public static String convertObjectToJson(Object obj) {
        String json = null;
        try {
            json = JSON.toJSONString(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

}
