package com.jxzg.share.toolUtil;

import java.util.List;
import java.util.Map;

/**
 * 检查敏感字.
 */
public class FilterwordUtil {
    public static boolean checkFilter(List<String> content, Map initfilterword) {
        for (String s : content) {
            boolean isCon = SensitivewordEngine.isContaintSensitiveWord(s, 0, initfilterword);
            if (!isCon) {
                return false;
            }
        }
        return true;
    }
}
