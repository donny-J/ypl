package com.jxzg.share.toolUtil;


import com.jxzg.share.domain.meeting.Meeting;
import com.jxzg.share.domain.meeting.Meetingticket;
import com.jxzg.share.domain.meeting.Myticket;
import com.jxzg.share.domain.meeting.Spellgroup;
import com.jxzg.share.domain.other.Notification;
import com.jxzg.share.domain.other.User;
import com.jxzg.share.domain.pay.Chargelog;
import com.jxzg.share.domain.pay.Orderdtl;
import com.jxzg.share.domain.pay.Orders;
import com.jxzg.share.domain.pay.Payee;
import com.jxzg.share.mapper.*;
import com.jxzg.share.pay.refund.Alirefund;
import com.jxzg.share.service.v1.IPayeeService;
import org.jdom.output.SAXOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static com.jxzg.share.toolUtil.JuheDemo.getRequest2;


/**
 * 修改会议状态定时器
 */
@Component
public class TaskCool {

    private static final Logger log = LoggerFactory.getLogger(TaskCool.class);
    @Autowired
    private Alirefund alirefund;
    @Autowired
    private MeetingMapper meetingMapper;
    @Autowired
    private MeetingticketMapper meetingticketMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private SpellgroupMapper spellgroupMapper;
    @Autowired
    private OrdersMapper ordersMapper;
    @Autowired
    private OrderdtlMapper orderdtlMapper;
    @Autowired
    private MyticketMapper myticketMapper;
    @Autowired
    private ChargelogMapper chargelogMapper;
    @Autowired
    private IPayeeService payeeService;
    @Autowired
    private NotificationMapper notificationMapper;

    /**
     * 检查和更改会议状态
     */
    @Scheduled(cron = "0/5 * * * * ?")
    public void checkMeetingStatus() {
        List<Meeting> meetingList = meetingMapper.checkMeetingStatus();
        for (Meeting meeting : meetingList) {
            Date date = new Date();
            Date min = meetingticketMapper.selectMinTime(meeting.getMid());
            Date max = meetingticketMapper.selectMaxTime(meeting.getMid());
            /*int maxamount = meetingticketMapper.selectMaxAmount(meeting.getMid());
            int amount = meetingticketMapper.selectMinAmount(meeting.getMid());*/
            List<Meetingticket> meetingtickets = meetingticketMapper.selectAllMeetingTicket(meeting.getMid());
            if (date.getTime() > max.getTime() && date.getTime() > meeting.getStarttime().getTime()) {
                meetingMapper.updateMeetingStatus(meeting.getMid(), Meeting.MEETING_STATUS_ACTIVITY_STOP);
            }
            if (date.getTime() < meeting.getStarttime().getTime() && date.getTime() > min.getTime() && meeting.getStatus() != Meeting.MEETING_STATUS_ACTIVITY_STOP) {//如果当前时间小于活动开始时间&&大于卖票时间,报名中
                meetingMapper.updateMeetingStatus(meeting.getMid(), Meeting.MEETING_STATUS_ACTIVITY_APPLY);
            } else if (date.getTime() > meeting.getStarttime().getTime() && date.getTime() < meeting.getEndtime().getTime()) {//如果当前时间大于活动开始时间小于活动结束时间,活动中
                meetingMapper.updateMeetingStatus(meeting.getMid(), Meeting.MEETING_STATUS_ACTIVITY_YES);
            } else if (date.getTime() < meeting.getStarttime().getTime() && date.getTime() < min.getTime() && meeting.getStatus() != Meeting.MEETING_STATUS_ACTIVITY_STOP) {//如果当前时间小于活动开始时间&&小于卖票时间,售票未开始
                meetingMapper.updateMeetingStatus(meeting.getMid(), Meeting.MEETING_STATUS_ACTICITY_NO);
            }
            if (date.getTime() > meeting.getEndtime().getTime()) {//如果当前时间大于活动结束时间,活动已结束
                meetingMapper.updateMeetingStatus(meeting.getMid(), Meeting.MEETING_STATUS_ACTIVITY_END);
            }
        }
    }

    /**
     * 检查订单时间
     */
    @Scheduled(cron = "0/5 * * * * ?")
    public void checkorderTime() {
        List<Orders> orderdtlses = ordersMapper.selectByStatus(Orders.ORDER_STATUS_NO_PAY);
        for (Orders orderse : orderdtlses) {
            Date date = new Date();
            if (date.getTime() > orderse.getUpdatetime().getTime()) {
                ordersMapper.updateStatus(Orders.ORDER_STATUS_FAIL, orderse.getOrderno(), date);
                orderdtlMapper.updateStatus(Orderdtl.ORDER_STATUS_FAIL, orderse.getOrderno(), date);
                List<Myticket> myticket = myticketMapper.selectGroupByOrderno(orderse.getOrderno());
                for (Myticket myticket1 : myticket) {
                    Meeting meeting = meetingMapper.selectMeetingByMid(myticket1.getMid());
                    if (meeting.getMeetingtype() == Meeting.MEETINGTYPEA_TEAM) {
                        Spellgroup spellgroup = spellgroupMapper.selectByTicketid(myticket1.getId());
                        if (spellgroup.getTid() == 0) {
                            spellgroupMapper.updateStatusMany(spellgroup.getId(), Spellgroup.STATUS_TIME, Spellgroup.STATUS_STAY);
                        } else {
                            spellgroupMapper.updateStatusMany(spellgroup.getTid(), Spellgroup.STATUS_TIME, Spellgroup.STATUS_STAY);
                        }
                    }
                    if (myticket1.getCreateip() != null) {
                        meetingticketMapper.updateTicketNum(myticket1.getCreateip(), myticket1.getMid(), -1);
                    } else {
                        meetingticketMapper.updateTicketNumByid(myticket1.getTicketid(), myticket1.getMid(), -1);
                    }
                }
            }
        }
    }

    @Scheduled(cron = "0/5 * * * * ?")
    public void checkSpellgroupTime() {
        List<Spellgroup> spellgroups = spellgroupMapper.selectOuttime(Spellgroup.STATUS_SUCCESS_A);
        for (Spellgroup spellgroup : spellgroups) {
            Date date = new Date();
            Meeting meeting = meetingMapper.selectMeetingByMid(spellgroup.getMid());
            Myticket myticket = myticketMapper.selectByPrimaryKey(spellgroup.getTicketid());
            Meetingticket meetingticket = meetingticketMapper.selectTicketByNameMid(myticket.getTicketid(), myticket.getMid());
            if (date.getTime() > spellgroup.getUpdatetime().getTime()  //如果当前时间大于拼团过期时间
                    || date.getTime() > meetingticket.getEndtime().getTime()) {
                if (meetingticket.getPrice().compareTo(Meetingticket.PRICE) == 0) {
                    ordersMapper.updateStatus(Orders.ORDER_STATUS_FAIL, myticket.getOrderno(), date);
                    orderdtlMapper.updateStatus(Orderdtl.ORDER_STATUS_FAIL, myticket.getOrderno(), date);
                    if (myticket.getCreateip() != null) {
                        meetingticketMapper.updateTicketNum(myticket.getCreateip(), myticket.getMid(), -1);
                    } else {
                        meetingticketMapper.updateTicketNumByid(myticket.getTicketid(), myticket.getMid(), -1);
                    }
                    spellgroupMapper.updateStatusMany(spellgroup.getId(), Spellgroup.STATUS_OUTTIME, Spellgroup.STATUS_SUCCESS_A);
                } else {
                    spellgroupMapper.updateStatusMany(spellgroup.getId(), Spellgroup.STATUS_FAIL, Spellgroup.STATUS_SUCCESS_A);
                }
            }
        }
    }

    /**
     * 退款  每天晚上执行一次
     */
    @Scheduled(cron = "0 * */2 * * ?")
    public void spellgroupRegund() {
        List<Spellgroup> spellgroups = spellgroupMapper.selectOuttime(Spellgroup.STATUS_FAIL);
        for (Spellgroup spellgroup : spellgroups) {
            Date date = new Date();
            Myticket myticket = myticketMapper.selectByPrimaryKey(spellgroup.getTicketid());
            Orders orders = ordersMapper.selectByOrderno(myticket.getOrderno());
            if (orders.getPaymethd() == Orders.ALIPAY) {//支付宝
                if (alirefund.aliRefund(orders)) {
                    Payee payee = new Payee();
                    payee.setUid(orders.getUid());
                    payee.setType(1);
                    payee.setIsdefault(1);
                    payee.setAccount(orders.getMoney().toString());
                    payee.setStatus(1);
                    payee.setName(orders.getOrderno());
                    payee.setCreatetime(date);
                    payee.setUpdatetime(date);
                    payeeService.insert(payee);
                    ordersMapper.updateByOrderno(Orders.ORDER_STATUS_FAIL, Orders.PRODUCTTYPE_REFUND, orders.getOrderno(), date);
                    orderdtlMapper.updateByOrderno(Orders.ORDER_STATUS_FAIL, Orders.PRODUCTTYPE_REFUND, orders.getOrderno(), date);
                    Orderdtl orderdtl = orderdtlMapper.selectGroupByOrderno(orders.getOrderno());
                    Meeting meeting = meetingMapper.selectMeetingByMid(orderdtl.getMid());
                    User user = userMapper.selectByUid(meeting.getUid());
                    Chargelog chargelog = new Chargelog();
                    chargelog.setOrderno(orders.getOrderno());
                    chargelog.setUid(meeting.getUid());
                    chargelog.setType(Chargelog.REFUND);
                    chargelog.setMethod(1);
                    chargelog.setBillno(orders.getOrderno());
                    chargelog.setAmount(orders.getMoney());
                    chargelog.setCurrentamount(user.getBalance());
                    chargelog.setRemainamount(user.getBalance().subtract(orders.getMoney()));
                    chargelog.setCreatetime(date);
                    chargelog.setUpdatetime(date);
                    chargelog.setRemarks("拼团退款");
                    chargelog.setStatus(2);
                    chargelogMapper.insert(chargelog);
                    userMapper.updateBalance(meeting.getUid(), orders.getMoney().multiply(new BigDecimal("-1")));
                    myticketMapper.updateStatus(myticket.getId(), Myticket.ORDER_STATUS_OUTTIME, date);
                    spellgroupMapper.updateStatusOne(spellgroup.getId(), Spellgroup.STATUS_OUTTIME);
                    meetingMapper.updateSoldticketReduce(meeting.getMid(), 1);
                    meetingticketMapper.updateTicketNumByid(myticket.getTicketid(), spellgroup.getMid(), -1);
                }
            } else {//微信
                try {
                    if (spellgroup.getStatus() == Spellgroup.STATUS_FAIL) {
                        if (alirefund.weixinrefund(orders)) {
                            Orderdtl orderdtl = orderdtlMapper.selectGroupByOrderno(orders.getOrderno());
                            Meeting meeting = meetingMapper.selectMeetingByMid(orderdtl.getMid());
                            Payee payee = new Payee();
                            payee.setUid(meeting.getUid());
                            payee.setType(2);
                            payee.setIsdefault(1);
                            payee.setAccount(orders.getMoney().toString());
                            payee.setStatus(1);
                            payee.setName(orders.getOrderno());
                            payee.setCreatetime(date);
                            payee.setUpdatetime(date);
                            payeeService.insert(payee);
                            ordersMapper.updateByOrderno(Orders.ORDER_STATUS_FAIL, Orders.PRODUCTTYPE_REFUND, orders.getOrderno(), date);
                            orderdtlMapper.updateByOrderno(Orderdtl.ORDER_STATUS_FAIL, Orders.PRODUCTTYPE_REFUND, orders.getOrderno(), date);
                            User user = userMapper.selectByUid(meeting.getUid());
                            Chargelog chargelog = new Chargelog();
                            chargelog.setOrderno(orders.getOrderno());
                            chargelog.setUid(meeting.getUid());
                            chargelog.setType(Chargelog.REFUND);
                            chargelog.setMethod(2);
                            chargelog.setBillno(orders.getOrderno());
                            chargelog.setAmount(orders.getMoney());
                            chargelog.setCurrentamount(user.getBalance());
                            chargelog.setRemainamount(user.getBalance().subtract(orders.getMoney()));
                            chargelog.setCreatetime(date);
                            chargelog.setUpdatetime(date);
                            chargelog.setRemarks("拼团退款");
                            chargelog.setStatus(2);
                            chargelogMapper.insert(chargelog);
                            userMapper.updateBalance(meeting.getUid(), orders.getMoney().multiply(new BigDecimal("-1")));
                            myticketMapper.updateStatus(myticket.getId(), Myticket.ORDER_STATUS_OUTTIME, date);
                            spellgroupMapper.updateStatusOne(spellgroup.getId(), Spellgroup.STATUS_OUTTIME);
                            meetingMapper.updateSoldticketReduce(meeting.getMid(), 1);
                            meetingticketMapper.updateTicketNumByid(myticket.getTicketid(), spellgroup.getMid(), -1);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 定时发送短信
     */
    /*@Scheduled(cron = "0/5 * * * * ?")
    public void send_note() {
        List<Notification> notifications = notificationMapper.selectByStatus(Notification.STATUS_INIT);
        Date date = new Date();
        for (Notification notification : notifications) {
            if (date.getTime() > notification.getUpdatetime().getTime()) {
                isOk = JuheDemo.getRequest2(notification.getTarget(), notification.getContent());
                notificationMapper.updateByPrimaryKey(notification.getNid(), date, Notification.STATUS_INFROM);
            }
        }
    }*/
}
