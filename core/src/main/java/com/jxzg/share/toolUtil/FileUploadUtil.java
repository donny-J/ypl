package com.jxzg.share.toolUtil;

import org.apache.commons.io.FileUtils;
import org.springframework.web.multipart.MultipartFile;
import sun.misc.BASE64Decoder;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

public class FileUploadUtil {

    public static String saveFile(MultipartFile file, String basePath,String fileName) throws Exception {
        try {
            File sourceFile = new File(basePath, fileName);
            FileUtils.writeByteArrayToFile(sourceFile, file.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileName;
    }

    //在删除货品/修改货品图片的时候,删除之前图片文件 ; 根据文件的保存路径删除掉该图片
    public static void deleteFile(String pic, HttpServletRequest request) {
        String path = request.getSession().getServletContext().getRealPath("/") + pic;
        File file = new File(path);
        if (file.exists()) file.delete();
    }

    public static String meetingimage(String baseimage, HttpServletRequest request) throws IOException {
        String image = "upload/meeting/";
        String uuid = UUID.randomUUID().toString();
        String fileName = request.getSession().getServletContext().getRealPath("/upload/meeting/") + uuid + ".jpg"; //生成的新文件
        // 解码，然后将字节转换为文件
        byte[] bytes = new BASE64Decoder().decodeBuffer(baseimage);//将字符串转换为byte数组
        ByteArrayInputStream in = new ByteArrayInputStream(bytes);
        byte[] buffer = new byte[1024];
        FileOutputStream out = new FileOutputStream(fileName);
        int bytesum = 0;
        int byteread = 0;
        while ((byteread = in.read(buffer)) != -1) {
            bytesum += byteread;
            out.write(buffer, 0, byteread); //文件写操作
        }
        //返回上传保存路径和保存的文件名 ; 返回文件保存在服务端的路径
        return image + uuid + ".jpg";
    }

}
