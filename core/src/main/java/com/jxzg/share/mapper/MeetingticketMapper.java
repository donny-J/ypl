package com.jxzg.share.mapper;

import com.jxzg.share.domain.meeting.Meetingticket;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface MeetingticketMapper {
    int delectByMid(Integer ticdketid);

    int insert(Meetingticket record);

    Meetingticket selectByPrimaryKey(Integer ticdketid);

    List<Meetingticket> selectAll();

    BigDecimal selectMeetingTicket(int mid);

    List<Meetingticket> selectAllMeetingTicket(int mid);

    Meetingticket selectByMid(int mid);

    Meetingticket selectTicketByNameMid(@Param("ticketid") int ticketid, @Param("mid") int mid);

    Integer selectIsRemaincount(@Param("mid") int mid, @Param("name") String name, @Param("remaincount") Integer remaincount);

    Date selectMinTime(int mid);

    Date selectMaxTime(int mid);

    void updateByPrimaryKey(Meetingticket meetingticket);

    void updateTicketNum(@Param("ticketname") String ticketname, @Param("mid") int mid, @Param("amount") int amount);

    void updateTicketNumByid(@Param("ticketid") int ticketid,@Param("mid") int mid,@Param("amount") int count);

    void updateStatus(@Param("tid") int ticdketid, @Param("status") int statusStock);

    int selectMaxAmount(int mid);

    Meetingticket selectTicketByName(@Param("ticketname") String ticketname,@Param("mid") int mid);

    int selectMinAmount(int mid);

}