package com.jxzg.share.mapper;

import com.jxzg.share.domain.meeting.Meetingpic;
import com.jxzg.share.querypage.MeetingQueryObject;

import java.util.List;

public interface MeetingpicMapper {

    int deleteByPrimaryKey(Long id);

    int insert(Meetingpic record);

    Meetingpic selectByPrimaryKey(Long id);

    int updateByPrimaryKey(Meetingpic record);

    int queryForCount(MeetingQueryObject qo);

    List<Meetingpic> query(MeetingQueryObject qo);

}