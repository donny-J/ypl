package com.jxzg.share.mapper;

import com.jxzg.share.domain.meeting.SendTicketRecord;

import java.util.List;

public interface SendTicketRecordMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SendTicketRecord record);

    SendTicketRecord selectByPrimaryKey(Integer id);

    List<SendTicketRecord> selectAll();

    int updateByPrimaryKey(SendTicketRecord record);
}