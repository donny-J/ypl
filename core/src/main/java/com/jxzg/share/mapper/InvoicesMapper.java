package com.jxzg.share.mapper;

import com.jxzg.share.domain.meeting.Invoices;

import java.util.List;

public interface InvoicesMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Invoices record);

    Invoices selectByPrimaryKey(Integer id);

    List<Invoices> selectAll();

    int updateByPrimaryKey(Invoices record);

    List<Invoices> selectByMid(int mid);
}