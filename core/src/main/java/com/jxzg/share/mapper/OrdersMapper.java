package com.jxzg.share.mapper;

import com.jxzg.share.domain.pay.Orders;
import com.jxzg.share.querypage.IncomeDetailQueryObject;
import com.jxzg.share.querypage.OrderModQueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface OrdersMapper {
    int deleteByPrimaryKey(Long orderid);

    int insert(Orders record);

    List<Orders> selectAll();

    int updateByPrimaryKey(Orders record);

    Orders selectByOrderno(String orderno);

    void updateByOrderno(@Param("status") int status, @Param("ordertype") int ordertype, @Param("orderno") String orderno, @Param("updatetime") Date updatetime);

    List<Orders> selectByType(IncomeDetailQueryObject qo);

    int queryForCount(IncomeDetailQueryObject qo);

    Orders selectByOrderid(Long orderid);

    List<Orders> selectByStatus(int status);

    void updateOrderStatusType(@Param("status") int status, @Param("orderno") String orderno, @Param("updatetime") Date updatetime, @Param("type") int type);

    void updateStatus(@Param("status") int status, @Param("orderno") String orderno, @Param("updatetime") Date date);

    List<Orders> getOrdersByCon(OrderModQueryObject queryObject);

    List<Orders> getIncomeOrdersAll(OrderModQueryObject queryObject);

    List<Orders> getOutputOrdersAll(OrderModQueryObject queryObject);

    Integer getOrdersCountByCon(OrderModQueryObject queryObject);

    Integer getIncomeOrdersAllCount(OrderModQueryObject queryObject);

    Integer getOutputOrdersAllCount(OrderModQueryObject queryObject);
}