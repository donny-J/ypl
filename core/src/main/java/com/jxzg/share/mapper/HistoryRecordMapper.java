package com.jxzg.share.mapper;

import com.jxzg.share.domain.other.HistoryRecord;

import java.util.List;

public interface HistoryRecordMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(HistoryRecord record);

    HistoryRecord selectByPrimaryKey(Integer id);

    List<HistoryRecord> selectAll();

    int updateByPrimaryKey(HistoryRecord record);
}