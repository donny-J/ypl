package com.jxzg.share.mapper;

import com.jxzg.share.domain.other.Loginlog;
import com.jxzg.share.querypage.QueryObject;

import java.util.List;

public interface LoginlogMapper {

    int insert(Loginlog record);

    Loginlog selectByPrimaryKey(Long id);

    int updateByPrimaryKey(Loginlog record);

    int queryForCount(QueryObject qo);

    List<Loginlog> query(QueryObject qo);

}