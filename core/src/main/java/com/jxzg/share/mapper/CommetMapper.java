package com.jxzg.share.mapper;

import com.jxzg.share.domain.other.Commet;
import com.jxzg.share.querypage.CommetQueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CommetMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Commet record);

    List<Commet> selectAll();

    int updateByPrimaryKey(Commet record);

    List<Commet> selectCommetByMid(int mid);

    int selectCommetByMidAndUid(@Param("id") long id,@Param("type") int type,@Param("uid")int uid);

    void updateType(@Param("id") long mid,@Param("type") int type);

    int queryForCount(CommetQueryObject qo);

    List<Commet> query(CommetQueryObject qo);

    /**
     * 登录用户的回复
     * @param uid
     * @return
     */
    List<Commet> selectReplyByUid(int uid);

    /**
     * 游客的回复
     * @return
     */
    List<Commet> selectReplyByid(@Param("id") long id,@Param("type") int type);

    /**
     * 评论点赞数+1
     * @param id
     */
    void updateCommetLikeCount(long id);
}