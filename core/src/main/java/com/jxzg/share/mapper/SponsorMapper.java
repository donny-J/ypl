package com.jxzg.share.mapper;

import com.jxzg.share.domain.meeting.Sponsor;
import com.jxzg.share.querypage.MeetingQueryObject;
import com.jxzg.share.querypage.QueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SponsorMapper {
    int deleteByPrimaryKey(Integer oid);

    int insert(Sponsor record);

    Sponsor selectByPrimaryKey(int uid);

    int updateByPrimaryKey(Sponsor sponsor);

    List<Sponsor> selectAll(QueryObject qo);

    Sponsor selectByName(String name);

    Sponsor selectByOid(Integer oid);

    List<Sponsor> selectSponsorByContent(MeetingQueryObject qo);

    int queryForCount(MeetingQueryObject qo);

    void updateStatus(@Param("oid") int oid, @Param("status") int status);

    int selectAllCount(QueryObject qo);

    List<Sponsor> selectByNameLike(QueryObject qo);

    Sponsor selectByNameOne(@Param("uid") int uid,@Param("name") String name);

    int selectByNameLikeCount(QueryObject qo);
}