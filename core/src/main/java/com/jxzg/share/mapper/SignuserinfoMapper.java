package com.jxzg.share.mapper;

import com.jxzg.share.domain.meeting.Signuserinfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SignuserinfoMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Signuserinfo record);


    List<Signuserinfo> selectAll();

    int updateByPrimaryKey(Signuserinfo record);

    Signuserinfo selectById(@Param("id") int id,@Param("status") int status);

    List<Signuserinfo> selectSignType(@Param("id") int id,@Param("status") int status);

    List<String> selectChoiceFieldid(@Param("id") int id,@Param("status") int status);

    List<String> selectChoiceFieldval(@Param("id") int id,@Param("fieldid") String fieldid,@Param("status") int status);
}