package com.jxzg.share.mapper;

import com.jxzg.share.domain.other.Verifycode;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface VerifycodeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Verifycode record);

    Verifycode selectByPrimaryKey(Integer id);

    List<Verifycode> selectAll();

    int updateByPrimaryKey(Verifycode record);

    int selectByLoginname(@Param("loginname") String loginname,@Param("verifyCode") String verifyCode,@Param("type")int type);

    void deleteByloginname(@Param("loginname") String loginname,@Param("type") int type);

}