package com.jxzg.share.mapper;

import com.jxzg.share.domain.meeting.Meetingsponsor;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MeetingsponsorMapper {
    int insert(Meetingsponsor record);

    Meetingsponsor selectByPrimaryKey(@Param("mid") Integer mid, @Param("oid") Integer oid);

    List<Meetingsponsor> selectAll();

    int updateByPrimaryKey(Meetingsponsor record);

    List<Meetingsponsor> selectByMid(int mid);

    List<Meetingsponsor> selectByOid(int oid);

    List<Meetingsponsor> selectSponsor(@Param("mid") int mid,@Param("uid") int uid);

    List<Meetingsponsor> selectlistbyoid(@Param("list") List list);

    void delectByMid(int mid);

    List<Integer> selectMids(int id);
}