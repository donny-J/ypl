package com.jxzg.share.mapper;

import com.jxzg.share.domain.meeting.Meetingtag;
import com.jxzg.share.querypage.SearchQueryObject;

import java.util.List;

public interface MeetingtagMapper {
    int insert(Meetingtag record);

    Meetingtag selectByPrimaryKey(Integer tagid);

    List<Meetingtag> selectAll();

    int updateByPrimaryKey(Meetingtag record);

    List<Integer> selectByTag(SearchQueryObject qo);

    List<String> selectTagByMid(int mid);

    void delectByMid(int mid);
}