package com.jxzg.share.mapper;

import com.jxzg.share.domain.other.Userprofile;
import com.jxzg.share.querypage.QueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserprofileMapper {

    int insert(Userprofile userprofile);

    List<Userprofile> selectAll();

    int queryForCount(QueryObject qo);

    List<Userprofile> query(QueryObject qo);

    /**
     * 完善个人资料
     *
     * @param uid 用户id
     */
    void updatePerson(@Param("uid") int uid, @Param("userprofile") Userprofile userprofile);

    int updateNickname(@Param("nickname") String nickname, @Param("uid") int uid);

    int updateRealname(@Param("realname") String realname, @Param("uid") int uid);

    int updateGender(@Param("gender") int gender, @Param("uid") int uid);

    int updateIdcardtype(@Param("idcardtype") int idcardtype,@Param("idcard")String idcard, @Param("uid") int uid);

    int updateAddress(@Param("provice") String provice, @Param("city") String city, @Param("dist") String dist, @Param("uid") int uid);

    void updateOrganizationname(@Param("organizationname") String organizationname,@Param("uid") int uid);

    void updateOrganizationtype(@Param("organizationtype")int organizationtype, @Param("uid")int uid);

    void updateOrganizationsize(@Param("organizationsize")int organizationsize, @Param("uid")int uid);

    Userprofile selectByUid(int uid);

    Userprofile selectByPid(Long pid);
}