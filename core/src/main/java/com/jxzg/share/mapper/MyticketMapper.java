package com.jxzg.share.mapper;

import com.jxzg.share.domain.meeting.Myticket;
import com.jxzg.share.querypage.MeetingTicketQueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface MyticketMapper {
    int insert(Myticket record);

    Myticket selectByPrimaryKey(Integer id);

    List<Myticket> selectAll(int mid);

    int updateByPrimaryKey(@Param("id") int id,@Param("uid") int uid);

    List<Integer> selectByUid(int uid);

    List<Myticket> selectByMid(int mid);

    int selectCount(@Param("uid") int uid, @Param("mid") int mid);

    List<Myticket> selectByUids(MeetingTicketQueryObject qo);

    List<Myticket> selectByUidAndMid(int mid);

    Myticket selectMinTime(int mid);

    int selectCountByMid(@Param("mid") int mid, @Param("uid") int uid, @Param("orderno") String orderno, @Param("tickettype") int tickettype);

    int queryMyTicketCount(MeetingTicketQueryObject qo);

    List<Myticket> selectByOrderno(String orderno);

    void updateStatus(@Param("id") int id,@Param("status") int status,@Param("updatetime") Date updatetime);

    void updateSignStatus(int tid);

    void updateQcode(@Param("id") int id,@Param("qcode") String qcode,@Param("signcode") String signcode);

    List<Myticket> selectGroupByOrderno(String orderno);

    List<Myticket> selectByMidAndStatus(@Param("mid") int mid,@Param("status") int status);

    int selectSignCode(@Param("signcode") String signcode,@Param("mid") int mid);

    Myticket selectBySignCode(@Param("signcode") String signcode,@Param("mid") int mid);

    int selectSendTicketCount(MeetingTicketQueryObject qo);

    List<Myticket> selectSendTickets(MeetingTicketQueryObject qo);
}