package com.jxzg.share.mapper;

import com.jxzg.share.domain.other.Verifylog;
import com.jxzg.share.querypage.QueryObject;

import java.util.List;

public interface VerifylogMapper {

    int insert(Verifylog record);

    Verifylog selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(Verifylog record);

    /**
     * 用户注册
     */
    void register(String loginname,String loginpassword,String uuid);

    int queryForCount(QueryObject qo);

    List<Verifylog> query(QueryObject qo);

}