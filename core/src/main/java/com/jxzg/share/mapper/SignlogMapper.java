package com.jxzg.share.mapper;

import com.jxzg.share.domain.meeting.Signlog;

import java.util.List;

public interface SignlogMapper {
    int deleteByPrimaryKey(Long nid);

    int insert(Signlog record);

    List<Signlog> selectAll();

    int updateByPrimaryKey(Signlog record);
}