package com.jxzg.share.mapper;

import com.jxzg.share.domain.pay.Chargelog;

import java.util.List;

public interface ChargelogMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Chargelog record);

    Chargelog selectByPrimaryKey(Long id);

    List<Chargelog> selectAll();

    int updateByPrimaryKey(Chargelog record);
}