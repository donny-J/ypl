package com.jxzg.share.mapper;

import com.jxzg.share.domain.other.Userconnect;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserconnectMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Userconnect record);

    List<Userconnect> selectAll();

    int updateByPrimaryKey(Userconnect record);

    Userconnect selectByPrimaryKey(String openid);

    Integer selectByUnionid(String unionid);

    Userconnect selectByUidAndType(@Param("uid") int uid,@Param("type") int type);
}