package com.jxzg.share.mapper;

import com.jxzg.share.domain.other.Dictcate;

import java.util.List;

public interface DictcateMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Dictcate record);

    List<Dictcate> selectAll();

    int updateByPrimaryKey(Dictcate record);
}