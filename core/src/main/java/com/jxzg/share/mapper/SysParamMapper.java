package com.jxzg.share.mapper;

import com.jxzg.share.domain.other.SysParam;
import com.jxzg.share.querypage.SysParamModQueryObject;

import java.util.List;

public interface SysParamMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysParam record);

    SysParam selectByPrimaryKey(Integer id);

    List<SysParam> selectAll();
    List<SysParam> getSysParamsByCon(SysParamModQueryObject queryObject);

    int updateByPrimaryKey(SysParam record);
}