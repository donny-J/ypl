package com.jxzg.share.mapper;

import com.jxzg.share.domain.meeting.Spellgroup;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SpellgroupMapper {
    int insert(Spellgroup record);

    List<Spellgroup> selectByTid(@Param("tid") int tid, @Param("mid") int mid, @Param("status") int status);

    Spellgroup selectById(int tid);

    /**
     * 随机两条待成团的团长信息
     *
     * @return
     */
    List<Spellgroup> selectRandom(int mid);

    /**
     * 检查拼团超时的拼团
     *
     * @param
     * @return
     */
    List<Spellgroup> selectOuttime(int status);

    Spellgroup selectByTicketid(int id);

    int updateStatusByMid(@Param("mid") int mid, @Param("beforestatus") int status, @Param("afterstatus") int afterstatus);

    void updateStatusMany(@Param("tid") int tid, @Param("beforestatus") int status, @Param("afterstatus") int afterstatus);

    void updateStatusOne(@Param("tid") int tid, @Param("status") int status);

    int selectCountByTid(@Param("tid") int tid, @Param("mid") int mid);

    List<Spellgroup> selectFailByTid(@Param("tid") int tid, @Param("mid") int mid);

    int selectCountByStatus(@Param("tid") int tid, @Param("mid") int mid, @Param("status") int status);

    int selectApplyById(@Param("tid") int id,@Param("mid") int mid);

    int selectCountStatus(@Param("mid") int mid,@Param("status") int status);
}