package com.jxzg.share.mapper;

import com.jxzg.share.domain.meeting.Signfield;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SignfieldMapper {
    int insert(Signfield record);

    List<Signfield> selectAll();

    int updateByPrimaryKey(Signfield record);

    List<Signfield> selectSignfieldName(@Param("mid") int mid,@Param("fielddescription") String fielddescription);

    List<Signfield> selectSignfieldMid(int mid);

    List<String> selectSignfieldVal(@Param("fieldname") String fieldname,@Param("fielddescription") String fielddescription,@Param("mid") int mid);

    long selectSignfieldSid(@Param("mid") int mid,@Param("fieldname") String fieldname);

    void delectByMid(int mid);
}