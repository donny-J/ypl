package com.jxzg.share.mapper;

import com.jxzg.share.domain.other.Usertoken;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UsertokenMapper {
    int insert(Usertoken usertoken);

    int deleteByUid(@Param("uid") int uid,@Param("logintype") int logintype);

    List<Usertoken> selectAll();

    int updateByPrimaryKey(Usertoken record);

    Usertoken selectUidBytoken(String token);

    void updateExpiretime(Usertoken usertoken);

    void updateLoginname(@Param("loginname") String loginname,@Param("uid") int uid);

    void updateToken(@Param("uid") int uid,@Param("token") String token,@Param("type") int type);

    Usertoken selectByUidAndType(@Param("uid") int uid,@Param("type") int type);
}