package com.jxzg.share.mapper;

import com.jxzg.share.domain.pay.Orderdtl;
import com.jxzg.share.domain.pay.Orders;
import com.jxzg.share.querypage.CommetQueryObject;
import com.jxzg.share.querypage.IncomeDetailQueryObject;
import com.jxzg.share.querypage.OrderModQueryObject;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface OrderdtlMapper {
    int deleteByPrimaryKey(Long orderdtlid);

    int insert(Orderdtl record);

    Orderdtl selectByPrimaryKey(Long orderdtlid);

    List<Orderdtl> selectAll();

    List<Orderdtl> selectOrderdtl(int mid);

    int selectCount(Long orderid);

    int selectProducttype(@Param("mid") int mid, @Param("producttype") int producttype);

    int selectByOrdernoCount(String orderno);

    int selectCountByOrderno(String orderno);

    List<Orderdtl> selectByOrderno(String orderno);

    List<Orderdtl> selectByMid(CommetQueryObject qo);

    void updateByOrderno(@Param("status") int status,@Param("producttype")int producttype, @Param("orderno") String orderno, @Param("updatetime") Date updatetime);

    BigDecimal selectSum(int mid);

    void updateByPrimaryKey(Orderdtl orderdtl);

    int selectByMyticketId(int id);

    Orderdtl selectGroupByOrderno(String orderno);

    List<Orderdtl> selectByQuery(IncomeDetailQueryObject qo);

    Orderdtl selectOneByOrderno(String orderno);

    int selectByQueryCount(IncomeDetailQueryObject qo);

    void updateStatus(@Param("status") int status,@Param("orderno") String orderno,@Param("updatetime") Date date);

    List<Orderdtl> selectByStatus(int status);

    Orderdtl selectByTicketid(int ticketid);

    List<Orderdtl>  getOrderdtlByCon(OrderModQueryObject queryObject);

    int selectOutcomeDetailCount(IncomeDetailQueryObject qo);

    List<Orderdtl> selectOutcomeDetail(IncomeDetailQueryObject qo);

    int selectActivityCount(CommetQueryObject qo);

    int selectCountByMid(int mid);
}