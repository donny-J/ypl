package com.jxzg.share.mapper;

import com.jxzg.share.domain.meeting.MyMeeting;

import java.util.List;

public interface MymeetingMapper {

    List<MyMeeting> selectMeetingByUid(int uid);
}