package com.jxzg.share.mapper;

import com.jxzg.share.domain.other.Filterword;

import java.util.List;

public interface FilterwordMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Filterword record);

    Filterword selectByPrimaryKey(Integer id);

    List<Filterword> selectAll();

    int updateByPrimaryKey(Filterword record);

    List<Filterword> selecthotword(int statue);

}