package com.jxzg.share.mapper;

import com.jxzg.share.domain.other.Collectattention;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CollectattentionMapper {
    int deleteCollectattention(@Param("smid") int smid, @Param("uid") int uid, @Param("type") int type);

    int insert(Collectattention record);

    Collectattention selectByPrimaryKey(Integer id);

    List<Collectattention> selectAll();

    int updateByPrimaryKey(Collectattention record);

    //是否已经关注
    int IsCollectAttentionNum(@Param("smid") Integer id, @Param("uid") Integer uid, @Param("type") Integer type);

    List<Integer> selectCollectAttentionCount(@Param("type") Integer type, @Param("uid") Integer uid);

    //我关注的总数
    int selectMyCollectAttentionNum(@Param("uid") Integer uid, @Param("type") Integer type);

    //关注的总数
    int selectCollectAttentionNum(@Param("type") int type,@Param("smid") int id);

    int updateByStatus(@Param("mid") int mid, @Param("status") int status);
}