package com.jxzg.share.mapper;

import com.jxzg.share.domain.meeting.Meeting;
import com.jxzg.share.querypage.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MeetingMapper {
    int insert(Meeting record);

    List<Meeting> selectMeetingByTime();

    List<Meeting> selectMeetingByUidAndStatus(@Param("uid") int uid, @Param("status") int status);

    List<Meeting> selectPicByNumber();

    Meeting selectMeetingByMid(int mid);

    void updateCommetCount(int mid);

    void updateCommetLikeCount(long id);

    void updateOid(@Param("mid") int mid, @Param("oid") int oid);

    int updateMeetingStatus(@Param("mid") int mid, @Param("status") int status);

    int queryForCount(MeetingQueryObject qo);

    List<Meeting> query(MeetingQueryObject qo);

    List<Integer> selectQoMid(MeetingTicketQueryObject qo);

    List<Meeting> selectAll();

    List<Meeting> selectlistbymid(@Param("list") List list, @Param("start") Integer start, @Param("pagesize") Integer pagesize);

    int selectcountbymid(List list);

    List<Meeting> selectByLatLng(MeetingQueryObject qo);

    int selectNearCount(MeetingQueryObject qo);

    int selectMeetingByUidMid(@Param("uid") int uid, @Param("mid") int mid);

    List<Meeting> checkMeetingStatus();

    void updateByPrimaryKey(Meeting meeting);

    void delectMeetingByMid(int mid);

    void updateMeetingAddflow(int mid);

    void updateSoldticketAdd(@Param("mid") int mid, @Param("soldticket") int soldticket);

    int queryForCountCity(MeetingQueryObject qo);

    List<Meeting> queryCity(MeetingQueryObject qo);

    List<Meeting> selectBymis(List<Integer> mids);

    int SearchqueryForCount(SearchQueryObject qo);

    List<Meeting> SearcgQuery(SearchQueryObject qo);

    List<Meeting> selectByUid(Integer uid);

    List<Meeting> selectNameLike(QueryObject qo);

    void updateSoldticketReduce(@Param("mid") int mid,@Param("count") int count);

    List<Meeting> getMeetingsByCon(MeetingModQueryObject queryObject);

    List<Integer> selectMidsIncomeDetail(int uid);

    List<Integer> selectMidsOutcomeDetail(int uid);

    int selectNameLikeCount(QueryObject qo);
}