package com.jxzg.share.mapper;

import com.jxzg.share.domain.other.User;
import com.jxzg.share.querypage.QueryObject;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface UserMapper {

    int insert(User record);

    User selectByPrimaryKey(Integer uid);

    int updateByPrimaryKey(User record);

    /**
     * 检查用户手机或者邮箱正确
     */
    User selectUserByPhone(@Param("loginname") String loginname, @Param("loginpassword")String loginpassword);

    /**
     * 检查用户名是否存在
     */
    User selectByLoginname(String loginname);

    int queryForCount(QueryObject qo);

    List<User> query(QueryObject qo);

    int countByLoginname(String loginname);

    /**
     * 修改密码查看,当前用户手机号密码是否正确
     * @param loginpassword 密码
     * @param id 用户id
     * @return 影响行数
     */
    int selectByPassword(@Param("loginpassword") String loginpassword, @Param("id") int id);

    /**
     * 修改密码
     * @param newloginpassword 新密码
     * @param uid 用户id
     */
    int updatePassword(@Param("newloginpassword") String newloginpassword, @Param("uid") int uid);

    /**
     * 查询当前用户是不是第一次登录
     * @param uid
     * @return
     */
    User selectUser(int uid);

    /**
     * 第一次登录之后修改状态
     * @param uid
     */
    void updateStatus(int uid);

    int updateHeadimage(@Param("avatar") String avatar,@Param("uid") int uid);

    /**
     * 查询头像
     * @param uid
     * @return
     */
    String selectImage(int uid);

    User selectImageAndLoginname(int uid);

    String selectHeadImage(int uid);

    int selectremainmsg(int uid);

    /**
     * 查询评论用户资料
     * @param uid
     * @return
     */
    User selectCommetUser(int uid);

    User selectByUid(int uid);

    void updateLoginname(@Param("loginname") String loginname,@Param("uid") int uid);

    int updateBalance(@Param("uid") int uid,@Param("money") BigDecimal money);

    void updateMsg(@Param("uid") int uid,@Param("amount") int amount);

    int updateApplyPassword(@Param("uid") int uid,@Param("paypassword") String paypassword);

    int selectverifypaypassword(@Param("uid") int uid,@Param("paypassword") String paypassword);

    String selectisPaypassword(int uid);
}