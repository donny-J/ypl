package com.jxzg.share.mapper;

import com.jxzg.share.domain.pay.Payee;

import java.util.List;

public interface PayeeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Payee record);

    Payee selectByPrimaryKey(Integer id);

    List<Payee> selectAll();

    int updateByPrimaryKey(Payee record);
}