package com.jxzg.share.mapper;

import com.jxzg.share.domain.other.Notification;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface NotificationMapper {
    int deleteByPrimaryKey(Long nid);

    int insert(Notification record);

    Notification selectByPrimaryKey(Long nid);

    List<Notification> selectAll();

    int selectNotificationNum(int uid);

    List<Notification> selectByStatus(int status);

    void updateByPrimaryKey(@Param("nid") Long nid, @Param("updatetime")Date date,@Param("status")int status);
}